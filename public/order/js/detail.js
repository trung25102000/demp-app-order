$(document).ready(function () {
    $('.delete-button').on('click', function (e) {
        e.preventDefault();
        var rowId = $(this).data('id');
        var route = $(this).data('route');
        if (confirm('Bạn muốn xóa món này ?')) {
            $.ajax({
                url: route,
                type: 'DELETE',
                success: function (response) {
                    $('tr[data-row-id="' + rowId + '"]').remove();

                    updateTotalPrice();
                },
                error: function (xhr) {

                }
            });
        }
    })
});
function updateTotalPrice() {
    var totalPrice = 0;

    $('#table_detail tbody tr').each(function () {
        var price = parseFloat($(this).find('#price').text());
        var quantity = parseInt($(this).find('.quantity-field').text());
        var subtotal = price * quantity;

        totalPrice += subtotal;
        if (quantity === 0) {
            var rowId = $(this).closest('tr').data('row-id');

            $.ajax({
                url: '/details/' + rowId,
                type: 'DELETE', 
                data: {_token: '{{ csrf_token() }}'},
                success: function (response) {
                    $('tr[data-row-id="' + rowId + '"]').remove();
                    $('.total-price').html(response.totalPrice);
                },
                error: function (xhr, status, error) {
                }
            });
        }
    });

    $('.total-price').text(totalPrice);
}