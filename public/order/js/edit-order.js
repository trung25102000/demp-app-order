var csrfToken = $('input[name="_token"]').val();



document.addEventListener('DOMContentLoaded', function () {
    //gọi hàm getDetail để lấy dữ liệu từ db và renderDetail để đổ data ra view, calcToTalPrice đế tính toán lại tổng tiền
    function start() {
        getDetails(function (detail) {
            var details=processOrderDetailList(detail);
            renderDetail(details);
        });
    }

    //đổ dữ liệu ra view
    function renderDetail(detail) {
        var hmtls = details.map(function (item) {
            return `
        <tr class="detail" data-detail="${item.id}">
            <td id="product_id" data-value="${item.product_id}">${item.product_id ? item.product.product_name : item.order_detail_other_name}</td>
            <td>${item.order_detail_other_unit_name}</td>
            <td style="vertical-align: middle">
                <input type="number" min="1" max="30" class="quantity-input" data-product-id="${item.product_id}" id="quantity-input-${item.id}" value="${item.order_detail_quantity}" style="width: 2.5rem; height: 2.5rem;border-radius: 5px; text-align:center; border: 1px solid #e4e6e7"/>
            </td>
            <td class="price-amount" data-price="${item.order_detail_price}">${currency((item.order_detail_quantity * item.order_detail_price), {symbol: "đ"}).format()}</td>
            <td style="vertical-align: middle">
                <button type="button" class="btn btn-danger bi bi-trash-fill delete-detail"></button>
            </td>
        </tr>
    `;
        });
        $('#tbody').html(hmtls.join(''));

    }

    start();

    $('#modal-voucher-in-edit').on('click', function () {
        $('#voucherInEdit').modal('show');
    })

    function calcTotalPrice() {
        let totalMoneyOrder = document.getElementById('total-money-order');
        let discountAmount = parseFloat($('.voucher-checkbox:checked').attr('discount-amount'));
        let totalPrice = orderItems.reduce((total, item) => {
            let itemTotalPrice = item.quantity * parseInt(item.product_price);
            return total + itemTotalPrice;
        }, 0);
        if (!isNaN(discountAmount)) {
            var discountMoney = totalPrice * discountAmount;
            totalPrice -= discountMoney;
        }
        totalMoneyOrder.textContent = `${formatNumber(totalPrice)} đ`;
    }

    $('#save-modal-voucher-edit2').on('click', function () {
        calcTotalPrice();
        $('#voucherInEdit').modal('hide');
    })

    $('.voucher-checkbox').on('change', function () {
        if ($(this).is(':checked')) {
            $('.voucher-checkbox').not(this).prop('checked', false).prop('disabled', true);
            $(this).prop('disabled', false);
        } else {
            $('.voucher-checkbox').prop('disabled', false);
        }
    });

    $('#save-order-btn').on('click', function (e) {
        e.preventDefault();
        let data = {
            table_id: $('select[name="table_id_edit"]').val(),
            count_of_customer: $('#count-customer-edit').val(),
            voucher_id: $('.voucher-checkbox:checked').val(),
            user_id: $('#user-id-edit').val(),
            orderItems: orderItems,
            orderItemsEdit:orderItemsEdit,
            customer_id: $('#customer-id-edit').val(),
            type_id: $('.type-id-edit').val(),
        };
        $.ajax({
            url: URL_SAVE_ORDER,
            data: data,
            method: 'post',
            dataType: 'json',
            success: function (response) {
                location.href = URL_LIST_ORDER;
            },
            error: function (err) {
                if (err.status == 422) {
                    showErrors(res.responseJSON.errors);
                }
            }
        })
    });

});

