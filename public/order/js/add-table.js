var csrfToken = $('meta[name="csrf-token"]').attr('content');
<!--Start script in modal add-table-->

// document.getElementById('searchIcon').addEventListener('click', function () {
//     var searchBar = document.getElementById('searchBar');
//     searchBar.classList.toggle('active');
// });

function moveToBottom(icon) {
    var rectangle = icon.parentNode;
    var rectangle2 = rectangle.closest('.modal').querySelector('.rectangle2');
    rectangle2.parentNode.insertBefore(rectangle, rectangle2.nextSibling);

    // Thay đổi biểu tượng
    icon.setAttribute('class', 'bi bi-dash text-white bg-danger');
    icon.setAttribute('onclick', 'moveToTop(this)');
    icon.setAttribute('data-moved-to-top', 'true');
}

function moveToTop(icon) {
    var rectangle = icon.parentNode;
    var rectangleContainer = rectangle.closest('.modal').querySelector('.rectangle-container');

    // Lấy vị trí ban đầu từ thuộc tính data
    var initialIndex = parseInt(rectangle.dataset.initialIndex);

    // Kiểm tra nếu vị trí hiện tại khác vị trí ban đầu thì di chuyển về vị trí ban đầu
    if (Array.from(rectangleContainer.children).indexOf(rectangle) !== initialIndex) {
        rectangleContainer.insertBefore(rectangle, rectangleContainer.children[initialIndex]);
    }

    // Trả về biểu tượng ban đầu
    icon.setAttribute('class', 'bi bi-plus-circle text-white');
    icon.setAttribute('onclick', 'moveToBottom(this)');
    icon.setAttribute('data-moved-to-top', 'false');
}

// Hàm di chuyển tất cả các phần tử giữa rectangle và rectangle2 xuống phía dưới
function moveToTopAll() {
    var modal = event.currentTarget.closest('.modal');
    var rectangleContainer = modal.querySelector('.rectangle-container');
    var startElement = modal.querySelector('.rectangle2');
    var endElement = modal.querySelector('.rectangle2.d-flex.justify-content-end');

    var elementsToMove = [];
    var currentElement = startElement.nextSibling;

    // Lặp qua các phần tử giữa startElement và endElement
    while (currentElement !== endElement) {
        elementsToMove.push(currentElement);
        currentElement = currentElement.nextSibling;
    }

    // Di chuyển các phần tử xuống phía dưới rectangleContainer
    elementsToMove.forEach(function (element) {
        rectangleContainer.appendChild(element);
        var icon = element.querySelector('i');
        icon.setAttribute('class', 'bi bi-plus-circle text-white');
        icon.setAttribute('onclick', 'moveToBottom(this)');
        icon.setAttribute('data-moved-to-top', 'false');
    });
}

function mergeTables(button) {
    var orderId = button.getAttribute('data-order-id');

    var modal = event.currentTarget.closest('.modal');
    var rectangleContainer = modal.querySelector('.rectangle-container');
    var startElement = modal.querySelector('.rectangle2');
    var endElement = modal.querySelector('.rectangle2.d-flex.justify-content-end');

    var elementsToMove = [];
    var currentElement = startElement.nextSibling;

    // Lặp qua các phần tử giữa startElement và endElement
    while (currentElement !== endElement) {
        elementsToMove.push(currentElement);
        currentElement = currentElement.nextSibling;
    }
    var idTables = [];
    // Di chuyển các phần tử xuống phía dưới rectangleContainer
    elementsToMove.forEach(function (element) {
        if (element instanceof HTMLElement) {
            var span = element.querySelector('span');
            var tableId = span.getAttribute('data-table-id');
            idTables.push(tableId);
        }
    });

    var checkedValue =  $(button).closest('.modal-content').find('input[type="checkbox"]').prop('checked');

    var data = {
        orderId: orderId,
        idTables: idTables,
        checkedValue :checkedValue,
        _token:csrfToken,
    };
    // Gọi AJAX
    $.ajax({
        url: '/add-table',
        type: 'POST',
        data: data,
        success: function (response) {
            // Đóng modal
            $('#match_'+orderId).modal('hide');
            searchOrders();
            getCountItemsInListOrderByOrderType()

        },
        error: function (xhr, status, error) {
            // Xử lý lỗi (nếu có)

        }
    });
}

<!--End script in modal add-table-->
