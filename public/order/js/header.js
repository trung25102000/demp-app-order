
window.onload = function () {
    var pusher = new Pusher('1bb3220122678349c45f', {
        cluster: 'mt1',
        encrypted: true
    });

    var channel = pusher.subscribe('NotifyEvent');

    let unreadNotificationCount = 0; 

    channel.bind('Notify', function ($data) {
        productName = $data.productName;
        tableName = $data.tableName;
        let newNotiFy = `
    <li class="notification-item">
      <i class="bi bi-check-circle text-success"></i>
      <div>
        <p style="font-size: 15px"><b>Món ${$data.productName ?? ''} ${$data.tableName ? 'của bàn ' + `${$data.tableName}` : ''} đã chế biến xong</b></p>
        <p>${$data.notificationTime}</p>    
      </div>
    </li>
    <li>
      <hr class="dropdown-divider">
    </li>`;

        let listNotify = document.querySelector('.list-notify');
        listNotify.insertAdjacentHTML('afterbegin', newNotiFy);

        unreadNotificationCount++;

        updateNotificationCount();
    });

    $('.ul-noti').on('click', function () {
        $.ajax({
            url: 'set-is-read-noti',
            type: 'POST',
            success: function (res) {
                unreadNotificationCount = 0;
                updateNotificationCount();
            },
            error: function (err) {

            },
        });
    });

    function updateNotificationCount() {
        $('.noti-count').text(unreadNotificationCount);

        if (unreadNotificationCount > 0) {
            $('.noti-count').show();
            $('.noti-count').addClass('badge-number');
        } else {
            $('.noti-count').hide();
            $('.noti-count').removeClass('badge-number');
        }
    }

}