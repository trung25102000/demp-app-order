var dropdownMenu = $('#dropdownMenuButtonTable');
var dropdownItems = $('#SortStatus .dropdown-item');

//bắt sự kiện khi thay đổi item thì sẽ hiển thị ra ở button đồng thời đưa id vào no button
dropdownItems.on('click', function () {
    dropdownMenu.html($(this).html());
    dropdownMenu.attr('data-id', $(this).attr('id'));
});


//sự kiện reload trang
$('#table .card content container mt-2').ready(function () {
    sendRequest();
});
// Sự kiện thay đổi khu vưcj
$("#dropdownArea").change(function () {
    sendRequest();
});
//sự kiện thay đổ item khi click
$('#SortStatus .dropdown-item').on('click', function () {
    sendRequest();
});
//Sự kiện keyup khi thay đổi giá trị ở ô tìm kiếm
$('#search').keyup(function () {
    sendRequest();
});

// Hàm tìm kiếm dùng ajax gọi về tra ra view
function sendRequest(Page) {
    var selectedId = $('#dropdownArea').children("option:selected").attr("id");
    var dropdownId = dropdownMenu.attr('data-id');
    var searchValue = $("#search").val();
    $.ajax({
        url: "/search",
        method: 'GET',
        data: {
            selectedId: selectedId,
            searchValue: searchValue,
            dropdownId: dropdownId,
            page:Page
        },
        success: function (response) {
            $('#table_search').html(response);
            $('.pagination a').unbind('click').on('click', function(e) {
                e.preventDefault();
                var page = $(this).attr('href').split('page=')[1];
                sendRequest(page);
            });
            $('#tableSearch').ready(function ()
            {
                $('#tableSearch .card-body').each(function () {
                    var tableId = $(this).find("input[type=hidden]").val();
                    $.ajax({
                            url: '/getTableAdd',
                            method: 'GET',
                            data: {
                                tableId: tableId
                            },
                            success: function (response) {
                                console.log(response.result);
                                $('#'+tableId+'tableAdd').html(response.result);
                            },
                            error: function (xhr) {
                            }
                        }
                    )
                });

            })
        },
        error: function (xhr) {
        }
    });
}
