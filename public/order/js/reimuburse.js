//Get dữ liệu theo feature
$(document).ready(function () {
  var searchString = $('#searchReimuburse').val();
  var dataType = $('#reimuburse button.nav-link.active').data('type');
  var selectedValue = $("#category-select").val();
  getDataInReimuburse(searchString, dataType, selectedValue);
});

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

// Lắng nghe sự kiện khi mục select thay đổi giá trị
$("#category-select").change(function () {
  var searchString = $('#searchReimuburse').val();
  var dataType = $('#reimuburse button.nav-link.active').data('type');
  var selectedValue = $("#category-select").val();
  getDataInReimuburse(searchString, dataType, selectedValue)
  // Tiếp tục xử lý dữ liệu theo nhu cầu của bạn
});
// Lắng nghe sự kiện khi thay đổi giá trị trên input của thanh tìm kiếm
$("#searchReimuburse").keyup(function () {
  var searchString = $('#searchReimuburse').val();
  var dataType = $('#reimuburse button.nav-link.active').data('type');
  var selectedValue = $("#category-select").val();
  // Tiếp tục xử lý dữ liệu theo nhu cầu của bạn
  getDataInReimuburse(searchString, dataType, selectedValue)
});
// lắng nghe sự kiện khi thay đổi giá trị trên nak link
$("#reimuburse .nav-link").click(function () {
  var searchString = $('#searchReimuburse').val();
  var dataType = $('#reimuburse button.nav-link.active').data('type');
  var selectedValue = $("#category-select").val();
  // Tiếp tục xử lý dữ liệu theo nhu cầu của bạn
  getDataInReimuburse(searchString, dataType, selectedValue)
});

//hàm gộp mảng theo product_id
function processOrderDetailList(orderDetailList) {
  var tempArray = [...orderDetailList];

  for (var i = 0; i < tempArray.length; i++) {
    var productId = tempArray[i].product_id;
    var quantity = tempArray[i].order_detail_quantity;

    // Tìm các phần tử cùng product_id và cộng dồn quantity
    for (var j = i + 1; j < tempArray.length; j++) {
      if (tempArray[j].product_id === productId) {
        quantity += tempArray[j].order_detail_quantity;
        // Xóa phần tử trùng
        tempArray.splice(j, 1);
        j--; // Giảm giá trị của j để duyệt qua phần tử mới sau khi xóa
      }
    }

    tempArray[i].order_detail_quantity = quantity; // Cập nhật quantity mới
  }

  return tempArray;
}


//Dùng ajax gọi về controller để hiển thị dữ liệu ra view
function getDataInReimuburse(searchString, dataType, selectedValue) {
  $.ajax({
    type: "GET",
    url: "/searchReimburse",
    data: {
      search_string: searchString,
      dataType: dataType,
      selectedValue: selectedValue
    },
    success: function (response) {
      $('#reimburse_data').html(response);
      $(document).on('click', '[id^="cancel-detail-button-"]', function () {
        var detailId = $(this).val();
        var reason = $('#reason-cancel-' + detailId).val();
        $('#cancel-detail_' + detailId).modal('hide');
        let thisElement = $(this).closest('tr');
        thisElement.remove();
        $.ajax({
          url: CANCEL_DETAIL,
          method: 'post',
          data: {
            detailId: detailId,
            order_detail_cancel_reason: reason,
          },
          success: function () {
            cancelDetailSuccess();
          }, error: function () {
            cancelDetailError();
          }
        })
      });
      //Bắt sự kiện readdy tab-table-reimburse để đưa dữ liệu ra
      $('#table-detail-for-table').ready(function () {
        var selectedValue = $("#category-select").val();
        $(".table-row-table").each(function () {
          var rowTable = $(this); // Lưu trữ đối tượng dòng trên
          var tableId = $(this).find("input[type=hidden]").val();
          var url = "/getItemCountTable";
          $.ajax({
            url: url,
            type: 'GET',
            data: {
              selectedValue: selectedValue,
              tableId: tableId
            },
            success: function (response) {
              var orderDetailList = response.orderDetails;
              var finalArray = processOrderDetailList(orderDetailList);
              $("." + tableId + "item-count-table").text(finalArray.length);
              // Xóa nội dung hiện tại của tbody trong bảng
              $("." + tableId + "modal-body-table tbody").empty();

              // Kiểm tra nếu danh sách orderDetailList không rỗng
              if (finalArray.length <= 0) {
                $(rowTable).remove();
              } else {
                // Tạo HTML cho từng orderDetail và thêm vào tbody trong bảng
                $.each(finalArray, function (index, orderDetail) {
                  var html = '<tr>' +
                    '<td>' + (orderDetail.product_id ? orderDetail.product_id : "Món khác") + '</td>' +
                    '<td>' + (orderDetail.product ? orderDetail.product.product_name : orderDetail.order_detail_other_name) + '</td>' +
                    '<td>' + orderDetail.order_detail_quantity + '</td>' +
                    '<td>' + (orderDetail.order_detail_note ? orderDetail.order_detail_note : "") + '</td>' +
                    '</tr>';
                  $("." + tableId + "modal-body-table tbody").append(html);
                });
              }
            },
            error: function (xhr, status, error) {
            }
          });
        });
      });
      //xử lý sự kiện khi load trang trảng món theo order
      $('#table-detail-for-order').ready(function () {
        var selectedValue = $("#category-select").val();
        $(".table-row").each(function () {
          var orderId = $(this).find("input[type=hidden]").val();
          var row = $(this); // Lưu trữ đối tượng dòng trên
          var url = "/getItemCount";
          $.ajax({
            url: url,
            type: 'GET',
            data: {
              selectedValue: selectedValue,
              orderId: orderId
            },
            success: function (response) {
              var orderDetailList = response.orderDetails;
              var finalArray = processOrderDetailList(orderDetailList);
              $("." + orderId + "item-count").text(finalArray.length);
              // Xóa nội dung hiện tại của tbody trong bảng
              $("." + orderId + "modal-body tbody").empty();

              // Kiểm tra nếu danh sách finalArray không rỗng
              if (finalArray.length <= 0) {
                $(row).remove();
              } else {
                // Tạo HTML cho từng orderDetail và thêm vào tbody trong bảng
                $.each(finalArray, function (index, orderDetail) {
                  var html = '<tr>' +
                    '<td>' + (orderDetail.product_id ? orderDetail.product_id : "Món khác") + '</td>' +
                    '<td>' + (orderDetail.product ? orderDetail.product.product_name : orderDetail.order_detail_other_name) + '</td>' +
                    '<td>' + orderDetail.order_detail_quantity + '</td>' +
                    '<td>' + (orderDetail.order_detail_note ? orderDetail.order_detail_note : "") + '</td>' +
                    '</tr>';
                  $("." + orderId + "modal-body tbody").append(html);
                });
              }
            },
            error: function (xhr, status, error) {
            }
          });
        });
      });

    },
    error: function (xhr) {
    }
  });
}
//Dùng ajax gọi về controller lấy dữ liệu cần thiết để trả món

function confirmAndRedirect(id) {
  if (confirm('Bạn có chắc chắn muốn trả món?')) {
    var loader = document.createElement('div');
    loader.className = 'loader';
    var overlay = document.createElement('div');
    overlay.className = 'overlay';
    overlay.appendChild(loader);
    document.body.appendChild(overlay);

    $.ajax({
      url: '/reimbursed',
      type: "GET",
      data: { id: id },
      success: function (response) {
        // Xử lý phản hồi từ controller (nếu cần)
        var searchString = $('#searchReimuburse').val();
        var dataType = $('#reimuburse button.nav-link.active').data('type');
        var selectedValue = $("#category-select").val();
        // Tiếp tục xử lý dữ liệu theo nhu cầu của bạn
        getDataInReimuburse(searchString, dataType, selectedValue);
        $('.loader').remove();
        $('.overlay').remove();
        reimbursedSuccess();
      },
      error: function (xhr, status, error) {
        reimbursedError();
      }
    });
  }
};

function confirmReimbursedOrder(id) {
  if (confirm('Bạn có chắc chắn muốn trả món?')) {
    var loader = document.createElement('div');
    loader.className = 'loader';
    var overlay = document.createElement('div');
    overlay.className = 'overlay';
    overlay.appendChild(loader);
    document.body.appendChild(overlay);

    $.ajax({
      url: '/reimburseForOrder/' + id,
      type: "GET",
      success: function (response) {
        // Xử lý phản hồi từ controller (nếu cần)
        var searchString = $('#searchReimuburse').val();
        var dataType = $('#reimuburse button.nav-link.active').data('type');
        var selectedValue = $("#category-select").val();
        // Tiếp tục xử lý dữ liệu theo nhu cầu của bạn
        getDataInReimuburse(searchString, dataType, selectedValue);
        $('.loader').remove();
        $('.overlay').remove();
        reimbursedSuccess();
      },
      error: function (xhr, status, error) {
        reimbursedError();
      }
    });
  }
};

function confirmReimbursedTable(id) {
  if (confirm('Bạn có chắc chắn muốn trả món?')) {
    var loader = document.createElement('div');
    loader.className = 'loader';
    var overlay = document.createElement('div');
    overlay.className = 'overlay';
    overlay.appendChild(loader);
    document.body.appendChild(overlay);

    $.ajax({
      url: '/reimburseForTableByOrder/' + id,
      type: "GET",
      success: function (response) {
        // Xử lý phản hồi từ controller (nếu cần)
        var searchString = $('#searchReimuburse').val();
        var dataType = $('#reimuburse button.nav-link.active').data('type');
        var selectedValue = $("#category-select").val();
        // Tiếp tục xử lý dữ liệu theo nhu cầu của bạn
        getDataInReimuburse(searchString, dataType, selectedValue);
        $('.loader').remove();
        $('.overlay').remove();
        reimbursedSuccess();
      },
      error: function (xhr, status, error) {
        reimbursedError();
      }
    });
  }
};

window.onload = function () {
  var pusher = new Pusher('1bb3220122678349c45f', {
    cluster: 'mt1',
    encrypted: true
  });

  var channel = pusher.subscribe('MoveKitchenEvent');

  channel.bind('move-to-kitchen', function (data) {
    let startTime = new Date(data[0].updated_at);
    let currentTime = new Date();
    let timeDiff = currentTime.getTime() - startTime.getTime();
    let minutes = Math.floor(timeDiff / (1000 * 60));
    let hours = minutes % 60;
    data.forEach(function (detail) {
      if ($('#home-tab').hasClass('active')) {
        let newDetailForAll = `
      <tr>
        <th scope="row">${detail.product_key}</th>
        <td>${detail.product_name}</td>
        <td>${detail.order_detail_quantity}</td>
        <td>${detail.table_area_key ?? ''}${detail.table_floor}${detail.table_name}</td>
        <td>${minutes < 60 ? `${minutes} phút trước` : `${hours} giờ trước`}</td>
        <td>${detail.order_detail_note ?? ''}</td>
        <td><input type="checkbox" class="form-check-input"></td>
        <td>
          <div>
            <button class="btn text-white" style="background-color:#06357A" onclick="confirmAndRedirect(${detail.id})">
              <i class="bi bi-check-circle-fill"></i>
            </button>
            <button class="btn btn-danger text-white" value="${detail.id}"
            data-bs-toggle="modal" data-bs-target="#cancel-detail_${detail.id}"><i
                class="bi bi-trash"></i></button>
          </div>
        </td>
      </tr>
    `;
        let listDetailForAll = document.querySelector('#table-detail-for-all tbody');
        listDetailForAll.insertAdjacentHTML('beforeend', newDetailForAll);
        $('#empty-span').closest('tr').remove();

        let listModalCancel = document.querySelector('#modal-cancel-detail');
        if (!listModalCancel) {
          let newModalCancel = `<div class="modal fade" id="cancel-detail_${detail.id}" tabindex="-1">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title">Hủy Món</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal"
                          aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                      <h6>Nhập lý do hủy món:</h6>
                      <textarea type="text" name="reason_cancel_detail" class="form-control"></textarea>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-danger"
                          data-bs-dismiss="modal">Hủy
                      </button>
                      <button type="submit" class="btn text-white"
                          style="background-color: #06357A">Lưu
                      </button>
                  </div>
              </div>
          </div>
      </div>`;
          document.body.insertAdjacentHTML('beforeend', newModalCancel);
        }
        else {
          listModalCancel.insertAdjacentHTML('beforeend', newModalCancel);
        }
      }
    });

    if ($('#profile-tab').hasClass('active')) {
      let newDetailForTable = `<tr>
  <td>${data[0].table_name ? data[0].table_name : "Mang về hoặc ship"}</td>
  <td>${data.length}</td>
  <td>${minutes < 60 ? `${minutes} phút trước` : `${hours} giờ trước`}</td>
  <td><input class="form-check-input" type="checkbox" id="gridCheck1"></td>
  <td>
  <button class="btn text-white" style="background-color:#06357A" onclick="confirmReimbursedTable(${data[0].order_id})">
  <i class="bi bi-check-circle-fill"></i>
  </button>
    <button type="button" class="btn btn-secondary" data-bs-toggle="modal"
      data-bs-target="#verticalycentered_table_${data[0].table_id}">
      <i class="bi bi-three-dots"></i>
    </button>
  </td>
</tr>`;

      let listDetailForTable = document.querySelector('#table-detail-for-table tbody');
      listDetailForTable.insertAdjacentHTML('beforeend', newDetailForTable);

      $('#empty-span').closest('tr').remove();

      let listDetailForTableModal = document.querySelector('#modal-detail-table');
      let newModalDetailTable = `<div class="modal fade" id="verticalycentered_table_${data[0].table_id}" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title">Danh sách món</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal"
                          aria-label="close"></button>
              </div>
              <div class="modal-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th scope="col">Mã món</th>
                            <th scope="col">Tên món</th>
                            <th scope="col">Số lượng</th>
                            <th scope="col">Ghi chú</th>
                        </tr>
                        </thead>
                        <tbody>`;
      data.forEach(detail => {
        newModalDetailTable += `
                              <tr>
                                  <td>${detail.product_id ? detail.product_id : 'Món khác'}</td>
                                  <td>${detail.product_id ? detail.product_name : detail.order_detail_other_name}</td>
                                  <td>${detail.order_detail_quantity}</td>
                                  <td>${detail.order_detail_note ?? ''}</td>
                              </tr>`;
      }); if (data.length === 0) {
        newModalDetailTable += `
                            <tr>
                                <td colspan="4">Danh sách món của bàn ${data[0].table_name} đang trống.</td>
                            </tr>`;
      } newModalDetailTable += `
                        </tbody>
                      </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Đóng</button>
                </div>
          </div>
        </div>
      </div>`
      if (!listDetailForTableModal) {
        document.body.insertAdjacentHTML('beforeend', newModalDetailTable);
      } else {
        listDetailForTableModal.insertAdjacentHTML('beforeend', newModalDetailTable);
      }
    }
    if ($('#contact-tab').hasClass('active')) {
      let newDetailForOrder = `<tr>
        <td>${data[0].order_id ?? ``}</td>
        <td>${data.length}</td>
        <td>${minutes < 60 ? `${minutes} phút trước` : `${hours} giờ trước`}</td>
        <td><input class="form-check-input" type="checkbox" id="gridCheck1"></td>
        <td>
            <button class="btn text-white" style="background-color:#06357A "><i
                    class="bi bi-check-circle-fill" onclick="confirmReimbursedOrder(${data[0].order_id})"></i></button>
            <button type="button" class="btn btn-secondary" data-bs-toggle="modal"
                    data-bs-target="#verticalycentered_order${data[0].order_id}">
                <i class="bi bi-three-dots"></i>
            </button>
        </td>
    </tr>`;
      let listDetailForOrder = document.querySelector('#table-detail-for-order tbody');
      listDetailForOrder.insertAdjacentHTML('beforeend', newDetailForOrder);
      $('#empty-span').closest('tr').remove();

      let listDetailForOrderModal = document.querySelector('#modal-detail-order');
      let newModalDetailOrder = `<div class="modal fade" id="verticalycentered_order${data[0].order_id}" tabindex="-1">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Danh sách món</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal"
                        aria-label="close"></button>
            </div>
            <div class="modal-body">
                  <table class="table table-bordered">
                      <thead>
                      <tr>
                          <th scope="col">Mã món</th>
                          <th scope="col">Tên món</th>
                          <th scope="col">Số lượng</th>
                          <th scope="col">Ghi chú</th>
                      </tr>
                      </thead>
                      <tbody>`;
      data.forEach(detail => {
        newModalDetailOrder += `
                            <tr>
                                <td>${detail.product_id ? detail.product_id : 'Món khác'}</td>
                                <td>${detail.product_id ? detail.product_name : detail.order_detail_other_name}</td>
                                <td>${detail.order_detail_quantity}</td>
                                <td>${detail.order_detail_note ?? ''}</td>
                            </tr>`;
      }); if (data.length === 0) {
        newModalDetailOrder += `
                          <tr>
                              <td colspan="4">Danh sách món của bàn ${data[0].table_name} đang trống.</td>
                          </tr>`;
      } newModalDetailOrder += `
                      </tbody>
                    </table>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Đóng</button>
              </div>
        </div>
      </div>
    </div>`;
      if (!listDetailForOrderModal) {
        document.body.insertAdjacentHTML('beforeend', newModalDetailOrder);
      } else {
        listDetailForOrderModal.insertAdjacentHTML('beforeend', newModalDetailOrder);
      }
    }
  });

}

$('.reimbursed-many').on('click', function () {
  if ($('#profile-tab').hasClass('active')) {
    if (confirm('Bạn muốn trả những món đã chọn ?')) {
      var selectedTables = [];
      $('.selected-for-table:checked').each(function () {
        selectedTables.push($(this).val());
      },)
      reimbursedManyTable(selectedTables);
    }
  }
  if ($('#contact-tab').hasClass('active')) {
    if (confirm('Bạn muốn trả những món đã chọn ?')) {
      var selectedOrders = [];
      $('.selected-for-order:checked').each(function () {
        selectedOrders.push($(this).val());
      },)
      reimbursedManyOrder(selectedOrders);
    }
  }
  if ($('#home-tab').hasClass('active')) {
    if (confirm('Bạn muốn trả những món đã chọn ?')) {
      var selectedAll = [];
      $('.selected-for-all:checked').each(function () {
        selectedAll.push($(this).val());
      },)
      reimbursedManyProduct(selectedAll);
    }
  }
});

function reimbursedManyProduct(dataProduct) {
  var loader = document.createElement('div');
  loader.className = 'loader';
  var overlay = document.createElement('div');
  overlay.className = 'overlay';
  overlay.appendChild(loader);
  document.body.appendChild(overlay);
  $.ajax({
    url: REIMBURSED_MANY_PRODUCT,
    method: 'POST',
    data: {
      dataProduct: dataProduct,
    },
    success: function (res) {
      var searchString = $('#searchReimuburse').val();
      var dataType = $('#reimuburse button.nav-link.active').data('type');
      var selectedValue = $("#category-select").val();
      getDataInReimuburse(searchString, dataType, selectedValue);
      reimbursedSuccess();
      $('.loader').remove();
      $('.overlay').remove();
    },
    error: function () {
      reimbursedError();
    }
  })
}

function reimbursedManyOrder(dataOrder) {
  var loader = document.createElement('div');
  loader.className = 'loader';
  var overlay = document.createElement('div');
  overlay.className = 'overlay';
  overlay.appendChild(loader);
  document.body.appendChild(overlay);
  $.ajax({
    url: REIMBURSED_MANY_ORDER,
    type: 'POST',
    data: {
      dataOrder: dataOrder
    },
    success: function (res) {
      var searchString = $('#searchReimuburse').val();
      var dataType = $('#reimuburse button.nav-link.active').data('type');
      var selectedValue = $("#category-select").val();
      getDataInReimuburse(searchString, dataType, selectedValue);
      reimbursedSuccess();
      $('.loader').remove();
      $('.overlay').remove();
    },
    error: function (err) {
      reimbursedError();
    }
  })
}

function reimbursedManyTable(dataTable) {
  var loader = document.createElement('div');
  loader.className = 'loader';
  var overlay = document.createElement('div');
  overlay.className = 'overlay';
  overlay.appendChild(loader);
  document.body.appendChild(overlay);
  $.ajax({
    url: REIMBURSED_MANY_TABLE,
    type: 'POST',
    data: {
      dataTable: dataTable,
    },
    success: function (res) {
      var searchString = $('#searchReimuburse').val();
      var dataType = $('#reimuburse button.nav-link.active').data('type');
      var selectedValue = $("#category-select").val();
      getDataInReimuburse(searchString, dataType, selectedValue);
      reimbursedSuccess();
      $('.loader').remove();
      $('.overlay').remove();
    },
    error: function (err) {
      reimbursedError();
    }
  })
}

function reimbursedSuccess() {
  toast({
    title: "Thành công!",
    message: "Trả món thành công !",
    type: "success",
    duration: 4000
  });
}

function reimbursedError() {
  toast({
    title: "Thất bại!",
    message: "Trả món thất bại!",
    type: "error",
    duration: 4000
  });
}

function cancelDetailSuccess() {
  toast({
    title: "Thành công!",
    message: "Hủy món thành công!",
    type: "success",
    duration: 4000
  });
}

function cancelDetailError() {
  toast({
    title: "Thất bại!",
    message: "Hủy món thất bại!",
    type: "error",
    duration: 4000
  });
}

function toast({ title = "", message = "", type = "info", duration = 3000 }) {
  const main = document.getElementById("toast");
  if (main) {
    const toast = document.createElement("div");

    // Auto remove toast
    const autoRemoveId = setTimeout(function () {
      main.removeChild(toast);
    }, duration + 1000);

    // Remove toast when clicked
    toast.onclick = function (e) {
      if (e.target.closest(".toast__close")) {
        main.removeChild(toast);
        clearTimeout(autoRemoveId);
      }
    };

    const icons = {
      success: "fas fa-check-circle",
      info: "fas fa-info-circle",
      warning: "fas fa-exclamation-circle",
      error: "fas fa-exclamation-circle"
    };
    const icon = icons[type];
    const delay = (duration / 1000).toFixed(2);

    toast.classList.add("toast", `toast--${type}`);
    toast.style.animation = `slideInLeft ease .3s, fadeOut linear 1s ${delay}s forwards`;

    toast.innerHTML = `
                    <div class="toast__icon">
                        <i class="${icon}"></i>
                    </div>
                    <div class="toast__body">
                        <h3 class="toast__title">${title}</h3>
                        <p class="toast__msg">${message}</p>
                    </div>
                    <div class="toast__close">
                        <i class="fas fa-times"></i>
                    </div>
                `;
    main.appendChild(toast);
  }
}
