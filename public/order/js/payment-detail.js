// Hàm xử định dạng tiền tệ theo chuẩn VN
function formatNumber(number) {
    return number.toLocaleString('vi-VN');
}

// Hàm chuyển string thành số
function parseNumber(stringNumber) {
    return parseInt(stringNumber.split('.').join(''));
}
// Xử lí sự kiện checkbox để chọn chương trình khuyến mãi
const checkboxes = addContent.querySelectorAll('.form-check-input');
const insertPromotion = document.getElementById('insertPromotion');
const discountForVoucher= document.getElementById('discountForVoucher');

checkboxes.forEach(checkbox => {
    checkbox.addEventListener('change', function () {
        checkboxes.forEach(otherCheckbox => {
            if (otherCheckbox !== checkbox) {
                otherCheckbox.checked = false;
            }
        });
        if (checkbox.checked) {
                //chèn chương trình km vào mục tính tiền
            const contentPromotion = checkbox.parentElement.querySelector('#contentPromotion').textContent;
            insertPromotion.textContent = contentPromotion;
            // Hiển thị số tiền được giảm
            const percentReduced = checkbox.parentElement.querySelector('#percentReduce').textContent;
            const discountForVoucherCustomer = totalMoney * (parseFloat(percentReduced));
            discountForVoucher.textContent = formatNumber(discountForVoucherCustomer)+ " đ";
            // Tính toán lại số tiền khách hàng cần thanh toán
            totalPayment.textContent = formatNumber(totalMoney - discountForVoucherCustomer);
            totalAmount.textContent = totalPayment.textContent;
            let data = {
                voucher_id: $('.form-check-input:checked').val(),
            };
            fetchToUpdateVoucher(data);
        } else {
            insertPromotion.textContent = '';
            discountForVoucher.textContent = '0';
            totalPayment.textContent = formatNumber(totalMoney);
            totalAmount.textContent = totalPayment.textContent;
            let data = {
                voucher_id: null,
            };
            fetchToUpdateVoucher(data)
        }
        calculateTotalPayment();
    });
});
function fetchToUpdateVoucher(data) {


    fetch(URL_UPDATE_VOUCHER, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        body: JSON.stringify(data)
    })
    .then(response => {
        if (response.ok) {
            alert("Cập nhật voucher thành công");
        } else {
            alert("Cập nhật voucher không thành công");
        }
    })
    .catch(error => {
        alert("Đã xảy ra lỗi khi cập nhật voucher");
    });
}

// Checkbox lựa chọn có tính thuế GTGT hay không
if ($('#discountCheckbox').prop('checked', true)) {
    var totalMoney = parseNumber($('#totalMoneyOrder').text());
    const discountedValue = totalMoney * 0.1;
    $('#discountValue').text(formatNumber(discountedValue) + " đ");
    $('#totalPayment').text(formatNumber(totalMoney + discountedValue));
    $('#totalAmount').text($('#totalPayment').text());
}
$('#discountCheckbox').on('change', function () {
    var totalMoney = parseNumber($('#totalMoneyOrder').text());
    if ($('#discountCheckbox').prop('checked')) {
        const discountedValue = totalMoney * 0.1;
        $('#discountValue').text(formatNumber(discountedValue) + " đ");
        $('#totalPayment').text(formatNumber(totalMoney + discountedValue));
        $('#totalAmount').text($('#totalPayment').text());
    } else {
        $('#discountValue').text("0");
        $('#totalPayment').text(formatNumber(totalMoney));
        $('#totalAmount').text($('#totalPayment').text());
    }
    calculateTotalPayment();
});

// Thêm hàm calculateTotalPayment() để cập nhập giá trị của tổng tiền khi có bất kì thay đổi nào
function calculateTotalPayment() {
    var totalMoney = parseNumber($('#totalMoneyOrder').text());
    const discountValueAmount = parseNumber($("#discountValue").text());
    const discountForVoucherAmount = parseNumber($("#discountForVoucher").text());

    const totalPaymentAmount = totalMoney - discountForVoucherAmount + discountValueAmount;
    $("#totalPayment").text(formatNumber(totalPaymentAmount) + ' đ');
    $("#totalAmount").text($("#totalPayment").text());
}
calculateTotalPayment();


// Xử lí chức năng tính tiền cho khách (xử lí nội dung ở modal tính tiền)
$('#openModal').on('click', ()=> {
    $('#stillAmount').text($('#totalAmount').text());
    $('#stillAmountMobile').text($('#totalAmount').text());
})
let total = 0;
// Refresh giá trị về 0 khi nhập sai
$('.refreshTotalMoney').on('click',()=> {
    $('#customerSentAmount').val('0');
    $('#returnAmount').val('0');
    $('#tipAmount').val('0');
    total = 0;
})
//Chuyển đổi hình thức nhập số tiền
$('#customerSentAmount').on('click',()=> {
    $('.insertMoneyNumber').toggleClass('d-none');
    $('.insertMoneyPrice').toggleClass('d-none');
    total = 0;
    $('#customerSentAmount').val('0');
    $('#returnAmount').val('0');
    $('#tipAmount').val('0')
})
// Nhập số tiền bằng hình thức chọn mệnh giá
$('.column-price').each(function(){
    $(this).on('click', () => {
        const value = parseInt($(this).data('value'));
        const stillAmountText = $('#stillAmount').text();
        const numericalValue = parseNumber(stillAmountText.replace(/[^\d.,]/g, ''));

        if (!isNaN(value)) {
          total += value;
          $('#customerSentAmount').val(formatNumber(total));
          $('#returnAmount').val(formatNumber(parseNumber($('#customerSentAmount').val()) - numericalValue));
        }
    });
});
// Nhập số tiền bằng hình thức nhập số
$('.column-number').each(function(){
    $(this).on('click', () => {
        const value = $(this).data('number').toString();
        const stillAmountText = $('#stillAmount').text();
        const numericalValue = parseNumber(stillAmountText.replace(/[^\d.,]/g, ''));

        if (!isNaN(value)) {
          total += value;
          $('#customerSentAmount').val(formatNumber(parseInt((total.replace(/^0+/, '') + "000"))));
          $('#returnAmount').val(formatNumber(parseNumber($('#customerSentAmount').val()) - numericalValue));
        }
    });
});
$('.clear-number').on('click', ()=> {
    const stillAmountText = $('#stillAmount').text();
    const numericalValue = parseNumber(stillAmountText.replace(/[^\d.,]/g, ''));
    if (total.length > 0) {
        total = total.substring(0, total.length - 1); // Xoá số cuối cùng
        $('#customerSentAmount').val(formatNumber(parseInt((total.replace(/^0+/, '') + "000"))));
        $('#returnAmount').val(formatNumber(parseNumber($('#customerSentAmount').val()) - numericalValue));
    }
})
// Xử lí checkbox xác nhận khách tip tiền thừa hay không?
$('#tipAmountCheckbox').on('change', function () {
    if ($('#tipAmountCheckbox').prop('checked')) {
        $('#tipAmount').val($('#returnAmount').val());
        $('#returnAmount').val('0');
    } else {
        $('#tipAmount').val('0');
        const stillAmountText = $('#stillAmount').text();
        const numericalValue = parseNumber(stillAmountText.replace(/[^\d.,]/g, ''));
        $('#returnAmount').val(formatNumber(parseNumber($('#customerSentAmount').val()) - numericalValue));
    }
});

// In phiếu tạm tính nếu khách hàng có nhu cầu
$("#printBillButton").on("click", function() {
    // Mở trang phiếu tạm tính trong cửa sổ in
    var printWindow = window.open(URL_PRINT_PROVISIONAL, "_blank");
    printWindow.print();
});

// Lưu hoá đơn
$("#savePaymentButton").on("click", function() {
    if ($('#customerSentAmount').val() == 0) {
        alert('Vui lòng nhập số tiền khách đưa');
        return;
    }
    if (($('#totalAmount').text().replace(/[^0-9]/g, '') - $('#customerSentAmount').val().replace(/[^0-9]/g, '')) > 1000) {
        alert('Số tiền khách đưa chưa đủ. Vui lòng kiểm tra lại');
        return;
    }
    fetchToSavePayment();
});

$("#saveAndPrintPaymentButton").on("click", function() {
    if ($('#customerSentAmount').val() == 0) {
        alert('Vui lòng nhập số tiền khách đưa');
        return;
    }
    if (($('#totalAmount').text().replace(/[^0-9]/g, '') - $('#customerSentAmount').val().replace(/[^0-9]/g, '')) > 1000) {
        alert('Số tiền khách đưa chưa đủ. Vui lòng kiểm tra lại');
        return;
    }
    fetchToSavePaymentPrint();
});

function fetchToSavePayment() {
    let data = {
        payment_amount: $('#totalAmount').text().replace(/[^0-9]/g, ''),
        customer_pay: $('#customerSentAmount').val(),
        customer_refund: $('#returnAmount').val(),
        tax_VAT: $('#discountValue').text().replace(/[^0-9]/g, ''),
    };
    fetch(URL_SAVE_PAYMENT, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        body: JSON.stringify(data)
    })
        .then(response => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error('Lưu thanh toán không thành công');
            }
        })
        .then(data => {
            if (data.success) {
                window.location.href = data.redirect_url;
            } else {
                alert('Lưu thanh toán không thành công');
            }
        })
}


function fetchToSavePaymentPrint() {
    let data = {
        payment_amount: $('#totalAmount').text().replace(/[^0-9]/g, ''),
        customer_pay: $('#customerSentAmount').val(),
        customer_refund: $('#returnAmount').val(),
        tax_VAT: $('#discountValue').text().replace(/[^0-9]/g, ''),
    };

    fetch(URL_SAVE_PAYMENT, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        body: JSON.stringify(data)
    })
        .then(response => {
            if (response.ok) {
                // Check if the response is a PDF file
                const contentType = response.headers.get('content-type');
                if (contentType === 'application/pdf') {
                    // Download the PDF file
                    response.blob().then(blob => {
                        const url = URL.createObjectURL(blob);
                        const link = document.createElement('a');
                        link.href = url;
                        link.setAttribute('download', 'print_invoice.pdf');
                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                    });
                } else {
                    return response.json();
                }
            } else {
                throw new Error('Lưu thanh toán không thành công');
            }
        })
        .then(data => {
            if (data && data.success) {
                if (data.redirect_url) {
                    window.open(data.pdf);
                    window.location.href = data.redirect_url;
                } else {
                    // Handle success without redirect
                }
            } else {
                throw new Error('Lưu thanh toán thành công');
            }
        })
        .catch(error => {
        });
}

$(document).ready(function () {
    $('#mergeBillModal').on('hidden.bs.modal', function () {
        // Xóa lớp 'selected' khỏi các phần tử có lớp CSS 'selectable-p'
        $('.btnChoose').removeClass('selected');
    });

    $('#mergeBillModal').on('hidden.bs.modal', function () {
        // Xóa lớp 'selected' khỏi các phần tử có lớp CSS 'selectable-p'
        $('.btnChoose').removeClass('selected');
    });

    var csrfToken = $('meta[name="csrf-token"]').attr('content');
    $('.btnChoose').click(function () {
        var checkedValue = $(this).closest('.card-footer').find('button').hasClass('selected');
        $(this).toggleClass('selected');
    });
    $('.selectable-p').click(function () {
        // Đảo ngược trạng thái 'selected' khi nhấn vào
        $(this).toggleClass('selected');
    });
    $(".btn-merge").click(function () {
        var selectedIds = [];
        $(".btnChoose.selected").each(function () {
            var id = $(this).attr("data-id");
            selectedIds.push(id);
        });

        if (selectedIds.length > 0) {
            $.ajax({
                url: '/payment-detail-order',
                method: 'POST',
                data: {
                    selectedIds: selectedIds,
                    _token: csrfToken,
                },
                success: function (response) {

                    $('#mergeBillModal').modal('hide');

                    $('#abc').html(response);
                    // Xử lý kết quả từ controller
                    $('#abc').modal('toggle');
                    $('#abc .btn-select-all').click(function () {
                        var checkboxes = $(this).closest('.card').find('.form-check-input');
                        checkboxes.each(function () {
                            $(this).prop('checked', !$(this).prop('checked'));
                        });
                    });
                    $('#abc .btn-detail').click(function () {
                        $('#mergeBillModal').modal('toggle');
                    });
                    // trả về view mới trong thẻ div có id ='alo'
                    $('#abc .btn-merge-detail').click(function () {
                        var selectedIds2 = [];

                        var totalPrice = 0;


                        // Lặp qua tất cả các checkbox đã được chọn
                        $('#abc input[type="checkbox"]:checked').each(function () {
                            var id = $(this).attr("id"); // Lấy giá trị của thuộc tính 'id' của checkbox
                            var quantity = parseInt($(this).attr('quantity'));
                            var price = parseFloat($(this).attr('price'));
                            totalPrice += quantity * price;
                            selectedIds2.push(id); // Thêm id vào mảng selectedIds
                        });

                        // Gửi mảng selectedIds về controller qua Ajax hoặc thực hiện xử lý khác
                        $.ajax({
                            url: '/getDataTemp',
                            method: 'POST',
                            data: {
                                selectedIds: selectedIds2,
                                _token: csrfToken,
                            },
                            success: function (response) {
                                // Xử lý kết quả từ controller
                                $('#dataTemp').html(response);
                                $('#abc').modal('hide');
                                $('#mergeBillModal').modal('hide');
                                let totalMoneyOrder = document.getElementById('totalPayment');
                                let totalMoneyOrder2 = document.getElementById('totalMoneyOrder');
                                let discountForVoucher = document.getElementById('discountForVoucher');
                                var totalPayment = parseNumber($('#totalMoneyOrder').text());
                                total = totalPayment + totalPrice;
                                totalMoneyOrder2.textContent = `${formatNumber(total)} đ`
                                var discountAmount = 0;
                                checkboxes.forEach(function (checkbox) {
                                    if (checkbox.checked) {
                                        // Lấy giá trị discount từ thuộc tính "discount-amount" của checkbox
                                        discountAmount = parseFloat(checkbox.getAttribute('discount-amount'));

                                        // Tính tổng giá trị
                                    }
                                });
                                totalDiscount = total * discountAmount;

                                $totalAmount = total - totalDiscount;

                                discountForVoucher.textContent=`${(formatNumber((totalDiscount)))} đ`
                                totalMoneyOrder.textContent = `${(formatNumber(($totalAmount)))} đ`;
                                totalAmount.textContent = `${(formatNumber(($totalAmount)))} đ`;
                                $('.btn-print').click(function () {
                                    var dataId = $(this).data('id');
                                    var tempIds = [];
                                    $('#dataTemp input[type="hidden"]').each(function () {
                                        var id = $(this).val();
                                        tempIds.push(id);
                                    });


                                    $.ajax({
                                        url: '/payment', // Thay thế 'your_url' bằng URL mà bạn muốn gửi yêu cầu Ajax đến
                                        method: 'POST', // Thay thế 'POST' bằng phương thức HTTP bạn muốn sử dụng (GET, POST, PUT, DELETE, vv.)
                                        data: {
                                            ids: tempIds,
                                            _token: csrfToken,
                                            dataId: dataId
                                        }, // Dữ liệu bạn muốn gửi đi, ở đây tôi sử dụng mảng tempIds và đặt tên là 'ids'
                                        success: function (response) {
                                            // Xử lý kết quả thành công ở đây
                                        },
                                        error: function (xhr, status, error) {
                                            // Xử lý lỗi ở đây
                                        }
                                    });
                                });

                            },
                            error: function (xhr) {
                                // Xử lý lỗi (nếu có)
                            }
                        });
                    });
                },
                error: function (xhr, status, error) {
                    // Xử lý lỗi (nếu có)
                }
            });
        }
    });


});

