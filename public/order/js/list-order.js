$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

$(document).ready(function ()
{
    searchOrders();
    getCountItemsInListOrderByOrderType();
});

function getCountItemsInListOrderByOrderType()
{
    $.ajax({
        type: "GET",
        url: '/getCountItemsInListOrder',
        success: function (response) {
            var orderByUsing=response.orderByUsing;
            var orderByBringBack=response.orderByBringBack;
            var orderByShip=response.orderByShip;
            var orderByResever=response.orderByReserve;
            $('#count-using').html(orderByUsing);
            $('#count-bringBack').html(orderByBringBack);
            $('#count-ship').html(orderByShip);
            $('#count-resever').html(orderByResever)
        },
        error: function (xhr) {
        }
    });
}


//live search order
$("#search").keyup(function () {
  searchOrders();

});
$(".row.card-body .nav-link").click(function () {
  searchOrders();
});

function filterByStatus() {
  searchOrders();
}

function searchOrders(page) {
    var statusId = $('#status').val();
    var searchString = $('#search').val();
    var dataType = $('button.nav-link.active').data('type');
    // Tiếp tục xử lý dữ liệu theo nhu cầu của bạn
  $.ajax({
    type: "GET",
    url: URL_SEARCH,
    data: {
      search_string: searchString,
      status_id: statusId,
      type: dataType,
        page:page
    },
    success: function (response) {
      $('#table_data').html(response);
      $('.pagination a').unbind('click').on('click', function(e) {
            e.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
          searchOrders(page);
      });
      splitTable();
    },
    error: function (xhr) {
    }
  });
}

let orderDetail = [];
function getAllDetail($orderTableId) {
  return new Promise(function (resolve, reject) {
    $.ajax({
      url: URL_GET_DATA_TO_PUSHER,
      method: 'get',
      data: {
        orderTableId: $orderTableId,
      },
      success: function (data) {
        orderDetail = data;
        resolve();
      },
      error: function (err) {
        reject(err);
      }
    });
  });
}


$('body').on('click', '.move-to-kitchen, #deleteIT', async function () {
  if ($(this).hasClass('move-to-kitchen')) {
    let $orderTableId = $(this).attr('data-order-id');
    if (confirm('Bạn có chắc chuyển đơn hàng này xuống bếp/bar không?')) {
      var loader = document.createElement('div');
      loader.className = 'loader';
      var overlay = document.createElement('div');
      overlay.className = 'overlay';
      overlay.appendChild(loader);
      document.body.appendChild(overlay);

      await getAllDetail($orderTableId);

      $.ajax({
        url: 'move-to-kitchen/' + $orderTableId,
        method: 'POST',
        data: {
          orderTableId: $orderTableId,
          orderDetail: orderDetail,
        },
        success: function (response) {
          var statusId = $('#status').val();
          var searchString = $('#search').val();
          var dataType = $('button.nav-link.active').data('type');
          searchOrders(searchString, statusId, dataType);
            getCountItemsInListOrderByOrderType();
          moveToKitchenSuccess();
          $('.loader').remove();
          $('.overlay').remove();
        },
        error: function (res) {
          moveToKitchenError();
        },
      })
    }
  } else if ($(this).attr('id') === 'deleteIT') {
    if (confirm('Bạn có chắc muốn xóa order trên không?')) {
      var deleteLink = $(this);
      var id = deleteLink.data("id");

      // Gửi yêu cầu xóa bằng AJAX
      $.ajax({
        url: "/deleteOrder/" + id,
        type: "get",
        success: function (response) {
          var statusId = $('#status').val();
          var searchString = $('#search').val();
          var dataType = $('button.nav-link.active').data('type');
          searchOrders(searchString, statusId, dataType);
          getCountItemsInListOrderByOrderType();
          deleteOrderSuccess();
        },
        error: function (xhr, status, error) {
          deleteOrderError();
        }
      });
    }
  }
})

function moveToKitchenSuccess() {
  toast({
    title: "Thành công",
    message: "Bạn đã chuyển món xuống Bếp/Bar thành công !",
    type: "success",
    duration: 4000
  });
}

function deleteOrderSuccess() {
  toast({
    title: "Thành công",
    message: "Bạn đã xóa Order thành công !",
    type: "success",
    duration: 4000
  });
}

function deleteOrderError() {
  toast({
    title: "Thất bại!",
    message: "Xóa Order thất bại!",
    type: "error",
    duration: 4000
  });
}

function moveToKitchenError() {
  toast({
    title: "Thất bại!",
    message: "Chuyển món xuống Bếp/Bar thất bại !",
    type: "error",
    duration: 4000
  });
}

function toast({ title = "", message = "", type = "info", duration = 3000 }) {
  const main = document.getElementById("toast");
  if (main) {
    const toast = document.createElement("div");

    // Auto remove toast
    const autoRemoveId = setTimeout(function () {
      main.removeChild(toast);
    }, duration + 1000);

    // Remove toast when clicked
    toast.onclick = function (e) {
      if (e.target.closest(".toast__close")) {
        main.removeChild(toast);
        clearTimeout(autoRemoveId);
      }
    };

    const icons = {
      success: "fas fa-check-circle",
      info: "fas fa-info-circle",
      warning: "fas fa-exclamation-circle",
      error: "fas fa-exclamation-circle"
    };
    const icon = icons[type];
    const delay = (duration / 1000).toFixed(2);

    toast.classList.add("toast", `toast--${type}`);
    toast.style.animation = `slideInLeft ease .3s, fadeOut linear 1s ${delay}s forwards`;

    toast.innerHTML = `
                    <div class="toast__icon">
                        <i class="${icon}"></i>
                    </div>
                    <div class="toast__body">
                        <h3 class="toast__title">${title}</h3>
                        <p class="toast__msg">${message}</p>
                    </div>
                    <div class="toast__close">
                        <i class="fas fa-times"></i>
                    </div>
                `;
    main.appendChild(toast);
  }
}




