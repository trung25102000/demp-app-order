$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
const dropdownMenu = document.getElementById('dropdownMenuButton');
const dropdownItems = document.querySelectorAll('.dropdown-item');

dropdownItems.forEach(item => {
    item.addEventListener('click', function () {
        dropdownMenu.innerHTML = item.innerHTML;
        $(dropdownMenu).attr('data-status-order', $(item).attr('data-status'));
    });
});

// fetch dữ liệu từ Products
async function fetchData() {
    try {
        let response = await fetch('/products');
        if (!response.ok) {
            throw new Error('Something went wrong!');
        }
        let dataApi = await response.json();
        renderListDataApi(dataApi);
    } catch {

    }
}

fetchData();


let searchNameEating = document.getElementById('searchNameEating');

searchNameEating.addEventListener('input', function (event) {
    event.preventDefault(); // Ngăn chặn gửi biểu mẫu mặc định
    let keyword = searchNameEating.value;
    let category_id = selectedCategoryId;

    // Gửi yêu cầu tìm kiếm đến API bằng Ajax
    async function fetchDataSearch() {
        try {
            let response = await fetch('/products/search', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                body: JSON.stringify({
                    keyword: keyword,
                    category_id: category_id
                })
            });

            if (!response.ok) {
                throw new Error('Something went wrong!');
            }

            let dataSearch = await response.json();
            renderListDataApi(dataSearch);
        } catch (error) {
            console.error(error);
        }
    }

    fetchDataSearch();
});

// Thêm danh sách các món ăn từ database của Product
let foodList = document.getElementById('food-list');

function renderListDataApi(dataApi) {
    let html = "";
    dataApi.data.data.forEach((food) => {
        html += `
    <div class="mb-3 grid-3 grid-2 food-list-detail" style="width: 20%; box-sizing:border-box; padding: 0 8px; position: relative; cursor:pointer; " data-id="${food.id}" id="quantityDetail${food.id}" type='submit'>
        <div class=""style="background-image: url('${food.product_image}'); padding-top: 89%; background-repeat: no-repeat; background-size: cover; background-position: center"></div>
        <div class="py-2 bg-light" style="font-weight:600; line-height:1.3rem ;text-align:center; min-height: 3.5rem">${food.product_name}</div>
        <div class="ms-1" style="background-color: #52a2d0bf; position:absolute; top:0; right: 8px; color: white; padding: 5px 10px; font-weight: 700;"><span>${food.product_price / 1000}K</span></div>
    </div>`
    });
    foodList.innerHTML = html;
    handleClickImage(dataApi);
}

//Cập nhật mảng nếu có trùng product_id để hiển thị ra view
function processOrderDetailList(orderDetailList) {
    var tempArray = [...orderDetailList];

    for (var i = 0; i < tempArray.length; i++) {
        var productId = tempArray[i].id;
        var quantity = tempArray[i].quantity;

        // Tìm các phần tử cùng product_id và cộng dồn quantity
        for (var j = i + 1; j < tempArray.length; j++) {
            if (tempArray[j].id === productId) {
                quantity += tempArray[j].quantity;
                // Xóa phần tử trùng
                tempArray.splice(j, 1);
                j--; // Giảm giá trị của j để duyệt qua phần tử mới sau khi xóa
            }
        }

        tempArray[i].quantity = quantity; // Cập nhật quantity mới
    }

    return tempArray;
}

// Xử lí sự kiện click vào ảnh để hiển thị chi tiết order
let orderItems = [];

function getDetails(callback) {
    $.ajax({
        url: URL_GET_DETAIL_IN_EDIT,
        method: 'get',
        success: function (data) {
            orderItems = data;
            // var orderitem=processOrderDetailList(orderItems);
            renderOrderItems(orderItems);
        },
        error: function (res) {

        }
    })
}



let orderItemsEdit = [];
function handleClickImage(dataApi) {
    let foodListDetail = document.querySelectorAll('.food-list-detail');

    foodListDetail.forEach((btn, i) => {
        btn.addEventListener("click", () => {
            let dataId = btn.getAttribute('data-id');
            // lấy dwx liệu đưa xuống bar/bếp
            let existingItemIndexEdit = orderItemsEdit.findIndex(item => item.id == dataId);
            if (existingItemIndexEdit !== -1) {
                orderItemsEdit[existingItemIndexEdit].quantity++;
            }
            else {
                // Nếu chưa có trong orderItems, thêm mới vào mảng
                let foodDetailEdit = dataApi.data.data[i];
                orderItemsEdit.push({
                    ...foodDetailEdit,
                    quantity: 1,
                    status:1
                })
            }
            //kết thúc hàm lấy dữ liệu đưa bar/bếp
            // lấy dwx liệu đưa xuống ra view
            let existingItemIndex = orderItems.findIndex(item => item.id == dataId);
            // Nếu đã có trong orderItems, chỉ cập nhật số lượng
            if (existingItemIndex !== -1) {
                    orderItems[existingItemIndex].quantity++;
                // Cập nhật giá trị số lượng lên input tương ứng
                document.getElementById(`quantityDetail${dataId}`).value = orderItems[existingItemIndex].quantity;
            } else {
                // Nếu chưa có trong orderItems, thêm mới vào mảng
                let foodDetail = dataApi.data.data[i];

                orderItems.push({
                    ...foodDetail,
                    quantity: 1,
                    status:1
                })
            }
            //kết thúc hàm lấy dữ liệu đưa view
            renderOrderItems(orderItems);

        });
    })
}




// Hàm render số lượng chi tiết các món ăn của 1 order
function renderOrderItems(orderItems) {
    let pictureModel = document.querySelector('.picture-model');
    let orderDetailEating = document.querySelector('.order-detail-eating');
    if (pictureModel) {
        if (pictureModel) {
            if (orderItems.length) {
                pictureModel.classList.add('d-none');
                orderDetailEating.classList.remove('d-none');
            } else {
                pictureModel.classList.remove('d-none');
                orderDetailEating.classList.add('d-none');
            }
        }
    }
    let tbodyElement = document.getElementById('tbody');
    // Xóa toàn bộ nội dung trong tbodyElement
    tbodyElement.innerHTML = "";
    // Render lại thông tin chi tiết từ mảng orderItems
    orderItems.forEach(item => {
        let row = document.createElement("tr");
        row.innerHTML = `
    <td style = "vertical-align: middle; cursor: pointer" data-bs-toggle="modal" data-bs-target="#request-cooking-modal-${item.id}">${item.product_name ?? item.order_detail_other_name}</td>
    <td style="vertical-align: middle">${item.unit ? item.unit.unit_name : item.order_detail_other_unit_name}</td>
    <td style="vertical-align: middle">
        <input id="quantityDetail${item.id}" type="number" min="1" value="${item.quantity}"
               style="width: 2.5rem; height: 2.5rem; text-align:center; border: 1px solid #e4e6e7"
               class="quantity-input"/>
    </td>
    <td style="vertical-align: middle">${formatNumber(parseInt(item.product_price) * (item.quantity))} đ</td>
    <td style="vertical-align: middle">
        <button type="button" class="btn btn-danger delete-list">Xoá</button>
    </td>
        `;
        if (item.note) {
            const extraRow = showExtraRowRequest(item);
            tbodyElement.appendChild(extraRow);
        }
        tbodyElement.appendChild(row);
        // Tạo modal tương ứng với mỗi hàng
        const modalElement = document.createElement("div");
        modalElement.classList.add("modal");
        modalElement.classList.add("close-modal-request");
        modalElement.id = `request-cooking-modal-${item.id}`;
        modalElement.innerHTML = `
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="background-color:#0973b9; color: #fff">
                        <h5 class="modal-title" id="exampleModalLabel">Yêu cầu chế biến</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                    <form>
                        <div class="mb-3">
                            <label for="message-text" class="col-form-label">Nhập yêu cầu chế biến</label>
                            <textarea class="form-control request-main" id="message-text"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary save-data-request">Lưu </button>
                </div>
            </div>
            `;
        tbodyElement.appendChild(modalElement);

        //Thêm hàng mới khi có yêu cầu chế biến riêng với món ăn
        if (item.order_detail_note) {
            const extraRow = createExtraRowRequest(item);
            tbodyElement.appendChild(extraRow);
        }
    });

//Xử lí chức năng thêm yêu cầu chế biến món ăn theo yêu cầu của khách
    const requestMains = document.querySelectorAll('.request-main');
    const saveDataRequests = document.querySelectorAll('.save-data-request');
    const closeModalRequest = document.querySelectorAll('.close-modal-request');

    saveDataRequests.forEach((saveDataRequest, index) => {
        saveDataRequest.addEventListener('click', function () {
            orderItems[index].order_detail_note = requestMains[index].value;

            renderOrderItems(orderItems);
            requestMains[index].value = "";
            const bootstrapModal = bootstrap.Modal.getInstance(closeModalRequest[index]);
            bootstrapModal.hide();
        })
    })

//Hàm thêm hàng mới khi khách có yêu cầu chế biến theo yêu cầu
    function createExtraRowRequest(item) {
        const extraRow = document.createElement("tr");
        extraRow.classList.add("plus-tr-request");
        extraRow.innerHTML = `
        <td style="vertical-align: middle; color: red; font-weight: 500; padding-left: 1rem">Yêu cầu chế biến</td>
            <td style="vertical-align: middle">${item.order_detail_note}</td>
            <td style="vertical-align: middle"></td>
            <td style="vertical-align: middle"></td>
        <td style="vertical-align: middle"></td>
            `;
        return extraRow;
    }
    function showExtraRowRequest(item) {
        const extraRow = document.createElement("tr");
        extraRow.classList.add("plus-tr-request");
        extraRow.innerHTML = `
        <td style="vertical-align: middle; color: red; font-weight: 500; padding-left: 1rem">Yêu cầu chế biến</td>
            <td style="vertical-align: middle">${item.note}</td>
            <td style="vertical-align: middle"></td>
            <td style="vertical-align: middle"></td>
        <td style="vertical-align: middle"></td>
            `;
        return extraRow;
    }



    // Xử lí sự kiện thay đổi value của ô số lượng giá sẽ cập nhập theo
    tbodyElement.querySelectorAll('.quantity-input').forEach((e, index) => {
        e.addEventListener('change', () => {
            let quantity = parseInt(e.value);
            if (isNaN(quantity) || quantity < 1) {
                quantity = 1;
            } else {
                orderItems[index].quantity = parseInt(e.value);
                calcTotalPrice();
                //Cập nhập cột thành tiền
                let itemTotalPrice = parseInt(orderItems[index].product_price) * quantity;
                let totalTd = e.parentNode.nextElementSibling;
                totalTd.textContent = `${formatNumber(itemTotalPrice)} đ`;
            }
        })
    })
    calcTotalPrice();

    // Hàm tính tổng tiền khách hàng đã order
    function calcTotalPrice() {
        let totalMoneyOrder = document.getElementById('total-money-order');
        let discountAmount = parseFloat($('.voucher-checkbox:checked').attr('discount-amount'));
        let totalPrice = orderItems.reduce((total, item) => {
            let itemTotalPrice = item.quantity * parseInt(item.product_price);
            return total + itemTotalPrice;
        }, 0);
        if (!isNaN(discountAmount)) {
            var discountMoney = totalPrice * discountAmount;
            totalPrice -= discountMoney;
        }
        totalMoneyOrder.textContent = `${formatNumber(totalPrice)} đ`;
    }

    $('#save-modal-voucher-edit').on('click', function () {
        calcTotalPrice();
        $('#voucherInEdit2').modal('hide');
    })

    // Xoá 1 món ăn khỏi order khách hàng
    tbodyElement.querySelectorAll('.delete-list').forEach((e, index) => {
        e.addEventListener('click', () => {
            if (confirm("Bạn muốn xóa món này ?")) {
                orderItems.splice(index, 1);
                renderOrderItems(orderItems);
            }
        })
    })
}

//Xử lí chức năng thêm món khác
let addFoodBtn = document.getElementById('add-food-button');
let foodNameInput = document.getElementById('other-food-name');
let foodQuantityInput = document.getElementById('other-food-quantity');
let foodPriceInput = document.getElementById('other-food-price');
let foodUnitInput = document.getElementById('other-food-unit');

addFoodBtn.addEventListener('click', function () {
    let foodName = foodNameInput.value;
    let foodQuantity = foodQuantityInput.value;
    let foodPrice = foodPriceInput.value;
    let foodUnit = foodUnitInput.value;

    // Kiểm tra dữ liệu hợp lệ
    if (!foodName || !foodQuantity || !foodPrice || !foodUnit) {
        alert('Vui lòng nhập đầy đủ thông tin món ăn');
        return;
    }

    // Tạo món mới
    let newFood = {
        product_name: foodName,
        order_detail_other_unit_name: foodUnit,
        quantity: foodQuantity,
        product_price: foodPrice,
    };

    // Tạo danh sách tạm thời để lưu trữ món mới và các món hiện có
    orderItems.push({
        ...newFood
    })

    // Render danh sách món ăn
    renderOrderItems(orderItems);

    // Reset modal
    [foodNameInput, foodQuantityInput, foodPriceInput, foodUnitInput].forEach(input => {
        input.value = '';
    });

    // Đóng modal
    var modal = document.getElementById('exampleModal');
    var modalInstance = bootstrap.Modal.getInstance(modal);
    modalInstance.hide();

});

function ajaxToStoreOrder() {
    let data = {
        table_id: $('select[name="table_id"]').val(),
        count_of_customer: $('input[name="count_of_customer"]').val(),
        status: $('.status_order').attr('data-status-order'),
        voucher_id: $('.voucher-checkbox:checked').val(),
        user_id: $('select[name="user_id"]').val(),
        customer: selectCustomer,
        orderItems: orderItems,
        customer_id: cusomter_id,
        type_order_data: selectTypeOrderData,
    };
    $.ajax({
        type: "post",
        url: URL_STORE_ORDER,
        data: data,
        dataType: "json",
        success: function (response) {
            resetErrors();
            location.href = URL_LIST_ORDER;
        },
        error: function (res) {
            if (res.status == 422) {
                showErrors(res.responseJSON.errors)
            }
        }
    });
}

function showErrors(dataErrors) {
    resetErrors();
    $.each(dataErrors, function (key, message) {
        $(".error-" + key).removeClass("d-none");
        $(".error-" + key + " small").html(message);
    });
}

function resetErrors() {
    $(".error").addClass("d-none");
}

$(function () {
    $('.btn-store-order').on('click', function () {
        ajaxToStoreOrder();
    })
});

// Xử lí chức năng tìm kiếm khách hàng đã là thành viên để chọn hoặc thêm mới khách hàng
const searchCustomer = document.querySelector('.search-customer');
const customerTableBody = document.getElementById('customerTableBody');
const saveDataCustomer = document.querySelector('.save-data-customer');
const errorMessageElement = document.getElementById('error-message');
let selectCustomer = [];
let cusomter_id = "";

saveDataCustomer.addEventListener('click', () => {
    // Đóng modal
    let customerID = $('input[name="customer_id"]:checked').val();

    if (!customerID) {
        alert('Chỉ chọn 1 khách hàng từ danh sách, nếu không có trong danh sách thì nhấn thêm để thêm thông tin khách hàng');
    }
    cusomter_id = customerID;
    const modal = document.getElementById('verticalycentered');
    const modalInstance = bootstrap.Modal.getInstance(modal);
    modalInstance.hide();
})
//Click bên ngoài modal vẫn xử lí chức năng lưu thông tin nếu đã người dùng đã click chọn rồi
$('#verticalycentered').on('click', function (e) {
    if (!$(e.target).closest('.modal-content').length) {
        let customerID = $('input[name="customer_id"]:checked').val();
        if (!customerID) {
            alert('Chỉ chọn 1 khách hàng từ danh sách, nếu không có trong danh sách thì nhấn thêm để thêm thông tin khách hàng');
        }
        cusomter_id = customerID;
    }
});
searchCustomer.addEventListener('input', function (event) {
    event.preventDefault(); // Ngăn chặn gửi biểu mẫu mặc định
    let keyword = searchCustomer.value;
    let html = "";

    // Gửi yêu cầu tìm kiếm đến API bằng Ajax
    async function fetchDataSearch() {
        try {

            const response = await fetch('/create-order/search?keyword=' + encodeURIComponent(keyword));
            if (!response.ok) {
                throw new Error('Something went wrong!');
            }
            const customers = await response.json();
            customerTableBody.innerHTML = "";
            customers.forEach((customer) => {
                html += `
                    <tr>
                        <th scope="row">${customer.id}</th>
                        <td>${customer.customer_name}</td>
                        <td>${customer.customer_birthday}</td>
                        <td>${customer.customer_phone}</td>
                        <td>${new Date(customer.created_at).toISOString().slice(0, 10)}</td>
                        <td><input type="radio" name="customer_id" value="${customer.id}"></td>
                    </tr>`
            });
            customerTableBody.innerHTML += html;
            if (customers.length == 1) {
                selectCustomer = [...customers];
            }
        } catch (error) {
            errorMessageElement.textContent = 'Có lỗi khi gọi API: ' + error.message;
        }
    }

    fetchDataSearch();
})
//Thêm mới khách hàng
const savePlusCustomerNew = document.querySelector('.save-plus-customer-new');

savePlusCustomerNew.addEventListener('click', () => {
    const customerName = document.querySelector('.customer-name');
    const customerEmail = document.querySelector('.customer-email');
    const customerPhone = document.querySelector('.customer-phone');
    const customerBirthday = document.querySelector('.customer-birthday');
    const customerGender = document.querySelector('.customer-gender');
    const customerAddress = document.querySelector('.customer-address');

    if ($('.customer-name').val() =='' || $('.customer-phone').val() =='' || $('.customer-address').val() =='') {
        alert('Bạn chưa nhập thông tin khách hàng');
        return;
    }
    selectCustomer = {
        customer_name: customerName.value,
        customer_email: customerEmail.value,
        customer_phone: customerPhone.value,
        customer_birthday: customerBirthday.value,
        customer_gender: customerGender.value,
        customer_address: customerAddress.value
    };
    // Đóng modal
    const modal = document.getElementById('modal2');
    const modalInstance = bootstrap.Modal.getInstance(modal);
    modalInstance.hide();
})
//Click bên ngoài modal vẫn xử lí chức năng lưu thông tin nếu đã người dùng đã điền thông tin rồi
$('#modal2').on('click', function (e) {
    if (!$(e.target).closest('.modal-content').length) {
        if ($('.customer-name').val() =='' || $('.customer-phone').val() =='' || $('.customer-address').val() =='') {
            alert('Bạn chưa nhập thông tin khách hàng');
            return;
        }
        selectCustomer = {
            customer_name: $('.customer-name').val(),
            customer_email: $('.customer-email').val(),
            customer_phone: $('.customer-phone').val(),
            customer_birthday: $('.customer-birthday').val(),
            customer_gender: $('.customer-gender').val(),
            customer_address:$('.customer-address').val(),
        }
    }
});

// Xử lí chức năng click checkbox để lưạ chon voucher cho khách order
const checkboxSelects = document.querySelectorAll('.checkbox-select');

let selectVoucher = [];

checkboxSelects.forEach(checkbox => {
    checkbox.addEventListener('change', function () {
        checkboxSelects.forEach(otherCheckbox => {
            if (otherCheckbox !== checkbox) {
                otherCheckbox.checked = false;
            }
        });
        if (checkbox.checked) {
            selectVoucher = {
                id: checkbox.value,
                description: checkbox.getAttribute('data-description')
            };
        } else {
            selectedCheckbox = null;
        }
    });
});

const categoryFilters = document.querySelectorAll('.categoryFilter');
const bestSellerFilter = document.querySelector('#best-seller');
let selectedCategoryId = null;

categoryFilters.forEach(categoryFilter => {
    categoryFilter.addEventListener('click', (e) => {
        e.preventDefault();
        const categoryId = categoryFilter.getAttribute('data-id');
        selectedCategoryId = categoryId;

        async function fetchDataCategories() {
            let response = await fetch('/filter?category_id=' + categoryId);
            if (!response.ok) {
                throw new Error('Something went wrong!');
            }
            let dataSearch = await response.json();
            renderListDataApi(dataSearch);
        }

        fetchDataCategories();
        searchNameEating.value = "";
    });
});
bestSellerFilter.addEventListener('click', function (e) {
    e.preventDefault();

    async function fetchDataBestSeller() {
        let response = await fetch('/best-seller');
        if (!response.ok) {
            throw new Error('Something went wrong!');
        }
        let dataSearch = await response.json();
        renderListDataApi(dataSearch);
    }

    fetchDataBestSeller();
    searchNameEating.value = "";
})


const buttonsFilter = document.getElementsByClassName("categoryFilter");

for (var i = 0; i < buttonsFilter.length; i++) {
    buttonsFilter[i].addEventListener("click", function () {
        for (var j = 0; j < buttonsFilter.length; j++) {
            buttonsFilter[j].classList.remove("active");
            buttonsFilter[j].style.backgroundColor = "";
        }
        this.classList.add("active");
        this.style.backgroundColor = "lightblue"
    });
}

const allProductFilter = document.querySelector('#all-products');

allProductFilter.addEventListener('click', function (e) {
    e.preventDefault();
    fetchData();
})

// Xử lí chức năng chọn loại order và lưu trữ các thông tin cần thiết
let selectTypeOrderData = [];
//Hiển thị loại order lên thanh dropdown
$('.type-order').click(function(e) {
    e.preventDefault();

    var selectedValue = $(this).data('value');
    var selectedStatus = $(this).data('status');

    $('.status_order').text(selectedValue);
    $('.status_order').data('status-order', selectedStatus);
})
// // Thông tin khách hàng mang về
$('.save-customer-back').on('click',()=> {
    selectTypeOrderData.push({
        name: $('.name-customer-back').val(),
        phone: $('.phone-customer-back').val()
    });

    const modalInstance = bootstrap.Modal.getInstance($('#back'));
    modalInstance.hide();

})
//Thông tin khách hàng đặt trước
$('.save-customer-reserve').on('click',()=> {
    if ($('.name-customer-reserve').val() == "" || $('.phone-customer-reserve').val() == "" || $('.date-customer-reserve').val() == "" || $('.time-customer-reserve').val() == "") {
        alert('Vui lòng nhập đầy đủ thông tin thời gian khách nhận bàn');
        return;
    }
    selectTypeOrderData = ({
        name: $('.name-customer-reserve').val(),
        phone: $('.phone-customer-reserve').val(),
        date: $('.date-customer-reserve').val(),
        time: $('.time-customer-reserve').val(),
        orther: $('.orther-customer-reserve').val()
    });
    bootstrap.Modal.getInstance($('#reserved')).hide();
})
//Thông tin khách đặt hàng yêu cầu giao tận nơi
$('.save-customer-ship').on('click',()=> {
    if ($('.date-customer-ship').val() == "" || $('.time-customer-ship').val() == "" || $('.address-customer-ship').val() == "" || $('.phone-customer-ship').val() == "") {
        alert('Vui lòng nhập đầy đủ thông tin thời gian khách nhận hàng');
        return;
    }
    selectTypeOrderData = ({
        name: $('.name-customer-ship').val(),
        date: $('.date-customer-ship').val(),
        time: $('.time-customer-ship').val(),
        phone: $('.phone-customer-ship').val(),
        address_ship: $('.address-customer-ship').val(),
        expense_ship: $('.expense-customer-ship').val(),
        amount_prepaid: $('.amount-prepaid-customer').val()
    });
    bootstrap.Modal.getInstance($('#ship')).hide();
})
