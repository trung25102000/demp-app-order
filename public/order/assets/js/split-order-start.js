let listFromArrOrder = []

let listToArrOrder = []
let order_to = 0;

// const tableFrom = 1;
let product = {}
let maxQuantityOrder = 0;
let currentQuantityOrder = 0;

const orderToList = $('#orderToList');

const orderFrom = $('#orderFrom');
const listFromOrder = $('#listFromOrder');
const listToOrder = $('#listToOrder');
const btnMinusOrder = $('#btnMinusOrder');
const btnPlusOrder = $('#btnPlusOrder');
const tbQuantityOrder = $('#tbQuantityOrder');
const btnNextOrder = $('#btnNextOrder');
const btnPrevOrder = $('#btnPrevOrder');

const btnSplitOrder = $('#btnSplitOrder');




const handleSelectOrderTo = () => {
    $('.order-to').on('click', function () {
        order_to = +$(this).data('id');
        $('.order-to').parent().parent().removeClass('order-to-selected');
        $(this).parent().parent().addClass('order-to-selected');
    })
}



const getAllOrderDetailsByOrderId = (orderTableId) => {
    return $.ajax({
        type: 'GET',
        url: '/api/get-all-order-details-exist/' + orderTableId,
    });
}


const orderTableId = $('#orderFrom').data('id');

getAllOrderDetailsByOrderId(orderTableId).then((res) => {
    $('#errorContainerSplitTable').empty();
    // Ẩn errorContainer
    $('#errorContainerSplitTable').hide();
    listFromArrOrder = res.orderDetails;
    const order_from = res.tableFrom;

    const order_to_list = res.orderTableTo;
    orderFrom.empty().append('ID order: ' , orderTableId);
    if (listFromArrOrder !== null) {
        listFromOrder.empty();

        listFromArrOrder.forEach(item => {
            const str = renderProductList(item);
            listFromOrder.append(str)
        })

        orderToList.empty();
        order_to_list.forEach(item => {
            if (orderTableId != item.id){
                const str = renderTableToList(item);
                orderToList.append(str);
            }
        })
        handleSelectOrderTo();
    }
    else {
        alert('Hóa đơn không có món thức ăn nào');
    }
});


const renderTableToList = (obj) => {
    return `
        <div class="MuiPaper-root MuiPaper-outlined MuiPaper-rounded css-wkkely">
            <div class="MuiBox-root css-1mw3cm6"></div>
            <div class="&quot; MuiBox-root css-rmekqa table_group">
                <h6 class="MuiTypography-root MuiTypography-h6 css-1rukwjf order-to" data-id="${obj.id}">ID order: ${obj.id}</h6>
            </div>
        </div>
    `;
}


listFromOrder.on('click', function () {
    const productId = +$(this).val()

    maxQuantityOrder = +$(this).find('option:selected').data('quantity');
    maxQuantityOrder = maxQuantityOrder > 0 ? maxQuantityOrder : 0;
    tbQuantityOrder.val(maxQuantityOrder);
    listToOrder.find('option').removeClass('selected');
    listToOrder.prop('selectedIndex', -1);
    listFromOrder.find('option').removeClass('selected');
    listFromOrder.find('option:selected').addClass('selected');
    btnNextOrder.attr('disabled', false);
    btnPrevOrder.attr('disabled', true);
    product = listFromArrOrder.find(item => item.product_id === productId);
})

listToOrder.on('click', function () {
    const productId = +$(this).val();
    maxQuantityOrder = +$(this).find('option:selected').data('quantity');
    maxQuantityOrder = maxQuantityOrder > 0 ? maxQuantityOrder : 0;
    tbQuantityOrder.val(maxQuantityOrder);
    listFromOrder.find('option').removeClass('selected');
    listFromOrder.prop('selectedIndex', -1);
    listToOrder.find('option').removeClass('selected');
    listToOrder.find('option:selected').addClass('selected');
    btnNextOrder.attr('disabled', true);
    btnPrevOrder.attr('disabled', false);
    product = listToArrOrder.find(item => item.product_id === productId);
})

const renderProductList = (obj) => {
    return `
        <option name="product${obj.product_id}" value="${obj.product_id}" data-quantity="${obj.order_detail_quantity}">
            ${obj.product_name} (${obj.order_detail_quantity})
        </option>
    `
}

btnMinusOrder.on('click', () => {
    let quantity = +tbQuantityOrder.val();
    if (quantity > 1) {
        quantity--;
        tbQuantityOrder.val(quantity);
    }
})

tbQuantityOrder.on('change', function (e) {
    const newQuantity = +e.target.value;

    if (newQuantity < 1 || newQuantity > maxQuantityOrder) {
        tbQuantityOrder.val(currentQuantityOrder);
    }
})

tbQuantityOrder.on("keypress", function (e) {
    currentQuantityOrder = +tbQuantityOrder.val();
    const charCode = (e.which) ? e.which : e.keyCode;

    if (String.fromCharCode(charCode).match(/[^0-9]/g)) {
        return false;
    }
});

btnPlusOrder.on('click', () => {
    let quantity = +tbQuantityOrder.val();
    if (quantity < maxQuantityOrder) {
        quantity++;
        tbQuantityOrder.val(quantity);
    }
})

const handleTransferProductorder = (eListStartArr, eListEndArr, eBtn, eListStart, eListEnd) => {

    const newQuantity = +tbQuantityOrder.val();

    if (product.product_id > 0) {
        if (eListEndArr.length) {
            const index = eListEndArr.findIndex((item) => item.product_id === product.product_id);

            if (index >= 0) {
                eListEndArr[index].order_detail_quantity += newQuantity
            }
            else {
                const newProduct = { ...product, order_detail_quantity: newQuantity }
                eListEndArr.push(newProduct)
            }
        }
        else {
            const newProduct = { ...product, order_detail_quantity: newQuantity }
            eListEndArr.push(newProduct)
        }

        const index = eListStartArr.findIndex((item) => item.product_id === product.product_id)
        if (index >= 0) {
            if (eListStartArr[index].order_detail_quantity === newQuantity) {
                eListStartArr.splice(index, 1)
                eBtn.attr('disabled', true)
            }
            else {
                eListStartArr[index].order_detail_quantity -= newQuantity;
            }
        }

        eListStart.empty();

        eListStartArr.forEach(item => {
            const str = renderProductList(item);
            eListStart.append(str)
        })

        eListStart.find(`[name="product${product.product_id}"]`).addClass('selected')


        eListEnd.empty();

        eListEndArr.forEach(item => {
            const str = renderProductList(item);
            eListEnd.append(str)
        })

        eListEnd.find(':selected').removeClass('selected')
        eListEnd.prop('selectedIndex', -1);

    }
}

btnNextOrder.on('click', () => {
    handleTransferProductorder(listFromArrOrder, listToArrOrder, btnNextOrder, listFromOrder, listToOrder);
})

btnPrevOrder.on('click', () => {
    handleTransferProductorder(listToArrOrder, listFromArrOrder, btnPrevOrder, listToOrder, listFromOrder);
})

btnSplitOrder.on('click', () => {

    const obj = {
        order_from: {
            id_order_from: orderTableId,
            order_details: listFromArrOrder
        },
        order_to: {
            id_order_to: order_to,
            order_details: listToArrOrder
        }
    }


    $.ajax({
        headers: {
            'accept': 'application/json',
            'content-type': 'application/json',
        },
        type: 'POST',
        url: URL_SPLIT_ORDER,
        data: JSON.stringify(obj)
    })
        .done((data) => {
            location.reload();
        })
        .fail((jqXHR) => {
            let str = '';
            if (jqXHR.responseJSON && jqXHR.responseJSON.errors) {
                const errors = jqXHR.responseJSON.errors;
                for (const field in errors) {
                    if (errors.hasOwnProperty(field)) {
                        const fieldErrors = errors[field];

                        for (let i = 0; i < fieldErrors.length; i++) {
                            str += `<label class="error">${fieldErrors[i]}</label>`;
                            console.log(fieldErrors)
                        }
                    }
                }

                $("#errorContainerSplitOrder").html(str);
                $("#errorContainerSplitOrder").show();
            } else {
                // Xử lí lỗi mặc định
                str = 'Có lỗi xảy ra.';
                $("#errorContainerSplitOrder").html(str);
                $("#errorContainerSplitOrder").show();
            }
        });
})

const loadProductListFrom = () => {
    listFromArrOrder.forEach(item => {
        const str = renderProductList(item);
        listFromOrder.append(str)
    })
}

loadProductListFrom();
