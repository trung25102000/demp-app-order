let listFromArrBill = [
]

let listToArrBill = []

// const tableFrom = 1;
let product = {}
let maxQuantityBill = 0;
let currentQuantityBill = 0;

let billIdFrom = 1;
let billIdTo = 0;

const billFrom = $('#billFrom');

const listFromBill = $('#listFromBill');
const listToBill = $('#listToBill');
const btnMinusBill = $('#btnMinusBill');
const btnPlusBill = $('#btnPlusBill');
const tbQuantityBill = $('#tbQuantityBill');
const btnNextBill = $('#btnNextBill');
const btnPrevBill = $('#btnPrevBill');

const btnSplitTable = $('#btnSplitBill');


const handleSelectTableTo = () => {
    $('.bill-to').on('click', function () {
        billIdTo = +$(this).data('id');
        $('.bill-to').parent().parent().removeClass('table-to-selected');
        $(this).parent().parent().addClass('table-to-selected');
    })
}



const getAllOrderDetailsByOrderId = (orderTableId) => {
    return $.ajax({
        type: 'GET',
        url: '/api/get-all-order-details-bill/' + orderTableId,
    });
}


const orderTableId = $('#billFrom').data('id');

getAllOrderDetailsByOrderId(orderTableId).then((res) => {
    listFromArrBill = res.orderDetails;
    const table_from = res.tableFrom;

    billFrom.empty().append(table_from.table_name);

    if (listFromArrBill !== null) {
        listFromBill.empty();

        listFromArrBill.forEach(item => {
            const str = renderProductListBillInOrder(item);
            listFromBill.append(str)
        })

        handleSelectTableTo();
    }
    else {
        alert('Hóa đơn không có món thức ăn nào');
    }
});

const renderProductListBillInOrder = (obj) => {
    return `
        <option name="product${obj.product_id}" value="${obj.product_id}" data-quantity-bill="${obj.order_detail_quantity}">
            ${obj.product_name} (${obj.order_detail_quantity})
        </option>
    `
}

listFromBill.on('click', function () {
    const productId = +$(this).find(':selected').val();
    maxQuantityBill = +$(this).find('option:selected').data('quantity-bill');

    maxQuantityBill = maxQuantityBill > 0 ? maxQuantityBill : 0;
    tbQuantityBill.val(maxQuantityBill);
    listToBill.find('option').removeClass('selected');
    listToBill.prop('selectedIndex', -1);
    listFromBill.find('option').removeClass('selected');
    listFromBill.find('option:selected').addClass('selected');
    btnNextBill.attr('disabled', false);
    btnPrevBill.attr('disabled', true);
    product = listFromArrBill.find((item) => item.product_id === productId);
});

listToBill.on('click', function () {
    const productId = +$(this).find(':selected').val();
    maxQuantityBill = +$(this).find('option:selected').data('quantity-bill');
    maxQuantityBill = maxQuantityBill > 0 ? maxQuantityBill : 0;
    tbQuantityBill.val(maxQuantityBill);
    listFromBill.find('option').removeClass('selected');
    listFromBill.prop('selectedIndex', -1);
    listToBill.find('option').removeClass('selected');
    listToBill.find('option:selected').addClass('selected');
    btnNextBill.attr('disabled', true);
    btnPrevBill.attr('disabled', false);
    product = listToArrBill.find((item) => item.product_id === productId);
});


btnMinusBill.on('click', () => {
    let quantity = +tbQuantityBill.val();
    if (quantity > 1) {
        quantity--;
        tbQuantityBill.val(quantity);
    }
})

tbQuantityBill.on('change', function (e) {
    const newQuantity = +e.target.value;

    if (newQuantity < 1 || newQuantity > maxQuantityBill) {
        tbQuantityBill.val(currentQuantityBill);
    }
})

tbQuantityBill.on("keypress", function (e) {
    currentQuantityBill = +tbQuantityBill.val();
    const charCode = (e.which) ? e.which : e.keyCode;

    if (String.fromCharCode(charCode).match(/[^0-9]/g)) {
        return false;
    }
});

btnPlusBill.on('click', () => {
    let quantity = +tbQuantityBill.val();
    if (quantity < maxQuantityBill) {
        quantity++;
        tbQuantityBill.val(quantity);
    }
})


const handleTransferProductBill = (eListStartArrBill, eListEndArrBill, eBtnBill, eListStartBill, eListEndBill) => {

    const newQuantity = +tbQuantityBill.val();
    if (product.product_id > 0) {

        if (eListEndArrBill.length) {
            const index = eListEndArrBill.findIndex((item) => item.product_id === product.product_id);
            if (index >= 0) {
                eListEndArrBill[index].order_detail_quantity += newQuantity
            }
            else {
                const newProduct = { ...product, order_detail_quantity: newQuantity }
                eListEndArrBill.push(newProduct)
            }
        }
        else {
            const newProduct = { ...product, order_detail_quantity: newQuantity }
            eListEndArrBill.push(newProduct)
        }

        const index = eListStartArrBill.findIndex((item) => item.product_id === product.product_id)
        if (index >= 0) {
            if (eListStartArrBill[index].order_detail_quantity === newQuantity) {
                eListStartArrBill.splice(index, 1)
                eBtnBill.attr('disabled', true)
            }
            else {
                eListStartArrBill[index].order_detail_quantity -= newQuantity;
            }
        }

        eListStartBill.empty();

        eListStartArrBill.forEach(item => {
            const str = renderProductListBillInOrder(item);
            eListStartBill.append(str)
        })

        eListStartBill.find(`[name="product${product.product_id}"]`).addClass('selected')


        eListEndBill.empty();

        eListEndArrBill.forEach(item => {
            const str = renderProductListBillInOrder(item);
            eListEndBill.append(str)
        })

        eListEndBill.find(':selected').removeClass('selected')
        eListEndBill.prop('selectedIndex', -1);

    }
}

btnNextBill.on('click', () => {
    handleTransferProductBill(listFromArrBill, listToArrBill, btnNextBill, listFromBill, listToBill);
})

btnPrevBill.on('click', () => {
    handleTransferProductBill(listToArrBill, listFromArrBill, btnPrevBill, listToBill, listFromBill);
})



btnSplitTable.on('click', function() {
    const obj = {
        tableFrom: {
            id_order_table: orderTableId,
            orderDetails: listFromArrBill,
            order_details_to: listToArrBill
        }
    };

    $.ajax({
        headers: {
            'accept': 'application/json',
            'content-type': 'application/json',
        },
        type: 'POST',
        url: '/api/split-bill-in-order-table',
        data: JSON.stringify(obj)
    })
        .done(function(data) {
            window.location.href = 'http://localhost:2002/payment-detail';
            location.reload();
        })
        .fail(function(jqXHR) {
            let str = '';

            if (jqXHR.responseJSON && jqXHR.responseJSON.errors) {
                const errors = jqXHR.responseJSON.errors;
                for (const field in errors) {
                    if (errors.hasOwnProperty(field)) {
                        const fieldErrors = errors[field];

                        for (let i = 0; i < fieldErrors.length; i++) {
                            str += `<label class="error">${fieldErrors[i]}</label>`;
                            console.log(fieldErrors);
                        }
                    }
                }

                $("#errorContainerSplitTable").html(str);
                $("#errorContainerSplitTable").show();
            } else {
                // Xử lí lỗi mặc định
                str = 'Có lỗi xảy ra.';
                $("#errorContainerSplitTable").html(str);
                $("#errorContainerSplitTable").show();
            }
        });
});
const loadProductListFrom = () => {

    listFromArrBill.forEach(item => {
        const str = renderProductListBillInOrder(item);
        listFromBill.append(str)
    })
}

loadProductListFrom();
