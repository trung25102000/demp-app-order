function splitTable()
{
    let listFromArr = []

    let listToArr = []

// const tableFrom = 1;
    let product = {}
    let maxQuantity = 0;
    let currentQuantity = 0;

    let tableIdFrom = 1;
    let tableIdTo = 0;
    const tableToList = $('#tableToList');

    const tableFrom = $('#tableFrom');
    const listFrom = $('#listFrom');
    const listTo = $('#listTo');
    const btnMinus = $('#btnMinus');
    const btnPlus = $('#btnPlus');
    const tbQuantity = $('#tbQuantity');
    const btnNext = $('#btnNext');
    const btnPrev = $('#btnPrev');

    const btnSplitTable = $('#btnSplitTable');


    $('.btn-show-modal-split-table').on('click', function () {
        const orderId = $(this).data('order-id');
        tableIdFrom = orderId;
        listToArr = [];
        listTo.empty();

        getAllOrderDetailsByOrderId(orderId).then((res) => {
            $('#errorContainerSplitTable').empty();
            // Ẩn errorContainer
            $('#errorContainerSplitTable').hide();
            listFromArr = res.orderDetails;
            const table_from = res.tableFrom;
            const table_to_list = res.tableToList;
            tableFrom.empty().append(table_from.table_name);

            if (listFromArr !== null) {
                listFrom.empty();

                listFromArr.forEach(item => {
                    const str = renderProductList(item);
                    listFrom.append(str)
                })

                tableToList.empty();
                table_to_list.forEach(item => {
                    const str = renderTableToList(item);
                    tableToList.append(str);
                })

                handleSelectTableTo();

                $('#modalSplitTable').modal('show');
            }
            else {
                alert('Hóa đơn không có món thức ăn nào');
            }
        });
    });


    const getAllOrderDetailsByOrderId = (orderId) => {
        return $.ajax({
            type: 'GET',
            url: '/api/get-all-order-details/' + orderId,
        })
    }


    const renderTableToList = (obj) => {
        return `
        <div class="MuiPaper-root MuiPaper-outlined MuiPaper-rounded css-wkkely">
            <div class="MuiBox-root css-1mw3cm6"></div>
            <div class="&quot; MuiBox-root css-rmekqa table_group">
                <h6 class="MuiTypography-root MuiTypography-h6 css-1rukwjf table-to" data-id="${obj.id}">ID bàn: ${obj.table_name}</h6>
            </div>
        </div>
    `;
    }


    const handleSelectTableTo = () => {
        $('.table-to').on('click', function () {
            tableIdTo = +$(this).data('id');
            $('.table-to').parent().parent().removeClass('table-to-selected');
            $(this).parent().parent().addClass('table-to-selected');
        })
    }

    listFrom.on('click', function () {
        const productId = +$(this).val()

        maxQuantity = +$(this).find('option:selected').data('quantity');
        maxQuantity = maxQuantity > 0 ? maxQuantity : 0;
        tbQuantity.val(maxQuantity);
        listTo.find('option').removeClass('selected');
        listTo.prop('selectedIndex', -1);
        listFrom.find('option').removeClass('selected');
        listFrom.find('option:selected').addClass('selected');
        btnNext.attr('disabled', false);
        btnPrev.attr('disabled', true);
        product = listFromArr.find(item => item.product_id === productId);
    })

    listTo.on('click', function () {
        const productId = +$(this).val();
        maxQuantity = +$(this).find('option:selected').data('quantity');
        maxQuantity = maxQuantity > 0 ? maxQuantity : 0;
        tbQuantity.val(maxQuantity);
        listFrom.find('option').removeClass('selected');
        listFrom.prop('selectedIndex', -1);
        listTo.find('option').removeClass('selected');
        listTo.find('option:selected').addClass('selected');
        btnNext.attr('disabled', true);
        btnPrev.attr('disabled', false);
        product = listToArr.find(item => item.product_id === productId);
    })

    const renderProductList = (obj) => {
        return `
        <option name="product${obj.product_id}" value="${obj.product_id}" data-quantity="${obj.order_detail_quantity}">
            ${obj.product_name} (${obj.order_detail_quantity})
        </option>
    `
    }

    btnMinus.on('click', () => {
        let quantity = +tbQuantity.val();
        if (quantity > 1) {
            quantity--;
            tbQuantity.val(quantity);
        }
    })

    tbQuantity.on('change', function (e) {
        const newQuantity = +e.target.value;

        if (newQuantity < 1 || newQuantity > maxQuantity) {
            tbQuantity.val(currentQuantity);
        }
    })

    tbQuantity.on("keypress", function (e) {
        currentQuantity = +tbQuantity.val();
        const charCode = (e.which) ? e.which : e.keyCode;

        if (String.fromCharCode(charCode).match(/[^0-9]/g)) {
            return false;
        }
    });

    btnPlus.on('click', () => {
        let quantity = +tbQuantity.val();
        if (quantity < maxQuantity) {
            quantity++;
            tbQuantity.val(quantity);
        }
    })

    const handleTransferProduct = (eListStartArr, eListEndArr, eBtn, eListStart, eListEnd) => {

        const newQuantity = +tbQuantity.val();

        if (product.product_id > 0) {
            if (eListEndArr.length) {
                const index = eListEndArr.findIndex((item) => item.product_id === product.product_id);

                if (index >= 0) {
                    eListEndArr[index].order_detail_quantity += newQuantity
                }
                else {
                    const newProduct = { ...product, order_detail_quantity: newQuantity }
                    eListEndArr.push(newProduct)
                }
            }
            else {
                const newProduct = { ...product, order_detail_quantity: newQuantity }
                eListEndArr.push(newProduct)
            }

            const index = eListStartArr.findIndex((item) => item.product_id === product.product_id)
            if (index >= 0) {
                if (eListStartArr[index].order_detail_quantity === newQuantity) {
                    eListStartArr.splice(index, 1)
                    eBtn.attr('disabled', true)
                }
                else {
                    eListStartArr[index].order_detail_quantity -= newQuantity;
                }
            }

            eListStart.empty();

            eListStartArr.forEach(item => {
                const str = renderProductList(item);
                eListStart.append(str)
            })

            eListStart.find(`[name="product${product.product_id}"]`).addClass('selected')


            eListEnd.empty();

            eListEndArr.forEach(item => {
                const str = renderProductList(item);
                eListEnd.append(str)
            })

            eListEnd.find(':selected').removeClass('selected')
            eListEnd.prop('selectedIndex', -1);

        }
    }

    btnNext.on('click', () => {
        handleTransferProduct(listFromArr, listToArr, btnNext, listFrom, listTo);
    })

    btnPrev.on('click', () => {
        handleTransferProduct(listToArr, listFromArr, btnPrev, listTo, listFrom);
    })


    btnSplitTable.on('click', () => {

        const obj = {
            table_from: {
                id_order_table: tableIdFrom,
                order_details: listFromArr
            },
            table_to: {
                id_table: tableIdTo,
                order_details: listToArr
            }
        }


        $.ajax({
            headers: {
                'accept': 'application/json',
                'content-type': 'application/json',
            },
            type: 'POST',
            url: '/api/split-order',
            data: JSON.stringify(obj)
        })
            .done((data) => {
                $('#modalSplitTable').modal('hide');
                location.reload();
            })
            .fail((jqXHR) => {
                let str = '';

                if (jqXHR.responseJSON && jqXHR.responseJSON.errors) {
                    const errors = jqXHR.responseJSON.errors;
                    for (const field in errors) {
                        if (errors.hasOwnProperty(field)) {
                            const fieldErrors = errors[field];

                            for (let i = 0; i < fieldErrors.length; i++) {
                                str += `<label class="error">${fieldErrors[i]}</label>`;
                            }
                        }
                    }
                } else if (jqXHR.responseJSON && jqXHR.responseJSON.error) {
                    // Xử lý lỗi phản hồi chung
                    str = `<label class="error">${jqXHR.responseJSON.error}</label>`;
                } else {
                    // Xử lý lỗi mặc định
                    str = 'Có lỗi xảy ra.';
                }

                $("#errorContainerSplitTable").html(str);
                $("#errorContainerSplitTable").show();
            });
    })

    const loadProductListFrom = () => {
        listFromArr.forEach(item => {
            const str = renderProductList(item);
            listFrom.append(str)
        })
    }

    loadProductListFrom();
}
