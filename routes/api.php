<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Order\OrderController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/split-order', [OrderController::class, 'splitTableAndSplitOrder'])->name('order.split_table');
Route::get('/order-table-render', [OrderController::class, 'getAllOrderTableRender'])->name('orderTable.Render');

Route::get('/get-all-order-details/{orderId}', [OrderController::class, 'getAllOrderDetailsByOrderTableId'])->name('order.get_all_order_details_by_order_id');

Route::get('/get-all-order-details-exist/{orderId}', [OrderController::class, 'getAllOrderDetailsByOrderTableIdExist'])->name('order.get_all_order_details_by_order_id-exist');

Route::post('/split-order-exist-view', [OrderController::class, 'splitOrderExist'])->name('split-order-exist-api');
Route::get('/get-all-order-details-bill/{orderId}', [OrderController::class , 'getAllOrderDetailsByBillId'])->name('order.get-all-order-detail-in-order-table-id');


Route::get('get-all-order-detail-in-order-table/{orderTableId}', [OrderController::class , 'getAllOrderDetailInOrderTableId'])->name('get-all-order-detail-in-order-table');
Route::post('/split-bill-in-order-table' , [OrderController::class , 'splitBillInOrder'])->name('order.split_bill');

