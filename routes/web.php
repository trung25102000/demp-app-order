<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\Invoice\InvoiceController;
use App\Http\Controllers\Order\CustomerController;
use App\Http\Controllers\Order\OrderController;
use App\Http\Controllers\Order\ProductController;
use App\Http\Controllers\PdfController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
//route ở user
Route::get('dashboard', [AuthController::class, 'orders.dashboard']);
Route::get('login', [AuthController::class, 'index'])->name('users.login');
Route::post('custom-login', [AuthController::class, 'customLogin'])->name('users.login_custom');
Route::get('registration', [AuthController::class, 'registration'])->name('users.register');
Route::get('user', [AuthController::class, 'user'])->name('user');
Route::post('custom-registration', [AuthController::class, 'customRegistration'])->name('users.register_custom');
Route::get('sign-out', [AuthController::class, 'signOut'])->name('users.sign_out');

//route ở index
Route::get('search', [OrderController::class, 'search'])->name('table.search');
Route::get('/', [OrderController::class, 'index'])->name('orders.home');
Route::get('/getTableAdd', [OrderController::class, 'getTableAdd'])->name('orders.getTableAdd');

//route thêm order
Route::get('create-order', [OrderController::class, 'create'])->name('orders.create_order');
Route::post('store-order', [OrderController::class, 'storeOrder'])->name('orders.store_order');

//route xử lý ở list-order
Route::get('list-order', [OrderController::class, 'listOrder'])->name('orders.list_order');
Route::post('add-table', [OrderController::class, 'addTable'])->name('orders.add_table');
Route::get('getAllOrder' ,[ OrderController::class , 'getAllOrder' ]) ->name('get.AllOrder');
Route::get('/split-bill/{id}' ,[OrderController::class ,'splitBillAndSplitorderDetail'])->name('split-bill');

Route::get('/split-bill-exist/{id}' ,[OrderController::class ,'splitBillAndSplitorderDetailExist'])->name('split-order-exist');
Route::get('search-order', [OrderController::class, 'searchInListOrder'])->name('orders.search_order');
Route::post('move-to-kitchen/{id}', [OrderController::class, 'moveToKitchen'])->name('orders.kitchen');
Route::get('deleteOrder/{id}', [OrderController::class, 'deleteOrder'])->name('orders.delete');
Route::get('getCountItemsInListOrder', [OrderController::class, 'getCountItemsInListOrder'])->name('orders.getCountItemsInListOrder');


//route xử lý chi tiết order
Route::get('details/{id}', [OrderController::class, 'orderDetails'])->name('orders.details');
Route::delete('details/{id}', [OrderController::class, 'deleteDetail'])->name('detail.delete');
Route::post('finish/{id}', [OrderController::class, 'finishOrder'])->name('orders.finish');
Route::post('save-detail/{id}', [OrderController::class, 'saveDetail'])->name('detail.save');
Route::post('cancel-order/{id}', [OrderController::class, 'cancelOrder'])->name('orders.cancel');
Route::get('get-all-detail', [OrderController::class, 'getAllDetail']);

// Route search món ăn
Route::post('/products/search', [ProductController::class, 'search']);
Route::get('/products', [ProductController::class, 'index']);
Route::get('edit-order/{id}', [OrderController::class, 'editOrder'])->name('orders.edit_order');
Route::get('/get-detail/{id}', [OrderController::class, 'getDetails'])->name('detail.get_detail');
Route::post('/add-detail/{orderId}', [OrderController::class, 'addDetailInEditOrder'])->name('detail.add_detail');
Route::post('/add-other-detail/{orderId}', [OrderController::class, 'addOtherDetail'])->name('detail.add_other_detail');
Route::post('save-order/{orderTableId}', [OrderController::class,'saveOrderTable'])->name('orders.save_order');
Route::get('get-discount-amount/{voucherId}', [OrderController::class, 'getDiscountAmount'])->name('orders.get_discount_amount');

// Route search tên/sdt của khách hàng
Route::get('/create-order/search', [CustomerController::class, 'searchCustomer']);
Route::get('/filter', [OrderController::class, 'filterProductByCategories'])->name('products.filter');
Route::get('/best-seller', [OrderController::class, 'bestSellerOfProduct'])->name('products.best_seller');
Route::post('/update-quantity/{orderTableId}', [OrderController::class, 'updateQuantity'])->name('orders.update_quantity_product');
Route::get('/get-detail-in-edit/{orderTableId}', [OrderController::class, 'getOrderDetailInEdit'])->name('order_detail_in_edit');

// Route trang tính tiền
Route::get('payment-list', [OrderController::class, 'paymentList'])->name('orders.payment_list');
Route::get('payment-detail/{id}', [InvoiceController::class, 'paymentDetail'])->name('orders.payment_detail');
Route::post('/updateVoucher', [OrderController::class, 'updateVoucher'])->name('invoice.updateVoucher');
Route::post('set-is-read-noti',[OrderController::class, 'setIsReadNoti'])->name('setIsReadNoti');
Route::get('pdf', [PdfController::class, 'index'])->name('index.payment');
Route::POST('/payment-detail-order', [OrderController::class, 'getOrderAddBill'])->name('detail.getOrderAddBill');
Route::POST('/getDataTemp', [OrderController::class, 'getDataTemp'])->name('detail.getDataTemp');
Route::POST('/payment', [OrderController::class, 'payment'])->name('invoice.payment');
//Route để in phiếu tạm tính
Route::get('print-provisional/{id}', [InvoiceController::class, 'printProvisional'])->name('orders.print_provisional');
// Route để lưu hoá đơn, không in
Route::post('save-payment/{id}',[InvoiceController::class, 'savePayment'])->name('orders.save_payment');
//Route update voucher_id cho orderTable và invoice
Route::post('update-voucher-in-invoice/{id}',[InvoiceController::class, 'updateVoucherInInvoice'])->name('orders.update_voucher');

//route tra mon
Route::get('/searchReimburse', [OrderController::class, 'searchReimburse'])->name('order.searchReimburse');
Route::get('/reimburseForTable/{id}', [OrderController::class, 'reimburseForTable'])->name('order.reimburseForTable');
Route::get('/reimbursed', [OrderController::class, 'reimbursed'])->name('orders.reimbursed');
Route::get('/reimburseForOrder/{id}', [OrderController::class, 'reimburseForOrder'])->name('order.reimburseForOrder');
Route::get('/reimburseForTableByOrder/{id}', [OrderController::class, 'reimburseForTableByOrder'])->name('order.reimburseForTableByOrder');
Route::get('get-data-to-pusher', [OrderController::class, 'getDataToPusher'])->name('get_data_to_pusher');
Route::get('/getItemCount', [OrderController::class, 'getItemCount'])->name('order.getItemCount');
Route::post('reimbursed-many-table', [OrderController::class, 'reimbursedManyTable'])->name('order.reimburse_many_table');
Route::post('reimbursed-many-order', [OrderController::class, 'reimbursedManyOrder'])->name('order.reimburse_many_order');
Route::post('reimbursed-many-product', [OrderController::class, 'reimbursedManyProduct'])->name('order.reimburse_many_product');
Route::get('/get-reimburse-data', [OrderController::class, 'reimburseData'])->name('orders.reimburse_data');
Route::get('/get-category-list/{category}', [OrderController::class, 'searchCategory'])->name('orders.get_category_list');
Route::get('reimburse', [OrderController::class, 'reimburse'])->name('orders.reimburse');
Route::post('cancel-detail', [OrderController::class, 'cancelDetail'])->name('detail.cancel');
Route::get('/getItemCountTable', [OrderController::class, 'getItemCountTable'])->name('order.getItemCountTable');


