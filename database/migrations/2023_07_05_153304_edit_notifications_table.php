<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('notifications', function (Blueprint $table) {
            $table->foreignIdFor(\App\Models\OrderDetail::class)->nullable()->change();
            $table->string('noti_content')->nullable()->change();
            $table->foreignIdFor(\App\Models\Table::class)->nullable();
            $table->foreignIdFor(\App\Models\Order::class)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('notifications', function (Blueprint $table) {
            $table->dropColumn('order_detail_id');
        });
    }
};
