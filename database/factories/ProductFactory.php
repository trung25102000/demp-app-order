<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Product;
use App\Models\Unit;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\WithFaker;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{

    use WithFaker;

    protected $model=Product::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $unit=Unit::all()->random();
        $category=Category::all()->random();
        return [
            'category_id'=>$category->id,
            'unit_id'=>$unit->id,
            'product_name'=>$this->faker->name(),
            'product_price'=>$this->faker->numberBetween(200000,500000),
            'product_image'=>$this->faker->image('public/order/images', 640, 480, 'food', true),
            'product_key'=>'HDSKOQ',
            'product_description'=>$this->faker->text(),
            'product_available'=>$this->faker->boolean,
            ];
    }
}
