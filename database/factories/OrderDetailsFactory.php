<?php

namespace Database\Factories;

use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\WithFaker;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\OrderDetail>
 */
class OrderDetailsFactory extends Factory
{
    use WithFaker;
    protected $model=OrderDetail::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $notes=['không cay','Ít cay','nhạt'];
        $product=Product::all();
        $order=Order::all();
        return [
            'product_id'=>$this->faker->randomElement($product)->id,
            'note'=>$this->faker->randomElement($notes),
            'quantity'=>$this->faker->numberBetween(1,6),
            'order_id'=>$this->faker->randomElement($order)->id
        ];
    }
}
