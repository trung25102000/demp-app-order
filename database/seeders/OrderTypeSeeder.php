<?php

namespace Database\Seeders;

use App\Models\OrderType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class OrderTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        // Xóa dữ liệu cũ trong bảng order_types (nếu cần)
//        OrderType::truncate();

        // Tạo dữ liệu mẫu cho bảng order_types
        // Ví dụ:
        OrderType::create([
            'order_type_name' => 'Tại chỗ',
        ]);

        OrderType::create([
            'order_type_name' => 'Mang về',
        ]);

        OrderType::create([
            'order_type_name' => 'Đặt trước',
        ]);

        OrderType::create([
            'order_type_name' => 'Giao tận nơi',
        ]);
    }
}
