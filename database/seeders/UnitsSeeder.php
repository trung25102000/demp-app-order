<?php

namespace Database\Seeders;

use App\Models\Unit;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UnitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        // Xóa dữ liệu cũ trong bảng units (nếu cần)
//        Unit::truncate();

        // Tạo dữ liệu mẫu cho bảng units
        Unit::create([
            'unit_name' => 'Đĩa',
        ]);

        Unit::create([
            'unit_name' => 'Bát',
        ]);

        Unit::create([
            'unit_name' => 'Thùng',
        ]);

        Unit::create([
            'unit_name' => 'Chai',
        ]);

        Unit::create([
            'unit_name' => 'Nồi',
        ]);

        Unit::create([
            'unit_name' => 'Con',
        ]);

        Unit::create([
            'unit_name' => 'Lon',
        ]);

        Unit::create([
            'unit_name' => 'Ly',
        ]);

        Unit::create([
            'unit_name' => 'Lạng',
        ]);
    }
}
