<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Product;
use App\Models\TableArea;
use App\Models\Unit;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this::call([
            CategorySeeder::class,
            UnitsSeeder::class,
            UsersSeeder::class,
            OrderTypeSeeder::class,
            TableAreaSeeder::class,
            TableSeeder::class,
            ProductSeeder::class,
            VoucherSeeder::class,

        ]);
    }
}
