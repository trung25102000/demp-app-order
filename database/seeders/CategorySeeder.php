<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // Xóa dữ liệu cũ trong bảng categories (nếu cần)
//        Category::truncate();

        // Tạo dữ liệu mẫu cho bảng categories
        // Ví dụ:
        Category::create([
            'id' => 2,
            'category_name' => 'Món ăn',
        ]);

        Category::create([
            'id' => 3,
            'category_name' => 'Đồ uống',
        ]);

        Category::create([
            'id' => 4,
            'category_name' => 'Khác',
        ]);


        // Thêm các dòng dữ liệu khác nếu cần thiết
    }
}
