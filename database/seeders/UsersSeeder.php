<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        // Xóa dữ liệu cũ trong bảng users (nếu cần)
//        User::truncate();

        // Tạo dữ liệu mẫu cho bảng users
        // Ví dụ:
        User::create([
            'user_name' => 'Hoàng Quốc Trung',
            'user_email' => 'trungquoc2510@gmail.com',
            'user_birthday' => '2013-06-06',
            'user_address' => 'Huế',
            'user_phone' => '09.2392.2836',
            'gender' => 1,
            'password' => Hash::make('trungquoc'),
        ]);

        User::create([
            'user_name' => 'Đỗ Minh Phước',
            'user_email' => 'minhphuoc2402@gmail.com',
            'user_birthday' => '2015-06-04',
            'user_address' => 'Huế',
            'user_phone' => '05.6768.9328',
            'gender' => 1,
            'password' => Hash::make('phuocdo'),
        ]);

        User::create([
            'user_name' => 'Lê Bá Lộc',
            'user_email' => 'baloc2402@gmail.com',
            'user_birthday' => '2014-06-04',
            'user_address' => 'Huế',
            'user_phone' => '05.6768.2932',
            'gender' => 1,
            'password' => Hash::make('baloc'),
        ]);

        User::create([
            'user_name' => 'Thắng Lê',
            'user_email' => 'thanglee22@gmail.com',
            'user_birthday' => '2017-06-09',
            'user_address' => 'Huế',
            'user_phone' => '08.2422.5273',
            'gender' => 1,
            'password' => Hash::make('Thangle'),
        ]);

        // Thêm các dòng dữ liệu khác nếu cần thiết
    }
}
