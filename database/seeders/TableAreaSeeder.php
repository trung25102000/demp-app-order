<?php

namespace Database\Seeders;

use App\Models\TableArea;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TableAreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        // Xóa dữ liệu cũ trong bảng tables (nếu cần)
//        TableArea::truncate();

        $data = [
            [
                'table_floor' => 1,
                'table_area_name' => 'Tiền sảnh A',
            ],
            [
                'table_floor' => 1,
                'table_area_name' => 'Ngoại sảnh A',
            ],
            [
                'table_floor' => 1,
                'table_area_name' => 'Ban Công A',
            ],
            [
                'table_floor' => 2,
                'table_area_name' => 'Tiền sảnh B',
            ],
            [
                'table_floor' => 2,
                'table_area_name' => 'Ngoài ảnh B',
            ],
            [
                'table_floor' => 2,
                'table_area_name' => 'Ban Công B',
            ],
            [
                'table_floor' => 3,
                'table_area_name' => 'Tiền sảnh C',
            ],
            [
                'table_floor' => 3,
                'table_area_name' => 'Ngoại sảnh C',
            ],
            [
                'table_floor' => 3,
                'table_area_name' => 'Ban Công C',
            ],
        ];

        // Insert dữ liệu vào bảng table_area
        foreach ($data as $item) {
            TableArea::create($item);
        }
    }
}
