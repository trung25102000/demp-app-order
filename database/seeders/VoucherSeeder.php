<?php

namespace Database\Seeders;

use App\Models\Voucher;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class VoucherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        $vouchers = [
            [
                'id' => 1,
                'voucher_key' => 'GT25',
                'voucher_description' => 'Giảm giá 10% cho các hóa đơn',
                'voucher_expiration_date' => '2023-06-12',
                'created_at' => now(),
                'updated_at' => now(),
                'discount_amount' => 0.1,
                'voucher_start_date' => '2023-06-12',
                'deleted_at' => null,
                'voucher_name' => 'Giảm giá ngày 25',
            ],
            [
                'id' => 2,
                'voucher_key' => 'QK3004',
                'voucher_description' => 'Giảm 15% cho các hóa đơn',
                'voucher_expiration_date' => '2023-06-12',
                'created_at' => now(),
                'updated_at' => now(),
                'discount_amount' => 0.15,
                'voucher_start_date' => '2023-06-12',
                'deleted_at' => null,
                'voucher_name' => 'Giảm giá ngày quốc khánh',
            ],
            [
                'id' => 3,
                'voucher_key' => 'TK23vouche',
                'voucher_description' => 'Giảm 20% cho các hóa đơn',
                'voucher_expiration_date' => '2023-06-12',
                'created_at' => now(),
                'updated_at' => now(),
                'discount_amount' => 0.2,
                'voucher_start_date' => '2023-06-12',
                'deleted_at' => null,
                'voucher_name' => 'Giảm giá ngày 24',
            ],
        ];

        foreach ($vouchers as $voucher) {
            Voucher::create($voucher);
        }
    }
}
