<?php

namespace Database\Seeders;

use App\Models\Table;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        // Xóa dữ liệu cũ trong bảng tables (nếu cần)
//        Table::truncate();

        $commonTableName = 1; // Giá trị ban đầu của table_name

        $tableAreas = [1,2,3,4,5,6,7,8,9]; // Các ID của các tầng cần thêm bàn

        foreach ($tableAreas as $tableAreaId) {
            for ($i = 1; $i <= 8; $i++) {
                Table::create([
                    'table_area_id' => $tableAreaId,
                    'table_name' => $commonTableName,
                    'created_at' => now(),
                    'updated_at' => now(),
                    'status' => 1,
                    'deleted_at' => null,
                ]);

                $commonTableName++;
            }
        }
    }
}
