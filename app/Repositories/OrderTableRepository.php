<?php

namespace App\Repositories;

use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderTable;
use App\Models\Table;
use Illuminate\Support\Facades\DB;

class OrderTableRepository
{
    protected $orderTable;

    /**
     * @param $orderTable
     */
    public function __construct(OrderTable $orderTable)
    {
        $this->orderTable = $orderTable;
    }

    public function getAllOrderTableRender()
    {
        return $this->orderTable::all();
    }

    public function getAllOrderTableStatusNotPayment()
    {
        return $this->orderTable::where(function ($query) {
            $query->orWhereDoesntHave('invoice')
                ->orWhereHas('invoice', function ($query) {
                    $query->where('invoice_status_id', 0);
                });
        })->get();
    }

    public function getById($id)
    {
        return $this->orderTable->where('id', $id)->first();
    }

    /**
     * Lấy danh sách các OrderTable dựa trên trạng thái và phân trang
     * @param $status
     * @param $perPage
     * @return OrderTable
     */
    public function getAllOder($perPage)
    {
        $orderTables= $this->orderTable
            ->with('order', 'table', 'customer', 'user') // Load các bảng liên quan
            ->orderBy('created_at', "DESC")
            ->paginate($perPage);
        return $orderTables;
    }

    public function getAllOderTable()
    {
        return $this->orderTable
            ->with('order', 'table', 'customer', 'user') // Load các bảng liên quan
            ->orderBy('created_at', "DESC");
    }

    public function getAllOrderDetailsInOrder()
    {
        $orderTable = OrderTable::find(1);
        $tablaSplitFrom = Table::find(1);
        return [$orderTable, $tablaSplitFrom];
    }

    public function findById($id)
    {
        $orderTable = $this->orderTable::find($id);
        return $orderTable;
    }

    public function getOrderDetailInOrderTable()
    {
        $orderDetail = OrderDetail::get();
        return $orderDetail;
    }


    public function getAllTableStatusOnline()
    {
        $tableStatus = Table::get();
        return $tableStatus;
    }

    public function getAllOrderDetailInOrder()
    {
        $orderDetailInOrder = OrderDetail::get();
        return $orderDetailInOrder;
    }

    public function getByTableId(int $tableId)
    {
        return OrderDetail::where('order_table_id', $tableId)->get();
    }


    public function deleteByOrderTableId(int $fromTableId)
    {
        OrderDetail::where('order_table_id', $fromTableId)->delete();
    }

    public function createOrderDetail($data)
    {
        return OrderDetail::create($data);
    }

    /**
     * @param $orderid
     * @return void
     */
    public function getItem($orderid)
    {
        return OrderTable::where('order_id', $orderid)->first();
    }

    public function createOrderTable($data)
    {
        return $this->orderTable->create($data);
    }

    public function createOrderDetailByOrderTable($orderTableID, $data)
    {
        $orderTable = $this->orderTable->findOrFail($orderTableID);
        return $orderTable->orderDetail()->createMany($data);
    }
    public function UpdateOrderDetailByOrderTable($orderTableID, $data)
    {
        $orderTable = $this->orderTable->findOrFail($orderTableID);

        // Xóa các order detail cũ
        $orderTable->orderDetail()->delete();

        // Tạo mới các order detail với dữ liệu mới
        $orderTable->orderDetail()->createMany($data);
    }

    /**
     * Sửa order
     * @param $orderTableId
     * @param $userId
     * @param $customerId
     * @param $countCustomer
     * @param $typeOrder
     * @param $tableId
     * @param $orderItems
     * @param $voucherId
     * @return void
     */
    public function saveOrderTable($orderTableId, $userId, $customerId, $countCustomer, $typeOrder, $tableId, $orderItems, $voucherId)
    {
        $orderTable = $this->orderTable->find($orderTableId);
        $table = $orderTable->table;
        $tableNew = Table::find($tableId);
        if ($orderTable) {
            $orderTable->user_id = $userId;
            $orderTable->customer_id = $customerId;
            $orderTable->count_of_customer = $countCustomer;
            $orderTable->order->order_type_id = $typeOrder;
            if ($orderTable->table_id != $tableId) {
                $table->status = 1;
                $table->save();
            }
            if ($tableNew) {
                $tableNew->status = 3;
                $orderTable->table_id = $tableId;
                $tableNew->save();
            }
            $orderTable->voucher_id = $voucherId;
            $details = $orderTable->orderDetail;

            foreach ($orderItems as $item) {
                if ($details->contains('product_id', $item['id'] ?? '') && $item['status']!=3 && $item['status']!=1) {

                    DB::table('order_details')
                        ->where('order_table_id', $orderTableId)
                        ->where('product_id', $item['id'])
                        ->orWhere('order_detail_other_name', $item['product_name'])
                        ->update([
                            'order_detail_quantity' => $item['quantity'],
                            'order_detail_status' => $item['status'],
                        ]);
                }elseif($item['status']!=3){
                    DB::table('order_details')->insert([
                        'product_id' => $item['id'] ?? null,
                        'order_table_id' => $orderTableId,
                        'order_detail_price' => $item['product_price'],
                        'order_detail_quantity' => $item['quantity'],
                        'order_detail_status' => 1,
                        'order_detail_other_name' => $item['product_name'],
                        'order_detail_other_unit_name' => $item['unit']['unit_name'],
                    ]);
                }
            }
            $productIds = collect($orderItems)->pluck('id')->toArray();
            $productsToDelete = $details->whereNotIn('product_id', $productIds);
            foreach ($productsToDelete as $product) {
                $product->delete();
            }
        }
        $orderTable->save();
    }

    public function updateQuantity($productId, $quantity, $orderTableId)
    {
        $orderTable = $this->orderTable->find($orderTableId);
        $orderDetail = $orderTable->orderDetail()->where('product_id', $productId)->first();
        if ($orderDetail) {
            $orderDetail->order_detail_quantity = $quantity;
        }
        $orderDetail->save();
        return true;
    }

    public function create($data)
    {
        return $this->orderTable->create($data);
    }

    public function getOneLineOrderTable($orderTableId)
    {
        return $this->orderTable::where('id', $orderTableId)->get();
    }

    /**
     * Lấy danh sách orderTable theo status
     * @param $status
     * @return mixed
     */
    public function getOrderTableByStatus($status)
    {
        return $this->orderTable::where('status',$status)->get();
    }

    /**
     * Xóa Ordertable với các trương hợp ghép bàn
     * @param $OrdeTable
     * @return bool
     */
    public function deleteOrderTable($OrdeTable)
    {
        if ($OrdeTable) {
            //Lấy danh sách các orderTable có từng tồn tại order_id chung với $order tìm được hay ko(xử lý trường hợp ghép bàn và order
            $existingOrderTables = OrderTable::where('order_id', $OrdeTable->order_id)
                ->where('id', '!=', $OrdeTable->id)
                ->get();

            if ($existingOrderTables->count() > 0 && $OrdeTable->display_order==1) {
                $OrdeTable->table->status = 1;
                $OrdeTable->table->save();
                // Xóa đơn hàng
                $OrdeTable->delete();
                foreach ($existingOrderTables as $existingOrderTable) {
                    if ($existingOrderTable->status == 6) {
                        $existingOrderTable->table->status = 1;
                        $existingOrderTable->table->save();
                        $existingOrderTable->delete();
                    } elseif ($existingOrderTable->status == 5) {
                        $order = new Order();
                        $order->order_type_id = $existingOrderTable->order->order_type_id;
                        $order->order_create_time = now();
                        $order->order_accept_time = now();
                        $order->save();
                        if ($existingOrderTable->orderDetail->count() > 0) {
                            $existingOrderTable->status = 2;
                        } else {
                            $existingOrderTable->status = 1;
                        }
                        $existingOrderTable->display_order=1;
                        $existingOrderTable->order_id = $order->id;
                        $existingOrderTable->save();
                    }
                }
                return true;
            } else {
                $OrdeTable->table->status = 1;
                $OrdeTable->table->save();
                // Xóa đơn hàng
                $OrdeTable->delete();
                // Thực hiện các công việc khác sau khi xóa đơn hàng

                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * lấy danh sách orderTable để ghép bill
     * @return void
     */
    public function getOrderTableToAddBill($orderTableId)
    {
        $orderTable = OrderTable::whereIn('status', [2, 5])
            ->whereHas('orderDetail', function ($query) {
                $query->where('order_detail_status', 3);
            })
            ->where('id','!=',$orderTableId)
            ->get();
        return $orderTable;

    }
}
