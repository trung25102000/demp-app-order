<?php

namespace App\Repositories;

use App\Models\OrderDetail;
use App\Models\OrderTable;

class OrderDetailRepository
{
    protected $orderDetail;
    protected $orderTable;

    /**
     * @param $orderTable
     */
    public function __construct(OrderDetail $orderDetail, OrderTable $orderTable)
    {
        $this->orderDetail = $orderDetail;
        $this->orderTable = $orderTable;
    }
    public function getOrderDetailInEdit($orderTableId)
    {
        $orderTable = $this->orderTable->where('id', $orderTableId)->first();

        if ($orderTable) {
            $details = $orderTable->orderDetail()
            ->with('product.unit')
            ->get();
            return $this->FormatCode($details);

        }
        return null;
    }
    public function FormatCode($details)
    {
        $detailformated = [];
        foreach ($details as $data) {
            if ($data->product) {
                $result = [
                    'category_id' => $data->product->category_id,
                    'id' => $data->product->id,
                    'product_available' => $data->product->product_available,
                    'product_description' => $data->product->product_description,
                    'product_image' => $data->product->product_image,
                    'product_key' => $data->product->product_key,
                    'product_name' => $data->product->product_name,
                    'product_price' => $data->product->product_price,
                    'quantity' => $data->order_detail_quantity,
                    'unit' => $data->product->unit,
                    'unit_id' => $data->product->unit_id,
                    'note' => $data->order_detail_note,
                    'status'=>$data->order_detail_status,
                ];
            } else {
                $result = [
                    'category_id' => null,
                    'id' => null,
                    'product_available' => null,
                    'product_description' => null,
                    'product_image' => null,
                    'product_key' => null,
                    'product_name' => $data->order_detail_other_name,
                    'product_price' => $data->order_detail_price,
                    'quantity' => $data->order_detail_quantity,
                    'unit' => [
                        'unit_name' => $data->order_detail_other_unit_name,
                    ],
                    'unit_id' => null,
                    'note' => $data->order_detail_note,
                    'status'=>$data->order_detail_status,
                ];
            }

            array_push($detailformated, $result);
        }
        return response()->json($detailformated, 200);
    }
    public function getAllDetail()
    {
        return $this->orderDetail->all();
    }
    public function findById($id)
    {
        return $this->orderDetail::findOrFail($id);
    }
    public function getProductInOrderDetail($orderTableId)
    {
        return $this->orderDetail::with('product')
            ->where('order_table_id', $orderTableId)
            ->get();
    }

    public function getAllOrderDetailInOrderTableId($orderTableId)
    {
        return $this->orderDetail::where('order_table_id', $orderTableId)->get();
    }

    /**
     * Lấy danh sách các món thuộc loại hình bar or bếp theo order_id
     * @param $orderID
     * @param $selectedValue
     * @return mixed
     */
    public function getOrderDetailByStatusAndOrderTableId($orderID,$selectedValue)
    {
        $orderDetails = $this->orderDetail::whereHas('orderTable', function ($query) use ($orderID) {
            $query->whereHas('order', function ($query) use ($orderID) {
                $query->where('order_id', $orderID);
            });
        })
            ->where('order_detail_status',2)
            ->whereHas('product', function ($query) use ($selectedValue) {
                $query->where('category_id', $selectedValue);
            })
            ->get();
        return $orderDetails;
    }

    /**
     * Lấy danh sách orderDetails theo mã orderTableId và trạng thái của món ăn đó
     * @param $status
     * @param $orderTableId
     * @return mixed
     */
    public function getOrderDetailByStatus($status,$orderTableId)
    {
        $orderDetails=$this->orderDetail::where('order_table_id',$orderTableId)
            ->where('order_detail_status',$status)
            ->get();
        return $orderDetails;
    }

    /**
     * Lấy danh sách các món thuộc loại hình bar or bếp theo table_id
     * @param $orderID
     * @param $selectedValue
     * @return mixed
     */
    public function getOrderDetailByStatusAndTableId($tableId,$selectedValue)
    {
        $orderDetails = $this->orderDetail::whereHas('orderTable', function ($query) use ($tableId) {
            $query->whereHas('table', function ($query) use ($tableId) {
                $query->where('table_id', $tableId);
            });
        })
            ->where('order_detail_status',2)
            ->whereHas('product', function ($query) use ($selectedValue) {
                $query->where('category_id', $selectedValue);
            })
            ->get();
        return $orderDetails;
    }
}
