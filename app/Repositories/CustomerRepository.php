<?php

namespace App\Repositories;

use App\Models\Customer;

class CustomerRepository
{
    protected $customer;

    /**
     * @param $orderTable
     */
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    public function create($data)
    {
        return $this->customer->create($data);
    }
    public function getCustomer(){
        return $this->customer->all();
    }
    public function searchCustomer($keyword)
    {
        $customersQuery = $this->customer::select('id', 'customer_name', 'customer_phone', 'customer_birthday', 'created_at');
        if (!empty($keyword)) {
            $customersQuery->where('customer_name', 'like', '%' . $keyword . '%')
                ->orWhere('customer_phone', 'like', '%' . $keyword . '%');
        }
        return $customersQuery->get();
    }
    public function getCustomerById($customer_id) 
    {
        return $this->customer::where('id', $customer_id)->value('customer_name');
    }
}
