<?php
namespace App\Repositories;

use App\Models\OrderTable;
use App\Models\Table;

class TableRepository
{
    protected $table;

    /**
     * Construct
     * @param $table
     */
    public function __construct(Table $table)
    {
        $this->table = $table;
    }

    /**
     * Lấy danh sách bàn cùng khu vực bàn chỉ định
     * @param $tableId
     * @return array
     */
    public function getTablesInSameArea($tableId)
    {
        $table = Table::with('tableArea')->find($tableId);

        if (!$table) {
            return [];
        }

        $tableAreaId = $table->tableArea->id;

        $tables = Table::whereHas('tableArea', function ($query) use ($tableAreaId) {
            $query->where('table_area_id', $tableAreaId);
        })->where('status',1)->get();

        return $tables;


    }
    public function getOneLineTable($tableId)
    {
        return $this->table::where('id', $tableId)->get();
    }
    /**
     * @param $perPage
     * @return mixed
     */
    public function getAll($perPage)
    {
        return $this->table->orderBy('table_name')->paginate($perPage);
    }

    public function getTableByOrderTable($tableId)
    {
        return $this->table->where('id', $tableId)->orderBy('table_name');
    }

    public function getById($id)
    {
        return $this->table->where('id', $id)->first();
    }

    public function getAllByStatus($status)
    {
        return $this->table
            ->where('status', $status)->get();
        $tables = $this->table::orderBy('table_name')->paginate($perPage);
        return $tables;
    }

    public function getAllTable($idTable){
        $tables = $this->table::find($idTable);
        return $tables;
    }
    /**
     * Tìm kiếm theo fillter và đổ danh sách ra
     * @param $searchValue1
     * @param $searchValue2
     * @return mixed
     */
    public function searchItems($searchString, $statusId, $AreaId, $perPage)
    {

        $query = Table::query();
        if (!empty($AreaId)) {
            $query->whereHas('tableArea', function ($query) use ($AreaId) {
                $query->where('id', $AreaId);
            });
        }
        if (!empty($searchString)) {
            $query->where(function ($query) use ($searchString) {
                $query->whereHas('tableArea', function ($query) use ($searchString) {
                    $query->where('table_floor', 'like', '%' . $searchString . '%')
                        ->orWhere('table_area_key', 'like', '%' . $searchString . '%')
                        ->orWhere('table_area_name', 'like', '%' . $searchString . '%');
                });
            });
        }
        if (!empty($statusId)) {
            $query->where('status', $statusId);
        }

        $query->orderBy('table_name');
        return $query->paginate($perPage);


    }

    public function exists($id)
    {
        return $this->table::where('id', $id)->exists();
    }


    /**
     * @param $tableid
     * @return void
     */
    public function changeStatusAddtable($tableid)
    {
        $table = Table::find($tableid);

        if (!$table) {
            // Bàn không tồn tại
            return ;
        }

        // Thay đổi trạng thái của bàn thành 4
        $table->status = 4;
        $table->save();

        return;
    }

    public function getListTableCanOrder()
    {
        return $this->table->where('status', 1)->get();
    }

    public function updateStatusTable($tableID, $status)
    {
        return $this->table->where('id', $tableID)->update(['status' => $status]);
    }

    /**
     * Lấy tất cả danh sách trả món theo table
     * @return mixed
     */
    public function getTableWithTable($searchString)
    {
        $tables = $this->table::whereHas('orderTable', function ($query) {
            $query->whereIn('status', [2,5])
                ->whereHas('orderDetail', function ($query) {
                    $query->where('order_detail_status', 2);
                })
            ->orderBy('updated_at');
        })
            ->whereIn('status',[3,2,4])
            ->get();
        return $tables;
    }

    /**
     * lấy bàn gốc ghép với bàn hiện tại
     * @param $tableId
     * @return void
     */
    public function getTableTheAdd($tableId)
    {
        $table=$this->getById($tableId);
        if($table && $table->status==4)
        {
            $orderId=$table->orderTable->first()->order_id;

            $orderTable=OrderTable::where('order_id',$orderId)
                ->where('display_order',1)
                ->first();

            $tableReponse= $orderTable->table ? $orderTable->table->tableArea->table_area_key.''.$orderTable->table->tableArea->table_floor .''.sprintf("%02d", $orderTable->table->table_name) : $orderTable->order->orderType->order_type_name;

            return $tableReponse;
        }
        return null;
    }
}
