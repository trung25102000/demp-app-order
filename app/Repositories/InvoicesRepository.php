<?php

namespace App\Repositories;


use App\Models\Invoice;
use App\Models\OrderTable;
use App\Models\Payment;
use App\Models\Table;
use App\Models\Order;
use Carbon\Carbon;

class InvoicesRepository
{
    protected $invoice;
    protected $payment;
    protected $orderTable;
    protected  $table;
    protected $order;
    public function __construct(Invoice $invoice, Payment $payment, OrderTable $orderTable, Table $table, Order $order)
    {
        $this->invoice = $invoice;
        $this->payment = $payment;
        $this->orderTable = $orderTable;
        $this->table = $table;
        $this->order = $order;
    }

    public function findById($id){
        $invoices = $this->invoice->findOrFail($id);
        return $invoices;
    }

    public function getInvoicesWithSpecificOrdertableid ($ordertableid){
        $invoicesTableId = $this->invoice::where('order_table_id' , $ordertableid)->first();
        return $invoicesTableId;
    }

    public function createInvoice($data)
    {
        return $this->invoice->create($data);
    }

    public function createPayment($data)
    {
        return $this->payment->create($data);
    }

    public function updateStatusInvoice($invoiceId, $status)
    {
        return $this->invoice::where('id', $invoiceId)->update(['invoice_status_id'=>$status]);
    }

    public function updateVoucherInInvoice($invoiceId, $voucherId)
    {
        return $this->invoice::where('id', $invoiceId)->update(['voucher_id'=>$voucherId['voucher_id']]);
    }

    public function updateVoucherInOrderTable($orderTableId, $voucherId)
    {
        return $this->orderTable::where('id', $orderTableId)->update(['voucher_id'=>$voucherId['voucher_id']]);
    }

    public function getOneObjectInvoice($invoiceId){
      return $this->invoice::where('id', $invoiceId)->get();
    }

    public function updateStatusOrderTable($orderTableId, $status)
    {
        return $this->orderTable::where('id', $orderTableId)->update(['status'=>$status]);
    }

    public function updateStatusTable($orderTableId, $status)
    {
        $tableId = $this->orderTable::where('id', $orderTableId)->value('table_id');
        if($tableId){
            $countTable = $this->orderTable::where('table_id', $tableId)->whereIn('status', [2,5,6])->count();
            if ($countTable == 1) {
                return $this->table::where('id', $tableId)->update(['status'=>$status]);
            }
        }
    }

    public function updateTimeInOrder($orderTableId)
    {
        $orderId = $this->orderTable::where('id', $orderTableId)->value('order_id');
        $order_finish_time = Carbon::now('Asia/Ho_Chi_Minh')->format("Y-m-d H:i:s");
        return $this->order::where('id', $orderId)->update(['order_finish_time'=>$order_finish_time]);
    }

}
