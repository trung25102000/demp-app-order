<?php

namespace App\Repositories;

use App\Http\Resources\ProductCollection;
use App\Models\Invoice;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderTable;
use App\Models\Product;
use App\Models\Table;
use App\Models\User;
use App\Models\Voucher;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Pusher\Pusher;
use Carbon\Carbon;
use Exception;

class OrderRepository
{
    protected $order;
    protected $orderDetail;
    protected $orderTable;
    protected $voucher;
    protected $user;

    /**
     * @param $order
     */
    public function __construct(Order $order, OrderDetail $orderDetail, OrderTable $orderTable, Voucher $voucher, User $user)
    {
        $this->order = $order;
        $this->orderDetail = $orderDetail;
        $this->orderTable = $orderTable;
        $this->voucher = $voucher;
        $this->user = $user;
    }

    public function getAllOrder($perPage)
    {
        return $this->order->paginate($perPage);
    }

    public function newPusher()
    {
        $options = array(
            'cluster' => 'mt1',
            'encrypted' => true
        );

        return new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );
    }

    /**
     * Lay orderTable tu id
     * @param $orderTableId
     * @return mixed
     */
    public function getOrderTable($orderTableId)
    {
        return $this->orderTable->find($orderTableId);
    }

    public function getOrderDetail($orderTableId)
    {
        $orderTable = $this->orderTable->where('id', $orderTableId)->first();

        if ($orderTable) {
            $details = $orderTable->orderDetail()
                ->with('product.unit', 'orderTable.table')
                ->get();
            return $details;
        }
        return null;
    }


    public function getAllOrderDetailsByOrderTableId($orderTableId)
    {
        $orderTable = $this->orderTable->where('id', $orderTableId)->first();

        if ($orderTable) {
            $orderDetails = $orderTable->orderDetail;

            if ($orderDetails) {
                foreach ($orderDetails as $item) {
                    $orderDetail = clone $item;
                    if ($orderDetail->product) {
                        $product_name = $orderDetail->product['product_name'];
                    } else {
                        $product_name = $orderDetail->order_detail_other_name;
                    }
                    $item->product_name = $product_name;
                }
            }

            return $orderDetails;
        }

        return null;
    }



    public function getAllOrderDetailsByBillTableId($orderTableId)
    {
        $orderTable = $this->orderTable->where('id', $orderTableId)->first();

        if ($orderTable) {
            $orderDetails = $orderTable->orderDetail()->where('order_detail_status', 3)->get();

            if ($orderDetails) {
                foreach ($orderDetails as $item) {
                    $orderDetail = clone $item;
                    if ($orderDetail->product) {
                        $product_name = $orderDetail->product['product_name'];
                    } else {
                        $product_name = $orderDetail->order_detail_other_name;
                    }
                    $item->product_name = $product_name;
                }
            }

            return $orderDetails;
        }

        return null;
    }

    public function getOneLineOrder($orderId)
    {
        return $this->order::where('id', $orderId)->get();
    }

    public function getOneLineVoucher($voucherId)
    {

        return $this->voucher::where('id', $voucherId)->get();
    }

    public function moveToKitchen($orderTableId)
    {
        $order = $this->orderTable->find($orderTableId);
        if ($order) {
            $order->status = 2;
            $order->order->order_accept_time = now();
            $order->order->save();
            foreach ($order->orderDetail as $item) {
                $item->order_detail_status = 2;
                $item->save();
            }
            $order->save();
            return true;
        }
        return false;
    }

    /**
     * Tìm kiếm theo fillter và gửi danh sách lấy được ra services
     * @param $searchString
     * @param $statusId
     * @param $perPage
     * @param $orderTypeId
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function searchInListOrder($searchString, $statusId, $perPage, $orderTypeId)
    {
        $query = OrderTable::query();
        if (!empty($statusId)) {
            $query->where('status', $statusId);
        }
        if (!empty($searchString)) {
            $query->where(function ($query) use ($searchString) {
                $query->where('id', 'like', '%' . $searchString . '%')
                    ->orWhereHas('table', function ($query) use ($searchString) {
                        $query->where('table_name', 'like', '%' . $searchString . '%');
                    });
            });
        }
        if (!empty($orderTypeId)) {
            $query->whereHas('order', function ($query) use ($orderTypeId) {
                $query->where('order_type_id', $orderTypeId);
            });
        }
        $query->whereNotIn('status', [3, 4, 6]);
        $query->orderByDesc(function ($query) {
            $query->select('order_create_time')
                ->from('orders')
                ->whereColumn('order_tables.order_id', 'orders.id');
        });
        return $query->paginate($perPage);
    }

    public function deleteDetail($detail_id)
    {
        $this->orderDetail::findOrFail($detail_id)->delete($detail_id);
        return response()->json(true, Response::HTTP_OK);
    }

    public function finishOrder($order_id)
    {
        $order = $this->order->find($order_id);
        if ($order && $order->invoice_status_id == 1) {
            $order->status_id = 3;
            $order->order_finish_time = now();
            $order->save();
            return true;
        }
        return response()->json(['error' => 'Không thể hoàn tất đơn hàng'], 400);
    }

    public function saveDetail($detail_id, $quantity = 1, $note)
    {
        $item = $this->orderDetail->find($detail_id);
        $item->order_detail_quantity = $quantity;
        $item->order_detail_note = $note;
        $item->save();
        return $item;
    }

    public function cancelOrder($order_id, $reason)
    {
        $order = Order::find($order_id);
        $orderTable = $order->orderTable;
        $table = $orderTable->table;
        if ($order) {
            $order->order_cancel_reason = $reason;
            $orderTable->status = 4;
            $table->status = 1;
            $orderTable->save();
            $table->save();
        }
        $order->save();

        return response()->json(['success' => 'Đã huỷ order'], 200);
    }

    public function getVoucher()
    {
        $voucher = $this->voucher->all();
        return $voucher;
    }

    public function getVoucherById($voucher_id)
    {
        return $this->voucher::where('id', $voucher_id)
            ->select('voucher_description', 'discount_amount')
            ->first();
    }

    public function addDetailInEditOrder($orderTableId, $productId, $orderDetailQuantity, $orderDetailPrice)
    {
        $orderTable = $this->orderTable->find($orderTableId);
        if ($orderTable) {
            $orderDetail = $orderTable->orderDetail()->where('product_id', $productId)->first();
            if ($orderDetail) {
                $orderDetail->order_detail_quantity += 1;
            } else {
                // Món chưa tồn tại, tạo mới
                $orderDetail = new OrderDetail();
                $orderDetail->product_id = $productId;
                $orderDetail->order_detail_price = $orderDetailPrice;
                $orderDetail->order_detail_quantity = $orderDetailQuantity;
                $orderDetail->order_table_id = $orderTable->id;
            }
            $orderDetail->save();
        }
        return $orderTable;
    }

    public function getUser()
    {
        $user = $this->user->all();
        return $user;
    }

    public function addOtherDetail($orderTableId, $orderOtherName, $orderOtherPrice, $orderOtherQuantity)
    {
        $orderTable = $this->orderTable->find($orderTableId);
        if ($orderTable) {
            $orderDetail = new OrderDetail();
            $orderDetail->order_detail_other_name = $orderOtherName;
            $orderDetail->order_detail_price = $orderOtherPrice;
            $orderDetail->order_detail_quantity = $orderOtherQuantity;
            $orderDetail->order_table_id = $orderTable->id;
            $orderDetail->save();
        }
        return $orderTable;
    }


    /**
     * Lấy tất cả món ăn được chuyển xuống bếp/bar
     * @return mixed
     */
    public function getAllOrderDetails($CategoryId, $searchString)
    {
        $orderDetails = $this->orderDetail::where('order_detail_status', 2)
            ->orderBy('updated_at')
            ->whereHas('orderTable', function ($query) {
                $query->whereIn('status', [2, 5]);
            })
            ->whereHas('product', function ($query) use ($CategoryId) {
                $query->where('category_id', $CategoryId);
            })
            ->whereHas('product', function ($query) use ($searchString) {
                $query->where('product_name', 'LIKE', '%' . $searchString . '%')
                    ->orwhere('product_key', 'LIKE', '%' . $searchString . '%');
            })
            ->get();
        return $orderDetails;
    }

    /**
     * Lấy tất cả danh sách order_table theo order_id
     * @return mixed
     */
    public function getOrderWithOrder($searchString)
    {
        $orders = $this->order::whereHas('orderTable', function ($query) {
            $query->whereIn('status', [2, 5])
                ->whereHas('orderDetail', function ($query) {
                    $query->where('order_detail_status', 2);
                });
        })
            ->orderBy('order_accept_time')
            ->get();
        return $orders;
    }


    /**
     * Lấy tất cả danh sách đồ uống trả xuống bar
     * @return mixed
     */
    public function getAllOrderDetailsForBar()
    {
        $orderDetails = $this->orderDetail::where('order_detail_status', 2)
            ->whereHas('orderTable', function ($query) {
                $query->where('status', 2)
                    ->whereHas('order', function ($query) {
                        $query->orderBy('order_create_time');
                    });
            })
            ->whereHas('product', function ($query) {
                $query->where('category_id', 3);
            })
            ->paginate(12);
        return $orderDetails;
    }

    /**
     * Lấy tất cả danh sách order_table theo order chuyển xuống bar
     * @return mixed
     */
    public function getOrderTableWithOrderForBar()
    {
        $orderTables = OrderTable::whereIn('order_id', function ($query) {
            $query->select('id')
                ->from('orders')
                ->where('status', 2);
        })
            ->whereHas('order', function ($query) {
                $query->orderBy('order_create_time');
            })
            ->whereHas('orderDetail.product', function ($query) {
                $query->where('category_id', 3);
            })->has('orderDetail')->paginate(12);


        return $orderTables;
    }

    /**
     * Lấy tất cả danh sách order_table theo table xuống bar
     * @return mixed
     */
    public function getOrderTableWithTableForBar()
    {
        $orderTables = OrderTable::whereIn('order_id', function ($query) {
            $query->select('id')
                ->from('orders')
                ->where('status', 2);
        })
            ->whereHas('order', function ($query) {
                $query->orderBy('order_create_time');
            })
            ->whereHas('orderDetail.product', function ($query) {
                $query->where('category_id', 3);
            })->has('orderDetail')->paginate(12);

        return $orderTables;
    }

    public function getDiscountAmount($voucherId)
    {
        $discount = $this->voucher->find($voucherId);
        return $discount->discount_amount;
    }

    public function create($data)
    {
        return $this->order->create($data);
    }


    /**
     * Lấy danh sách orderTable đang chế biến thuộc loại hình ăn tại chỗ
     * @return mixed
     */
    public function getPaymentListUsing($perPage)
    {
        $orderTable = OrderTable::whereIn('order_id', function ($query) {
            $query->select('id')
                ->from('orders')
                ->wherein('status', [2, 5]);
        })
            ->whereHas('order', function ($query) {
                $query->where('order_type_id', 1);
            })
            ->orderByDesc(function ($query) {
                $query->select('order_create_time')
                    ->from('orders')
                    ->whereColumn('order_tables.order_id', 'orders.id');
            })
            ->paginate($perPage);
        return $orderTable;
    }

    /**
     * Lấy danh sách orderTable đang chế biến thuộc loại hình giao hàng
     * @return mixed
     */
    public function getPaymentListShip($perPage)
    {
        $orderTable = OrderTable::whereIn('order_id', function ($query) {
            $query->select('id')
                ->from('orders')
                ->wherein('status', [2, 5]);
        })
            ->whereHas('order', function ($query) {
                $query->where('order_type_id', 4);
            })
            ->orderByDesc(function ($query) {
                $query->select('order_create_time')
                    ->from('orders')
                    ->whereColumn('order_tables.order_id', 'orders.id');
            })
            ->paginate($perPage);
        return $orderTable;
    }

    /**
     * Lấy danh sách orderTable đang chế biến thuộc loại hình đặt trước
     * @return mixed
     */
    public function getPaymentListReserve($perPage)
    {
        $orderTable = OrderTable::whereIn('order_id', function ($query) {
            $query->select('id')
                ->from('orders')
                ->wherein('status', [2, 5]);
        })
            ->whereHas('order', function ($query) {
                $query->where('order_type_id', 3);
            })
            ->orderByDesc(function ($query) {
                $query->select('order_create_time')
                    ->from('orders')
                    ->whereColumn('order_tables.order_id', 'orders.id');
            })
            ->paginate($perPage);
        return $orderTable;
    }

    /**
     * Lấy danh sách orderTable đang chế biến thuộc loại hình mang về
     * @return mixed
     */
    public function getPaymentListBringBack($perPage)
    {
        $orderTable = OrderTable::whereIn('order_id', function ($query) {
            $query->select('id')
                ->from('orders')
                ->wherein('status', [2, 5]);
        })
            ->whereHas('order', function ($query) {
                $query->where('order_type_id', 2);
            })
            ->orderByDesc(function ($query) {
                $query->select('order_create_time')
                    ->from('orders')
                    ->whereColumn('order_tables.order_id', 'orders.id');
            })
            ->paginate($perPage);
        return $orderTable;
    }

    public function filterProductByCategories($categoryId)
    {
        $product = Product::with('category', 'unit')
            ->where('category_id', 'like', "%" . $categoryId . "%")
            ->get();

        $productCollection = new ProductCollection($product);
        return response()->json(
            ['data' => $productCollection],
            Response::HTTP_OK
        );
    }

    public function bestSellerOfProduct()
    {
        $bestSellerItems = Product::with('orderDetail', 'unit')
            ->where('category_id', 2)
            ->join(DB::raw('(SELECT product_id, SUM(order_detail_quantity) AS total_quantity FROM order_details WHERE order_detail_status = 3 GROUP BY product_id) od'), 'products.id', '=', 'od.product_id')
            ->orderByDesc('od.total_quantity')
            ->limit(10)
            ->get();
        $productCollection = new ProductCollection($bestSellerItems);
        return response()->json(
            ['data' => $productCollection],
            Response::HTTP_OK
        );
    }

    /**
     * Lấy danh sách orderTable đã được tạo theo  loại hình và giá trị tìm kiếm và status của order đó.
     * @return mixed
     */
    public function getOrderListOrderType($searchValue, $statusId, $perPage, $orderTypeId)
    {
        $query = OrderTable::whereIn('order_id', function ($query) {
            $query->select('id')
                ->from('orders')
                ->where('status', '!=', 4);
        });

        if (!empty($statusId) && !empty($searchValue) && !empty($orderTypeId)) {
            $query->where(function ($query) use ($statusId, $searchValue, $orderTypeId) {
                $query->where('status', $statusId)
                    ->where(function ($query) use ($searchValue) {
                        $query->where('table_id', 'like', '%' . $searchValue . '%')
                            ->orWhere('id', 'like', '%' . $searchValue . '%');
                    })
                    ->whereHas('order', function ($query) use ($orderTypeId) {
                        $query->where('order_type_id', $orderTypeId);
                    });
            });
        } elseif (!empty($statusId) && !empty($orderTypeId)) {
            $query->where('status', $statusId)
                ->whereHas('order', function ($query) use ($orderTypeId) {
                    $query->where('order_type_id', $orderTypeId);
                });
        } elseif (!empty($statusId) && !empty($searchValue)) {
            $query->where('status', $statusId)
                ->where(function ($query) use ($searchValue) {
                    $query->where('table_id', 'like', '%' . $searchValue . '%')
                        ->orWhere('id', 'like', '%' . $searchValue . '%');
                });
        } elseif (!empty($searchValue)) {
            $query->where(function ($query) use ($searchValue) {
                $query->where('table_name', 'like', '%' . $searchValue . '%')
                    ->orWhere('id', 'like', '%' . $searchValue . '%');
            });
        }


        return $query->paginate($perPage);
    }

    /**
     * Lấy danh sách ordertable theo loại hình với status!=4
     * @param $perPage
     * @param $orderTypeId
     * @param $searchValue
     * @param $statusId
     * @return mixed
     */
    public function getOrderListOrderTypeById($orderTypeId)
    {
        $orderTable = OrderTable::whereIn('order_id', function ($query) {
            $query->select('id')
                ->from('orders')
                ->whereNotIn('status', [4, 3, 6]);
        })
            ->whereHas('order', function ($query) use ($orderTypeId) {
                $query->where('order_type_id', $orderTypeId);
            })
            ->orderByDesc(function ($query) {
                $query->select('order_create_time')
                    ->from('orders')
                    ->whereColumn('order_tables.order_id', 'orders.id');
            });
        return $orderTable;
    }

    public function getById($id)
    {
        return $this->order->findOrFail($id);
    }

    /**
     * Ghép bàn theo loại hình muốn ghép
     * @param $orderId
     * @param $idTables
     * @param $checkedValue
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function AddTable($orderId, $idTables, $checkedValue)
    {
        if ($checkedValue == 'false') {
            $orderTable = OrderTable::find($orderId);
            $i = 2;
            DB::beginTransaction();
            foreach ($idTables as $id) {
                $table = Table::find($id);

                if (!$table) {
                    // Bàn không tồn tại
                    return;
                }
                // Thay đổi trạng thái của bàn thành 4
                $table->status = 4;
                $table->save();
                $newOrderTable = new OrderTable();
                $newOrderTable->order_id = $orderTable->order_id;
                $newOrderTable->table_id = $id;
                $newOrderTable->customer_id = $orderTable->customer_id;
                $newOrderTable->user_id = $orderTable->user_id;
                $newOrderTable->status = 5;
                $newOrderTable->display_order = $i;
                $newOrderTable->save();
                $i = $i + 1;
            }
            DB::commit();
        } else {
            DB::beginTransaction();
            $orderTable = OrderTable::find($orderId);;
            $i = 2;
            foreach ($idTables as $id) {
                $table = Table::find($id);

                if (!$table) {
                    // Bàn không tồn tại
                    return;
                }
                // Thay đổi trạng thái của bàn thành 4
                $table->status = 4;
                $table->save();
                $newOrderTable = new OrderTable();
                $newOrderTable->order_id = $orderTable->order_id;
                $newOrderTable->table_id = $id;
                $newOrderTable->customer_id = $orderTable->customer_id;
                $newOrderTable->user_id = $orderTable->user_id;
                $newOrderTable->status = 6;
                $newOrderTable->display_order = $i;
                $orderDetails = $orderTable->orderDetail;
                foreach ($orderDetails as $orderDetail) {
                    DB::table('order_details')
                        ->where('order_table_id', $orderTable->id) // Điều kiện cập nhật (ví dụ: order_id = 1)
                        ->update([
                            'order_id' => $orderTable->order_id,
                            // Các cột khác mà bạn muốn cập nhật
                        ]);
                }
                $newOrderTable->save();

                $i = $i + 1;
            }
            DB::commit();
        }
    }


    public function update($order)
    {
        return $order->save();
    }

    /**
     * Tao hoa don
     * @param $id :id cua orderTable
     * @return void
     */
    public function createInvoice($invoice)
    {
        Invoice::create($invoice);
    }

    public function reimbursed($detailId)
    {
        $orderDetail = OrderDetail::find($detailId);
        $pusher = $this->newPusher();
        $orderDetail->order_detail_status = 3;
        $orderDetail->notification()->create([
            'order_detail_id' => $orderDetail->id,
            'status' => 1,
            'noti_content' => 'Món ' . $orderDetail->product->product_name . ($orderDetail->orderTable->table_name ? ' của bàn ' . $orderDetail->orderTable->table_name : '') . ' đã chế biến xong!',
        ]);

        $detail = OrderDetail::with('product', 'orderTable.table')->where('id', $orderDetail->id)->get();
        $productName = $detail->pluck('product.product_name');
        $tableName = $detail->pluck('orderTable.table.table_name');
        $currentTime = Carbon::now()->format('Y-m-d H:i:s');

        $data = [
            "tableName" => $tableName,
            "productName" => $productName,
            "notificationTime" => $currentTime,
        ];
        $pusher->trigger('NotifyEvent', 'Notify', $data);

        $orderDetail->save();
        return true;
    }

    public function getDataToPusher($orderTableId)
    {
        $orderTable = $this->orderTable->where('id', $orderTableId)->first();

        if ($orderTable) {
            $details = $orderTable->orderDetail;
            $table = $orderTable->table;
            $dataPush = [];
            foreach ($details as $item) {
                $data = [
                    'id' => $item->id ?? "",
                    'product_id' => $item->product ? $item->product->id : "",
                    'order_table_id' => $item->orderTable->id ?? "",
                    'order_id' => $item->orderTable->order_id ?? '',
                    'product_name' => $item->product ? $item->product->product_name : "Món khác",
                    'product_key' => $item->product ? $item->product->product_key : '',
                    'table_id' => $table->id ?? '',
                    'table_area_key' => $table->tableArea->table_area_key ?? "",
                    'table_floor' => $table->tableArea->table_floor ?? "",
                    'table_name' => $table->table_name ?? "",
                    'order_detail_other_name' => $item->order_detail_other_name ?? "",
                    'order_detail_quantity' => $item->order_detail_quantity ?? 0,
                    'order_detail_note' => $item->order_detail_note ?? "",
                    'table_name' => $item->orderTable->table ? $item->orderTable->table->table_name : $item->orderTable->order->orderType->order_type_name,
                    'created_at' => $item->created_at ?? "",
                    'updated_at' => $item->updated_at ?? "",
                    'order_detail_note' => $item->order_detail_note ?? "",
                ];
                array_push($dataPush, $data);
            }
            return $dataPush;
        }
        return null;
    }
    public  function reimbursedManyTable($dataTable)
    {
        try {
            foreach ($dataTable as $id) {
                $orderTables = OrderTable::where('table_id', $id)->get();
                DB::beginTransaction();
                foreach ($orderTables as $orderTable) {
                    $orderDetails = $orderTable->orderDetail;
                    foreach ($orderDetails as $orderDetail) {
                        $orderDetail->order_detail_status = 3;
                        $orderDetail->save();
                    }
                }
                $pusher = $this->newPusher();
                $table = Table::find($id);
                $table->notification()->create([
                    'table_id' => $id,
                    'status' => 1,
                    'noti_content' => 'Món của bàn ' . $table->table_name . ' đã chế biến xong !',
                ]);
                $table->save();
                $data = [
                    'tableName' => $table->table_name,
                    'notificationTime' => Carbon::now()->format('Y-m-d H:i:s'),
                ];
                $pusher->trigger('NotifyEvent', 'Notify', $data);
                DB::commit();
            }
            session()->flash('success', 'Các món của Bàn đã được trả !');
            return response()->json(true, Response::HTTP_OK);
        } catch (Exception $e) {
            DB::rollBack();
            session()->flash('error', 'Các món của Bàn chưa được trả !');
            return response()->json(false, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function reimbursedManyOrder($dataOrder)
    {
        try {
            foreach ($dataOrder as $id) {
                $orderTables = OrderTable::where('order_id', $id)->get();
                DB::beginTransaction();
                foreach ($orderTables as $orderTable) {
                    $orderDetails = $orderTable->orderDetail;
                    foreach ($orderDetails as $orderDetail) {
                        $orderDetail->order_detail_status = 3;
                        $orderDetail->save();
                    }
                }
                $pusher = $this->newPusher();

                $order = Order::find($id);
                $order->notification()->create([
                    'order_id' => $id,
                    'status' => 1,
                    'noti_content' => 'Món của bàn ' . $order->orderTable->table->table_name . ' đã chế biến xong !',
                ]);
                $order->save();
                $data = [
                    'tableName' => $order->orderTable->table->table_name,
                    'notificationTime' => Carbon::now()->format('Y-m-d H:i:s'),
                ];
                $pusher->trigger('NotifyEvent', 'Notify', $data);
                DB::commit();
            }
            session()->flash('success', 'Các món của Order đã được trả !');
            return response()->json(true, Response::HTTP_OK);
        } catch (Exception $e) {
            DB::rollBack();
            session()->flash('error', 'Các món của Order chưa được trả !');
            return response()->json(false, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function reimbursedManyProduct($dataProduct)
    {
        foreach ($dataProduct as $detailId) {
            $orderDetail = OrderDetail::find($detailId);
            $pusher = $this->newPusher();
            $orderDetail->order_detail_status = 3;
            $orderDetail->notification()->create([
                'order_detail_id' => $orderDetail->id,
                'status' => 1,
                'noti_content' => 'Món ' . $orderDetail->product->product_name . ($orderDetail->orderTable->table_name ? ' của bàn ' . $orderDetail->orderTable->table_name : '') . ' đã chế biến xong!',
            ]);

            $detail = OrderDetail::with('product', 'orderTable.table')->where('id', $orderDetail->id)->get();
            $productName = $detail->pluck('product.product_name');
            $tableName = $detail->pluck('orderTable.table.table_name');
            $currentTime = Carbon::now()->format('Y-m-d H:i:s');

            $data = [
                "tableName" => $tableName,
                "productName" => $productName,
                "notificationTime" => $currentTime,
            ];
            $pusher->trigger('NotifyEvent', 'Notify', $data);

            $orderDetail->save();
        }
        return response()->json(true, Response::HTTP_OK);
    }
    public function reimburseForTable($id)
    {
        try {
            $table = Table::find($id);
            $orderTables = OrderTable::where('table_id', $id)->get();
            DB::beginTransaction();
            foreach ($orderTables as $orderTable) {
                $orderDetails = $orderTable->orderDetail;
                foreach ($orderDetails as $orderDetail) {
                    $orderDetail->order_detail_status = 3;
                    $orderDetail->save();
                }
            }
            $table->notification()->create([
                'table_id' => $id,
                'status' => 1,
                'noti_content' => 'Món của bàn ' . $table->table_name  . ' đã chế biến xong !',
            ]);
            $pusher = $this->newPusher();
            $data = [
                'tableName' => $table->table_name,
                'notificationTime' => Carbon::now()->format('Y-m-d H:i:s'),
            ];
            $pusher->trigger('NotifyEvent', 'Notify', $data);
            DB::commit();
            return redirect()->back()->with('success', 'Bàn ' . $table->table_name . ' đã trả món xong!');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Bàn ' . $table->table_name . '  trả món bị lỗi!');
        }
    }

    public function reimburseForOrder($id)
    {
        try {
            $orderTables = OrderTable::where('order_id', $id)->get();
            $order = Order::find($id);
            $table = OrderTable::find($id)->table;
            DB::beginTransaction();
            foreach ($orderTables as $orderTable) {
                $orderDetails = $orderTable->orderDetail;
                foreach ($orderDetails as $orderDetail) {
                    $orderDetail->order_detail_status = 3;
                    $orderDetail->save();
                }
            }
            $order->notification()->create([
                'order_id' => $id,
                'status' => 1,
                'noti_content' => 'Món của bàn ' . $table->table_name . ' đã chế biến xong !',
            ]);
            $pusher = $this->newPusher();
            $data = [
                'tableName' => $table->table_name,
                'notificationTime' => Carbon::now()->format('Y-m-d H:i:s'),
            ];
            $pusher->trigger('NotifyEvent', 'Notify', $data);
            DB::commit();
            return redirect()->back()->with('success', 'Order có mã ' . $id . ' đã trả món xong!');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Order có mã ' . $id . '  trả món bị lỗi!');
        }
    }
    public function reimburseForTableByOrder($id)
    {
        try {
            $orderTables = OrderTable::find($id)->get();
            DB::beginTransaction();
            foreach ($orderTables as $orderTable) {
                $orderDetails = $orderTable->orderDetail;
                foreach ($orderDetails as $orderDetail) {
                    $orderDetail->order_detail_status = 3;
                    $orderDetail->save();
                }
            }
            $pusher = $this->newPusher();
            $data = [
                'tableName' => $orderTable->table->table_name,
                'notificationTime' => Carbon::now()->format('Y-m-d H:i:s'),
            ];
            $pusher->trigger('NotifyEvent', 'Notify', $data);
            DB::commit();
            return redirect()->back()->with('success', 'Order có mã ' . $id . ' đã trả món xong!');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Order có mã ' . $id . '  trả món bị lỗi!');
        }
    }

    public function cancelDetail($detailId, $order_detail_cancel_reason)
    {
        try {
            $detail = OrderDetail::find($detailId);
            $detail->order_detail_status = 4;
            $detail->order_detail_cancel_reason = $order_detail_cancel_reason;
            $detail->save();
            return response()->json(true, Response::HTTP_OK);
        } catch (Exception $e) {
            return response()->json(false, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
