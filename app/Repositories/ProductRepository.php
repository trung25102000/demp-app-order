<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository
{
    protected $product;

    /**
     * @param $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function getAllProduct(){
        $product = $this->product->all();
        return $product;
    }
    public function getProductWithUnit()
    {
        return $this->product::with('unit')->get();
    }
    public function searchProduct($keyword, $categoryId)
    {
        $query = $this->product::with('unit')
            ->where(function ($query) use ($keyword) {
                $query->where('product_key', 'like', "%". $keyword . "%")
                    ->orWhere('product_name', 'like', "%". $keyword . "%");
            });

        if ($categoryId) {
            $query->where('category_id', $categoryId);
        }

        return $query->get();
    }

}
