<?php

namespace App\Models;

use App\Models\Notification as ModelsNotification;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'order_details';

    protected $fillable = [
        'product_id',
        'order_table_id',
        'order_detail_price',
        'order_detail_note',
        'order_detail_other_name',
        'order_detail_quantity',
        'order_detail_status',
        'order_detail_other_unit_name',
        'order_detail_cancel_reason',
    ];


    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function orderTable()
    {
        return $this->belongsTo(OrderTable::class);
    }
    public function notification()
    {
        return $this->hasOne(ModelsNotification::class);
    }
    public function getStatusTextAttribute()
    {
        switch ($this->order_detail_status) {
            case 1:
                return 'Chưa chế biến';
            case 2:
                return 'Đang chế biến';
            case 3:
                return 'Đã trả món';
            case 4:
                return 'Đã hủy';
            default:
                return '';
        }
    }
}
