<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use HasFactory, SoftDeletes;

    protected $table='invoices';

    protected $fillable = [
        'id',
        'order_id',
        'voucher_id',
        'invoice_note',
        'invoice_status_id',
        'order_table_id'
    ];

    protected $status = [
        '0' => 'Chưa thanh toán',
        '1' => 'Đã thanh toán',
    ];

    public function getInvoiceAttribute($value)
    {
        return $this->status[$value] ?? '';
    }

    public function setInvoiceAttribute($value)
    {
        $statusKey = array_search($value, $this->status);
        if ($statusKey !== false) {
            $this->attributes['status'] = $statusKey;
        }
    }

    public function orderTable(){
        return $this->belongsTo(OrderTable::class);
    }
    public function voucher(){
        return $this->belongsTo(Voucher::class);
    }


}
