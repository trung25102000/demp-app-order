<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Notification as ModelsNotification;

class Table extends Model
{
    use HasFactory, SoftDeletes;

    public function orderTables()
    {
        return $this->hasMany(OrderTable::class);
    }

    protected $table = 'tables';
    protected $fillable = [
        'id',
        'table_area_id',
        'table_name',
        'status'
    ];

    public function orderTable()
    {
        return $this->hasMany(OrderTable::class);
    }

    public function tableArea()
    {
        return $this->belongsTo(TableArea::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
    public function notification()
    {
        return $this->hasOne(ModelsNotification::class);
    }

    public function getStatusTextAttribute()
    {
        switch ($this->status) {
            case 1:
                return 'Trống';
            case 2:
                return 'Đặt trước';
            case 3:
                return 'Bận';
            case 4:
                return 'Bàn gộp';
            default:
                return '';
        }
    }
}
