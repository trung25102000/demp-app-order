<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Notification as ModelsNotification;

class Order extends Model
{
    use HasFactory, SoftDeletes;


    public function orderTables()
    {
        return $this->hasMany(OrderTable::class);
    }

    protected $table = 'orders';


    protected $fillable = [
        'id',
        'order_type_id',
        'order_create_time',
        'order_finish_time',
        'order_accept_time',
        'order_note',
        'order_cancel_reason',
    ];

    protected $status = [
        '0' => 'Mới',
        '1' => 'Đang chế biến',
        '2' => 'Đã hủy',
        '3' => 'Đã hoàn thành',
    ];

    public function getStatusAttribute($value)
    {
        return $this->status[$value] ?? '';
    }

    public function setStatusAttribute($value)
    {
        $statusKey = array_search($value, $this->status);
        if ($statusKey !== false) {
            $this->attributes['status'] = $statusKey;
        }
    }

    public function orderDetail()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function orderTable()
    {
        return $this->hasOne(OrderTable::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function orderType()
    {
        return $this->belongsTo(OrderType::class);
    }
    public function invoice()
    {
        return $this->hasMany(Invoice::class);
    }

    public function orderDetailByOrderID()
    {
        return $this->hasMany(OrderDetail::class, 'order_id');
    }
    public function notification()
    {
        return $this->hasOne(ModelsNotification::class);
    }
}
