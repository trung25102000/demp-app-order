<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'customers';

    protected $fillable = [
        'id',
        'customer_name',
        'customer_email',
        'customer_phone',
        'customer_birthday',
        'customer_address',
        'customer_gender'
    ];
    public function orderTable(){
        return $this->hasMany(OrderTable::class);
    }
}
