<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $table='products';
    protected $fillable=[
        'id',
        'category_id',
        'product_name',
        'product_price',
        'product_image',
        'product_key',
        'product_description',
        'product_available',
    ];

    public function orderDetail(){
        return $this->hasMany(OrderDetail::class);
    }

    public function unit() {
        return $this->belongsTo(Unit::class);
    }
    public function category(){
        return $this->belongsTo(Category::class);
    }
}
