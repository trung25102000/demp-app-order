<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Voucher extends Model
{
    use HasFactory, SoftDeletes;
    protected $table='vouchers';

    protected $fillable = [
        'id',
        'voucher_key',
        'voucher_description',
        'voucher_expiration_date',
    ];

    public function invoice(){
        return $this->hasMany(Invoice::class);
    }
}
