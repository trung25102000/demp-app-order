<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Notification extends Model
{
    protected $table = 'notifications';
    protected $fillable = [
        'id',
        'order_detail_id',
        'status',
        'noti_content',
    ];
    public function orderDetail()
    {
        return $this->hasOne(OrderDetail::class);
    }
    public function table()
    {
        return $this->hasOne(Table::class);
    }
    public function order()
    {
        return $this->hasOne(Order::class);
    }
}
