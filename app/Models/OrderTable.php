<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderTable extends Model
{
    use HasFactory;
    use SoftDeletes;


    protected $table='order_tables';

    protected $fillable = [
        'id',
        'order_id',
        'table_id',
        'customer_id',
        'user_id',
        'count_of_customer',
        'voucher_id',
        'status'
    ];

    public function getStatusTextAttribute()
    {
        switch ($this->status) {
            case 1:
                return 'Mới';
            case 2:
                return 'Đang chế biến';
            case 3:
                return 'Đã hoàn thành';
            case 4:
                return 'Đã hủy';
            case 5:
                return 'Order ghép bàn';
            case 6:
                return 'Order ghép bàn - order';
            default:
                return '';
        }
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
    public function invoice()
    {
        return $this->hasOne(Invoice::class);
    }
    public function table()
    {
        return $this->belongsTo(Table::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function orderDetail()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function invoices()
    {
        return $this->hasOne(Invoice::class);
    }


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


}
