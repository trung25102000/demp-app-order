<?php

namespace App\Http\Controllers\Invoice;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Services\CustomerServices;
use App\Services\InvoicesServices;
use App\Services\OrderDetailServices;
use App\Services\OrderServices;
use App\Services\OrderTableServices;
use App\Services\ProductServices;
use App\Services\TableServices;
use PDF;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{
    protected $orderServices;
    protected $productServices;
    protected $customerServices;
    protected $invoicesServices;
    protected $orderTableServices;
    protected $tableServices;
    protected $OrderTableServices;
    protected $orderDetailServices;
    public function __construct(TableServices $tableServices,OrderTableServices $orderTableServices, OrderServices $orderServices, ProductServices $productServices, CustomerServices $customerServices, InvoicesServices $invoicesServices,OrderDetailServices $orderDetailServices)
    {
        $this->orderServices = $orderServices;
        $this->productServices = $productServices;
        $this->customerServices = $customerServices;
        $this->invoicesServices = $invoicesServices;
        $this->orderTableServices = $orderTableServices;
        $this->tableServices =$tableServices;
        $this->OrderTableServices = $orderTableServices;
        $this->orderDetailServices=$orderDetailServices;
    }

    /**
     * Chi tiet hoa don
     * @param $orderTableId
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Http\JsonResponse
     */
    public function paymentDetail($orderTableId)
    {
        try {
            $orderTables = $this->OrderTableServices->getOrderTableToAddBill($orderTableId);
            $voucher = $this->orderServices->getVoucher();
            $product = $this->productServices->getProductWithUnit();
            $orderTable = $this->orderServices->getOrderTable($orderTableId);
            $orderDetail = $this->orderDetailServices->getOrderDetailByStatus('3',$orderTableId);
            $customerName =$this->customerServices->getCustomerById($orderTable->customer_id);
            if($orderTable->voucher_id) {
                $voucherSelect = $this->orderServices->getVoucherById($orderTable->voucher_id);
            } else {
                $voucherSelect = null;
            }
            $count = $orderTable->orderDetail->where('order_detail_status', '3')->count();
            if($count==0)
            {
                return redirect()->back()->with('error','Order vẫn đang được thực hiện, chưa thể thanh toán!');
            }
            $invoices=$this->invoicesServices->createInvoice($orderTableId, $orderTable->voucher_id);
            return view('order.payment-detail.payment-detail', compact('voucher', 'product','orderTable', 'orderDetail', 'customerName', 'voucherSelect','invoices','orderTables'));
        }
        catch (Exception $e) {
            return response()->json(false, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function printProvisional($orderTableId)
    {
        $voucher = $this->orderServices->getVoucher();
        $product = $this->productServices->getProductWithUnit();
        $orderTable = $this->orderServices->getOrderTable($orderTableId);
        $orderDetail = $this->orderServices->getOrderDetail($orderTableId);
        $customerName =$this->customerServices->getCustomerById($orderTable->customer_id);
        if($orderTable->voucher_id) {
            $voucherSelect = $this->orderServices->getVoucherById($orderTable->voucher_id);
        } else {
            $voucherSelect = null;
        }
        $invoiceId = Invoice::where('order_table_id', $orderTableId)->value('id');
        return view('order.payment-detail.print-provisional', compact('voucher', 'product','orderTable', 'orderDetail', 'customerName', 'voucherSelect', 'invoiceId'));
    }

    public function savePayment(Request $request, $orderTableId)
    {
        try {
            DB::beginTransaction();
            $voucher = $this->orderServices->getVoucher();
            $product = $this->productServices->getProductWithUnit();
            $orderTable = $this->orderServices->getOrderTable($orderTableId);
            $orderDetail = $this->orderServices->getOrderDetail($orderTableId);
            $customerName =$this->customerServices->getCustomerById($orderTable->customer_id);
            if($orderTable->voucher_id) {
                $voucherSelect = $this->orderServices->getVoucherById($orderTable->voucher_id);
            } else {
                $voucherSelect = null;
            }
            $invoiceId = Invoice::where('order_table_id', $orderTableId)->value('id');
            $payment = $this->invoicesServices->createPayment($request->all(), $invoiceId);
            $this->invoicesServices->updateStatusInvoice($invoiceId, 1);
            $this->invoicesServices->updateStatusTable($orderTableId, 1);
            $this->invoicesServices->updateStatusOrderTable($orderTableId, 3);
            $this->invoicesServices->updateTimeInOrder($orderTableId);

            DB::commit();
            $filename = 'order-pdf/print_invoice'.$invoiceId.'.pdf';
            $viewName = 'order.payment-detail.print-provisional-pdf';

            $data = [
                'vouncher' => $voucher,
                'product' => $product,
                'orderTable' => $orderTable,
                'orderDetail' => $orderDetail,
                'customerName' => $customerName,
                'voucherSelect' => $voucherSelect,
                'invoiceId' => $invoiceId,
                'customerPay' => $request->all()['customer_pay'],
                'customerRefund' => $request->all()['customer_refund'],
                'tax'=> $request->all()['tax_VAT']
            ];

            $view = \View::make($viewName, $data);
            $html = $view->render();

//            // Tạo file PDF
            $pdf = new \Elibyy\TCPDF\Facades\TCPDF;
            $pdf::SetTitle('Invoice');
            $pdf::SetSubject('Invoice');
            $pdf::setFontSubsetting(true);
            \TCPDF_FONTS::addTTFfont(public_path('fonts/arial.ttf'), 'Arial', '', 96);
            $pdf::SetFont('arial', '', 12);
            $pdf::AddPage();
            $pdf::writeHTML($html, true, false, true, false, '');
            $pdf::Output(public_path($filename), 'F');
            if (file_exists(public_path($filename))) {
                $fileUrl = url("order-pdf/".basename(public_path($filename)));
            }

            // return response()->download(public_path($filename));
            return response()->json([
                'success' => true,
                'redirect_url' => route('orders.payment_list'),
                'pdf' =>  $fileUrl
            ]);

        }
        catch (Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'error' => 'Lưu thanh toán không thành công'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function updateVoucherInInvoice(Request $request, $orderTableId)
    {
        $invoiceId = Invoice::where('order_table_id', $orderTableId)->value('id');
        $this->invoicesServices->updateVoucherInInvoice($invoiceId, $request->all());
        $this->invoicesServices->updateVoucherInOrderTable($orderTableId, $request->all());
    }

}
