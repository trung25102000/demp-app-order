<?php

namespace App\Http\Controllers;

use PDF;
use Elibyy\TCPDF\Facades\TCPDF;
use TCPDF_FONTS;

class PdfController extends Controller
{
    public function index()
    {
        $filename = 'hello_world.pdf';

    	$data = [
    		'title' => 'Hello world!'
    	];

    	$view = \View::make('order.pdf', $data);
        $html = $view->render();

    	$pdf = new TCPDF('P', 'mm', 'A7', true, 'UTF-8', false);

        $lg['a_meta_charset'] = 'UTF-8';
        $pdf::setLanguageArray($lg);
        $pdf::SetTitle('Hello World');
        $pdf::AddPage();
        $pdf::writeHTML($html, true, false, true, false, '');
        $fontname = TCPDF_FONTS::addTTFfont(public_path('fonts/arial.ttf'), 'Arial', '', 96);
        $pdf::setFont($fontname, '', 12);

        PDF::Output('hello_world.pdf');
    }
}
