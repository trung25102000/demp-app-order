<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{

    public function index()
    {
        return view('user.login');
    }


    public function customLogin(Request $request)
    {
        $request->validate([
            'user_email' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->only('user_email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect()->intended('/')
                ->withSuccess('Signed in');
        }

        return redirect("login")->withSuccess('Login details are not valid');
    }


    public function registration()
    {
        return view('user.registration');
    }

    public function customRegistration(Request $request)
    {
        try {
            $request->validate([
                'user_name' => 'required',
                'user_email' => 'required|email|unique:users',
                'user_birthday' => 'nullable|date',
                'user_address' => 'nullable|max:400',
                'user_phone' => 'required|numeric|digits:10|unique:users',
                'gender' => 'required',
                'password' => 'required|min:6',
            ]);
            $data = $request->all();
            $user = $this->create($data);
            Auth::login($user);
            return redirect(route('orders.home'))->withSuccess('You have signed-in');
        } catch (ValidationException $e) {
            return redirect()->back()->withErrors($e->errors())->withInput();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }
    }


    public function create(array $data)
    {
        return User::create([
            'user_name' => $data['user_name'],
            'user_email' => $data['user_email'],
            'user_birthday' => $data['user_birthday'],
            'user_address' => $data['user_address'],
            'user_phone' => $data['user_phone'],
            'gender' => $data['gender'],
            'password' => Hash::make($data['password'])
        ]);
    }


    public function dashboard()
    {
        if (Auth::check()) {
            return view('order.index');
        }

        return redirect("login")->withSuccess('You are not allowed to access');
    }


    public function signOut()
    {
        Session::flush();
        Auth::logout();

        return Redirect('login');
    }

    public function user()
    {
        $user = User::all();
        return view('user.employees', compact('user'));
    }
}
