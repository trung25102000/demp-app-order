<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Http\Requests\SplitBillExistRequest;
use App\Http\Requests\SplitBillNewRequest;
use App\Http\Requests\SplitTableRequest;
use App\Http\Requests\StoreOrderRequest;
use App\Models\Category;
use App\Models\Customer;
use App\Models\Invoice;
use App\Models\Notification;
use App\Models\OrderDetail;
use App\Models\OrderTable;
use App\Models\Product;
use App\Models\Table;
use App\Models\TableArea;
use App\Models\User;
use App\Models\Voucher;
use App\Repositories\OrderRepository;
use App\Repositories\OrderTableRepository;
use App\Services\CustomerServices;
use App\Services\InvoicesServices;
use App\Services\OrderDetailServices;
use App\Services\OrderServices;
use App\Services\OrderTableServices;
use App\Services\ProductServices;
use App\Services\TableServices;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use PharIo\Manifest\Exception;
use Pusher\Pusher;

class OrderController extends Controller
{
    protected $product;
    protected $orderServices;
    protected $tableServices;
    protected $orderTableServices;
    protected $orderDetailServices;
    protected $productServices;
    protected $customerServices;
    protected $orderTableRepository;
    protected $orderRepository;

    protected $invoicesServices;

    /**
     * @param $product
     */

    public function __construct(
        Product              $product,
        OrderServices        $orderServices,
        TableServices        $tableServices,
        OrderTableServices   $orderTableServices,
        ProductServices      $productServices,
        OrderDetailServices  $orderDetailServices,
        CustomerServices     $customerServices,
        OrderTableRepository $orderTableRepository,
        OrderRepository      $orderRepository,
        InvoicesServices     $invoicesServices
    ) {

        $this->product = $product;
        $this->invoicesServices = $invoicesServices;
        $this->orderTableServices = $orderTableServices;
        $this->tableServices = $tableServices;
        $this->orderServices = $orderServices;
        $this->productServices = $productServices;
        $this->orderDetailServices = $orderDetailServices;
        $this->customerServices = $customerServices;
        $this->orderTableRepository = $orderTableRepository;
        $this->orderRepository = $orderRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $perPage = 12;
        $table = $this->tableServices->getAllItems($perPage);
        $tableAreas = TableArea::all();
        return view('order.list-table.index', ['data' => $table, 'tableAreas' => $tableAreas]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|Factory|View|Application
     */
    public function search(Request $request)
    {
        $perPage = 12;
        $searchString = $request->input('searchValue', '');
        $statusId = $request->input('dropdownId');
        $AreaId = $request->input('selectedId');
        $data = $this->tableServices->searchItems($searchString, $statusId, $AreaId, $perPage);

        return view('order.list-table.table-search-data', compact('data'))->render();
    }

    /**
     * Danh sách trả món
     * @return \Illuminate\Contracts\Foundation\Application|Factory|View|Application
     */
    public function reimburse()
    {
        return view('order.reimburse.reimburse');
    }

    public function getAllOrderTableRender()
    {
        $getAllOrderTableRender = $this->orderTableServices->getAllOrderTableRender();
        return response()->json(['getAllOrderTableRender' => $getAllOrderTableRender], 200);
    }

    public function paymentList()
    {
        $perPage = 12;
        $orderTableUsing = $this->orderServices->getPaymentListUsing($perPage);
        $orderTableShip = $this->orderServices->getPaymentListShip($perPage);
        $orderTableReserve = $this->orderServices->getPaymentListReserve($perPage);
        $orderTableBringBack = $this->orderServices->getPaymentListBringBack($perPage);
        return view('order.payment.payment-list', ['orderTable' => $orderTableUsing, 'orderTableShip' => $orderTableShip, 'orderTableReserve' => $orderTableReserve, 'orderTableBringBack' => $orderTableBringBack]);
    }

    /**
     * Hiển thị danh sách list_order tại view của nhân viên
     * @return \Illuminate\Contracts\Foundation\Application|Factory|View|Application
     */
    public function listOrder()
    {
        Session::forget('successListOrder');
        return view('order.list-order.list-order');
    }

    public function orderDetails($orderTableId)
    {
        $details = $this->orderServices->getOrderDetail($orderTableId);
        $orderTable = $this->orderTableServices->getById($orderTableId);

        return view('order.detail-order.details', compact('details', 'orderTable'));
    }

    public function getDetails($orderId)
    {
        $details = $this->orderServices->getOrderDetail($orderId);
        return $details;
    }

    public function moveToKitchen($orderTableId, Request $request)
    {
        try {
            $data = $request->input('orderDetail');
            $pusher = $this->newPusher();
            $this->orderServices->moveToKitchen($orderTableId);
            $pusher->trigger('MoveKitchenEvent', 'move-to-kitchen', $data);
            return back()->with('success', 'Đã chuyển món xuống Bar/Bếp');
        } catch (\Throwable $th) {
            return back()->with('error', 'Chuyển món xuống Bar/Bếp thất bại');
        }
    }

    public function newPusher()
    {
        $options = array(
            'cluster' => 'mt1',
            'encrypted' => true
        );

        return new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );
    }

    public function splitBillAndSplitorderDetail($id)
    {
        $orderTable = $this->orderTableServices->findById($id);
        if (!$orderTable) {
            $errorMessage = 'Lỗi: Không tìm thấy order này.';
            return view('order.error404', compact('errorMessage'));
        } else {
            return view('order.list-order.split-bill-and-split-order', compact('orderTable'));
        }
    }

    public function splitBillAndSplitorderDetailExist($id)
    {
        $orderTable = $this->orderTableServices->findById($id);
        if (!$orderTable) {
            $errorMessage = 'Lỗi: Không tìm thấy order này.';
            return view('order.error404', compact('errorMessage'));
        } else {
            return view('order.list-order.split-order-exist', compact('orderTable'));
        }
    }

    public function getAllOrderDetailsByOrderTableIdExist($orderTableId)
    {
        try {
            DB::beginTransaction();
            $orderTable = $this->orderTableServices->getById($orderTableId);
            $table_id = $orderTable->table_id;
            $tableFrom = $this->tableServices->getById($table_id);
            $tableToList = $this->tableServices->getAllByStatus(1);
            $orderTableTo = $this->orderTableServices->getAllOrderTableStatusNotpayment();
            $orderDetails = $this->orderServices->getAllOrderDetailsByOrderTableId($orderTableId);
            DB::commit();
            return response()->json(['orderDetails' => $orderDetails, 'tableFrom' => $tableFrom, 'tableToList' => $tableToList, 'orderTableTo' => $orderTableTo], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(false, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getAllOrderDetailsByOrderTableId($orderTableId)
    {
        $orderTable = $this->orderTableServices->getById($orderTableId);
        $table_id = $orderTable->table_id;
        $tableFrom = $this->tableServices->getById($table_id);
        $tableToList = $this->tableServices->getAllByStatus(1);
        $orderDetails = $this->orderServices->getAllOrderDetailsByOrderTableId($orderTableId);
        return response()->json(['orderDetails' => $orderDetails, 'tableFrom' => $tableFrom, 'tableToList' => $tableToList], 200);
    }

    public function getAllOrderDetailsByBillId($orderTableId)
    {
        $orderTable = $this->orderTableServices->getById($orderTableId);
        $table_id = $orderTable->table_id;
        $tableFrom = $this->tableServices->getById($table_id);
        $tableToList = $this->tableServices->getAllByStatus(1);
        $orderDetails = $this->orderServices->getAllOrderDetailsByBillId($orderTableId);
        return response()->json(['orderDetails' => $orderDetails, 'tableFrom' => $tableFrom, 'tableToList' => $tableToList], 200);
    }

    /**
     * Tìm kiếm theo danh sách trả ra view list-order theo fillter
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|Factory|View|Application|JsonResponse
     */
    public function searchInListOrder(Request $request)
    {
        try {
            $perPage = 8;
            $searchString = $request->input('search_string', '');
            $statusId = $request->input('status_id');
            $type = $request->input('type');
            $orderTables = $this->orderServices->searchInListOrder($searchString, $statusId, $perPage, $type);
            return view('order.list-order.order-search-data-in-list-order', ['orderTables' => $orderTables]);
        } catch (\Exception $exception) {
            return view('order.list-order.list-order')->with(['errorMessage' => $exception->getMessage()]);
        }
    }

    public function addTable(Request $request)
    {
        try {
            $orderId = $request->input('orderId');
            $idTables = $request->input('idTables');
            $checkedValue = $request->input('checkedValue');
            $this->orderServices->AddTable($orderId, $idTables, $checkedValue);
            return response()->json(true, Response::HTTP_OK);
        } catch (Exception $e) {
            return response()->json(false, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function deleteDetail($detail_id)
    {
        return $this->orderServices->deleteDetail($detail_id);
        return redirect()->back();
    }

    public function finishOrder($order_id)
    {
        return $this->orderServices->finishOrder($order_id);
        return redirect()->back();
    }

    public function saveDetail($detail_id, Request $request)
    {
        $note = $request->input('note');
        $quantity = $request->input('quantity');
        return $this->orderServices->saveDetail($detail_id, $quantity, $note);
    }

    public function cancelOrder($order_id, Request $request)
    {
        $reason = $request->input('reason');
        $this->orderServices->cancelOrder($order_id, $reason);
        return redirect()->back();
    }

    public function editOrder($orderTableId)
    {
        $orderTable = $this->orderServices->getOrderTable($orderTableId);
        $orderDetail = $this->orderServices->getOrderDetail($orderTableId);
        $voucher = $this->orderServices->getVoucher();
        $product = $this->productServices->getAllProduct();
        $user = $this->orderServices->getUser();
        $customers = $this->customerServices->getCustomer($orderTableId);
        $categories = Category::all();
        $tables = Table::all();
        return view('order.edit-order.edit-order', compact('orderTable', 'orderDetail', 'voucher', 'product', 'user', 'customers', 'categories', 'tables'));
    }

    public function addDetailInEditOrder($orderTableId, Request $request)
    {
        $productId = $request->input('productId');
        $orderDetailPrice = $request->input('orderDetailPrice');
        $orderDetailQuantity = $request->input('orderDetailQuantity');
        return $this->orderServices->addDetailInEditOrder($orderTableId, $productId, $orderDetailQuantity, $orderDetailPrice);
    }

    public function splitBillInOrder(SplitBillNewRequest $request)
    {
        try {
            DB::beginTransaction();
            // Lấy id orderTable gửi lên từ table_from
            $tableOrderFrom = $request->tableFrom['id_order_table'];

            // Lấy thông tin orderTable
            $orderTable = $this->orderTableServices->getById($tableOrderFrom)->toArray();
            $idTable = $orderTable['table_id'];

            // Xóa thuộc tính id của orderTable
            unset($orderTable['id']);

            // Xóa orderTable cũ và orderDetail liên quan
            $this->orderTableServices->deleteOrderTableAndOrderDetailUp($tableOrderFrom);

            // Lấy thông tin order cũ
            $orderOld = $this->orderServices->getById($orderTable['order_id']);

            $order = $this->orderServices->createOrderSplitTable($orderOld);

            $orderTable['order_id'] = $order->id;
            // Tạo orderTable mới
            $orderTableOld = $this->orderTableServices->create($orderTable);

            //Tạo orderDetail cho orderTable mới
            $dataOrderDetailOld = $request->tableFrom['orderDetails'];

            $this->orderTableRepository->createOrderDetailByOrderTable($orderTableOld->id, $dataOrderDetailOld);

            // Tạo order mới dựa trên order cũ

            // Tạo orderDetail mới cho table_to
            $dataOrderDetail = $request->tableFrom['order_details_to'];

            $orderDetailNew = $this->orderTableServices->createOrderDetails($tableOrderFrom, $dataOrderDetail);

            DB::commit();
            return response()->json(true, Response::HTTP_OK);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(false, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        $tableSelected = $request->input('tableId');
        $listTable = $this->tableServices->getListTableCanOrder();
        $voucher = $this->orderServices->getVoucher();
        $userList = User::all();
        $categories = Category::all();
        $customers = Customer::select('id', 'customer_name', 'customer_phone', 'customer_birthday', 'created_at')->get();

        return view('order.create-order.create-order', compact('voucher', 'tableSelected', 'listTable', 'userList', 'customers', 'categories'));
    }

    public function splitTableAndSplitOrder(SplitTableRequest $request)
    {
        try {
            DB::beginTransaction();
            $fromOrderTableId = $request->table_from['id_order_table'];
            $idTable = $request->table_to['id_table'];
            $getTableId = $this->tableServices->getAllTable($idTable);

            $orderTable = $this->orderTableServices->getById($fromOrderTableId)->toArray();

            unset($orderTable['id']);
            $this->orderTableServices->deleteOrderTableAndOrderDetail($fromOrderTableId);
            //orderOld
            $orderOld = $this->orderServices->getById($orderTable['order_id']);
            //create again ordertable
            $orderTableOld = $this->orderTableServices->create($orderTable);
            //create order Detail
            $dataOrderDetailOld = $request->table_from['order_details'];
            $this->orderTableRepository->createOrderDetailByOrderTable($orderTableOld->id, $dataOrderDetailOld);

            //create new Order
            $order = $this->orderServices->createOrderSplitTable($orderOld);

            $orderTableNew = $this->orderTableServices->createOrderTableTaskSplit($order, $idTable, $orderTableOld);

            //create order Detail
            $dataOrderDetail = $request->table_to['order_details'];
            $orderDetailNew = $this->orderTableServices->createOrderDetails($orderTableNew->id, $dataOrderDetail);
            DB::commit();
            return response()->json(true, Response::HTTP_OK);
        } catch (Exception $e) {
            DB::rollback();

            return response()->json(false, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function splitOrderExist(SplitBillExistRequest $request)
    {
        try {
            DB::beginTransaction();
            $orderTabelIdFrom = $request->order_from['id_order_from'];
            $orderDetailFrom = $request->order_from['order_details'];
            $orderTabelIdTo = $request->order_to['id_order_to'];

            foreach ($orderDetailFrom as &$detail) {
                unset($detail['product_name']);
                $detail['created_at'] = date('Y-m-d H:i:s', strtotime($detail['created_at']));
                $detail['updated_at'] = date('Y-m-d H:i:s', strtotime($detail['updated_at']));
            }

            $this->orderTableRepository->UpdateOrderDetailByOrderTable($orderTabelIdFrom, $orderDetailFrom);

            $orderDetailTo = $request->order_to['order_details'];
            $orderDetailToOld = $this->orderDetailServices->getAllOrderDetailInOrderTableId($orderTabelIdTo);

            foreach ($orderDetailTo as $orderDetail) {
                $productId = $orderDetail['product_id'];

                $existingOrderDetail = $orderDetailToOld->first(function ($item) use ($productId) {
                    return $item['product_id'] === $productId;
                });

                if ($existingOrderDetail) {
                    // Nếu đã tồn tại, cập nhật số lượng
                    $existingOrderDetail->update([
                        'order_detail_quantity' => $existingOrderDetail->order_detail_quantity + $orderDetail['order_detail_quantity']
                    ]);
                } else {
                    // Nếu chưa tồn tại, thêm order detail mới
                    $this->orderTableRepository->createOrderDetailByOrderTable($orderTabelIdTo, [$orderDetail]);
                }
            }
            DB::commit();
            return response()->json(true, Response::HTTP_OK);
        } catch (Exception $e) {
            DB::rollback();

            return response()->json(false, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getAllOrderDetailInOrderTableId($orderTableId)
    {
        try {
            DB::beginTransaction();
            $orderTable = $this->orderTableServices->getById($orderTableId);
            if (!$orderTable->exists()) {
                $errorMessage = 'Lỗi: Không tìm thấy với id order table này.';
                return view('order.error404', compact('errorMessage'));
            }

            $orderDetails = $this->orderServices->getAllOrderDetailsByOrderTableId($orderTableId);

            $productInDetail = $this->orderDetailServices->getProductInOrderDetail($orderDetails);
            DB::commit();
            return response()->json([$orderDetails, $productInDetail], Response::HTTP_OK);
        } catch (Exception $e) {
            DB::rollback();

            return response()->json(false, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function reimbursed(Request $request)
    {
        $detailId = $request->input('id');
        return $this->orderServices->reimbursed($detailId);
    }

    public function addOtherDetail($orderTableId, Request $request)
    {
        $orderOtherName = $request->input('orderDetailOtherName');
        $orderOtherQuantity = $request->input('orderDetailQuantity');
        $orderOtherPrice = $request->input('orderDetailPrice');
        return $this->orderServices->addOtherDetail($orderTableId, $orderOtherName, $orderOtherPrice, $orderOtherQuantity);
    }

    public function saveOrderTable($orderTableId, Request $request)
    {
        $userId = $request->input('user_id');
        $customerId = $request->input('customer_id');
        $countCustomer = $request->input('count_of_customer');
        $typeOrder = $request->input('type_id');
        $tableId = $request->input('table_id');
        $orderItems = $request->input('orderItems');
        $voucherId = $request->input('voucher_id');
        $orderItemsEdit = $request->input('orderItemsEdit');
        $orderTable = OrderTable::find($orderTableId);
        $orderId = $orderTable->order_id;
        $tableName = Table::find($tableId)->table_name;
        if ($orderItemsEdit != null) {
            foreach ($orderItems as &$item) {
                foreach ($orderItemsEdit as $editItem) {
                    if ($item['id'] == $editItem['id'] && $item['status'] == 3) {
                        $item['quantity'] -= $editItem['quantity'];
                        $orderItems[] = $editItem;
                    }
                }
            }
        }
        try {
            $orderTable = OrderTable::find($orderTableId);
            if ($orderTable->status == 1) {
                DB::beginTransaction();
                $this->orderTableServices->saveOrderTable($orderTableId, $userId, $customerId, $countCustomer, $typeOrder, $tableId, $orderItems, $voucherId);
                DB::commit();
            }
            // chuyeển bar/bếp nếu status khác 1(mới)
            if ($orderTable->status == 2 || $orderTable->status == 5) {
                DB::beginTransaction();
                $this->orderTableServices->saveOrderTable($orderTableId, $userId, $customerId, $countCustomer, $typeOrder, $tableId, $orderItems, $voucherId);
                $dataDetail = [];
                $lastDetail = OrderDetail::latest('id')->first();
                if ($orderItemsEdit) {
                    foreach ($orderItemsEdit as $item) {
                        $data = [
                            'product_id' => $item['id'] ?? '',
                            'order_table_id' => $orderTableId ?? '',
                            'order_id' => $orderId ?? '',
                            'product_name' => $item['product_name'] ?? '',
                            'order_detail_quantity' => $item['quantity'] ?? '',
                            'order_detail_note' => $item['order_detail_note'] ?? '',
                            'table_name' => $tableName ?? '',
                            'created_at' => $item['created_at'] ?? '',
                            'updated_at' => Carbon::now() ?? '',
                        ];
                        array_push($dataDetail, $data);
                    }
                }
                foreach ($orderTable->orderDetail as $item) {
                    if ($item->order_detail_status == 1) {
                        $item->order_detail_status = 2;
                        $item->save();
                    }
                }
                $pusher = $this->newPusher();
                $pusher->trigger('MoveKitchenEvent', 'move-to-kitchen', $dataDetail);

                DB::commit();
            }
            return response()->json(true, Response::HTTP_OK);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(false, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


    public function getDiscountAmount($voucherId)
    {
        return $this->orderServices->getDiscountAmount($voucherId);
    }


    public function storeOrder(StoreOrderRequest $request)
    {
        try {
            DB::beginTransaction();
            $order = $this->orderServices->createOrder($request->all());
            //create customer
            $customer = $request->input('customer');
            $customerID = $request->input('customer_id');
            if (!$customerID && $customer) {
                $customer = $this->customerServices->create($customer);
                $customerID = $customer->id;
            }
            $orderTable = $this->orderTableServices->createOrderTable($customerID, $order->id, $request->all());
            $this->orderDetailServices->createOrderDetail($orderTable->id, $request->all());
            $statusOrder = $request->input('status');
            if ($statusOrder == 3) {
                $this->tableServices->updateStatusTable($request->table_id, 2);
            } else if ($statusOrder == 1) {
                $this->tableServices->updateStatusTable($request->table_id, 3);
            }
            DB::commit();

            return response()->json(true, Response::HTTP_OK);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(false, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function filterProductByCategories(Request $request)
    {
        $categoryId = $request->input('category_id');
        return $this->orderServices->filterProductByCategories($categoryId);
    }

    public function bestSellerOfProduct()
    {
        return $this->orderServices->bestSellerOfProduct();
    }

    public function updateQuantity(Request $request, $orderTableId)
    {
        $productId = $request->input('product_id');
        $quantity = $request->input('product_quantity');
        return $this->orderTableServices->updateQuantity($productId, $quantity, $orderTableId);
    }

    /**
     * Xóa 1 order chỉ định với id đã có
     * @param $id
     * @return void
     */
    public function deleteOrder($id)
    {
        try {
            // Tìm đơn hàng theo id
            $order = $this->orderTableServices->findById($id);
            DB::beginTransaction();
            $deleted = $this->orderTableServices->deleteOrderTable($order);
            DB::commit();
            if ($deleted == true) {
                return response()->json(true, 200);
            } else {
                return response()->json(false, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(false, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


    public function getOrderDetailInEdit($orderTableId)
    {
        return $this->orderDetailServices->getOrderDetailInEdit($orderTableId);
    }

    public function getAllDetail()
    {
        return $this->orderServices->getAllOrderDetails();
    }

    /**
     * Cập nhật VoucherId của invoices nếu có sự thay đổi voucher tại hóa đơn
     * @param Request $request
     * @return JsonResponse
     */
    public function updateVoucher(Request $request)
    {
        try {
            $voucherId = $request->input('voucherId');
            $invoiceId = $request->input('invoiceId');
            $invoice = Invoice::find($invoiceId);

            if ($invoice) {
                $invoice->voucher_id = $voucherId;
                $invoice->save();
            }
            return response()->json(true, Response::HTTP_OK);
        } catch (Exception $e) {
            return response()->json(false, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function setIsReadNoti()
    {
        Notification::query()->update([
            'status' => 0,
        ]);
        return response()->json('Da doc tat ca thong bao', Response::HTTP_OK);
    }

    /**
     * Lấy mảng id OrderTable đươc chọn và truyền đến modal xác nhận
     * @param Request $request
     * @return string
     */
    public function getOrderAddBill(Request $request)
    {
        try {
            $ids = $request->input('selectedIds');
            $orderTables = [];
            foreach ($ids as $id) {
                $orderTable = OrderTable::find($id);
                $orderTables[] = $orderTable;
            }
            return view('order.payment-detail.modal-add-bill-in-payment-detail', ['orderTables' => $orderTables]);
        } catch (Exception $e) {
            return response()->json(false, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Đẩy dữ liệu các món muốn ghép ra bill
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|Factory|View|Application|JsonResponse
     */
    public function getDataTemp(Request $request)
    {
        try {
            $ids = $request->input('selectedIds');
            $orderDetails = [];
            foreach ($ids as $id) {
                $orderDetail = OrderDetail::find($id);
                $orderDetails[] = $orderDetail;
            }
            return view('order.payment-detail.orderDetailTemp', ['orderDetails' => $orderDetails]);
        } catch (Exception $e) {
            return response()->json(false, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Hàm sử lý gộp bill, thanh toán và in hóa đơn
     * @return void
     */
    public function payment(Request $request)
    {
        try {
            $dataId = $request->input('dataId');
            $ids = $request->input('ids');
            if ($ids != null) {
                DB::beginTransaction();
                foreach ($ids as $id) {
                    $orderDetail = $this->orderDetailServices->findById($id);
                    $orderDetail->order_table_id = $dataId;
                    $orderDetail->save();
                }
                DB::commit();
            }
            return response()->json(true, Response::HTTP_OK);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(false, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Tra mon theo ban
     * @param $id
     * @return void
     */
    public function reimburseForTable($id)
    {
        return $this->orderServices->reimburseForTable($id);
    }

    /**
     * Tra mon theo order
     * @param $id
     * @return void
     */
    public function reimburseForOrder($id)
    {
        return $this->orderServices->reimburseForOrder($id);
    }

    /**
     * Tra mon theo order nhung khong su dung ban
     * @param $id
     * @return void
     */
    public function reimburseForTableByOrder($id)
    {
        return $this->orderServices->reimburseForTableByOrder($id);
    }
    /**
     * Lấy data đưa lên Pusher
     */
    public function getDataToPusher(Request $request)
    {
        $orderTableId = $request->input('orderTableId');
        return $this->orderRepository->getDataToPusher($orderTableId);
    }

    /**
     * Lấy dữ liệu tra về view trả món
     * @param Request $request
     * @return void
     */
    public function searchReimburse(Request $request)
    {
        try {
            $searchString = $request->input('search_string', '');
            $dataType = $request->input('dataType');
            $selectedValue = $request->input('selectedValue');

            if ($dataType == 1) {
                $orderDetails = $this->orderServices->getAllOrderDetails($selectedValue, $searchString);
                return view('order.reimburse.tab-all-in-reimburse', ['dataListDetails' => $orderDetails]);
            } elseif ($dataType == 2) {
                $orderTables = $this->tableServices->getTableWithTable($searchString);
                return view('order.reimburse.tab-for-table-in-reimburse', ['dataListOrderTableWithTables' => $orderTables]);
            } elseif ($dataType == 3) {
                $orderTables = $this->orderServices->getOrderTableWithOrder($searchString);
                return view('order.reimburse.tab-for-order-in-reimburse', ['dataListOrderTableWithOrders' => $orderTables]);
            }
        } catch (\Exception $exception) {
            return response()->json(false, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * đổ dữ liệu món theo bàn ra tùy vào khu vực bar or bếp
     * @param Request $request
     * @return JsonResponse|void
     */
    public function getItemCount(Request $request)
    {
        try {
            $orderId = $request->input('orderId');
            $selectedValue = $request->input('selectedValue');
            $orderDetails = $this->orderDetailServices->getOrderDetailByStatusAndOrderTableId($orderId, $selectedValue);
            return response()->json(['orderDetails' => $orderDetails]);
        } catch (\Exception $exception) {
            return response()->json(false, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * Trả món theo nhiều bàn
     */
    public  function reimbursedManyTable(Request $request)
    {
        $dataTable = $request->input('dataTable');
        return $this->orderServices->reimbursedManyTable($dataTable);
    }
    /**
     * Trả món theo nhiều Order
     */
    public function reimbursedManyOrder(Request $request)
    {
        $dataOrder = $request->input('dataOrder');
        return $this->orderServices->reimbursedManyOrder($dataOrder);
    }

    /**
     * Trả món theo nhiều món ăn.
     * @param Request $request
     * @return JsonResponse
     */
    public function reimbursedManyProduct(Request $request)
    {
        $dataProduct = $request->input('dataProduct');
        return $this->orderServices->reimbursedManyProduct($dataProduct);
    }
    public function cancelDetail(Request $request)
    {
        $detailId = $request->input('detailId');
        $order_detail_cancel_reason = $request->input('order_detail_cancel_reason');
        return $this->orderServices->cancelDetail($detailId, $order_detail_cancel_reason);
    }
    /**
     * Lấy số lượng của order dựa vào loại hình đặt order tại view list-order
     * @return void
     */
    public function getCountItemsInListOrder()
    {
        try {
            $orderByUsing = $this->orderServices->getOrderListOrderTypeById(1);
            $orderByBringBack = $this->orderServices->getOrderListOrderTypeById(2);
            $orderByReserve = $this->orderServices->getOrderListOrderTypeById(3);
            $orderByShip = $this->orderServices->getOrderListOrderTypeById(4);
            return response()->json(['orderByUsing' => $orderByUsing->count(), 'orderByBringBack' => $orderByBringBack->count(), 'orderByReserve' => $orderByReserve->count(), 'orderByShip' => $orderByShip->count()]);
        } catch (\Exception $exception) {
            return response()->json(false, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /** đổ dữ liệu món theo bàn ra tùy vào khu vực bar or bếp
     * @param Request $request
     * @return JsonResponse|void
     */
    public function getItemCountTable(Request $request)
    {
        try {
            $tableId = $request->input('tableId');
            $selectedValue = $request->input('selectedValue');
            $orderDetails = $this->orderDetailServices->getOrderDetailByStatusAndTableId($tableId, $selectedValue);
            return response()->json(['orderDetails' => $orderDetails]);
        } catch (\Exception $exception) {
            return response()->json(false, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Lấy Bàn hay loại hình mà bàn được ghép
     * @param Request $request
     * @return void
     */
    public function getTableAdd(Request $request)
    {
        try {
            $tableId = $request->input('tableId');
            $result = $this->tableServices->getTableTheAdd($tableId);
            return response()->json(['result' => $result]);
        } catch (\Exception $exception) {
            return response()->json(false, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
