<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductCollection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\ProductServices;


class ProductController extends Controller
{
    protected $productService;
    public function __construct(ProductServices $product) {
        $this->productService = $product;
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $product =  $this->productService->getProductWithUnit();
        $productCollection = new ProductCollection($product);
        return response() -> json(
            ['data' => $productCollection], 
            Response::HTTP_OK
        );
    }
    public function search(Request $request)
    {
        $keyword = $request->input('keyword');
        $categoryId = $request->input('category_id');
        $product = $this->productService->searchProduct($keyword, $categoryId);
        $productCollection = new ProductCollection($product);
        return response()->json(
            ['data' => $productCollection],
            Response::HTTP_OK
        );
    }

}
