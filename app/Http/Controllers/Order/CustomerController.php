<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\CustomerServices;

class CustomerController extends Controller
{
    private $customerService;
    
    public function __construct(CustomerServices $customerService)
    {
        $this->customerService = $customerService;
    }
    public function searchCustomer(Request $request)
    {
        $keyword = $request->input('keyword');
        $customers = $this->customerService->searchCustomer($keyword);
        return response()->json($customers);
    }
}
