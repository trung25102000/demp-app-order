<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'table_id' => "bail" . ($this->input('status') == "2" || $this->input('status') == "4" ? "|nullable" : "|required") . "|exists:tables,id",
            'count_of_customer' => "bail|required|max:100",
            'status' => "bail|required|in:1,2,3,4",
            'user_id' => "bail|required",
            'orderItems' => [ "bail","required", function ($attribute, $value, $fail) {
                if (count($value) == 0) {
                    return $fail('Cần ít nhất một sản phẩm cho order!');
                }
            }],
        ];
    }

    public function messages()
    {
        return [
            'table_id.required' => "Yêu cầu nhập số bàn!",
            'table_id.exists' => "Mã số bàn không tồn tại!",
            'count_of_customer.required' => "Yêu cầu nhập số lượng khách!",
            'count_of_customer.max' => "Số lượng khách vượt quá giới hạn!",
            'status.required' => "Yêu cầu chọn loại Order!",
            'status.in' => "Loại Order không tồn tại!",
            'user_id.required' => "Vui Lòng chọn Nhân Viên!",
            'orderItems.required' => "Cần ít nhất một sản phẩm cho order!",
        ];
    }
}
