<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SplitBillExistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'order_to.order_details.*.order_detail_quantity' => 'required|numeric|min:1',
            'order_from.order_details.*.order_detail_quantity' => 'required|numeric|min:1',
            'order_from.id_order_from' => 'required|exists:order_tables,id',
            'order_to.id_order_to' => 'required|exists:order_tables,id',
            'order_from.order_details' => 'required|array',
            'order_from.order_details.*.id' => 'required|numeric|exists:order_details,id',
            'order_from.order_details.*.product_id' => 'required|numeric|exists:products,id',
            'order_from.order_details.*.order_table_id' => 'required|numeric|exists:order_tables,id',
            'order_from.order_details.*.order_detail_price' => 'required|numeric',
            'order_from.order_details.*.order_detail_note' => 'nullable|string',
            'order_to.order_details' => 'required|array',
            'order_to.order_details.*.id' => 'required|numeric|exists:order_details,id',
            'order_to.order_details.*.product_id' => 'required|numeric|exists:products,id',
            'order_to.order_details.*.order_table_id' => 'required|numeric|exists:order_tables,id',
            'order_to.order_details.*.order_detail_price' => 'required|numeric',
            'order_to.order_details.*.order_detail_note' => 'nullable|string',
        ];
    }

    public function messages():array
    {
        return [
            'order_from.order_details.*.order_detail_quantity' => 'Chuyển quá số lượng món mà order hiện tại đang có',

            'order_from.id_order_from.required' => 'Trường  trong order không tô tại.',
            'order_from.id_order_from.numeric' => 'Trường "id_order_from" trong "order_from" phải là một số.',
            'order_from.id_order_from.exists' => 'Giá trị "id_order_from" trong "order_from" không tồn tại trong bảng "order_from".',
            'order_from.order_details.required' => 'Trường "order_details" trong "order_from" không được bỏ trống.',
            'order_from.order_details.array' => 'Trường "order_details" trong "order_from" phải là một mảng.',
            'order_from.order_details.*.id.required' => 'Trường "id" trong "order_details" của "order_from" không được bỏ trống.',
            'order_from.order_details.*.id.numeric' => 'Trường "id" trong "order_details" của "order_from" phải là một số.',
            'order_from.order_details.*.id.exists' => 'Giá trị "id" trong "order_details" của "order_from" không tồn tại trong bảng "order_details".',
            'order_from.order_details.*.product_id.required' => 'Order phải có sản phẩm, không được bỏ trống.',
            'order_from.order_details.*.product_id.numeric' => 'Trường "product_id" trong "order_details" của "order_from" phải là một số.',
            'order_from.order_details.*.product_id.exists' => 'Giá trị "product_id" trong "order_details" của "order_from" không tồn tại trong bảng "products".',
            'order_from.order_details.*.order_table_id.required' => 'Trường "order_table_id" trong "order_details" của "order_from" không được bỏ trống.',
            'order_from.order_details.*.order_table_id.numeric' => 'Trường "order_table_id" trong "order_details" của "order_from" phải là một số.',
            'order_from.order_details.*.order_table_id.exists' => 'Giá trị "order_table_id" trong "order_details" của "order_from" không tồn tại trong bảng "order_tables".',
            'order_from.order_details.*.order_detail_price.required' => 'Trường "order_detail_price" trong "order_details" của "order_from" không được bỏ trống.',
            'order_from.order_details.*.order_detail_price.numeric' => 'Trường "order_detail_price" trong "order_details" của "order_from" phải là một số.',
            'order_from.order_details.*.order_detail_note.string' => 'Trường "order_detail_note" trong "order_details" của "order_from" phải là một chuỗi.',
            'order_to.id_order_to.required' => 'Order nhận không tồn tại.',
            'order_to.id_order_to.numeric' => 'Trường "id_order_to" trong "order_to" phải là một số.',
            'order_to.id_order_to.exists' => 'Order không được chọn hoặc không tồn tại.',
            'order_to.order_details.required' => 'Vui lòng nhập món qua order đã được chọn.',
            'order_to.order_details.array' => 'Trường "order_details" trong "order_to" phải là một mảng.',
            'order_to.order_details.*.id.required' => 'Trường "id" trong "order_details" của "order_to" không được bỏ trống.',
            'order_to.order_details.*.id.numeric' => 'Trường "id" trong "order_details" của "order_to" phải là một số.',
            'order_to.order_details.*.id.exists' => 'Giá trị "id" trong "order_details" của "order_to" không tồn tại trong bảng "order_details".',
            'order_to.order_details.*.product_id.required' => 'Trường "product_id" trong "order_details" của "order_to" không được bỏ trống.',
            'order_to.order_details.*.product_id.numeric' => 'Trường "product_id" trong "order_details" của "order_to" phải là một số.',
            'order_to.order_details.*.product_id.exists' => 'Giá trị "product_id" trong "order_details" của "order_to" không tồn tại trong bảng "products".',
            'order_to.order_details.*.order_table_id.required' => 'Trường "order_table_id" trong "order_details" của "order_to" không được bỏ trống.',
            'order_to.order_details.*.order_table_id.numeric' => 'Trường "order_table_id" trong "order_details" của "order_to" phải là một số.',
            'order_to.order_details.*.order_table_id.exists' => 'Giá trị "order_table_id" trong "order_details" của "order_to" không tồn tại trong bảng "order_tables".',
            'order_to.order_details.*.order_detail_price.required' => 'Trường "order_detail_price" trong "order_details" của "order_to" không được bỏ trống.',
            'order_to.order_details.*.order_detail_price.numeric' => 'Trường "order_detail_price" trong "order_details" của "order_to" phải là một số.',
            'order_to.order_details.*.order_detail_note.string' => 'Trường "order_detail_note" trong "order_details" của "order_to" phải là một chuỗi.',
            'order_to.order_details.*.order_detail_quantity' => 'Chuyển món qua bên order khác phải có ít nhất 1 món hoặc số lượng trên 1 ',

        ];
    }
}
