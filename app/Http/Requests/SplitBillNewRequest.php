<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SplitBillNewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'tableFrom.id_order_table' => 'required|exists:order_tables,id',
            'tableFrom.order_details_to' => 'required|array',
            'tableFrom.order_details_to.*.id' => 'required|numeric|exists:order_details,id',
            'tableFrom.order_details_to.*.product_id' => 'required|numeric|exists:products,id',
            'tableFrom.order_details_to.*.order_table_id' => 'required|numeric|exists:order_tables,id',
            'tableFrom.order_details_to.*.order_detail_price' => 'required|numeric',
            'tableFrom.order_details_to.*.order_detail_note' => 'nullable|string',
            'tableFrom.order_details_to.*.order_detail_quantity' => 'required|numeric|min:1',
            'tableFrom.orderDetails.*.order_detail_quantity' => 'required|numeric|min:1',
            'tableFrom.orderDetails' => 'required|array',


        ];
    }

    public function messages():array
    {
        return [
            'tableFrom.orderDetails' => 'Không được để trống món ở bill đang chuyển',
            'tableFrom.order_details.order_detail_quantity' => 'Chuyển quá số lượng món của bill đang có ',
            'tableFrom.id_order_table' => 'Order không tồn tại',
            'tableFrom.order_details_to.required' => 'Sản phẩm bill mới không được bỏ trống.',
            'tableFrom.order_details_to.array' => 'Trường "order_details_to" phải là một mảng.',
            'tableFrom.order_details_to.*.id.required' => 'Trường "id" trong "order_details_to" không được bỏ trống.',
            'tableFrom.order_details_to.*.id.numeric' => 'Trường "id" trong "order_details_to" phải là một số.',
            'tableFrom.order_details_to.*.id.exists' => 'Giá trị "id" trong "order_details_to" không tồn tại trong bảng "order_details".',
            'tableFrom.order_details_to.*.product_id.required' => 'Trường "product_id" trong "order_details_to" không được bỏ trống.',
            'tableFrom.order_details_to.*.product_id.numeric' => 'Trường "product_id" trong "order_details_to" phải là một số.',
            'tableFrom.order_details_to.*.product_id.exists' => 'Giá trị "product_id" trong "order_details_to" không tồn tại trong bảng "products".',
            'tableFrom.order_details_to.*.order_table_id.required' => 'Trường "order_table_id" trong "order_details_to" không được bỏ trống.',
            'tableFrom.order_details_to.*.order_table_id.numeric' => 'Trường "order_table_id" trong "order_details_to" phải là một số.',
            'tableFrom.order_details_to.*.order_table_id.exists' => 'Giá trị "order_table_id" trong "order_details_to" không tồn tại trong bảng "order_tables".',
            'tableFrom.order_details_to.*.order_detail_price.required' => 'Trường "order_detail_price" trong "order_details_to" không được bỏ trống.',
            'tableFrom.order_details_to.*.order_detail_price.numeric' => 'Trường "order_detail_price" trong "order_details_to" phải là một số.',
            'tableFrom.order_details_to.*.order_detail_note.string' => 'Trường "order_detail_note" trong "order_details_to" phải là một chuỗi.',
            'tableFrom.order_details_to.*.order_detail_quantity' => 'Tách bill mới ít nhất phải có 1 món và số lượng trên 1',
            'tableFrom.orderDetails.*.order_detail_quantity' => 'Tách bill mới không đuợc quá số lượng món khi bill hiện tại không đủ ',


        ];
    }
}
