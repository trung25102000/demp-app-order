<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;


class SplitTableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'table_from.id_order_table' => 'required|exists:order_tables,id',
            'table_from.order_details' => 'required|array',
            'table_to.id_table' => 'required|exists:tables,id',
            'table_to.order_details' => 'required|array',
            'table_from.order_details.*.order_detail_quantity' => 'required|numeric|min:1',
            'table_to.order_details.*.id' => 'required|numeric',
            'table_to.order_details.*.product_id' => 'required|numeric',
            'table_to.order_details.*.order_table_id' => 'required|numeric',
            'table_to.order_details.*.order_detail_price' => 'required|numeric',
            'table_to.order_details.*.order_detail_quantity' => 'required|numeric|min:1',
        ];
    }

    public function messages():array
    {
        return [
            'table_from.order_details.*.order_detail_quantity' => 'Bạn đã chuyển quá số lượng món đang có ',

            'table_from.id_order_table.required' => 'Không được bỏ trống ba hiện tại.',
            'table_from.id_order_table.exists' => ' Không tồn tại trong bảng "order_tables.',
            'table_to.id_table.required' => ' Không được bỏ trống bàn cần tách.',
            'table_to.id_table.exists' => 'Không tồn tại trong bảng "tables.',
            'table_to.order_details.required' => ' Bàn mới phải có món mới tách được.',
            'table_to.order_details.array' => 'Món bên bàn nhận phải là một mảng.',
            'table_to.order_details.*.id.required' => 'Trường "id" trong "order_details" không được bỏ trống.',
            'table_to.order_details.*.id.numeric' => 'Trường "id" trong "order_details" phải là một số.',
            'table_to.order_details.*.product_id.required' => 'Trường "product_id" trong "order_details" không được bỏ trống.',
            'table_to.order_details.*.product_id.numeric' => 'Trường "product_id" trong "order_details" phải là một số.',
            'table_to.order_details.*.order_table_id.required' => 'Trường "order_table_id" trong "order_details" không được bỏ trống.',
            'table_to.order_details.*.order_table_id.numeric' => 'Trường "order_table_id" trong "order_details" phải là một số.',
            'table_to.order_details.*.order_detail_price.required' => 'Trường "order_detail_price" trong "order_details" không được bỏ trống.',
            'table_to.order_details.*.order_detail_price.numeric' => 'Trường "order_detail_price" trong "order_details" phải là một số.',
            'table_to.order_details.*.order_detail_quantity' => 'Món phải ít nhất bằng 1 hoặc số lượng trên 1 ',
            'table_from.order_details' => 'Món của bàn hiện tại phải có ít nhất 1 món',

            // Thêm các thông báo lỗi cho các trường khác trong 'order_details' nếu cần thiết
        ];
    }
}
