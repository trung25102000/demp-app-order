<?php

namespace App\Services;

use App\Repositories\InvoicesRepository;
use App\Models\Invoice;
use Carbon\Carbon;

class InvoicesServices
{
    protected $invoicesRepository;
    public function __construct(InvoicesRepository $invoicesRepository)
    {
        $this->invoicesRepository = $invoicesRepository;
    }

    public function findOrfail($id){
        return $this->invoicesRepository->findById($id);
    }
    public function createInvoice($orderTableId, $voucherID)
    {
        $invoice = Invoice::where('order_table_id', $orderTableId)->first();

        if ($invoice) {
            return $invoice;
        } else {
            $dataInvoice = [
                'voucher_id' => $voucherID ? $voucherID : null,
                'order_table_id' => $orderTableId,
                'invoice_status_id' => 0
            ];
            return $this->invoicesRepository->createInvoice($dataInvoice);
        }
    }
    public function createPayment($cashPayment, $invoiceId)
    {
        $data = [
            'invoice_id' => $invoiceId,
            'payment_date' => Carbon::now('Asia/Ho_Chi_Minh')->format("Y-m-d H:i:s"),
            'payment_amount' => $cashPayment['payment_amount']
        ];
        return $this->invoicesRepository->createPayment($data);
    }
    public function updateStatusInvoice($invoiceId, $status)
    {
        return $this->invoicesRepository->updateStatusInvoice($invoiceId, $status);
    }
    public function updateVoucherInInvoice($invoiceId, $voucherId)
    {
        return $this->invoicesRepository->updateVoucherInInvoice($invoiceId, $voucherId);
    }
    public function updateVoucherInOrderTable($orderTableId, $voucherId)
    {
        return $this->invoicesRepository->updateVoucherInOrderTable($orderTableId, $voucherId);
    }
    public function getOneObjectInvoice($invoiceId){
        return $this->invoicesRepository->getOneObjectInvoice($invoiceId);
    }

    public function updateStatusOrderTable($orderTableId, $status)
    {
        return $this->invoicesRepository->updateStatusOrderTable($orderTableId, $status);
    }

    public function updateStatusTable($orderTableId, $status)
    {
        return $this->invoicesRepository->updateStatusTable($orderTableId, $status);
    }

    public function updateTimeInOrder($orderTableId)
    {
        return $this->invoicesRepository->updateTimeInOrder($orderTableId);
    }
}
