<?php
namespace App\Services;

use App\Repositories\TableRepository;

class TableServices
{
    protected $tableRepository;

    /**
     * @param TableRepository $tableRepository
     */
    public function __construct(TableRepository $tableRepository)
    {
        $this->tableRepository=$tableRepository;
    }
    public function getOneLineTable($tableId)
    {
        return $this->tableRepository->getOneLineTable($tableId);
    }

    /**
     * @param $perPage
     * @return mixed
     */
    public function getAllItems($perPage)
    {
        return $this->tableRepository->getAll($perPage);
    }

    public function getTableByOrderTable($tableid)
    {
        return $this->tableRepository->getTableByOrderTable($tableid);
    }

    public function getById($id)
    {
        return $this->tableRepository->getById($id);
    }

    public function getAllByStatus($status)
    {
        return $this->tableRepository->getAllByStatus($status);
    }

    /**
     * @param $searchValue1
     * @param $searchValue2
     * @return mixed
     */
    public function searchItems($searchString, $statusId, $AreaId,$perPage)
    {
        return $this->tableRepository->searchItems($searchString, $statusId, $AreaId,$perPage);
    }

    public function getAllTable($idTable){
        return $this->tableRepository->getAllTable($idTable);
    }
    public function exists ($id){
        return $this->tableRepository->exists($id);
    }
    /**
     * @param $tableid
     * @return array
     */
    public function  getTablesInSameArea($tableid)
    {
        return $this->tableRepository->getTablesInSameArea($tableid);
    }

    /**
     * @param $tableid
     * @return void
     */
    public function changeStatusAddtable($tableid)
    {
        return $this->tableRepository->changeStatusAddtable($tableid);
    }

    public function getListTableCanOrder()
    {
        return $this->tableRepository->getListTableCanOrder();
    }

    public function updateStatusTable($tableID, $status)
    {
        return $this->tableRepository->updateStatusTable($tableID, $status);
    }
    /**
     * Lấy tất cả danh sách trả món theo table
     * @return mixed
     */
    public function getTableWithTable($searchString)
    {
        return  $this->tableRepository->getTableWithTable($searchString);
    }

    /**
     * lấy bàn gốc ghép với bàn hiện tại
     * @param $tableId
     * @return void
     */
    public function getTableTheAdd($tableId)
    {
        return $this->tableRepository->getTableTheAdd($tableId);
    }
}
