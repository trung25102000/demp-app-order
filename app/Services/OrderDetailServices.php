<?php

namespace App\Services;

use App\Repositories\OrderDetailRepository;
use App\Repositories\OrderTableRepository;

class OrderDetailServices
{
    protected $orderDetailRepository;
    protected $orderTableRepository;

    /**
     * @param $orderTableRepository
     */
    public function __construct(OrderDetailRepository $orderDetailRepository, OrderTableRepository $orderTableRepository)
    {
        $this->orderDetailRepository = $orderDetailRepository;
        $this->orderTableRepository = $orderTableRepository;
    }

    public function createOrderDetail($orderTableId, $data)
    {
        $dataOrderDetail = [];

        foreach ($data['orderItems'] as $key => $orderDetail) {
            $dataItem = [
                'product_id' => array_key_exists("id", $orderDetail) ? $orderDetail['id'] : null,
                'order_table_id' => $orderTableId,
                'order_detail_price' => $orderDetail['product_price'],
                'order_detail_note' => (array_key_exists('order_detail_note', $orderDetail) ? $orderDetail['order_detail_note'] : ""),
                'order_detail_other_name' => $orderDetail['product_name'],
                'order_detail_quantity' => $orderDetail['quantity'],
                'order_detail_status' => 1,
                'order_detail_other_unit_name'=>(array_key_exists('order_detail_other_unit_name', $orderDetail) ? $orderDetail['order_detail_other_unit_name'] : null)
            ];
            array_push($dataOrderDetail, $dataItem);
        }

        return $this->orderTableRepository->createOrderDetailByOrderTable($orderTableId, $dataOrderDetail);
    }

    public function getOrderDetailInEdit($orderTableId)
    {
        return $this->orderDetailRepository->getOrderDetailInEdit($orderTableId);
    }

    public function findById($id){
        return $this->orderDetailRepository->findById($id);
    }

    public function getProductInOrderDetail($orderTableId){
        return $this->orderDetailRepository->getProductInOrderDetail($orderTableId);
    }

    public function getAllOrderDetailInOrderTableId($orderTableId){
        return $this->orderDetailRepository->getAllOrderDetailInOrderTableId($orderTableId);
    }
    /**
     * Lấy danh sách các món thuộc loại hình bar or bếp theo order_table_id
     * @param $orderID
     * @param $selectedValue
     * @return mixed
     */
    public function getOrderDetailByStatusAndOrderTableId($orderID,$selectedValue)
    {
        return $this->orderDetailRepository->getOrderDetailByStatusAndOrderTableId($orderID,$selectedValue);
    }

    /**
     * Lấy danh sách orderDetails theo mã orderTableId và trạng thái của món ăn đó
     * @param $status
     * @param $orderTableId
     * @return mixed
     */
    public function getOrderDetailByStatus($status,$orderTableId)
    {
        return $this->orderDetailRepository->getOrderDetailByStatus($status,$orderTableId);
    }

    /**
     * Lấy danh sách các món thuộc loại hình bar or bếp theo table_id
     * @param $orderID
     * @param $selectedValue
     * @return mixed
     */
    public function getOrderDetailByStatusAndTableId($tableId,$selectedValue)
    {
        return $this->orderDetailRepository->getOrderDetailByStatusAndTableId($tableId,$selectedValue);
    }

}
