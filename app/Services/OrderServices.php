<?php

namespace App\Services;

use App\Models\OrderDetail;
use App\Repositories\OrderRepository;

class OrderServices
{
    protected $OrderRepository;

    /**
     * @param $OrderRepository
     */
    public function __construct(OrderRepository $OrderRepository)
    {
        $this->OrderRepository = $OrderRepository;
    }

    public function getAllOrder($perPage)
    {
        return $this->OrderRepository->getAllOrder($perPage);
    }

    public function updateOrderDetailStatus($orderDetailId, $status)
    {
        $orderDetail = OrderDetail::find($orderDetailId);

        if ($orderDetail) {
            $orderDetail->status = $status;
            $orderDetail->save();
        }
    }


    public function getOrderTable($orderTableId)
    {
        return $this->OrderRepository->getOrderTable($orderTableId);
    }

    public function getOrderDetail($orderTableId)
    {
        return $this->OrderRepository->getOrderDetail($orderTableId);
    }

    public function getAllOrderDetailsByOrderTableId($orderId)
    {
        return $this->OrderRepository->getAllOrderDetailsByOrderTableId($orderId);
    }

    public function getAllOrderDetailsByBillId($orderId)
    {
        return $this->OrderRepository->getAllOrderDetailsByBillTableId($orderId);
    }


    public function moveToKitchen($orderTableId)
    {
        return $this->OrderRepository->moveToKitchen($orderTableId);
    }
    public function getOneLineOrder($orderId)
    {
        return $this->OrderRepository->getOneLineOrder($orderId);
    }
    public function getOneLineVoucher($voucherId)
    {

        return $this->OrderRepository->getOneLineVoucher($voucherId);
    }
    public function searchInListOrder($searchString, $statusId, $perPage, $orderTypeId)
    {
        return  $this->OrderRepository->searchInListOrder($searchString, $statusId, $perPage, $orderTypeId);
    }

    public function deleteDetail($detail_id)
    {
        return $this->OrderRepository->deleteDetail($detail_id);
    }

    public function finishOrder($order_id)
    {
        return $this->OrderRepository->finishOrder($order_id);
    }

    public function saveDetail($detail_id, $quantity, $note)
    {
        return $this->OrderRepository->saveDetail($detail_id, $quantity, $note);
    }

    public function cancelOrder($order_id, $reason)
    {
        return $this->OrderRepository->cancelOrder($order_id, $reason);
    }

    public function getVoucher()
    {
        $voucher = $this->OrderRepository->getVoucher();
        return $voucher;
    }
    public function getVoucherById($voucher_id)
    {
        $voucher = $this->OrderRepository->getVoucherById($voucher_id);
        return $voucher;
    }

    public function addDetailInEditOrder($orderId, $productId, $orderDetailQuantity, $orderDetailPrice)
    {
        return $this->OrderRepository->addDetailInEditOrder($orderId, $productId, $orderDetailQuantity, $orderDetailPrice);
    }

    public function getUser()
    {
        return $this->OrderRepository->getUser();
    }

    public function addOtherDetail($orderTableId, $orderOtherName, $orderOtherPrice, $orderOtherQuantity)
    {
        return $this->OrderRepository->addOtherDetail($orderTableId, $orderOtherName, $orderOtherPrice, $orderOtherQuantity);
    }

    /**
     * Lấy tất cả món ăn được chuyển xuống bếp/bar
     * @return mixed
     */
    public function getAllOrderDetails($CategoryId, $searchString)
    {
        return $this->OrderRepository->getAllOrderDetails($CategoryId, $searchString);
    }

    /**
     * Lấy tất cả danh sách order_table theo order
     * @return mixed
     */
    public function getOrderTableWithOrder($searchString)
    {
        return $this->OrderRepository->getOrderWithOrder($searchString);
    }

    /**
     * Lấy tất cả danh sách đồ uống trả xuống bar
     * @return mixed
     */
    public function getAllOrderDetailsForBar()
    {
        return $this->OrderRepository->getAllOrderDetailsForBar();
    }

    /**
     * Lấy tất cả danh sách order_table theo order chuyển xuống bar
     * @return mixed
     */
    public function getOrderTableWithOrderForBar()
    {
        return $this->OrderRepository->getOrderTableWithOrderForBar();
    }

    /**
     * Lấy tất cả danh sách order_table theo table xuống bar
     * @return mixed
     */
    public function getOrderTableWithTableForBar()
    {
        return $this->OrderRepository->getOrderTableWithTableForBar();
    }

    public function getDiscountAmount($voucherId)
    {
        return $this->OrderRepository->getDiscountAmount($voucherId);
    }

    public function createOrder($data)
    {
        $dataOrder = [
            'order_type_id' => $data['status'],
            'order_create_time' => now()->format("Y-m-d H:i:s"),
            'order_note' => (isset($data['type_order_data']) ? json_encode($data['type_order_data']) : null)
        ];

        return $this->OrderRepository->create($dataOrder);
    }
    public function createOrderSplitTable($orderOld)
    {
        $dataOrder = [
            'order_type_id' => $orderOld->order_type_id = 1,
            'order_create_time' => now()->format("Y-m-d H:i:s")
        ];

        return $this->OrderRepository->create($dataOrder);
    }

    public function createOrderSplitBill($orderOld, $orderTableId)
    {
        $dataOrder = [
            'order_type_id' => $orderOld->order_type_id,
            'order_create_time' => now()->format("Y-m-d H:i:s"),
            'order_table_id' => $orderTableId // Thêm order_table_id mới vào đây
        ];

        return $this->OrderRepository->create($dataOrder);
    }
    /**
     * Lấy danh sách orderTable đang chế biến thuộc loại hình ăn tại chỗ
     * @return mixed
     */
    public function getPaymentListUsing($perPage)
    {
        return $this->OrderRepository->getPaymentListUsing($perPage);
    }

    /**
     * Lấy danh sách orderTable đang chế biến thuộc loại hình giao hàng
     * @return mixed
     */
    public function getPaymentListShip($perPage)
    {
        return $this->OrderRepository->getPaymentListShip($perPage);
    }

    /**
     * Lấy danh sách orderTable đang chế biến thuộc loại hình đặt trước
     * @return mixed
     */
    public function getPaymentListReserve($perPage)
    {
        return $this->OrderRepository->getPaymentListReserve($perPage);
    }

    /**
     * Lấy danh sách orderTable đang chế biến thuộc loại hình mang về
     * @return mixed
     */
    public function getPaymentListBringBack($perPage)
    {
        return $this->OrderRepository->getPaymentListBringBack($perPage);
    }

    public function updateOrder($order)
    {
        return $this->OrderRepository->update($order);
    }

    public function filterProductByCategories($categoryId)
    {
        return $this->OrderRepository->filterProductByCategories($categoryId);
    }

    public function bestSellerOfProduct()
    {
        return $this->OrderRepository->bestSellerOfProduct();
    }

    /**
     * Lấy danh sách orderTable đã được tạo thuộc loại hình ăn tại chỗ
     * @return mixed
     */
    public function getOrderListOrderType($perPage, $orderTypeId, $searchValue, $statusId)
    {
        return $this->OrderRepository->getOrderListOrderType($perPage, $orderTypeId, $searchValue, $statusId);
    }

    /**
     * Lấy danh sách ordertable theo loại hình với status!=4
     * @param $perPage
     * @param $orderTypeId
     * @param $searchValue
     * @param $statusId
     * @return mixed
     */
    public function getOrderListOrderTypeById($orderTypeId)
    {
        return $this->OrderRepository->getOrderListOrderTypeById($orderTypeId);
    }
    public function getById($id)
    {
        return $this->OrderRepository->getById($id);
    }

    /**
     * Ghép bàn theo loại hình muốn ghép
     * @param $orderId
     * @param $idTables
     * @param $checkedValue
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function AddTable($orderId, $idTables, $checkedValue)
    {
        return $this->OrderRepository->AddTable($orderId, $idTables, $checkedValue);
    }

    /**
     * Tao hoa don
     * @param $id:id cua orderTable
     * @return void
     */
    public function createInvoice($invoice)
    {
        return $this->createInvoice($invoice);
    }


    public function reimbursed($detailId)
    {
        return $this->OrderRepository->reimbursed($detailId);
    }
    public  function reimbursedManyTable($dataTable)
    {
        return $this->OrderRepository->reimbursedManyTable($dataTable);
    }
    public  function reimbursedManyOrder($dataOrder)
    {
        return $this->OrderRepository->reimbursedManyOrder($dataOrder);
    }
    public function reimburseForTable($id)
    {
        return $this->OrderRepository->reimburseForTable($id);
    }
    public function reimburseForOrder($id)
    {
        return $this->OrderRepository->reimburseForOrder($id);
    }
    public function reimburseForTableByOrder($id)
    {
        return $this->OrderRepository->reimburseForTableByOrder($id);
    }
    public function reimbursedManyProduct($dataProduct)
    {
        return $this->OrderRepository->reimbursedManyProduct($dataProduct);
    }
    public function cancelDetail($detailId, $order_detail_cancel_reason)
    {
        return $this->OrderRepository->cancelDetail($detailId, $order_detail_cancel_reason);
    }
}
