<?php

namespace App\Services;

use App\Repositories\CustomerRepository;

class CustomerServices
{
    protected $customerRepository;

    /**
     * @param $orderTableRepository
     */
    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;

    }

    public function create($data)
    {
        return $this->customerRepository->create($data);
    }
    public function getCustomer(){
        return $this->customerRepository->getCustomer();
    }
    public function searchCustomer($keyword)
    {
        return $this->customerRepository->searchCustomer($keyword);
    }
    public function getCustomerById($customer_id)
    {
        return $this->customerRepository->getCustomerById($customer_id);
    }
}
