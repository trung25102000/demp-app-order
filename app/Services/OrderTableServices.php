<?php

namespace App\Services;

use App\Models\OrderDetail;
use App\Models\Table;
use App\Repositories\OrderTableRepository;

class OrderTableServices
{
    protected $orderTableRepository;
    protected $table;

    /**
     * @param $orderTableRepository
     */
    public function __construct(Table $table,OrderTableRepository $orderTableRepository)
    {
        $this->orderTableRepository = $orderTableRepository;
        $this->table = $table;
    }

    public function getById($id)
    {
        return $this->orderTableRepository->getById($id);
    }

    public function findById($id)
    {
        return $this->orderTableRepository->findById($id);
    }

    /**
     * @param $perPage
     * @return \App\Repositories\LengthAwarePaginator
     */
    public function getAllOrderTable($perPage)
    {
        return $this->orderTableRepository->getAllOder($perPage);
    }
    public function getAllTableStatusOnline()
    {
        return $this->orderTableRepository->getAllTableStatusOnline();
    }

    /**
     * @param $orderId
     * @return null
     */
    public function getItem($orderId)
    {
        return $this->orderTableRepository->getItem($orderId);
    }

    public function getAllOrderDetailsInOrder()
    {
        return $this->orderTableRepository->getAllOrderDetailsInOrder();
    }

    public function getOrderDetailInOrderTable()
    {
        return $this->orderTableRepository->getOrderDetailInOrderTable();
    }

    public function createOrderDetail($data)
    {
        return $this->orderTableRepository->createOrderDetail($data);
    }

    public function getAllOrderTableRender()
    {
        return $this->orderTableRepository->getAllOrderTableRender();
    }

    public function getAllOrderTableStatusNotpayment (){
        return $this->orderTableRepository->getAllOrderTableStatusNotpayment();
    }
    public function deleteByOrderTableId(int $fromTableId)
    {
        return $this->orderTableRepository->deleteByOrderTableId($fromTableId);
    }

    public function getByTableId(int $tableId)
    {
        return $this->orderTableRepository->getByTableId($tableId);
    }

    public function getTable($tableId)
    {
        return $this->orderTableRepository->getTableById($tableId);
    }

    public function getMenuItems($tableId)
    {
        return $this->orderTableRepository->getMenuItemsByTableId($tableId);
    }

    public function createOrderTable($customerID, $orderId, $data)
    {
        $dataOrderTable = [
            'order_id' => $orderId,
            'table_id' => (($data['status'] == 2 || $data['status'] == 4) ? null : (isset($data['table_id']) ? $data['table_id'] : null)),
            'customer_id' => (isset($customerID) ? $customerID : null),
            'order_time' => now()->format("Y-m-d H:i:s"),
            'user_id' => $data['user_id'],
            'count_of_customer' => $data['count_of_customer'],
            'status' => 1,
            'display_order' => 1,
            'voucher_id' =>(isset($data['voucher_id']) ? $data['voucher_id'] : null),
        ];
        return $this->orderTableRepository->createOrderTable($dataOrderTable);
    }

    public function saveOrderTable($orderTableId, $userId, $customerId, $countCustomer, $typeOrder, $tableId, $orderItems, $voucherId)
    {
        return $this->orderTableRepository->saveOrderTable($orderTableId, $userId, $customerId, $countCustomer, $typeOrder, $tableId, $orderItems, $voucherId);
    }

    public function updateQuantity($productId, $quantity, $orderTableId)
    {
        return $this->orderTableRepository->updateQuantity($productId, $quantity, $orderTableId);
    }

    public function getOneLineOrderTable($orderTableId){
        return $this->orderTableRepository->getOneLineOrderTable($orderTableId);
    }
    public function deleteOrderTableAndOrderDetail($idOrderTable) {
        $orderTable = $this->getById($idOrderTable);
        //delete orderdetail
        $orderTable->orderDetail()->delete();
        //delete orderTable
        return $orderTable->delete();
    }

    public function deleteOrderTableAndOrderDetailUp($orderTableId)
    {
        // Xóa orderDetail liên quan
       OrderDetail::where('order_table_id', $orderTableId)->delete();

        // Cập nhật lại orderDetail trong id order table
        OrderDetail::where('order_table_id', $orderTableId)
            ->update(['order_table_id' => null]);
    }

    public function create($data)
    {
        return $this->orderTableRepository->create($data);
    }

    public function createOrderTableTaskSplit($order, $idTable, $orderTableOld)
    {
        $dataOrderTable = [
            'order_id' => $order->id,
            'table_id' => $idTable,
            'customer_id' => $orderTableOld->customer_id,
            'order_time' => now()->format("Y-m-d H:i:s"),
            'user_id' => $orderTableOld->user_id,
            'count_of_customer' => $orderTableOld->count_of_customer,
            'display_order' => 1,
            'status'=>$orderTableOld->status,
            'voucher_id'=>$orderTableOld->voucher_id
        ];

        $orderTable = $this->orderTableRepository->create($dataOrderTable);

        // Set trường status trong bảng table
        $table = $this->table::find($idTable);
        if ($table) {
            $table->status = 3;
            $table->save();
        }

        return $orderTable;
    }

    public function createOrderDetails($orderTableId, $orderDetails)
    {
        $dataOrderDetail = [];

        foreach ($orderDetails as $key => $orderDetail) {
            $dataItem = [
                'product_id' => array_key_exists("product_id", $orderDetail) ? $orderDetail['product_id'] : null,
                'order_table_id' => $orderTableId,
                'order_detail_price' => $orderDetail['order_detail_price'],
                'order_detail_note' => array_key_exists('order_detail_note', $orderDetail) ? $orderDetail['order_detail_note'] : null,
                'order_detail_other_name' => (array_key_exists('product_name', $orderDetail) ? $orderDetail['product_name'] : ""),
                'order_detail_quantity' => $orderDetail['order_detail_quantity'],
                'order_detail_status' => (array_key_exists('order_detail_status', $orderDetail) ? $orderDetail['order_detail_status'] : 1),
            ];

            $dataOrderDetail[] = $dataItem;

        }

        return $this->orderTableRepository->createOrderDetailByOrderTable($orderTableId, $dataOrderDetail);
    }

    /**
     * Lấy danh sách orderTable theo status
     * @param $status
     * @return mixed
     */
    public function getOrderTableByStatus($status)
    {
        return $this->orderTableRepository->getOrderTableByStatus($status);
    }

    /**
     * Xóa Ordertable với các trương hợp ghép bàn
     * @param $OrdeTable
     * @return bool
     */
    public function deleteOrderTable($OrdeTable)
    {
        return $this->orderTableRepository->deleteOrderTable($OrdeTable);
    }

    /**
     * lấy danh sách orderTable để ghép bill
     * @return void
     */
    public function getOrderTableToAddBill($orderTableId)
    {
        return $this->orderTableRepository->getOrderTableToAddBill($orderTableId);
    }
}
