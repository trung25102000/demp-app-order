<?php


namespace App\Services;

use App\Repositories\ProductRepository;

class ProductServices
{
    protected $productRepository;

    /**
     * @param $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }
    public function getAllProduct(){
        return $this->productRepository->getAllProduct();
    }
    public function getProductWithUnit()
    {
        return $this->productRepository->getProductWithUnit();
    }
    public function searchProduct($keyword, $categoryId)
    {
        return $this->productRepository->searchProduct($keyword, $categoryId);
    }

}
