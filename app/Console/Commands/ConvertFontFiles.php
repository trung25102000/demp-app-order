<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Intervention\Image\Facades\Image;

class ConvertFontFiles extends Command
{
    protected $signature = 'convert:font-files';

    protected $description = 'Convert TrueType font file to Laravel-compatible font files';

    public function handle()
    {
        $fontFile = public_path('fonts/TCPSD.ttf');
        $outputDirectory = public_path('fonts/laravel');
        $outputFormat = 'png'; // Change this to 'jpg' if desired

        $font = new \Imagick();
        $font->setFont($fontFile);

        if (!file_exists($outputDirectory)) {
            mkdir($outputDirectory, 0777, true);
        }

        for ($size = 10; $size <= 100; $size += 10) {
            $filename = $outputDirectory . '/' . $size . '.' . $outputFormat;
            $image = Image::canvas($size + 5, $size + 5);
            $image->text('A', 2, $size + 2, function ($font) use ($size) {
                $font->file(public_path('fonts/TCPSD.ttf'));
                $font->fontSize($size);
                $font->color('#000000');
                $font->align('center');
                $font->valign('top');
            });
            $image->save($filename);
        }

        $this->info('Font files converted successfully!');
    }
}
