@extends('layouts.app')
@section('title','Create Order')

@section('style-after')
    <link rel="stylesheet" href="{{asset('order/css/create-order.css')}}">
@endsection

@section('content')
    <!--Start #main-->
    <div>
        <div class="container-fluid mt-2">
            <div class="row">
                <div class="col-lg-7 col-md-6 card mb-3 mb-md-0">
                    <div class="bg-light mt-1">
                        <div class="d-flex mb-1 bg-white">
                            <div class="border btn px-2 me-1 categoryFilter" id="all-products" style="background-color: lightblue">
                                <a class="nav-link me-2 fw-bold set-fontsize" href="#">Tất cả</a>
                            </div>
                            <div class="border btn px-2 me-1 categoryFilter" id="best-seller">
                                <a class="nav-link me-2 fw-bold set-fontsize" href="#">Bán chạy</a>
                            </div>
                            @foreach($categories as $category)
                                <div class="border btn px-2 me-1 categoryFilter" data-id="{{$category->id}}">
                                    <a class="nav-link me-2 fw-bold set-fontsize" href="#">{{$category->category_name}}</a>
                                </div>
                            @endforeach
                        </div>
                        <!--Thanh search-->
                        <div class="mb-2">
                            <div class="input-group">
                                <div class="col-10 col-md-11">
                                    <input id="searchNameEating" type="text" class="form-control"
                                           placeholder="Tìm kiếm theo mã món, tên viết tắt, tên đầy đủ.">
                                </div>
                                <i class="col-2 col-md-1 btn form-control bi bi-search text-white"
                                   style="background-color: #06357a"></i>
                            </div>
                        </div>
                        <!-- Nội dung ảnh -->
                        <div id="food-list" class="d-flex flex-wrap bg-white over-flow-auto"
                             style="overflow: auto; max-height: 675px">
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-6 card shadow" style="margin-bottom:0px !important">
                    <div class="bg-light mt-1">
                        <div class="mb-1 d-flex align-items-center bg-white">
                            <div class="search-bar" style="max-width: 40%">
                                <form class="search-form d-flex align-items-center" method="POST" action="#">
                                    <select name="table_id" id="table_id" class="form-control text-dark"
                                            style="background-color: lightblue; height:38px; width: 130px">
                                        <option value="" disabled selected>Bàn số</option>
                                        @foreach ($listTable as $table)
                                            <option
                                                    value="{{$table->id}}" {{$tableSelected && $tableSelected == $table->id ? "selected" : "" }}>
                                                Bàn  {{$table->tableArea->table_area_key.''.$table->tableArea->table_floor .''.sprintf("%02d", $table->table_name)}}</option>
                                        @endforeach
                                    </select>
                                </form>
                            </div>
                            <div class="d-flex me-2" style="max-width: 25%">
                                <button class="form-control mx-1" style="height:38px;" data-bs-toggle="modal"
                                        data-bs-target="#verticalycentered">
                                    <i class="bi bi-person-fill ms-2"></i>
                                </button>
                                <input class="form-control text-dark" value="4" name="count_of_customer"
                                       style="width: 2.5rem; height: 38px; text-align:center; border: none;background-color: lightblue"/>
                            </div>
                            <!-- Chọn loại Order-->
                                <div class="dropdown" style="max-width: 35%">
                                    <button class="btn text-white dropdown-toggle mx-2 status_order" type="button"
                                            id="dropdownMenuButton"
                                            data-bs-toggle="dropdown" aria-expanded="false"
                                            style="background-color: #06357a; width: 110px" data-status-order="">
                                        Loại Order
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <li><a class="dropdown-item type-order" href="#" data-value="Tại chỗ"
                                               data-status="1">Tại chỗ</a></li>
                                        <li><a class="dropdown-item type-order" href="#" data-value="Mang về"
                                               data-bs-toggle="modal"
                                               data-bs-target="#back"
                                               data-status="2">Mang về</a></li>
                                        <li><a class="dropdown-item type-order" href="#" data-value="Đặt trước"
                                               data-bs-toggle="modal"
                                               data-bs-target="#reserved" data-status="3">Đặt trước</a></li>
                                        <li><a class="dropdown-item type-order" href="#" data-value="Giao tận nơi"
                                               data-bs-toggle="modal"
                                               data-bs-target="#ship" data-status="4">Giao tận nơi</a></li>
                                    </ul>
                                </div>
                        </div>
                        {{-- Show errors --}}
                        <div class="error error-table_id d-none">
                            <small class="text-danger">></small>
                        </div>
                        <div class="error error-count_of_customer d-none">
                            <small class="text-danger">></small>
                        </div>
                        <div class="error error-status d-none">
                            <small class="text-danger">></small>
                        </div>
                        <div class="" style="overflow: auto; height: 492px">
                            <!-- Tạo mẫu đang chờ để phục vụ -->
                            <div
                                    class="picture-model h-100 d-flex flex-column align-items-center justify-content-center"
                                    style=" position: relative">
                                <div style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;
                background-color: rgba(37, 36, 36, 0.31); pointer-events: none;">
                                </div>
                                <img src="order/anhbep.png" alt=""
                                     style="width: 250px; height: 250px; margin-bottom: 2rem;">
                                <p style="font-weight: 500; line-height:1.2rem; text-align: center; color: #656464">Quý
                                    khách vui lòng click vào hình ảnh món ăn để tiến hành chọn món</p>
                            </div>
                            <table class="order-detail-eating d-none table table-striped text-active mb-5">
                                <thead>
                                <tr class="" style="background-color: #e7e6e6; font-weight: 600;">
                                    <th scope="col">Tên món</th>
                                    <th scope="col">Đơn vị</th>
                                    <th scope="col">Số lượng</th>
                                    <th scope="col">Thành tiền</th>
                                    <th scope="col">Xoá</th>
                                </tr>
                                </thead>
                                <tbody id="tbody">

                                </tbody>
                            </table>
                        </div>
                        <div class="error error-orderItems d-none">
                            <small class="text-danger">></small>
                        </div>
                        <div>
                            <div class="mb-2">
                                <div class="d-flex align-items-center">
                                    <!--Button them mon khac-->
                                    <div class="mt-2 pe-2">
                                        <button
                                                class="form-control bg-light d-flex align-items-center justify-content-left"
                                                style=" height:48px; width:15rem; "
                                                type="button" title="Search" data-bs-toggle="modal"
                                                data-bs-target="#exampleModal" data-bs-whatever="@mdo">
                                            <i class="bi bi-plus-lg text-success" style="font-size: 1.5rem"></i> Thêm
                                            món khác
                                        </button>
                                    </div>
                                    <!-- Button gift -->
                                    <div
                                            class="form-control bg-light d-flex align-items-center justify-content-center mt-2"
                                            style=" height:48px; width:64px; font-size: 1.5rem" data-bs-toggle="modal"
                                            data-bs-target="#voucherInEdit2" data-bs-whatever="@mdo">
                                        <i class="bi bi-gift text-danger"></i>
                                    </div>
                                </div>
                            </div>
                            <hr class="my-2">
                            <div class="row set-margin" style="margin-bottom: 1.7rem">
                                <div class="col-md-6 search-bar pe-2 mb-0 mb-sm-1 ">
                                    <select class="form-select" style="height:48px; width: 80%" name="user_id">
                                        <option value="" selected>Nhân viên phục vụ</option>
                                        @foreach ($userList as $user)
                                            <option value="{{$user->id}}">{{$user->user_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6 d-flex align-items-center justify-content-between mt-2">
                                    <div class="d-flex align-items-center">
                                        <i class="bi bi-exclamation-circle text-primary me-1"
                                           style="font-size: 1.5rem;"></i>
                                        <span style="font-size: 1.2rem; font-weight: 600;">Tổng tiền</span>
                                    </div>
                                    <div class="fw-bold" id="total-money-order"
                                         style="font-size: 1.2rem; font-weight: 600;">0 đ
                                    </div>
                                </div>
                            </div>
                            <div class="error error-user_id d-none">
                                <small class="text-danger">></small>
                            </div>
                            <!-- Thanh tính tiền -->
                            <div class="d-flex justify-content-between align-items-center"
                            style="background-color: rgba(206, 206, 210, 0.77);">
                                <div class="justify-content-between align-items-center p-2">
                                    <button class="btn btn-store-order" style="background-color: #0C134F; border-color: #0973b9; width: 146px;">
                                        <a href="#"
                                           class="d-flex align-items-center text-white fw-bold justify-content-center">
                                            <i class="bi bi-send fs-3"></i>
                                            <span style="padding: 0 0.5rem" class="pe-2">LƯU</span>
                                        </a>
                                    </button>
                                    <button class="btn bg-danger" style="background-color: #fff; border-color: #0973b9; width: 146px">
                                        <a href="{{route('orders.home')}}"
                                           class="mx-2 d-flex align-items-center text-white fw-bold justify-content-center">
                                            <i class="bi bi-x-lg fs-3"></i>
                                            <span style="padding: 0 0.5rem"  class="pe-2">HUỶ</span>
                                        </a>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End #main -->
    <!--Modal-->
    @include('order.create-order.list-modal-in-create-order')
    <!--End Modal-->
@endsection


@section('script-after')
    <script>
        const URL_STORE_ORDER = @json(route('orders.store_order'));
        const URL_LIST_ORDER = @json(route('orders.list_order'));
    </script>
    <script src="{{asset('order/js/create-order.js')}}"></script>
    <script src="{{asset('order/assets/js/split-table.js')}}"></script>
@endsection
