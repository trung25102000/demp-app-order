<!-- Modal thêm món khác-->
<div class="modal fade" id="exampleModal" tabindex="-1"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"
                 style="background-color: #06357A; color: #fff">
                <h5 class="modal-title" id="exampleModalLabel">Thêm món khác</h5>
            </div>
            <div class="modal-body">
                <form>
                    <div class="mb-3">
                        <label for="food-name" class="col-form-label">Tên
                            món</label>
                        <input type="text" class="form-control"
                               id="other-food-name">
                    </div>
                    <div class="mb-3">
                        <label for="food-note" class="col-form-label">Đơn vị
                        </label>
                        <input type="text" class="form-control"
                               id="other-food-unit">
                    </div>
                    <div class="mb-3">
                        <label for="food-quantity" class="col-form-label">Số
                            lượng</label>
                        <input type="text" class="form-control"
                               id="other-food-quantity" oninput="this.value = this.value.replace(/[^0-9]/g, '')">
                    </div>
                    <div class="mb-3">
                        <label for="food-price" class="col-form-label">Đơn
                            giá</label>
                        <input type="text" class="form-control"
                               id="other-food-price" oninput="this.value = this.value.replace(/[^0-9]/g, '')">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                        data-bs-dismiss="modal">Huỷ
                </button>
                <button type="button" class="btn btn-primary" id="add-food-button">
                    Thêm món
                </button>
            </div>
        </div>
    </div>
</div>
<!-- Modal thêm khuyến mãi cho khách -->
<div class="modal fade" id="voucherInEdit2" tabindex="-1"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"
                 style="background-color: #0973b9; color: #fff">
                <h5 class="modal-title" id="exampleModalLabel">Chọn chương trình
                    khuyến mãi</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal"
                        aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="mb-5 d-flex flex-column">
                        <label class="col-form-label fs-2 h3">Voucher dành cho khách hàng</label>
                        @foreach ($voucher as $item)
                            <div class="mb-2">
                                <input type="checkbox" value="{{$item->id}}" id="voucher-in-edit" discount-amount="{{$item->discount_amount}}"
                                       name="apply-voucher-id"
                                       data-description="{{$item->voucher_description}}"
                                       style="height: 1rem; width: 1rem" class="checkbox-select voucher-checkbox">
                                <span class="description-voucher">{{$item->voucher_description}}</span>
                            </div>
                        @endforeach
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                        data-bs-dismiss="modal">Huỷ
                </button>
                <button type="button" class="btn btn-primary" id="save-modal-voucher-edit">Lưu</button>
            </div>
        </div>
    </div>
</div>

<!--Modal thanh vien-->
<div class="modal modal-lg" id="verticalycentered" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Thành viên</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal"
                        aria-label="Close"></button>
            </div>
            <div class="modal-body row">
                <div class="d-flex justify-content-between">
                    <div class="col-lg-9">
                        <input class="form-control search-customer" type="text"
                               placeholder="Nhập tên/ số điện thoại  khách hàng">
                    </div>
                    <button type="button" class="form-control btn btn-primary"
                            style="height: 38px; width: 150px" type="submit"
                            title="Search"
                            data-bs-toggle="modal" data-bs-target="#modal2">
                        Thêm mới
                    </button>
                </div>

                <div>
                    <p class="mt-2" style="font-weight: bold; font-size: 18px">Khách
                        hàng thành viên</p>
                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Tên</th>
                            <th scope="col">Ngày sinh</th>
                            <th scope="col">SĐT</th>
                            <th scope="col" colspan="2">Ngày ĐK</th>
                        </tr>
                        </thead>
                        <tbody id="customerTableBody">
                        @foreach ($customers as $customer)
                            <tr>
                                <th scope="row">{{$customer->id}}</th>
                                <td>{{$customer->customer_name}}</td>
                                <td>{{$customer->customer_birthday}}</td>
                                <td>{{$customer->customer_phone}}</td>
                                <td>{{$customer->created_at->format('Y-m-d')}}</td>
                                <td><input type="radio" name="customer_id" value="{{$customer->id}}"></td>
                            </tr>
                        @endforeach
                        </tbody>
                        <div id="error-message" style="color: red"></div>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger"
                        data-bs-dismiss="modal">
                    Đóng
                </button>
                <button type="button" class="btn btn-primary save-data-customer">Lưu</button>
            </div>
        </div>
    </div>
</div>
<!--Modal them thanh vien-->
<div class="modal fade modal-lg" id="modal2" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Thêm thành viên</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal"
                        aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row mb-3">
                        <label for="inputText"
                               class="col-sm-2 col-form-label">Tên</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control customer-name">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="inputEmail"
                               class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control customer-email">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="inputNumber" class="col-sm-2 col-form-label">Số
                            điện
                            thoại</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control customer-phone" oninput="this.value = this.value.replace(/[^0-9]/g, '')">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="inputDate" class="col-sm-2 col-form-label">Ngày
                            sinh</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control customer-birthday">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="inputDate" class="col-sm-2 col-form-label">Giới tính</label>
                        <div class="col-sm-10">
                            <select class="form-select customer-gender" aria-label="Default select example">
                                <option selected>Chọn giới tính</option>
                                <option value="1">Nam</option>
                                <option value="2">Nữ</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="inputText" class="col-sm-2 col-form-label">Địa
                            chỉ</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control customer-address">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger"
                        data-bs-toggle="modal" data-bs-target="#verticalycentered">
                    Hủy
                </button>
                <button type="button" class="btn btn-primary save-plus-customer-new">Lưu</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal giao tận nơi -->
<div class="modal fade modal-lg" id="ship" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header" style="color: white; background-color:#0973b9">
                <h5 class="modal-title">Thông tin order</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal"
                        aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row mb-3">
                        <label for="inputText"
                               class="col-sm-2 col-form-label">Thời gian giao hàng</label>
                        <div class="col-sm-10 d-flex justify-content-between">
                            <input type="date" class="form-control date-customer-ship">
                            <input type="time" class="form-control time-customer-ship">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="inputnmail"
                               class="col-sm-2 col-form-label">Tên khách nhận</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control name-customer-ship">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="inputNumber" class="col-sm-2 col-form-label">Số điện thoại</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control phone-customer-ship" oninput="this.value = this.value.replace(/[^0-9]/g, '')">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="inputDate" class="col-sm-2 col-form-label">Địa chỉ giao hàng</label>
                        <div class="col-sm-10">
                            <textarea class="form-control address-customer-ship"></textarea>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="inputText" class="col-sm-2 col-form-label">Phí giao hàng</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control expense-customer-ship">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="inputText" class="col-sm-2 col-form-label">Số tiền khách đã trả trước</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control amount-prepaid-customer">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                    Đóng
                </button>
                <button type="button" class="btn btn-primary save-customer-ship">Lưu thông tin</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal dặt trước-->
<div class="modal fade modal-lg" id="reserved" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header" style="color: white; background-color:#0973b9">
                <h5 class="modal-title">Hình thức Order Đặt trước</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal"
                        aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="mb-3">
                        <label for="customer-name" class="col-form-label">Tên khách
                            hàng:</label>
                        <input type="text" class="form-control name-customer-reserve" id="customer-name">
                    </div>
                    <div class="mb-3">
                        <label for="phone-number" class="col-form-label">Số điện
                            thoại:</label>
                        <input type="text" class="form-control phone-customer-reserve" id="phone-number" oninput="this.value = this.value.replace(/[^0-9]/g, '')">
                    </div>
                    <div class="mb-3">
                        <label for="delivery-date" class="col-form-label">Ngày đặt
                            bàn:</label>
                        <input type="date" class="form-control date-customer-reserve" >
                    </div>
                    <div class="mb-3">
                        <label for="delivery-time" class="col-form-label">Thời gian bắt nhận bàn:</label>
                        <input type="time" class="form-control time-customer-reserve" >
                    </div>
                    <div class="mb-3">
                        <label for="additional-info" class="col-form-label">Thông tin
                            thêm:</label>
                        <textarea class="form-control orther-customer-reserve" id="additional-info"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                    Đóng
                </button>
                <button type="button" class="btn btn-primary save-customer-reserve">Lưu thông tin</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal mang về -->
<div class="modal fade modal-lg" id="back" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header" style="color: white; background-color:#0973b9">
                <h5 class="modal-title">Hình thức Order mang về</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal"
                        aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="mb-3">
                        <label for="recipient-name" class="col-form-label ">Khách
                            mua:</label>
                        <input type="text" class="form-control name-customer-back" >
                    </div>
                    <div class="mb-3">
                        <label for="phone-number" class="col-form-label">Số điện
                            thoại:</label>
                        <input type="text" class="form-control phone-customer-back" oninput="this.value = this.value.replace(/[^0-9]/g, '')">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                    Đóng
                </button>
                <button type="button" class="btn btn-primary save-customer-back">Lưu thông tin</button>
            </div>
        </div>
    </div>
</div>
<!-- Them mon khac trang edit -->
<div class="modal fade" id="other-food" tabindex="-1"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"
                 style="background-color: #06357A; color: #fff">
                <h5 class="modal-title" id="exampleModalLabel">Thêm món khác</h5>
            </div>
            <div class="modal-body">
                <form>
                    <div class="mb-3">
                        <label for="food-name" class="col-form-label">Tên
                            món</label>
                        <input type="text" class="form-control"
                               id="other-food-name-edit">
                    </div>
                    <div class="mb-3">
                        <label for="food-quantity" class="col-form-label">Số
                            lượng</label>
                        <input type="text" class="form-control"
                               id="other-food-quantity-edit">
                    </div>
                    <div class="mb-3">
                        <label for="food-price" class="col-form-label">Đơn
                            giá</label>
                        <input type="text" class="form-control"
                               id="other-food-price-edit">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                        data-bs-dismiss="modal">Huỷ
                </button>

                <button type="button" id="add-other-btn-edit" class="btn btn-primary">
                    Thêm món
                </button>
            </div>
        </div>
    </div>
</div>

