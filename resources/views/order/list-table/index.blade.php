@extends('layouts.app')
@section('style-after')
    <link href="{{asset('order/css/style-in-index.css')}}" rel="stylesheet">
@endsection
@section('content')
    <!--Start content-->
    <div id="table" class="card content container mt-2">
        <div class="row mb-3 mx-lg-1 table-list">
            <div class="col-xl-12 mt-3">
                <h3 style="color: #0C1451; font-weight: bold;">Danh sách bàn</h3>
            </div>
            <div>
                <div class="mt-3">
                    <div class="row">
                        <div class="col-lg-2 col-xl-1 col-2 ">
                            <select id="dropdownArea" class="form-control select ">
                                <option selected class="btn" id="0">Chọn khu vực</option>
                                @foreach($tableAreas as $tableArea)
                                    <option id="{{$tableArea->id}}">{{$tableArea->table_floor}}
                                        - {{$tableArea->table_area_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-6 col-xl-5 search">
                            <input type="text" class="form-control " placeholder="Nhập mã/tên món cần tìm"
                                   name="searchValue" id="search">
                        </div>
                        <div class="col-lg-2 col-xl-2 col-2">
                            <button type="submit" class="btn text-white " style="background-color:#06357A">
                                <i class="bi bi-search"></i>Tìm kiếm
                            </button>
                        </div>
                        <div class="col-lg-2 col-xl-3 col-3" >
                            <div class="dropdown" style="float: right">
                                <button class="btn text-white dropdown-toggle mx-lg-2"
                                        style="background-color:#06357A"
                                        type="button" id="dropdownMenuButtonTable" data-bs-toggle="dropdown"
                                        data-id="0" aria-expanded="false">
                                    Sắp xếp
                                </button>
                                <ul id="SortStatus" class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton">
                                    <li><a class="dropdown-item" href="#" id="0">Hiển thị tất cả</a></li>
                                    <li><a class="dropdown-item" href="#" id="1">Trống</a></li>
                                    <li><a class="dropdown-item" href="#" id="3" class="btn-modal">Đang phục vụ</a></li>
                                    <li><a class="dropdown-item" href="#" id="2">Đặt trước</a></li>
                                    <li><a class="dropdown-item" href="#" id="4">Bàn gộp</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <div id="table_search">
        </div>
    </div>
    <!--End content-->

@endsection
@section('script-after')
    <script src="{{asset('order/js/index.js')}}"></script>
@endsection
