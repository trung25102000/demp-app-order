@if($data->count()<1)
    <h1>Danh sách trống!</h1>
@else
    <div id="tableSearch" class="row row-cols-2 row-cols-md-2 row-cols-lg-4 g-4 mx-lg-1">
        @foreach($data as $item)
            <div class="margin">
                <div class="card table-card font-weight-bold" style="background-color: #B8DFF6">
                    <div class="card-body">
                        <div class="table-icon">
                            <i class="bi bi-table"></i>
                        </div>
                        <input type="hidden" value="{{$item->id}}">
                    @if($item['status']=='1')
                            <a href="{{ route('orders.create_order') . "?tableId=".$item['id'] }}">
                                <h5 class="card-title">
                                    No.{{$item->tableArea->table_area_key.''.$item->tableArea->table_floor .''.sprintf("%02d", $item->table_name)}}
                                    <div class="dot-online" style="float: right"></div>
                                </h5>
                            </a>
                            <p class="card-text">Status: {{$item->getStatusTextAttribute()}}</p>
                            <p class="card-text">
                                <i class="bi bi-person-fill"></i> 0 guests
                            </p>
                        @elseif($item['status']=='3')
                            <a href="{{route('orders.details', $item->orderTable->first()->id)}}">
                                <h5 class="card-title">
                                    No.{{$item->tableArea->table_area_key.''.$item->tableArea->table_floor .''.sprintf("%02d", $item->table_name)}}
                                    <div class="dot-busy" style="float: right"></div>
                                </h5>
                            </a>
                            <p class="card-text">Status: {{$item->getStatusTextAttribute()}}</p>
                            <p class="card-text">
                                <i class="bi bi-person-fill"></i> {{$item->orderTable->first()->count_of_customer}}
                                guests
                            </p>
                        @elseif($item['status']=='4')
                            <a href="{{route('orders.details', $item->orderTable->first()->id)}}">
                                <h5 class="card-title">
                                    No.{{$item->tableArea->table_area_key.''.$item->tableArea->table_floor .''.sprintf("%02d", $item->table_name)}}
                                    <div class="dot-reserved" style="float: right"></div>
                                </h5>
                            </a>
                            <p class="card-text">Status: {{$item->getStatusTextAttribute()}} - <span id="{{$item->id}}tableAdd"></span>
                            </p>
                            <p class="card-text">
                                <i class="bi bi-person-fill"></i> {{$item->orderTable->first()->count_of_customer ?$item->orderTable->first()->count_of_customer:0}}
                                guests
                            </p>
                        @else
                            <a href="{{route('orders.details', $item->orderTable->first()->id)}}">
                                <h5 class="card-title">
                                    No.{{$item->tableArea->table_area_key.''.$item->tableArea->table_floor .''.sprintf("%02d", $item->table_name)}}
                                    <div class="dot-reserved" style="float: right"></div>
                                </h5>
                            </a>
                            <p class="card-text">Status: {{$item->getStatusTextAttribute()}}</p>
                            <p class="card-text">
                                <i class="bi bi-person-fill"></i> {{$item->orderTable->first()->count_of_customer}}
                                guests
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="d-flex justify-content-center link">
        {{ $data->links() }}
    </div>
@endif
