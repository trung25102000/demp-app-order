@extends('layouts.app')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="card mt-2 container">
        <div class="container-fluid mt-2">
            <div class="container">
                <!-- Main content -->
                <div class="row">
                    <div class="col-lg-8">
                        <!-- Details -->
                        <div class="card mb-4" style="border-radius: 15px">
                            <div class="card-body">
                                <div class="mb-3 justify-content-between">
                                    <div class="row d-flex justify-content-between align-items-center">
                                        {{-- Title --}}
                                        <div class=" py-3 col-xl-7">
                                            <h3 style="color: #0C134F; font-weight: bold;font-size: 25px">Chi tiết
                                                Order</h3>
                                        </div>
                                        <div class="col-xl-5 align-content-end justify-content-end d-flex">
                                            <a href="{{ route('orders.payment_list') }}">Hóa đơn</a></button>
                                        </div>
                                    </div>
                                </div>
                                {{-- Table --}}
                                <div class="container" style="max-height: 700px; min-height: 700px; overflow: auto">
                                    <table class="table table-striped border" id="table_detail">
                                        <thead>
                                            <tr class="text-dark" style="text-align: center">
                                                <th>Mã món</th>
                                                <th>Tên món</th>
                                                <th>Số lượng</th>
                                                <th>Ghi chú</th>
                                                <th>Giá</th>
                                                <th>Tùy chọn</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if ($details->count() < 1)
                                                <tr>
                                                    <td colspan="6">Danh sách món của order trên đang trống!</td>
                                                </tr>
                                            @else
                                                <?php $total = 0; ?>
                                                @foreach ($details as $item)
                                                    <tr data-row-id="{{ $item->id }}">
                                                        <td class="fw-bold" style="text-align: center"
                                                            data-id="{{ $item->product_id }}">
                                                            {{ $item->product_id ? $item->product_id : 'Món khác' }}
                                                        </td>
                                                        @if ($item->product)
                                                            <td>{{ $item->product->product_name }}</td>
                                                        @else
                                                            <td>{{ $item->order_detail_other_name }}</td>
                                                        @endif
                                                        <td style="text-align: center">
                                                            <span
                                                                class="editable-field quantity-field">{{ $item->order_detail_quantity }}</span>
                                                            <input type="number" name="quantity"
                                                                class="form-control input-field quantity-input d-none"
                                                                style="width: 50px"
                                                                value="{{ $item->order_detail_quantity }}">
                                                        </td>
                                                        <td>
                                                            <span
                                                                class="editable-field note-field">{{ $item->order_detail_note }}</span>
                                                            <input type="text" name="note"
                                                                class="form-control input-field note-input d-none"
                                                                value="{{ $item->order_detail_note }}">
                                                        </td>
                                                        <td style="text-align: right"
                                                            id="price" value="{{ $item->order_detail_price }}">
                                                            {{ $item->order_detail_price }} vnđ</td>
                                                        </td>
                                                        <td style="text-align: center">
                                                            <a href="#"
                                                                class="edit-button bi bi-pencil-fill text-black"></a>
                                                            <a href="#"
                                                                data-route="{{ route('detail.delete', $item->id) }}"
                                                                class="bi bi-trash-fill text-danger mx-xl-2 delete-button"
                                                                data-id="{{ $item->id }}"></a>
                                                            <button href="#"
                                                                class="btn save-button bi bi-check text-success d-none"></button>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $total += $item->order_detail_price * $item->order_detail_quantity;
                                                    ?>
                                                @endforeach

                                        </tbody>
                                        <tfoot>
                                            <tr class="border">
                                                <th colspan="4" class="text-left">Tổng cộng:</th>
                                                <th class="text-xl-end total-price fs-4"
                                                    data-initial-value="{{ $total }}">{{ $total, 0 }}
                                                </th>
                                                <th>vnđ</th>
                                            </tr>
                                        </tfoot>
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        {{-- Order Infomation --}}
                        <div class="card mb-4" style="border-radius: 15px; min-height: 400px;">
                            <div class="card-body">
                                <div class="row d-flex justify-content-between align-items-center">
                                    <label class="h6 fw-bold col-xl-6">Thông tin Order</label>
                                    {{-- Button --}}
                                    <div class="col-xl-6 justify-content-end align-items-center py-3 d-flex">
                                        <a href="javascript:history.back()" class="btn btn-success"
                                            style="background-color: #06357a;margin-right: 5px">
                                            Quay lại
                                        </a>
                                        <button class="btn dropdown-toggle text-white" type="button"
                                            id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false"
                                            style="background-color: #06357a">
                                            Xử lý
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            @if ($orderTable->status == 1)
                                                <form action="{{ route('orders.kitchen', $orderTable->id) }}"
                                                    method="get">
                                                    @csrf
                                                    <button type="submit" class="dropdown-item">Chuyển xuống Bar/Bếp
                                                    </button>
                                                </form>
                                            @endif
                                            <form action="{{ route('orders.finish', $orderTable->id) }}" method="post">
                                                @csrf
                                                <button type="submit" class="dropdown-item">Xác nhận hoàn tất
                                                </button>
                                            </form>
                                            @if ($orderTable->status == 1 || $orderTable->status == 2 || $orderTable->status == 5)
                                                <form action="{{ route('orders.edit_order', $orderTable->id) }}"
                                                    method="get">
                                                    @csrf
                                                    <button type="submit" class="dropdown-item">Sửa Order
                                                    </button>
                                                </form>
                                            @endif
                                            <hr class="dropdown-divider">
                                            </li>
                                            <li><a class="dropdown-item" href="#" data-bs-toggle="modal"
                                                    data-bs-target="#cancel-order">Hủy
                                                    Order</a>
                                            </li>
                                        </ul>
                                        <form action="{{ route('orders.cancel', $orderTable->id) }}" method="post">
                                            @csrf
                                            <div class="modal fade" id="cancel-order" tabindex="-1">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Hủy Order</h5>
                                                            <button type="button" class="btn-close"
                                                                data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h6>Nhập lý do hủy Order:</h6>
                                                            <textarea type="text" name="reason" class="form-control"></textarea>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger"
                                                                data-bs-dismiss="modal">Hủy
                                                            </button>
                                                            <button type="submit" class="btn text-white"
                                                                style="background-color: #06357A">Lưu
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                                <hr>
                                <div class="container mt-3">
                                    <div class="d-flex row">
                                        <label class="col-xl-6">Mã Order:</label>
                                        <p class="col-xl-6">{{ $orderTable->id }}</p>
                                    </div>
                                    <div class="d-flex row">
                                        <label class="col-xl-6">Thời gian tạo:</label>
                                        <p class="col-xl-6">{{ $orderTable->order->order_create_time }}</p>
                                    </div>
                                    @if ($orderTable->order_accept_time != null)
                                        <div class="d-flex row">
                                            <label class="col-xl-6">Thời gian chấp nhận:</label>
                                            <p class="col-xl-6">{{ $orderTable->order->order_accept_time }}</p>
                                        </div>
                                    @endif
                                    @if ($orderTable->order_finish_time != null)
                                        <div class="d-flex row">
                                            <label class="col-xl-6">Thời gian hoàn thành:</label>
                                            <p class="col-xl-6">{{ $orderTable->order->order_finish_time }}</p>
                                        </div>
                                    @endif
                                    <div class="d-flex row">
                                        <label class="col-xl-6">Nhân viên phụ trách:</label>
                                        <p class="col-xl-6">{{ $orderTable->user->user_name ?? '' }}</p>
                                    </div>
                                    <div class="d-flex row">
                                        <label class="col-xl-6">Trạng thái Order:</label>
                                        <p class="col-xl-6">{{ $orderTable->getStatusTextAttribute($orderTable->status) }}
                                        </p>
                                    </div>
                                    @if ($orderTable->status == 2)
                                        <div class="d-flex row">
                                            <label class="col-xl-6">Lý do hủy:</label>
                                            <p class="col-xl-6">{{ $orderTable->order_note }}</p>
                                        </div>
                                    @endif
                                    <div class="d-flex row">
                                        <label class="col-xl-6">Loại Order:</label>
                                        <p class="col-xl-6">{{ $orderTable->order->orderType->order_type_name }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Customer information -->
                        <div class="card mb-4" style="border-radius: 15px;min-height: 395px;">
                            <div class="card-body">
                                <h3 class="h6 fw-bold">Thông tin khách hàng</h3>
                                <hr>
                                <div class="container">
                                    <div class="d-flex row">
                                        <label class="col-xl-6">Tên khách hàng:</label>
                                        <p class="col-xl-6">{{ $orderTable->customer->customer_name ?? '' }}</p>
                                    </div>
                                    <div class="d-flex row">
                                        <label class="col-xl-6">Địa chỉ:</label>
                                        <p class="col-xl-6">{{ $orderTable->customer->customer_address ?? '' }}</p>
                                    </div>
                                    <div class="d-flex row">
                                        <label class="col-xl-6">Ngày sinh:</label>
                                        <p class="col-xl-6">{{ $orderTable->customer->customer_birthday ?? '' }}</p>
                                    </div>
                                    <div class="d-flex row">
                                        <label class="col-xl-6">Điện thoại:</label>
                                        <p class="col-xl-6">{{ $orderTable->customer->customer_phone ?? '' }}</p>
                                    </div>
                                    <div class="d-flex row">
                                        <label class="col-xl-6">Email:</label>
                                        <p class="col-xl-6">{{ $orderTable->customer->customer_email ?? '' }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endsection
            @section('script-after')
                <script>
                    const URL_SEARCH = @json(route('orders.search_order'));
                    let URL_BACK = @json(url()->previous());
                </script>
                <script src="{{ asset('order/js/list-order.js') }}"></script>
                <script src="{{ asset('order/js/detail.js') }}"></script>
            @endsection
