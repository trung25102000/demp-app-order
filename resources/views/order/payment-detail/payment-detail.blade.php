@extends('layouts.app')
@section('title','Payment')
@section('style-after')
    <link href="{{ asset('/order/css/payment-detail.css') }}" rel="stylesheet">
@endsection
@section('content')
    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-5 mb-md-0 mb-2">
                    <div class="bg-white">
                        <div class="mb-2 mt-1">
                            <div class="d-flex align-items-center mb-2 ps-1"
                                 style="background-color: rgba(206, 206, 210, 0.77); height: 2.75rem; color: #0973b9">
                                <i class="bi bi-card-checklist fs-4 px-1 pe-2 text-primary"
                                   style="font-size: 1.5rem;"></i><span><b>Thẻ thành viên DEHA</b></span>
                            </div>
                            <form class="ps-2">
                                <div class="row mb-1">
                                    <label for="inputPassword3" class="col-lg-6 col-6 col-form-label">Mã thành
                                        viên</label>
                                    <div class="col-lg-6 col-6 d-flex">
                                        <input type="text" class="form-control w-98" id="inputPassword3"
                                               style="height:2.5rem; " value="{{$orderTable->customer_id}}">
                                        <span class="btn btn-outline-secondary" style="max-height:2.5rem">
                                        <i class="bi bi-search"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="row mb-1">
                                    <label for="inputPassword3" class="col-lg-6 col-6 col-form-label">Tên thành
                                        viên</label>
                                    <div class="col-lg-6 col-6 d-flex">
                                        <input type="text" class="form-control w-100" id="inputPassword3"
                                               style="height:2.5rem;" value="{{$customerName}}">
                                        <span class="btn btn-outline-secondary" style="max-height:2.5rem">
                                        <i class="bi bi-search"></i>
                                        </span>
                                    </div>
                                </div>
                                <p>Hạng thẻ <span> VIP</span></p>
                            </form>
                        </div>

                        <div class="mb-2" style="overflow: auto; height: 421px">
                            <div class="d-flex align-items-center mb-3 "
                                 style="background-color: rgba(206, 206, 210, 0.77); height: 2.75rem; color: #0973b9">
                                <i class="bi bi-gift text-danger px-1 fs-4 pe-2" style="font-size: 1.5rem;"></i>
                                <span><b>Chương trình khuyến mãi</b></span>
                            </div>
                            <div id="addContent">
                                @foreach($voucher as $item)
                                    <div class="d-flex align-items-center">
                                        <input class="form-check-input me-3" type="checkbox"
                                               style="width: 2rem; height: 2rem;"
                                               {{ $invoices->voucher_id == $item->id ? 'checked' : '' }} discount-amount="{{$item->discount_amount}}"
                                               value="{{$item->id}}">
                                        <span id="contentPromotion">{{$item->voucher_description}}</span>
                                        <span class="d-none" id="percentReduce">{{$item->discount_amount}}</span>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="">
                            <div class="mb-3">
                                <div class="d-flex align-items-center">
                                    <div class="pe-2">
                                        <button
                                                class="form-control bg-white d-flex align-items-center justify-content-left"
                                                style=" height:48px; width:15rem; " type="button" data-bs-toggle="modal"
                                                data-bs-target="#exampleModal" data-bs-whatever="@mdo">
                                            <i class="bi bi-plus-lg text-success" style="font-size: 1.5rem"></i>
                                            <span style="color:#0973b9; font-weight: bolder;">Khuyến mãi khác</span>
                                        </button>

                                        <!-- Modal thêm chương trình khuyến mãi -->
                                        <div class="modal fade" id="exampleModal" tabindex="-1"
                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header"
                                                         style="background-color: #0973b9; color:#fff">
                                                        <h5 class="modal-title" id="exampleModalLabel">Thêm chương trình
                                                            khuyến mãi</h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                                aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form>
                                                            <div class="mb-3">
                                                                <label for="program-name" class="col-form-label"><b>Tên
                                                                        chương trình:</b></label>
                                                                <input type="text" class="form-control"
                                                                       id="program-name">
                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="percent-reduce" class="col-form-label"><b>Phần
                                                                        trăm giảm:</b></label>
                                                                <input type="text" class="form-control"
                                                                       id="percent-reduce">
                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="detail-name" class="col-form-label"><b>Chi
                                                                        tiết chương trình:</b></label>
                                                                <textarea class="form-control"
                                                                          id="detail-name"></textarea>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="modal-footer" data-bs-dismiss="modal">
                                                        <button type="button" class="btn btn-secondary"
                                                                data-bs-dismiss="modal">Huỷ
                                                        </button>
                                                        <button type="button" class="btn btn-primary"
                                                                id="addPromotions">Thêm mới khuyến mãi
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr style="margin: 1.7rem 0;">
                        <div class="p-1" style="background-color: #e3efff; border: 1px solid #0973b9">
                            <form class="ps-1">
                                <div class="row" style="color: #0973b9">
                                    <label for="inputPassword3"
                                           class="col-lg-6 col-md-6 col-5 col-form-label d-flex align-items-center">
                                        <i class="bi bi-credit-card-2-front me-1"
                                           style=" color: #0973b9; font-size: 1.5rem;"></i>
                                        <span><b> Mã ưu đãi</b></span>
                                    </label>
                                    <div class="col-lg-6 col-md-6 col-7 d-flex align-items-center">
                                        <input type="text" class="form-control w-100" id="inputPassword3"
                                               style="height:2.5rem; ">
                                        <span class="btn btn-outline-secondary" style="max-height:2.5rem">
                                        <i class="bi bi-search"></i>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <!-- Thanh tính tiền -->
                <div class="col-lg-8 col-md-7 ">
                    <div class="bg-white">
                        <div class="mb-2 mt-1">
                            <div class="d-flex align-items-center mb-2 justify-content-between"
                                 style="background-color: rgba(206, 206, 210, 0.77); height: 2.77rem; color: #0973b9; ">
                                <div class="d-flex align-items-center">
                                    <button title="Tùy chọn" class="form-control bg-light me-2 "
                                            style=" height:45px; width:45px"
                                            type="button" id="dropdownMenuButtonPayment" data-bs-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                        <i class="bi-three-dots-vertical"></i>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButtonPayment">
                                        <a class="dropdown-item" href="#" data-bs-toggle="modal"
                                           data-bs-target="#mergeBillModal">Gộp Hóa đơn</a>
                                    </div>
                                    <button class="form-control bg-light me-2" style=" height:45px; width:45px"
                                            type="submit">
                                        <a href="{{route('orders.edit_order',$orderTable->id)}}">
                                            <i class="bi bi-pencil"></i>
                                        </a>
                                    </button>
                                    <span><b>Mã order:{{$orderTable->id}}  <span>
                                      - {{$orderTable->table ? $orderTable->table->tableArea->table_area_key.''.$orderTable->table->tableArea->table_floor .''.sprintf("%02d", $orderTable->table->table_name) : $orderTable->order->orderType->order_type_name}}
                                    </b></span>
                                </div>
                            </div>
                            <div class="bg-white border height-body" style="overflow: auto; height: 473px">
                                <table class="table table-striped text-active mb-5">
                                    <thead>
                                    <tr style="background-color: #e7e6e6;">
                                        <th scope="col">Tên món</th>
                                        <th class="display-none" scope="col">Đơn vị</th>
                                        <th scope="col">Số lượng</th>
                                        <th scope="col">Đơn giá</th>
                                        <th scope="col">Thành tiền</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orderDetail as $item)
                                        <tr style="vertical-align: middle;">
                                            <td>{{$item->product?$item->product->product_name:$item->order_detail_other_name}}</td>
                                            <td class="display-none">{{$item->product ? $item->product->unit->unit_name : $item->order_detail_other_unit_name}}</td>
                                            <td>
                                                <span class="quantityOrder">{{$item->order_detail_quantity}}</span>
                                            </td>
                                            <td class="price">{{number_format($item->order_detail_price,0,',','.')}}đ
                                            </td>
                                            <td class="totalPrice">{{ number_format($item->order_detail_price * $item->order_detail_quantity, 0, ',', '.') }}
                                                đ
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tbody id="dataTemp">
                                    </tbody>
                                </table>
                            </div>
                            <div class="mt-4">
                                <div class="row">
                                    <div class="col-md-6 mb-md-0 mb-3">
                                        <div class="d-flex justify-content-between mb-1">
                                            <div><b>Thành tiền</b></div>
                                            @php
                                                $totalPrice = $orderDetail->sum(function($item) {
                                                    return $item->order_detail_price * $item->order_detail_quantity;
                                                });
                                            @endphp
                                            <div id="totalMoneyOrder"><b>{{number_format($totalPrice, 0,',','.')}} đ</b>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row mb-2">
                                            <div class="col-lg-7 d-flex align-items-center">
                                                <div class="form-control bg-white me-2 f-flex align-items-center"
                                                     style=" height:45px; width:45px " type="submit">
                                                    <i class="bi bi-flag text-danger"
                                                       style="font-weight: bold; font-size: 1.3rem;"></i>
                                                </div>
                                                <span style="color:#0973b9; font-weight: 500;"
                                                      id="insertPromotion">{{$voucherSelect ? $voucherSelect->voucher_description : ""}}</span>
                                            </div>
                                            <div id="discountForVoucher"
                                                 class="col-lg-5 d-flex align-items-center justify-content-end"
                                                 style="font-weight: bold;">{{$voucherSelect ? number_format($voucherSelect->discount_amount * $totalPrice,0,',','.')  : 0}}
                                                đ
                                            </div>
                                        </div>
                                        <div class="row mb-2">
                                            <div class="col-lg-7 d-flex align-items-center">
                                                <input id="discountCheckbox" class="mx-1" type="checkbox"
                                                       style="width: 2rem; height: 2rem;">
                                                <span style="color:#0973b9; font-weight: 500;">Thuế GTGT 10%</span>
                                            </div>
                                            <div id="discountValue"
                                                 class="col-lg-5 d-flex align-items-center justify-content-end"
                                                 style="font-weight: bold;">0
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <div class="d-flex justify-content-between" style="font-weight: bold;">
                                            <div>Tổng tiền</div>
                                            <div id="totalPayment">0 đ</div>
                                        </div>
                                        <hr>
                                        <div class="row mb-2" style="font-size: 1.1rem; margin-top: 68px">
                                            <div class="col-6"><b>Còn phải thu</b></div>
                                            <div class="col-6" id="totalAmount"
                                                 style="font-weight: bold; text-align: right;">0 đ
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Thanh tính tiền -->
                                <div class="d-flex justify-content-between align-items-center p-md-2"
                                     style="background-color: rgba(206, 206, 210, 0.77);">
                                    <button class="me-md-1 btn" style="background-color: #fff; border-color: #0973b9; width: 146px">
                                        <a href="{{route('orders.payment_list')}}"
                                           class="d-md-flex align-items-center text-black format-btn format-btn-tablet justify-content-center" >
                                            <i class="bi bi-chevron-compact-left" style="font-size: 1.5rem;"></i>
                                            <span style="padding: 0 0.5rem; font-size: 0.9rem" class="size-text-btn">QUAY LẠI</span>
                                        </a>
                                    </button>
                                    <button class="me-md-1 btn" style="background-color: #fff; border-color: #0973b9; width: 146px">
                                        <a href="{{route('split-bill', $orderTable->id)}}"
                                           class="d-md-flex align-items-center text-black format-btn format-btn-tablet justify-content-center">
                                           <i class="bi bi-arrows-expand" style="font-size: 1.5rem;"></i>
                                            <span style="padding: 0 0.5rem; font-size: 0.9rem" class="size-text-btn">TÁCH HĐ</span>
                                        </a>
                                    </button>
                                    <button class="me-md-1 btn " style="background-color: #fff; border-color: #0973b9; width: 146px"
                                            id="printBillButton">
                                        <a href="" class="d-md-flex align-items-center text-black format-btn format-btn-tablet justify-content-center">
                                            <i class="bi bi-printer" style="font-size: 1.5rem;"></i>
                                            <span style="padding: 0 0.5rem; font-size: 0.9rem" class="size-text-btn">IN TẠM</span>
                                        </a>
                                    </button>
                                    <button class="me-md-1 btn btn-print"
                                            style="background-color: #fff; border-color: #0973b9; width: 146px"
                                            data-id="{{$invoices->order_table_id}}">
                                        <a href="" class="d-md-flex align-items-center text-black format-btn format-btn-tablet justify-content-center">
                                            <i class="bi bi-save" style="font-size: 1.5rem;"></i>
                                            <span style="padding: 0 0.5rem; font-size: 0.9rem" class="size-text-btn">LƯU</span>
                                        </a>
                                    </button>
                                    <button id="openModal" data-bs-toggle="modal"
                                            data-bs-target="#modalDialogScrollable" class="me-md-1 bg-warning btn"
                                            style="border-color: #0973b9; width: 146px">
                                        <span class="d-md-flex align-items-center text-black format-btn format-btn-tablet justify-content-center">
                                        <i class="bi bi-calculator" style="font-size: 1.5rem;"></i>
                                        <span style="padding: 0 0.5rem; font-size: 0.9rem" class="size-text-btn">THU TIỀN</span>
                                        </span>
                                    </button>
                                </div>

                                <!-- Modal tính tiền cho khách -->
                                <div class="modal fade" id="modalDialogScrollable" tabindex="-1">
                                    <div class="modal-dialog modal-dialog-scrollable modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header" style="background-color: #0973b9; color:#fff">
                                                <h5 class="modal-title">Thu tiền</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                        aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body bg-light">
                                                <div class="row">
                                                    <div class="col-lg-7">
                                                        <div class="bg-white ">
                                                            <div
                                                                    class="d-flex align-items-center justify-content-center mb-2"
                                                                    style="background-color:rgba(206, 206, 210, 0.77); height: 2.75rem; ;">
                                                                <b> Nhập số tiền khách đưa</b>
                                                            </div>
                                                            <div
                                                                    class="column d-flex align-items-center justify-content-center mb-2"
                                                                    style="background-color:#e3efff; height: 2.75rem; ;">
                                                                <input id="customerSentAmount"
                                                                       style="text-align: right; background-color:#e3efff; height: 100%; width:100%; padding: 0 26px; cursor:pointer"
                                                                       value="0" readonly>
                                                                <span class="btn btn-outline-secondary refreshTotalMoney d-flex align-items-center"
                                                                      style="height: 44px;">
                                                                        <i class="bi bi-arrow-clockwise"></i>
                                                                        </span>
                                                            </div>
                                                            <div class="insertMoneyNumber d-none">
                                                                <p class="my-3"><b>Nhập số tiền theo số</b></p>
                                                                <table class="table table-bordered">
                                                                    <tbody style="text-align: center; cursor: pointer">
                                                                    <tr style="height: 73px; vertical-align: middle">
                                                                        <td class="column-number" data-number="1">1</td>
                                                                        <td class="column-number" data-number="2">2</td>
                                                                        <td class="column-number" data-number="3">3</td>
                                                                    </tr>
                                                                    <tr style="height: 73px; vertical-align: middle">
                                                                        <td class="column-number" data-number="4">4</td>
                                                                        <td class="column-number" data-number="5">5</td>
                                                                        <td class="column-number" data-number="6">6</td>
                                                                    </tr>
                                                                    <tr style="height: 73px; vertical-align: middle">
                                                                        <td class="column-number" data-number="7">7</td>
                                                                        <td class="column-number" data-number="8">8</td>
                                                                        <td class="column-number" data-number="9">9</td>
                                                                    </tr>
                                                                    <tr style="height: 73px; vertical-align: middle">
                                                                        <td></td>
                                                                        <td class="column-number" data-number="0">0</td>
                                                                        <td class="clear-number"><i
                                                                                    class="bi bi-arrow-return-left"></i>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="insertMoneyPrice">
                                                                <p class="my-3"><b>Nhập số tiền theo mệnh giá</b></p>
                                                                <table class="table table-bordered">
                                                                    <tbody style="text-align: center; cursor: pointer;">
                                                                    <tr style="height: 98px; vertical-align: middle; font-size: 1.2rem; font-weight: 600">
                                                                        <td class="column-price" data-value="500000">
                                                                            500.000
                                                                        </td>
                                                                        <td class="column-price" data-value="200000">
                                                                            200.000
                                                                        </td>
                                                                        <td class="column-price" data-value="100000">
                                                                            100.000
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="height: 98px; vertical-align: middle; font-size: 1.2rem; font-weight: 600">
                                                                        <td class="column-price" data-value="50000">
                                                                            50.000
                                                                        </td>
                                                                        <td class="column-price" data-value="20000">
                                                                            20.000
                                                                        </td>
                                                                        <td class="column-price" data-value="10000">
                                                                            10.000
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="height: 98px; vertical-align: middle; font-size: 1.2rem; font-weight: 600">
                                                                        <td class="column-price" data-value="5000">
                                                                            5.000
                                                                        </td>
                                                                        <td class="column-price" data-value="2000">
                                                                            2.000
                                                                        </td>
                                                                        <td class="column-price" data-value="1000">
                                                                            1.000
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-5 bg-light">
                                                        <div class="border">
                                                            <div
                                                                    class="d-flex align-items-center bg-white mb-2 px-2 justify-content-between"
                                                                    style="height: 4.75rem; width:100%; border:1px solid rgba(206, 206, 210, 0.77);">
                                                                <div class="" style="font-weight: 400;">
                                                                    <b>Số tiền còn phải thu</b>
                                                                </div>
                                                                <div class="" id="stillAmount"
                                                                     style="font-weight: bold;">
                                                                    0
                                                                </div>
                                                            </div>
                                                            <div class="d-none d-lg-block" style="height:200px">
                                                            </div>
                                                            <hr class="d-none d-md-block">
                                                            <div>
                                                                <div class="d-flex mb-3 justify-content-between">
                                                                    <div class="ps-2">Tiền trả lại cho khách</div>
                                                                    <div class="">
                                                                        <input type="text" class="form-control"
                                                                               id="returnAmount" value="0" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="d-flex mb-3 justify-content-between">
                                                                    <div class="ps-2">
                                                                        <input id="tipAmountCheckbox" type="checkbox">
                                                                        Khách tip tiền thừa
                                                                    </div>
                                                                    <div class="">
                                                                        <input type="text" class="form-control"
                                                                               id="tipAmount" value="0" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary" id="savePaymentButton">
                                                    Đóng
                                                </button>
                                                <button type="button" data-id="đ{{$invoices->order_table_id}}"
                                                        class="btn btn-primary " id="saveAndPrintPaymentButton">
                                                    In và Đóng
                                                </button>
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                                                    Huỷ bỏ
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    @include('order.payment-detail.modal-list-order-in-payment-detail')
@endsection
@section('script-after')
    <script src="{{asset('order/js/payment-detail.js')}}"></script>
    <script>
        const URL_PRINT_PROVISIONAL = @json(route('orders.print_provisional',$orderTable->id));
        const URL_SAVE_PAYMENT = @json(route('orders.save_payment',$orderTable->id));
        const URL_UPDATE_VOUCHER = @json(route('orders.update_voucher',$orderTable->id));
    </script>

@endsection
