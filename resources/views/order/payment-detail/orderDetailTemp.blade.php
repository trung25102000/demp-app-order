@foreach($orderDetails as $orderDetail)
    <input type="hidden" name="idTemp" value="{{$orderDetail->id}}">
    <tr>
        <td>{{ $orderDetail->orderTable->table ? $orderDetail->orderTable->table->tableArea->table_area_key.''. $orderDetail->orderTable->table->tableArea->table_floor .''.sprintf("%02d", $orderDetail->orderTable->table->table_name) : $orderDetail->orderTable->order->orderType->order_type_name }}
        <td>{{$orderDetail->product ? $orderDetail->product->unit->unit_name : $orderDetail->order_detail_other_unit_name}}</td>
        <td>{{$orderDetail->order_detail_quantity}}</td>
        <td class="price">
            {{number_format($orderDetail->order_detail_price,0,',','.')}}
            đ
        </td>
        <td class="totalPrice">  {{ number_format(($orderDetail->order_detail_quantity*$orderDetail->order_detail_price), 0, ',', '.') }}
            đ
        </td>
    </tr>
@endforeach

