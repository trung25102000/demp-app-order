<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link href="{{asset('order/css/print-invoice.css')}}" rel="stylesheet">
  <link href="{{asset('order/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('order/assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <style>
    * {
        font-family: Arial, Helvetica, sans-serif;
    }
  </style>
</head>
<body>
  <div class="container">
    <div class="restaurant-info">
        <h3>DEMOORDER RES</h3>
        <p style="margin: 0 auto">28 Nguyễn Tri Phương - TP Huế</p>
        <p>ĐT: 0909000999 - 0909009099</p>
    </div>
    <h5 style="text-align: center;">HOÁ ĐƠN TẠM TÍNH</h5>
    <div>
        <b>Số HĐ</b> <span style="padding-left: 3rem">00000{{$invoiceId}}</span>
    </div>
    <div class="d-flex">
      <b class="me-5">Ngày giờ in</b>
      @php
          date_default_timezone_set('Asia/Bangkok'); // Thiết lập múi giờ GMT+7
          $currentDateTime = new DateTime();
          $formattedDate = $currentDateTime->format('d/m/Y');
          $formattedTime = $currentDateTime->format('H:i:s');
      @endphp
      <span class="me-5"><?php echo $formattedDate; ?></span>
      <span><?php echo $formattedTime; ?></span>
    </div>
    <div class="d-flex">
        <b class="me-4">Hình thức order:</b>
        @if ($orderTable->table_id)
        <span class="me-4">Tại chổ</span> <span class="ms-5">Bàn {{ $orderTable->table->table_name }}</span>
        @else
        {{ $orderTable->order->order_type_id == 2 ? "Mang về" : "Giao tận nơi" }}
        @endif
    </div>
    <div><b>Thu ngân</b> <span style="padding-left: 4rem">Admin</span></div>
    <hr style="margin: 0.5rem 0 !important">
    <p><b>Chi tiết hoá đơn</b></p>
    <div class="bill-content">
        <table class="table ">
            <thead>
              <tr style="height: 10px">
                <th>Tên hàng</th>
                <th>SL</th>
                <th>Đơn giá</th>
                <th>Thành tiền</th>
              </tr>
            </thead>
            <tbody>
              @foreach($orderDetail as $item)
              <tr style="vertical-align: middle;">
                <td>{{$item->order_detail_other_name}}</td>
                <td>
                    <span class="quantityOrder">{{$item->order_detail_quantity}}</span>
                </td>
                <td class="price">{{number_format($item->order_detail_price,0,',','.')}} đ</td>
                <td class="totalPrice">{{ number_format($item->order_detail_price * $item->order_detail_quantity, 0, ',', '.') }} đ
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
        <hr style="margin: 0.5rem 0 !important">
        <table class="table">
          @php
          $totalPrice = $orderDetail->sum(function($item) {
            return $item->order_detail_price * $item->order_detail_quantity;
          });
          $tax = $totalPrice * 0.1;
          $totalPayment = $totalPrice + $tax;
          $discountAmount = $voucherSelect ? $voucherSelect->discount_amount * $totalPrice : 0;
          $cashPayment = $totalPayment - $discountAmount;
          @endphp
          <thead>
            <tr style="height: 10px">
              <th>Thành tiền</th>
              <th></th>
              <th></th>
              <th>{{number_format($totalPrice, 0,',','.')}} đ</th>
            </tr>
        </thead>
        <tbody>
            <tr style="height: 10px">
              <td>Thuế GTGT (10%)</td>
              <td></td>
              <td></td>
              <td>{{number_format($tax)}} đ</td>
            </tr>
        </tbody>
        <thead>
            <tr style="height: 10px">
              <th>Tổng thanh toán</th>
              <th></th>
              <th></th>
              <th>{{number_format($totalPayment)}} đ</th>
            </tr>
        </thead>

        </table>
        <table class="table">
            <tbody>
                <tr style="height: 10px">
                <td>Voucher</td>
                <td></td>
                <td></td>
                <td>{{number_format($discountAmount)}} đ</td>
                </tr>
            </tbody>
            <tbody>
                <tr style="height: 10px">
                <td>Khách cần trả</td>
                <td></td>
                <td></td>
                <td>{{number_format($cashPayment) }} đ</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="text-align: center">
      <p><b>Hẹn gặp laị quý khách!</b></p>
    </div>
</div>

</body>
</html>
