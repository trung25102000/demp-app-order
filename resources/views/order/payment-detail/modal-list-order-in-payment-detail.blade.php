<!-- Modal -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="modal fade" id="mergeBillModal" tabindex="-1" aria-labelledby="mergeBillModalLabel" aria-hidden="true">
    @if(count($orderTables)<2)
        <div class="modal-dialog modal-md" role="document">
            @elseif(count($orderTables)<4)
                <div class="modal-dialog modal-lg" role="document">
                    @elseif(count($orderTables)<6)
                        <div class="modal-dialog modal-xl" role="document">
                            @endif
                            <div class="modal-content">
                                <!-- Modal Header -->
                                <div class="modal-header d-flex justify-content-between align-items-center">
                                    <!-- Button close nằm ở góc bên cùng bên trái -->
                                    <button type="button" class="btn btn-close ms-2" data-bs-dismiss="modal"
                                            aria-label="Close"></button>

                                    <!-- Tiêu đề -->
                                    <h5 class="modal-title mx-auto" style="color: #06357a;">Chọn hóa đơn để ghép</h5>

                                    <!-- Button submit nằm ở góc cùng bên phải -->
                                    <button type="button"
                                            class="btn btn-primary btn-merge"
                                            {{count($orderTables)==0?'disabled':""}} style="background-color: #06357a;color: white">
                                        <span>Xác nhận</span>
                                    </button>
                                </div>

                                <div class="modal-body">
                                    <div id="orderTablesContainer">
                                        <div class="row">
                                            @if(count($orderTables)==0)
                                                <p class="card-text"
                                                   style="text-align: center">Không có Order nào có thể ghép đươc!</p>
                                            @elseif(count($orderTables)>=1)
                                                @foreach($orderTables as $orderTable)
                                                    @php
                                                        $orderDetails = $orderTable->orderDetail->all();
                                                        $prices = array_column($orderDetails, 'order_detail_price');
                                                        $quantities = array_column($orderDetails, 'order_detail_quantity');

                                                        $multipliedValues = array_map(function ($price, $quantity) {
                                                            return $price * $quantity;
                                                        }, $prices, $quantities);
                                                    @endphp
                                                    @if(count($orderTables)<2)
                                                        <div class="col-md-12">
                                                            @else
                                                                <div class="col-md-4">
                                                                    @endif
                                                                    <div class="card">
                                                                        <div class="card-header"
                                                                             style=" background-color: #0973b9; ">
                                                                            <h5 class="card-title"
                                                                                style="text-align: center">{{ $orderTable->table ? $orderTable->table->tableArea->table_area_key.''.$orderTable->table->tableArea->table_floor .''.sprintf("%02d", $orderTable->table->table_name) : $orderTable->order->orderType->order_type_name }}</h5>
                                                                        </div>
                                                                        <div class="card-body">
                                                                            <p class="card-text"
                                                                               style="text-align: center"> {{ number_format(array_sum($multipliedValues), 2, '.', ',') }}
                                                                                VNĐ</p>
                                                                        </div>
                                                                        <div class="card-footer" id="footerAdd">
                                                                            <button class="btn btn-primary btnChoose"
                                                                                    data-id="{{$orderTable->id}}">Lựa
                                                                                chọn
                                                                            </button>
                                                                            <a href="{{route('orders.details',$orderTable->id)}}"
                                                                               class="btn btn-secondary"
                                                                               style="margin-left: 15px;float:right">
                                                                                Chi tiết</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @endforeach
                                                            @endif
                                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
        </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="abc">
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

