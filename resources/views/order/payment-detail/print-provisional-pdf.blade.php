<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="{{ asset('order/css/print-invoice.css') }}" rel="stylesheet">
    <link href="{{ asset('order/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('order/assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
    <style>
        * {
            font-family: Arial, Helvetica, sans-serif;
            margin: 0;
            padding: 0;
            /* line-height: 10px; */
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="restaurant-info" style="line-height: 10px;">
            <h3 style="text-align: center; font-size: 20px; font-weight: bold;">DEMOORDER RES</h3>
            <p style="text-align: center; font-size: 10px;">28 Nguyễn Tri Phương - TP Huế</p>
            <p style="text-align: center; font-size: 10px;">ĐT: 0909000999 - 0909009099</p>
        </div>
        <h5 style="text-align: center;font-size:17ox; font-weight:bold; line-height: 10px;">HOÁ ĐƠN</h5>
        <div style="line-height: 5px;">
            <b style="font-weight: 500;font-size:10px;">Số HĐ:</b> <span
                style="padding-left: 3rem; font-size:10px;">{{ str_pad($invoiceId, 5, '0', STR_PAD_LEFT) }}</span>
        </div>
        <div class="d-flex" style="line-height: 5px;">
            <b class="me-5" b style="font-weight: 500; font-size:10px;">Ngày giờ in:</b>
            @php
                date_default_timezone_set('Asia/Bangkok'); // Thiết lập múi giờ GMT+7
                $currentDateTime = new DateTime();
                $formattedDate = $currentDateTime->format('d/m/Y');
                $formattedTime = $currentDateTime->format('H:i:s');
            @endphp
            <span class="me-5" style="font-size:10px;"><?php echo $formattedDate; ?></span>
            <span style="font-size:10px;"><?php echo $formattedTime; ?></span>
        </div>
        <div class="d-flex" style="line-height: 5px;">
            <b class="me-4" style="font-size:10px">Hình thức order:</b>
            @if ($orderTable->table_id)
                <span class="me-4" style="font-size:10px">Tại chổ</span> <span class="ms-5"
                    style="font-size:10px">Bàn {{ $orderTable->table->table_name }}</span>
            @else
                {{ $orderTable->order->order_type_id == 2 ? 'Mang về' : 'Giao tận nơi' }}
            @endif
        </div>
        <div><b>Thu ngân</b> <span style="padding-left: 4rem;">Admin</span></div>
        <hr style="margin: 5px !important">
        <p><b>Chi tiết hoá đơn</b></p>
        <div class="bill-content" style="line-height: 20px">
            <table class="table">
                <thead>
                    <tr style="height: 10px">
                        <th style="text-align: center">Tên hàng</th>
                        <th style="text-align: center">SL</th>
                        <th style="text-align: center">Đơn giá</th>
                        <th style="text-align: center">Thành tiền</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orderDetail as $item)
                        <tr style="vertical-align: middle;">
                            <td style="text-align: left">{{ $item->order_detail_other_name }}</td>
                            <td style="text-align: center">
                                <span class="quantityOrder">{{ $item->order_detail_quantity }}</span>
                            </td>
                            <td class="price" style="text-align: center">
                                {{ number_format($item->order_detail_price, 0, ',', '.') }} đ</td>
                            <td class="totalPrice" style="text-align: center">
                                {{ number_format($item->order_detail_price * $item->order_detail_quantity, 0, ',', '.') }}
                                đ
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <hr style="margin: 0.5rem 0 !important; margin-bottom: 20px !important">
        <div class="bill-content" style="line-height: 20px">
            <table class="table" style="line-height: 20px;">
                @php
                    $totalPrice = $orderDetail->sum(function ($item) {
                        return $item->order_detail_price * $item->order_detail_quantity;
                    });
                    // $tax = $totalPrice * 0.1;
                    $totalPayment = $totalPrice + $tax;
                    $discountAmount = $voucherSelect ? ($voucherSelect->discount_amount * $totalPrice) : 0;
                    $cashPayment = $totalPayment - $discountAmount;
                @endphp
                <thead>
                    <tr style="height: 10px">
                        <th>Thành tiền</th>
                        <th></th>
                        <th></th>
                        <th style="text-align: center">{{ number_format($totalPrice, 0, ',', '.') }} đ</th>
                    </tr>
                </thead>
                <tbody>
                    <tr style="height: 10px">
                        <td>Thuế GTGT (10%)</td>
                        <td></td>
                        <td></td>
                        <td style="text-align: center">{{ number_format($tax) }} đ</td>
                    </tr>
                    <tr style="height: 10px">
                        <th>Tổng thanh toán</th>
                        <th></th>
                        <th></th>
                        <th style="text-align: center">{{ number_format($totalPayment) }} đ</th>
                    </tr>
                </tbody>
            </table>
        </div>
        <hr style="margin: 0.5rem 0 !important; margin-bottom: 20px !important; line-height: 40px">
        <div class="bill-content" style="line-height: 20px">
            <table class="table">
                <tbody>
                    <tr style="height: 10px">
                        <td>Voucher</td>
                        <td></td>
                        <td></td>
                        <td style="text-align: center">{{ number_format($discountAmount) }} đ</td>
                    </tr>
                </tbody>
                <tbody>
                    <tr style="height: 10px">
                        <td colspan="3">Khách cần trả</td>
                        <td style="text-align: center">{{ number_format($cashPayment) }} đ</td>
                    </tr>
                    <tr style="height: 10px">
                        <td colspan="3">Số tiền khách đưa</td>
                        <td style="text-align: center">{{ $customerPay }} đ</td>
                    </tr>
                    <tr style="height: 10px">
                        <td colspan="3">Số tiền thối lại cho khách</td>
                        @if((str_replace('.', '', $customerPay)) != $cashPayment)
                        <td style="text-align: center">{{ $customerRefund > 0 ? $customerRefund .' đ': ($customerRefund < 0 ? 0 .' đ' : "Khách tip tiền thừa")}}</td>
                        @else 
                        <td style="text-align: center">0</td>
                        @endif
                    </tr>
                </tbody>
            </table>
        </div>
        <p style="text-align: center"><b>Hẹn gặp laị quý khách!</b></p>
    </div>
</body>

</html>

