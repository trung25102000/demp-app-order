@if(count($orderTables)<2)
    <div class="modal-dialog modal-md" role="document">
        @elseif(count($orderTables)<4)
            <div class="modal-dialog modal-lg" role="document">
                @elseif(count($orderTables)<6)
                    <div class="modal-dialog modal-xl" role="document">
                        @endif
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header d-flex justify-content-between align-items-center">
                                <!-- Button close nằm ở góc bên cùng bên trái -->
                                <button type="button" class="btn btn-close ms-2 btn-detail" data-bs-dismiss="modal"
                                        aria-label="Close"></button>

                                <!-- Tiêu đề -->
                                <h5 class="modal-title mx-auto" style="color: #06357a;">Chọn món để ghép</h5>

                                <!-- Button submit nằm ở góc cùng bên phải -->
                                <button type="button"
                                        class="btn btn-primary btn-merge-detail"
                                        style="background-color: #06357a;color: white">
                                    <span>Ghép</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    @if(count($orderTables)<2)
                                        @foreach($orderTables as $orderTable)
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-header"
                                                         style="height: 70px;background-color: #0973b9;align-items: center">
                                                        <h5 class="card-title"
                                                            style="text-align: center">{{ $orderTable->table ? $orderTable->table->tableArea->table_area_key.''.$orderTable->table->tableArea->table_floor .''.sprintf("%02d", $orderTable->table->table_name) : $orderTable->order->orderType->order_type_name }}</h5>
                                                    </div>
                                                    <div class="card-body"
                                                         style="max-height: 240px; overflow: auto;">
                                                        @php $orderDetails=\App\Models\OrderDetail::where('order_detail_status',3)->get(); @endphp
                                                    @foreach($orderDetails as $item)
                                                            <div class="row align-items-center">
                                                                <div class="col-1">
                                                                    <input id="{{$item->id}}"
                                                                           quantity="{{$item->order_detail_quantity}}"
                                                                           price="{{$item->order_detail_price}}"
                                                                           type="checkbox"
                                                                           class="form-check-input">
                                                                </div>
                                                                <div class="col-9">
                                                                    <p type="button" class="card-text selectable-p">
                                                                        {{$loop->iteration}}
                                                                        . {{ $item->product ? $item->product->product_name : $item->order_detail_other_name }}
                                                                        X
                                                                    </p>
                                                                </div>
                                                                <div class="col-2">
                                                                    <input type="number" class="form-control"
                                                                           value="{{$item->order_detail_quantity}}"
                                                                           min="1"
                                                                           max="{{$item->order_detail_quantity}}">
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach
                                                        <!-- Các thanh con khác -->
                                                    </div>
                                                    <div class="card-footer d-flex justify-content-end">
                                                        <button class="btn btn-primary btn-select-all">Chọn tất cả
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @elseif(count($orderTables)>=2)
                                        @foreach($orderTables as $orderTable)
                                            <div class="col-md-6">
                                                <div class="card" style="height: 100%;">
                                                    <div class="card-header"
                                                         style="height: 70px;background-color: #0973b9;align-items: center">
                                                        <h5 class="card-title"
                                                            style="text-align: center">{{ $orderTable->table ? $orderTable->table->tableArea->table_area_key.''.$orderTable->table->tableArea->table_floor .''.sprintf("%02d", $orderTable->table->table_name) : $orderTable->order->orderType->order_type_name }}</h5>
                                                    </div>
                                                    <div class="card-body"
                                                         style="max-height: 240px; overflow: auto;">
                                                        @php $orderDetails=\App\Models\OrderDetail::where('order_detail_status',3)->get(); @endphp
                                                        @foreach($orderDetails as $item)
                                                            <div class="row align-items-center">
                                                                <div class="col-1">
                                                                    <input id="{{$item->id}}"
                                                                           price="{{$item->order_detail_price}}"
                                                                           quantity="{{$item->order_detail_quantity}}"
                                                                           type="checkbox"
                                                                           class="form-check-input">
                                                                </div>
                                                                <div class="col-8">
                                                                    <p type="button" class="card-text selectable-p">
                                                                        {{$loop->iteration}}
                                                                        . {{ $item->product ? $item->product->product_name : $item->order_detail_other_name }}
                                                                    </p>
                                                                </div>
                                                                <div class="col-3">
                                                                    <input type="number" class="form-control"
                                                                           value="{{$item->order_detail_quantity}}"
                                                                           min="1"
                                                                           max="{{$item->order_detail_quantity}}">
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach
                                                        <!-- Các thanh con khác -->
                                                    </div>
                                                    <div class="card-footer d-flex justify-content-end">
                                                        <button class="btn btn-primary btn-select-all">Chọn tất cả
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach

                                    @endif

                                </div>

                            </div>

                        </div>
                    </div>
            </div>
    </div>
