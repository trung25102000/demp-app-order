<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Print Bill</title>
    <style>
        /* Container styles */
        .container {
            font-family: Arial, sans-serif;
            margin: 5px auto;
            padding: 5px;
            max-width: 74mm;
            height: 105mm;
            border: 1px solid #ccc;
        }
        body {
            font-size: 8px;
            width: 74mm;
            height: 105mm;
            margin: 0;
            padding: 0;

        }

        /* Card styles */
        .card {
            width: 100%;
            border-collapse: collapse;
            border: 1px solid #ccc;
        }
        @media print {
            @page {
                size: 74mm 105mm; /* Set the dimensions for A7 paper size */
                margin: 0; /* Remove default margins */
            }

            body {
                margin: 0;
                padding: 0;
                width: 74mm;
                height: 105mm;
            }

            /* Rest of your CSS styles for printing */
            /* ... */
        }

        .card-body {
            padding: 20px;
        }

        /* Invoice header styles */
        .invoice-header {
            width: 100%;
            margin-bottom: 20px;
            background-color: #f3f3f3;
            padding: 10px;
        }

        .invoice-header h3 {
            margin: 0;
            color: #333;
        }

        /* Invoice details styles */
        .invoice-details {
            width: 100%;
            margin-bottom: 20px;
        }

        .invoice-details ul {
            margin: 0;
            padding: 0;
            list-style: none;
        }

        .invoice-details .text-muted {
            font-size: 14px;
            color: #888;
            position: relative;
            top: -10px;
        }

        .invoice-details .fw-bold {
            font-weight: bold;
        }

        .invoice-details .badge {
            font-size: 12px;
            color: #555;
            background-color: #f3f3f3;
            padding: 4px 8px;
            border-radius: 4px;
        }

        /* Invoice table styles */
        .invoice-table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 20px;
        }

        .invoice-table th,
        .invoice-table td {
            border: 1px solid #ccc;
            padding: 10px;
        }

        .invoice-table thead th {
            background-color: #f3f3f3;
            color: #333;
        }

        .invoice-table tfoot td {
            text-align: right;
            font-weight: bold;
        }

        /* Custom table styles */
        .custom-table th,
        .custom-table td {
            border: 1px solid #ccc;
            padding: 10px;
        }

        /* Invoice footer styles */
        .invoice-footer {
            width: 100%;
            background-color: #f3f3f3;
            padding: 10px;
        }

        .invoice-footer p {
            margin: 0;
            font-size: 14px;
            color: #555;
        }

        /* Colors */
        .text-bold {
            font-weight: bold;
            color: #333;
        }

        .text-primary {
            color: #007bff;
        }

        .text-muted {
            color: #888;
        }

        .bg-warning {
            background-color: #ffc107;
        }

        .text-black {
            color: #000;
        }

        ul.list-unstyled {
            text-indent: 10px;
        }
        @media print {
            body {
                margin: 0;
                padding: 0;
                width: 74mm; /* Tương đương với chiều rộng của giấy A7 */
                height: 105mm; /* Tương đương với chiều cao của giấy A7 */
            }
        }
        /* Container styles */


        /* Invoice header styles */
        .invoice-header {
            /* ... */
            background-color: #f3f3f3;
        }

        .invoice-header h3 {
            /* ... */
            color: #333;
        }

        /* Invoice details styles */
        .invoice-details .text-muted {
            /* ... */
            color: #888;
        }

        .invoice-details .fw-bold {
            font-weight: bold;
        }

        .invoice-details .badge {
            /* ... */
            background-color: #f3f3f3;
            color: #555;
        }

        /* Invoice table styles */
        .invoice-table th,
        .invoice-table td {
            /* ... */
            border: 1px solid #ccc;
        }

        .invoice-table thead th {
            /* ... */
            background-color: #f3f3f3;
            color: #333;
        }

        .invoice-table tfoot td {
            /* ... */
            text-align: right;
            font-weight: bold;
        }

        /* Invoice footer styles */
        .invoice-footer {
            /* ... */
            background-color: #f3f3f3;
        }

        .invoice-footer p {
            /* ... */
            color: #555;
        }

        /* Colors */
        .text-bold {
            font-weight: bold;
            color: #333;
        }

        .text-primary {
            color: #007bff;
        }

        .text-muted {
            color: #888;
        }

        .bg-warning {
            background-color: #ffc107;
        }

        .text-black {
            color: #000;
        }

    </style>

</head>

<body>
<div class="container">
    <div class="card">
        <div class="card-body">
            <div class="invoice-header">
                <h3>Invoice >> <strong>ID: # {!! $getOnelinePayment->id !!}</strong></h3>
            </div>
            <hr>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 50%;">
                        <ul class="list-unstyled">
                            <li class="text-muted">Thu ngân: <span>{!! $getOrderTable[0]->customer_id !!}</span></li>
                            <li class="text-muted">Ngày: <span>{!! $getOnelinePayment->payment_date !!}</span></li>
                            <li class="text-muted">Bàn: <span>{!! $getTable[0]->table_name !!}</span></li>
                        </ul>
                    </td>
                    <td style="width: 50%;">
                        <ul class="list-unstyled">
                            <li class="text-muted"><i class="fas fa-circle"></i> Invoice</li>
                            <li class="text-muted"><i class="fas fa-circle"></i> <span class="fw-bold">Mở bàn:</span> <span>{!! $getOrder[0]->order_create_time !!}</span></li>
                            <li class="text-muted"><i class="fas fa-circle"></i> <span class="fw-bold">Đóng bàn:</span> <span>{!! $getOrder[0]->order_finish_time !!}</span></li>
                        </ul>
                    </td>
                </tr>
            </table>


            <table class="invoice-table custom-table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Tên sản phẩm</th>
                    <th scope="col">Số lượng</th>
                    <th scope="col">Giá</th>
                    <th scope="col">Tổng cộng</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orderDetails as $orderDetail)
                    <tr>
                        <th scope="row">{{$orderDetail->id}}</th>
                        <td>{{$orderDetail->product_name}}</td>
                        <td>{{$orderDetail->order_detail_quantity}}</td>
                        <td>{{$orderDetail->order_detail_price}}</td>
                        <td>{!! $orderDetail->product_amount !!}</td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="4" class="text-end"><strong>Tổng cộng:</strong></td>
                    <td><strong>${!! $getOnelinePayment->payment_amount !!}</strong></td>
                </tr>
                </tfoot>
            </table>

            <div class="invoice-footer">
                <div style="width: 50%;">
                    <p style="font-size: 12px">Thêm ghi chú bổ sung và thông tin thanh toán</p>
                </div>
                <div style="width: 50%;">
                    <table class="table-total" style="border-collapse: separate;">
                        <tr>
                            <td style="width: 70%;"><span class="text-bold">Tổng cộng:</span></td>
                            <td style="width: 30%;" align="right"><span>${!! $getOnelinePayment->payment_amount !!}</span></td>
                        </tr>
                        <tr>
                            <td style="width: 70%;"><span class="text-bold">Voucher</span></td>
                            <td style="width: 30%;" align="right"><span>{!!$getOnelinePayment->payment_amount * $getVoucher->discount_amount  !!}</span></td>
                        </tr>
                        <tr>
                            <td style="width: 70%;"><span class="text-bold">Tổng:</span></td>
                            <td style="width: 30%;" align="right"><span class="text-primary">{!! $getOnelinePayment->payment_amount - $getOnelinePayment->payment_amount * $getVoucher->discount_amount !!} </span></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
