<div class="tab-pane fade col col-sm-12 col-md-12 col-xl-12" id="ship-justified" role="tabpanel" aria-labelledby="contact-tab">
    <div class="container-fluid" style="position: relative">
        <div class="my-1">
            <div class="d-flex flex-wrap py-1">
                @if(count($orderTableShip) > 0)
                    @foreach($orderTableShip as $items)
                        @php
                            $orderDetails = $items->orderDetail->where('order_detail_status', 3)->all();
                             $prices = array_column($orderDetails, 'order_detail_price');
                            $quantities = array_column($orderDetails, 'order_detail_quantity');

                            $multipliedValues = array_map(function ($price, $quantity) {
                                return $price * $quantity;
                            }, $prices, $quantities);
                        @endphp
                        <div class="mb-3 grid-3 grid-2" style="width: 25%; box-sizing:border-box; padding: 0 8px;">
                            <div class="border">
                                <div class="d-flex align-items-center justify-content-between px-2 text-white" style="height: 3rem; background-color: #2A72D6; ">
                                    <div class="px-1">Mã order: {{$items->id}}</div>
                                    <div class="d-flex px-1">
                                        <i class="bi bi-person-fill ps-2"></i>
                                        <span style="margin-left: 4px">{{$items->count_of_customer}}</span>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between align-items-center " style="width: 100%; height: 8rem">
                                    <div class="bg-light d-flex align-items-center justify-content-center border" style="width:100%; height: 8rem">
                                        <div class="bg-light d-flex align-items-center justify-content-center border" style="width:100%; height: 8rem">
                                            <h4>{{ $items->table ? $items->table->tableArea->table_area_key.''.$items->table->tableArea->table_floor .''.sprintf("%02d", $items->table->table_name) : $items->order->orderType->order_type_name }}</h4>
                                        </div>
                                    </div>
                                    <div class="" style="width:100%; height: 8rem">
                                        <div class="border d-flex align-items-center justify-content-center" style="width:100%; height: 50%">
                                            {{ number_format(array_sum($multipliedValues), 2, '.', ',') }} VNĐ
                                        </div>
                                        @php
                                            $duration = floor((time() - strtotime($items->order->order_create_time)) / 60);
                                             $days = floor($duration / 1440); // Số ngày
                                             $hours = floor(($duration % 1440) / 60); // Số giờ
                                             $minutes = $duration % 60; // Số phút
                                             $formattedDuration = ($days > 0 ? $days . ' d ' : '') . ($hours > 0 ? $hours . ' h ' : '') . $minutes . ' i';
                                        @endphp
                                        <div class="border d-flex align-items-center justify-content-center" style="width:100%; height: 50%">
                                            <div class="border d-flex align-items-center justify-content-center" style="width:100%; height: 100%;">
                                                <i class="bi bi-alarm me-2" ></i>
                                                <span id="minutes">{{ $formattedDuration }}</span>
                                                <span>'</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between align-items-center " style="width: 100%; height: 3rem">
                                    <div title="Thanh toán" class="bg-light d-flex align-items-center justify-content-center border" type="button" style="width:100%; height: 100%" >
                                        <a href="{{route('orders.payment_detail',$items->id)}}" style="text-decoration: none; color:#666"><i class="bi bi-calculator" style="font-size: 1.5rem"></i></a>
                                    </div>
                                    <div title="Sửa" class="bg-light d-flex align-items-center justify-content-center border" type="button" style="width:100%; height: 100%">
                                        <a href="{{ route('orders.edit_order', $items->id) }}" style="text-decoration: none; color:#666"><i class="bi bi-pencil" style="font-size: 1.5rem"></i></a>
                                    </div>
                                    <div title="Kiểm đồ"
                                         class="bg-light d-flex align-items-center justify-content-center border"
                                         type="button" style="width:100%; height: 100%">
                                        <a data-bs-toggle="modal"
                                           data-bs-target="#verticalycentered_payment{{$items->id}}"
                                           style="text-decoration: none; color:#666"><i class="bi bi-journal-check"
                                                                                        style="font-size: 1.5rem"></i></a>
                                    </div>
                                    <div title="Tùy chọn"
                                         class="bg-light d-flex align-items-center justify-content-center border"
                                         type="button"  id="orderDropdown"
                                         data-bs-toggle="dropdown"
                                         aria-expanded="false"
                                         style="width:100%; height: 100%">
                                        <span style="font-size: 1.5rem">...</span>
                                    </div>
                                    <ul class="dropdown-menu" aria-labelledby="orderDropdown">
                                        <li>
                                            <a class="dropdown-item" style="font-weight: bolder"
                                               href="{{route('orders.details',$items->id)}}">
                                                Xem chi tiết
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="modal fade" id="verticalycentered_payment{{$items->id}}" tabindex="-1">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Danh sách món</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                        aria-label="close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">Mã món</th>
                                                        <th scope="col">Tên món</th>
                                                        <th scope="col">Số lượng</th>
                                                        <th scope="col">Thành tiền</th>
                                                        <th scope="col">Ghi chú</th>
                                                        <th scope="col">Trạng thái</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(!empty($orderDetails))
                                                        @foreach($orderDetails as $detail)
                                                            <tr>
                                                                <td>{{$detail->product ? $detail->product->id : 'Món khác'}}</td>
                                                                <td>{{$detail->product ? $detail->product->product_name : $detail->order_detail_other_name}}</td>
                                                                <td>{{$detail->order_detail_quantity}}</td>
                                                                <td>{{$detail->order_detail_price}}</td>
                                                                <td>{{$detail->order_detail_note}}</td>
                                                                <td>{{$detail->getStatusTextAttribute()}}</td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="6">Danh sách món của order có mã {{$items->id}}
                                                                đang trống.
                                                        </tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                        data-bs-dismiss="modal">Đóng
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="text-center">Phiếu tạm tính giao hàng đang trống</div>
                @endif
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-center align-items-center">
        {{$orderTableShip->links()}}
    </div>
</div>

