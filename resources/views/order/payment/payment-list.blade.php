@extends('layouts.app')

@section('title', 'Payment list')

@section('content')
    <div class="card container mt-2">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        @if(session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        <h3 class="mt-4 mx-lg-3" style="color: #0C1451; font-weight: bold">Danh sách phiếu tạm tính</h3><br>

        <div class="row card-body">
            <main style="position: relative">
                <ul class="nav nav-tabs d-flex shadow-sm" id="myTabjustified" role="tablist">
                    <li class="nav-item col-sm flex-fill" role="presentation">
                        <button class="nav-link w-100 active fw-bold" id="home-tab" data-bs-toggle="tab"
                                data-bs-target="#used-justified" type="button" role="tab" aria-controls="home"
                                aria-selected="true">Đang dùng (<span>{{$orderTable->count()}}</span>)
                        </button>
                    </li>
                    <li class="nav-item col-sm flex-fill" role="presentation">
                        <button class="nav-link w-100 fw-bold" id="profile-tab" data-bs-toggle="tab"
                                data-bs-target="#bringback-justified" type="button" role="tab" aria-controls="profile"
                                aria-selected="false">Mang về (<span>{{$orderTableBringBack->count()}}</span>)
                        </button>
                    </li>
                    <li class="nav-item col-sm flex-fill" role="presentation">
                        <button class="nav-link w-100 fw-bold" id="contact-tab" data-bs-toggle="tab"
                                data-bs-target="#ship-justified" type="button" role="tab" aria-controls="contact"
                                aria-selected="false">Giao hàng (<span>{{$orderTableShip->count()}}</span>)
                        </button>
                    </li>
                    <li class="nav-item col-sm flex-fill" role="presentation">
                        <button class="nav-link w-100 fw-bold" id="contact-tab" data-bs-toggle="tab"
                                data-bs-target="#resever-justified" type="button" role="tab" aria-controls="contact"
                                aria-selected="false">Đặt trước (<span>{{$orderTableReserve->count()}}</span>)
                        </button>
                    </li>
                </ul>


                <div class="tab-content pt-2 text-center" id="myTabjustifiedContent">

                    <!--Tab đang dùng-->
                    @include('order.payment.tab-using-in-payment-list')
                    <!--Tab mang về-->
                    @include('order.payment.tab-bring-back-in-payment-list')
                    <!--Tab giao hàng-->
                    @include('order.payment.tab-ship-in-payment-list')
                    <!--Tab đặt trước-->
                    @include('order.payment.tab-reserve-in-payment-list')
                </div>

            </main>
        </div>
    </div>
@endsection
