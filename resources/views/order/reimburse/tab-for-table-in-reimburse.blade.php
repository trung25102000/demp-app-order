<!--Tab Tra mon theo ban-->
<table class="table" id="table-detail-for-table" xmlns="">
    <thead>
        <tr>
            <th scope="col">Số bàn</th>
            <th scope="col">Số lượng món</th>
            <th scope="col">Thời gian tạo</th>
            <th scope="col">Chọn</th>
            <th scope="col">Tùy chọn</th>
        </tr>
    </thead>
    <tbody>
        @if ( $dataListOrderTableWithTables->count() > 0)
                @foreach ($dataListOrderTableWithTables as $item)
                    <tr class="table-row-table">
                        <input type="hidden" value="{{ $item->id }}">
                        <td>{{$item->tableArea->table_area_key . '' . $item->tableArea->table_floor . '' . sprintf('%02d', $item->table_name)  }}
                        </td>
                        <td class="{{ $item->id }}item-count-table"></td>
                        <td>
                            @php
                                $orderCreateTime = \Carbon\Carbon::parse($item->updated_at);
                                $timeDiff = $orderCreateTime->diffInMinutes(now());
                                if ($timeDiff >= 60) {
                                    $hours = $orderCreateTime->diffInHours(now());
                                    echo $hours . ' giờ trước';
                                } else {
                                    echo $timeDiff . ' phút trước';
                                }
                            @endphp
                        </td>
                        <td><input class="form-check-input selected-for-table" type="checkbox" id="gridCheck1"
                                value="{{ $item->id }}"></td>
                        <td id="button-modal">
                            <a href="{{ $item->table_id ? route('order.reimburseForTable', $item->table_id) : route('order.reimburseForTableByOrder', $item->id) }}}"
                                title="trả món"
                                onclick="return confirm('Bạn có chắc muốn trả món theo bàn này hay không?')"
                                class="btn text-white" style="background-color:#06357A "><i
                                    class="bi bi-check-circle-fill"></i></a>
                            <button type="button-table" class="btn btn-secondary" data-bs-toggle="modal"
                                data-bs-target="#verticalycentered_table_{{ $item->id }}">
                                <i class="bi bi-three-dots"></i>
                            </button>
                            <div class="modal-detail-table" id="modal-detail-table">
                                <div class="modal fade" id="verticalycentered_table_{{ $item->id }}" tabindex="-1">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Danh sách món</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="close"></button>
                                            </div>
                                            <div class="{{ $item->id }}modal-body-table">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">Mã món</th>
                                                            <th scope="col">Tên món</th>
                                                            <th scope="col">Số lượng</th>
                                                            <th scope="col">Ghi chú</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-bs-dismiss="modal">Đóng
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </td>
                    </tr>
                @endforeach
        @else
            <tr>
                <td colspan="5" id="empty-span">Danh sách món theo oder đang trống!</td>
            </tr>
        @endif
    </tbody>
</table>
