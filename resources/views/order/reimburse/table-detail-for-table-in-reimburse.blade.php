@if ($item->orderDetail->count() > 0)
    <tr>
        <td>{{ $item->table ? $item->table->table_name : $item->order->orderType->order_type_name }}</td>
        <td id="count">{{$item->orderDetail->count()}}</td>
        <td>
            @php
                $orderCreateTime = \Carbon\Carbon::parse($item->updated_at);
                $timeDiff = $orderCreateTime->diffInMinutes(now());
                if ($timeDiff >= 60) {
                    $hours = $orderCreateTime->diffInHours(now());
                    echo $hours . ' giờ trước';
                } else {
                    echo $timeDiff . ' phút trước';
                }
            @endphp
        </td>
        <td><input class="form-check-input" type="checkbox" id="gridCheck1"></td>
        <td>
            <a href="{{$item->table_id?route('order.reimburseForTable',$item->table_id):route('order.reimburseForTableByOrder',$item->id)}}}"
               title="trả món"
               onclick="return confirm('Bạn có chắc muốn trả món theo bàn này hay không?')"
               class="btn text-white" style="background-color:#06357A "><i
                    class="bi bi-check-circle-fill"></i></a>
            <button type="button" class="btn btn-secondary" data-bs-toggle="modal"
                    data-bs-target="#verticalycentered_table_{{ $loop->iteration }}">
                <i class="bi bi-three-dots"></i>
            </button>
            <div class="modal-detail-table">
                <div class="modal fade" id="verticalycentered_table_{{ $loop->iteration }}"
                     tabindex="-1">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Danh sách món</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="close"></button>
                            </div>
                            <div class="modal-body">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th scope="col">Mã món</th>
                                        <th scope="col">Tên món</th>
                                        <th scope="col">Số lượng</th>
                                        <th scope="col">Ghi chú</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if ($item->orderDetail->count() > 0)
                                        @foreach ($item->orderDetail as $detail)
                                            <tr>
                                                <td>{{ $detail->product ? $detail->product->id : 'Món khác' }}
                                                </td>
                                                <td>{{ $detail->product ? $detail->product->product_name : $detail->order_detail_other_name }}
                                                </td>
                                                <td>{{ $detail->order_detail_quantity }}
                                                </td>
                                                <td>{{ $detail->order_detail_note }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4">Danh sách món của
                                                bàn {{ $item->table->table_name }} đang
                                                trống.
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Đóng
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
@endif
