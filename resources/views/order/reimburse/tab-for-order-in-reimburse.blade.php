<table class="table" id="table-detail-for-order">
    <thead>
        <tr>
            <th scope="col">Mã Order</th>
            <th scope="col">Số lượng món</th>
            <th scope="col">Thời gian tạo</th>
            <th scope="col">Chọn</th>
            <th scope="col">Tùy chọn</th>
        </tr>
    </thead>
    <tbody>
        @if ($dataListOrderTableWithOrders->count() > 0)
                @foreach ($dataListOrderTableWithOrders as $item)
                    <tr class="table-row">
                        <input type="hidden" value="{{ $item->id }}">
                        <td>{{ $item->id }}</td>
                        <td class="{{ $item->id }}item-count"></td>
                        <td>
                            @php
                                $orderCreateTime = \Carbon\Carbon::parse($item->order_accept_time);
                                $timeDiff = $orderCreateTime->diffInMinutes(now());
                                if ($timeDiff >= 60) {
                                    $hours = $orderCreateTime->diffInHours(now());
                                    echo $hours . ' giờ trước';
                                } else {
                                    echo $timeDiff . ' phút trước';
                                }
                            @endphp
                        </td>
                        <td><input class="form-check-input selected-for-order" value="{{ $item->id }}"
                                type="checkbox" id="gridCheck1"></td>
                        <td>
                            <a title="trả món"
                                onclick="return confirm('Bạn có chắc muốn trả món theo bàn này hay không?')"
                                href="{{ route('order.reimburseForOrder', $item->id) }}" class="btn text-white"
                                style="background-color:#06357A "><i class="bi bi-check-circle-fill"></i></a>
                            <button type="button" class="btn btn-secondary" data-bs-toggle="modal"
                                data-bs-target="#verticalycentered_order_{{ $item->id }}">
                                <i class="bi bi-three-dots"></i>
                            </button>
                            <div id="modal-detail-order">
                                <div class="modal fade" id="verticalycentered_order_{{ $item->id }}" tabindex="-1">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Danh sách món</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="close"></button>
                                            </div>
                                            <div class="{{ $item->id }}modal-body">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">Mã món</th>
                                                            <th scope="col">Tên món</th>
                                                            <th scope="col">Số lượng</th>
                                                            <th scope="col">Ghi chú</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-bs-dismiss="modal">Đóng
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
        @else
            <tr>
                <td colspan="5" id="empty-span">Danh sách món theo oder đang trống!</td>
            </tr>
        @endif
    </tbody>
</table>
