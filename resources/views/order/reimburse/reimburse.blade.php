@extends('layouts.app')
@section('title','Reimburse')
@section('style-after')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous" />
@endsection
@section('content')
    <div class="card content container mt-2">
        @if (session('success'))
            <div class="alert alert-success" id="success-alert">
                {{ session('success') }}
            </div>
        @endif

        @if (session('error'))
            <div class="alert alert-danger" id="error-alert">
                {{ session('error') }}
            </div>
        @endif
        <div id="reimuburse" class="card-body">
            <div class="col-12 col-xl-12 col-mb-12 col-md-12 mt-3">
                <h3 style="color: #0C1451; font-weight: bold;">Danh sách trả món</h3>
                <div id="toast"></div>
            </div>
            <div class="mb-3">
                <div class="row">
                    <div class="col-xl-2 col-6 col-md-6 col-sm-6 mt-3 align-right">
                        <select id="category-select" class="form-select" aria-label="Default select example"
                                style="font-weight: bold">
                            @if(isset($category) && $category == '1')
                                <option value="2">Bếp</option>
                                <option selected value="3">Bar</option>
                            @else
                                <option selected value="2">Bếp</option>
                                <option value="3">Bar</option>
                            @endif
                        </select>
                        </form>
                    </div>

                    <div class="col-xl-4 col-6 col-md-7 col-sm-7">
                        <input type="text" name="search_order" class="form-control mt-3" id="searchReimuburse"
                               placeholder="Tìm kiếm món theo tên món, mã bàn, mã order">
                    </div>
                    <div class="col-xl-1 col-2 col-sm-2 col-md-1 col-md-2">
                        <button class="btn text-white mt-3 " style="background-color:#06357A" type="submit"><i
                                class="bi bi-search"></i>
                        </button>
                    </div>

                    <div class="col-xl-2 col-4 col-sm-3 col-md-3">
                        <button class="btn text-white mt-3 reimbursed-many" type="submit"
                                style="position: absolute; right: 30px;background-color:#06357A"><i
                                class="bi bi-check"></i>Trả
                            món
                        </button>
                    </div>
                </div>
            </div>

            <!-Thanh nava->
            <ul class="nav nav-tabs d-flex shadow-sm" id="myTabjustified" role="tablist">
                <li class="nav-item col-sm flex-fill" role="presentation">
                    <button class="nav-link w-100 active fw-bold" id="home-tab" data-bs-toggle="tab" data-type="1"
                            data-bs-target="#home-justified" type="button" role="tab" aria-controls="home"
                            aria-selected="true">Tất cả
                    </button>
                </li>
                <li class="nav-item col-sm flex-fill" role="presentation">
                    <button class="nav-link w-100 fw-bold" id="profile-tab" data-bs-toggle="tab" data-type="2"
                            data-bs-target="#profile-justified" type="button" role="tab" aria-controls="profile"
                            aria-selected="false">Trả món theo bàn
                    </button>
                </li>
                <li class="nav-item col-sm flex-fill" role="presentation">
                    <button class="nav-link w-100 fw-bold" id="contact-tab" data-bs-toggle="tab" data-type="3"
                            data-bs-target="#contact-justified" type="button" role="tab" aria-controls="contact"
                            aria-selected="false">Trả món theo Order
                    </button>
                </li>
            </ul>
            <!-End nava->
            <div class="tab-content pt-2 text-center list-detail-reimburse" id="myTabjustifiedContent">
                <!-Start in tab list->
                <div id="reimburse_data">
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script-after')
    <script>
        REIMBURSE_DATA = @json(route('orders.reimburse_data'));
        REIMBURSED_MANY_ORDER = @json(route('order.reimburse_many_order'));
        REIMBURSED_MANY_TABLE= @json(route('order.reimburse_many_table'));
        REIMBURSED_MANY_PRODUCT = @json(route('order.reimburse_many_product'));
        CANCEL_DETAIL = @json(route('detail.cancel'));
    </script>
    <script src="{{asset('order/js/reimuburse.js')}}"></script>
@endsection
