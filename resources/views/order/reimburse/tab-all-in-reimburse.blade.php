<!--Tab tat ca-->
<div class="tab-pane fade show active  col col-sm-12 col-md-12 col-xl-12" style="max-height: 630px; overflow-y: auto;"
    id="home-justified" role="tabpanel" aria-labelledby="home-tab">
    <div class="table-responsive" style="overflow-x: auto;">
        <table class="table" id="table-detail-for-all">
            <thead>
                <tr>
                    <th scope="col">Mã món</th>
                    <th scope="col">Tên món</th>
                    <th scope="col">Số lượng</th>
                    <th scope="col">Số bàn</th>
                    <th scope="col">Thời gian đặt</th>
                    <th scope="col">Ghi chú</th>
                    <th scope="col">Chọn</th>
                    <th scope="col">Tùy chọn</th>
                </tr>
            </thead>
            <tbody id="body-all">
                @if ($dataListDetails->count() > 0)
                    @foreach ($dataListDetails as $item)
                        <tr>
                            <th scope="row">{{ $item->product->product_key }}</th>
                            <td>{{ $item->product->product_name }}</td>
                            <td>{{ $item->order_detail_quantity }}</td>
                            <td>{{ $item->orderTable->table ? $item->orderTable->table->tableArea->table_area_key . '' . $item->orderTable->table->tableArea->table_floor . '' . sprintf('%02d', $item->orderTable->table->table_name) : $item->orderTable->order->orderType->order_type_name }}
                            </td>
                            <td>
                                @php
                                    $orderCreateTime = \Carbon\Carbon::parse($item->updated_at ?? '');
                                    $timeDiff = $orderCreateTime->diffInMinutes(now());
                                    if ($timeDiff >= 60) {
                                        $hours = $orderCreateTime->diffInHours(now());
                                        echo $hours . ' giờ trước';
                                    } else {
                                        echo $timeDiff . ' phút trước';
                                    }
                                @endphp
                            </td>
                            <td>{{ $item->order_detail_note }}</td>
                            <td><input type="checkbox" class="form-check-input selected-for-all"
                                    value="{{ $item->id }}"></td>
                            <td>
                                <div>
                                    <button class="btn text-white"style="background-color:#06357A"
                                        onclick="confirmAndRedirect(<?php echo $item->id; ?>)"><i
                                            class="bi bi-check-circle-fill"></i></button>
                                    <button class="btn btn-danger text-white" value="{{ $item->id }}"
                                        data-bs-toggle="modal" data-bs-target="#cancel-detail_{{ $item->id }}"><i
                                            class="bi bi-trash"></i></button>
                                    <div id="modal-cancel">
                                        <div class="modal fade" id="cancel-detail_{{ $item->id }}" tabindex="-1">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Hủy Món</h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h6>Nhập lý do hủy món:</h6>
                                                        <textarea type="text" name="reason-cancel-detail" id="reason-cancel-{{$item->id}}" class="form-control"></textarea>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger"
                                                            data-bs-dismiss="modal">Thoát
                                                        </button>
                                                        <button type="submit" id="cancel-detail-button-{{$item->id}}" 
                                                            value="{{ $item->id }}" class="btn text-white"
                                                            style="background-color: #06357A">Hủy món
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="8" id="empty-span">Danh sách món đang trống!</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
