<div class="classModalCss">
    <!--Start Tách order-->
    <div class="modal fade" id="modalSplitTable" tabindex="-1" >
        <div class="modal-dialog modal-xl" style="top: -24px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" style="color: #06357A">Tách Order</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="d-flex">
                        <div class="MuiPaper-root MuiPaper-elevation MuiPaper-rounded MuiPaper-elevation0 MuiCard-root css-39wqca col">
                            <div class="MuiCardContent-root css-18mhetb">
                                <h6 class="row col-4 text-start fw-bold">Order hiện tại của bàn</h6>
                                <div class="MuiBox-root css-17bj277">
                                    <div class="MuiPaper-root MuiPaper-outlined MuiPaper-rounded css-wkkely">
                                        <div class="MuiBox-root css-1mw3cm6"></div>
                                        <div class="&quot; MuiBox-root css-rmekqa table_group">
                                            <span id="tableFrom"></span>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="MuiPaper-root MuiPaper-elevation MuiPaper-rounded Mui Paper-elevation0 MuiCard-root css-39wqca col">
                            <div id="errorContainerSplitTable" class="alert alert-danger" style="display: none;"></div>
                            <div class="MuiCardContent-root css-18mhetb">
                                <h6 class="row col-4 text-start fw-bold">Tách bàn mới</h6>
                                <div class="MuiBox-root css-17bj277" id="tableToList" style="max-height: 300px;overflow-y: auto;">

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div id="d-split-table">
                            <div class="col-lg-4">
                                <div>
                                    <select id="listFrom" size="10" class="form-select"></select>
                                </div>
                            </div>
                            <div class="col-lg-4 mt-3 mb-3">
                                <div class="d-button">
                                    <div style="display: flex; flex-direction: row; column-gap: 10px;">
                                        <button id="btnMinus" class="btn btn-outline-secondary">
                                            <i class="bi bi-dash-lg"></i>
                                        </button>
                                        <input type="text" id="tbQuantity" class="form-control"
                                               style="width: 100px; text-align: center;">
                                        <button id="btnPlus" class="btn btn-outline-success">
                                            <i class="bi bi-plus-lg"></i>
                                        </button>
                                    </div>
                                    <div style="display: flex; flex-direction: row; column-gap: 20px;">
                                        <button id="btnNext" class="btn btn-outline-primary" style="width: 90px;" disabled>
                                            <i class="bi bi-arrow-right"></i>
                                        </button>
                                        <button id="btnPrev" class="btn btn-outline-warning" style="width: 90px;" disabled>
                                            <i class="bi bi-arrow-left"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div>
                                    <select id="listTo" size="10" class="form-select">

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                            data-bs-dismiss="modal">Hủy
                    </button>
                    <button type="button" id="btnSplitTable" class="btn text-white"
                            style="background-color:#06357A">Lưu
                    </button>
                </div>
            </div>
        </div>

    </div>

    <!--End tách order-->
</div>
<!--Start Ghép bàn-->
<meta name="csrf-token" content="{{ csrf_token() }}">
<div id="match_{{$items['id']}}" class="modal fade" data-modal-index="{{$items['id']}}">
    @php
        $table = new \App\Models\Table();
        $tableRepository = new \App\Repositories\TableRepository($table); // Tạo instance của TableRepository
        $tableServices = new \App\Services\TableServices($tableRepository); // Truyền TableRepository vào constructor của TableServices
        $tablesInSameArea = $tableServices->getTablesInSameArea($items->table_id);
    @endphp
    <div class="modal-dialog modal-fullscreen-sm-down">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header d-flex justify-content-between align-items-center">
                <!-- Button close nằm ở góc bên cùng bên trái -->
                <button type="button" class="btn btn-close ms-2" onclick="moveToTopAll()" data-bs-dismiss="modal"
                        aria-label="Close"></button>

                <!-- Tiêu đề -->
                <h5 class="modal-title mx-auto" style="color: #06357a;">Vui lòng chọn bàn để ghép</h5>

                <!-- Button submit nằm ở góc cùng bên phải -->
                <button type="button" data-order-id="{{ $items->id }}" onclick="mergeTables(this)"
                        class="btn btn-primary" style="background-color: #06357a;color: white"><span>Ghép</span>
                </button>
            </div>


            <!-- Modal Body -->
            <div class="modal-body">
                <div class="rectangle2">
                    <span class="text">Ghép với</span>
                    <span class="text align-right"><i type="button"
                                                      class="bi bi-dash text-white bg-danger rounded-circle"
                                                      onclick="moveToTopAll()"
                                                      style="margin-right: 5px;margin-left: auto"></i> Chọn lại</span>
                </div>
                <div class="rectangle2 d-flex justify-content-end">
                    <span class="text">Chọn bàn</span>
                    <span class="text align-right">
                            <input class="search-bar" id="searchBar" type="text" placeholder="Tìm kiếm">
                            <i class="bi bi-search" type="button" style="margin-left: auto; margin-right: 5px;"></i>
                            Tìm kiếm
                        </span>
                </div>
                <div class="rectangle-container" data-tables="{{ json_encode($tablesInSameArea) }}">
                    @foreach ($tablesInSameArea as $table)
                        <div class="rectangle">
                            <i type="button" class="bi bi-plus-circle text-white"
                               onclick="moveToBottom(this)" style="background-color: #06357a"></i>
                            <span class="text3" data-table-id="{{ $table->id }}">Bàn
                                    {{ $table->table_name }}</span>
                        </div>
                    @endforeach
                </div>
            </div>

            <!-- Thêm checkbox -->
            <div class="d-flex justify-content-end mt-3">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="exampleCheckbox" name="checkbox">
                    <label class="form-check-label" for="exampleCheckbox">Ghép order</label>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End Ghép bàn-->

<!--Start modal kiểm đồ-->
<div class="modal fade" id="verticalycentered_payment{{$items->id}}" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Danh sách món  {{$items->table?' bàn '.$items->table->table_name:$items->order->orderType->order_type_name}}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal"
                        aria-label="close"></button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">Mã món</th>
                        <th scope="col">Tên món</th>
                        <th scope="col">Số lượng</th>
                        <th scope="col">Thành tiền</th>
                        <th scope="col">Ghi chú</th>
                        <th scope="col">Trạng thái</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($items->orderDetail) && $items->orderDetail->count()>0)
                        @foreach($items->orderDetail as $detail)
                            <tr>
                                <td>{{$detail->product ? $detail->product->id : 'Món khác'}}</td>
                                <td>{{$detail->product ? $detail->product->product_name : $detail->order_detail_other_name}}</td>
                                <td>{{$detail->order_detail_quantity}}</td>
                                <td>{{$detail->order_detail_price}}</td>
                                <td>{{$detail->order_detail_note}}</td>
                                <td>{{$detail->product?$detail->getStatusTextAttribute():""}}</td>
                            </tr>
                        @endforeach
                    @elseif(!empty($items->order->orderDetailByOrderID) && $items->order->orderDetailByOrderID->count()>0 && $items->status==6)
                        @foreach($items->order->orderDetailByOrderID as $detail2)
                            <tr>
                                <td>{{$detail2->product ? $detail2->product->id : 'Món khác'}}</td>
                                <td>{{$detail2->product ? $detail2->product->product_name : $detail2->order_detail_other_name}}</td>
                                <td>{{$detail2->order_detail_quantity}}</td>
                                <td>{{$detail2->order_detail_price}}</td>
                                <td>{{$detail2->order_detail_note}}</td>
                                <td>{{$detail2->product?$detail2->getStatusTextAttribute():""}}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">Danh sách món của order có mã {{$items->id}}
                                đang trống.
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                        data-bs-dismiss="modal">Đóng
                </button>
            </div>
        </div>
    </div>
</div>
<!--End modal kiểm đồ-->
