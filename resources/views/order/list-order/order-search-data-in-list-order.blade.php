@if ($orderTables->count() > 0)
<div class="tab-pane fade show active  col col-sm-12 col-md-12 col-xl-12" id="used-justified" role="tabpanel"
     aria-labelledby="home-tab">
    <div class="container-fluid" style="position: relative">
        <div class="my-1">
            <div class="d-flex flex-wrap py-1">
                    @foreach ($orderTables as $items)
                        <div class="mb-3 grid-3 grid-2" style="width: 25%; box-sizing:border-box; padding: 0 8px;">
                            <div class="border">
                                <div class="d-flex align-items-center justify-content-between px-2"
                                     style="height: 3rem; color: white;
                                            {{ $items->status === 1 ? 'background-color: #7ecbe5' : ($items->status === 2 ? 'background-color: #2a72d6' : ($items->status === 3 ? 'background-color: #1A5F7A' : ($items->status === 4 ? 'background-color: #435B66' : 'background-color:#06357a'))) }}">
                                    <div class="px-1"><b>Mã order: {{ $items->order->id }}</b></div>
                                    <div class="d-flex px-1">
                                        <i class="bi bi-person-fill ps-2"></i>
                                        <span style="margin-left: 4px">{{ $items->count_of_customer }}</span>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between align-items-center "
                                     style="width: 100%; height: 8rem">
                                    <div class="bg-light d-flex align-items-center justify-content-center border"
                                         style="width:100%; height: 8rem">
                                        @php
                                            $orderTable=\App\Models\OrderTable::where('order_id', $items->order_id)->where('display_order', 1)->first();
                                        @endphp
                                        @if ($items->status == 5 )
                                            <h4 class="tableName">{{$items->table->tableArea->table_area_key.''.$items->table->tableArea->table_floor .''.sprintf("%02d", $items->table->table_name)}}{{$orderTable?'-'.$orderTable->table->tableArea->table_area_key.''.$orderTable->table->tableArea->table_floor .''.sprintf("%02d", $orderTable->table->table_name):""}}
                                            </h4>
                                        @else
                                            <h4>{{ $items->table ? $items->table->tableArea->table_area_key.''.$items->table->tableArea->table_floor .''.sprintf("%02d", $items->table->table_name) : $items->order->orderType->order_type_name }}
                                            </h4>
                                        @endif
                                    </div>
                                    <div class="" style="width:100%; height: 8rem">
                                        <div class="border d-flex align-items-center justify-content-center"
                                             style="width:100%; height: 50%">
                                            {{ $items->getStatusTextAttribute() }}
                                        </div>
                                        @php
                                            $duration = floor((time() - strtotime($items->order->order_create_time)) / 60);
                                            $days = floor($duration / 1440); // Số ngày
                                            $hours = floor(($duration % 1440) / 60); // Số giờ
                                            $minutes = $duration % 60; // Số phút
                                            $formattedDuration = ($days > 0 ? $days . ' d ' : '') . ($hours > 0 ? $hours . ' h ' : '') . $minutes . ' i';
                                        @endphp
                                        <div class="border d-flex align-items-center justify-content-center"
                                             style="width:100%; height: 50%">
                                            <div class="border d-flex align-items-center justify-content-center"
                                                 style="width:100%; height: 100%;">
                                                <i class="bi bi-alarm me-2"></i>
                                                <span id="minutes">{{ $formattedDuration }}</span>
                                                <span>'</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between align-items-center"
                                     style="width: 100%; height: 3rem">
                                    <button title="Chuyển Bếp/bar" style="width: 100%; height: 100%; padding: 0"
                                            class="move-to-kitchen bg-light d-flex align-items-center justify-content-center border"
                                            type="button" data-order-id="{{ $items->id }}"
                                        {{ $items->status == 2 || $items->status == 5  ? 'disabled' : '' }}>
                                        <i style="text-decoration: none; color:#666" class="bi bi-window-sidebar"></i>
                                    </button>
                                    <div title="Sửa"
                                         class="bg-light d-flex align-items-center justify-content-center border"
                                         type="button" style="width:100%; height: 100%">
                                        <a href="{{ route('orders.edit_order', $items->id) }}"
                                           style="text-decoration: none; color:#666"><i class="bi bi-pencil"
                                                                                        style="font-size: 1.5rem"></i></a>
                                    </div>

                                    <div title="Kiểm đồ"
                                         class="bg-light d-flex align-items-center justify-content-center border"
                                         type="button" style="width:100%; height: 100%">
                                        <a data-bs-toggle="modal"
                                           data-bs-target="#verticalycentered_payment{{ $items->id }}"
                                           style="text-decoration: none; color:#666"><i class="bi bi-journal-check"
                                                                                        style="font-size: 1.5rem"></i></a>
                                    </div>

                                    <div title="Tùy chọn"
                                         class="bg-light d-flex align-items-center justify-content-center border"
                                         type="button" id="orderDropdown" data-bs-toggle="dropdown"
                                         aria-expanded="false" style="width:100%; height: 100%">
                                        <span style="font-size: 1.5rem">...</span>
                                    </div>
                                    <!--Start líst tùy chọn-->
                                    <ul class="dropdown-menu" aria-labelledby="orderDropdown">
                                        <li>
                                            <a class="dropdown-item" style="font-weight: bolder" href="#"
                                               data-bs-toggle="modal" data-bs-target="#match_{{ $items->id }}">Ghép
                                                bàn</a>
                                        </li>
                                        <li>
                                            <button type="button" class="dropdown-item btn-show-modal-split-table"
                                                    style="font-weight: bolder" data-order-id="{{ $items->id }}">
                                                Tách Bàn (order)
                                            </button>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" style="font-weight: bolder"
                                               href="{{ route('split-order-exist', $items->id) }}">
                                                Tách order (Có sẵn)
                                            </a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" style="font-weight: bolder"
                                               href="{{ route('orders.details', $items->id) }}">
                                                Xem chi tiết
                                            </a>
                                        </li>
                                        <li>
                                            <a id="deleteIT" class="dropdown-item" style="font-weight: bolder"
                                               data-id="{{ $items->id }}" type="button"> Xóa order</a>
                                        </li>
                                    </ul>
                                    <!--End líst tùy chọn-->
                                </div>

                                @include('order.list-order.list-modal-in-list-order')
                            </div>
                        </div>
                    @endforeach
            </div>
        </div>
    </div>
</div>
<div class="d-flex justify-content-center align-items-center">
    {{ $orderTables->links() }}
</div>
@else
    <div class="text-center">Order đang trống</div>
@endif
