@extends('layouts.app')
@section('title', 'Order list')
@section('style-after')
    <link href="{{ asset('/order/css/style-in-list-order.css') }}" rel="stylesheet">
<link href="{{ asset('/order/css/style-in-modal-add-table.css') }}" rel="stylesheet">
<link href="{{ asset('/order/css/modal-split-table.css') }}" rel="stylesheet">
<link href="{{ asset('/order/css/style-modal-split-table.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous" />
@endsection

@section('content')

    <div class="card card-body container mt-2">
        <div class="mt-3 ">
            <h3 style="font-size: 25px; color: #0C1451; font-weight: bold">Danh sách order</h3>
            <div id="toast"></div>
        </div>

        <div class="row mb-3">
            <!--Start selectbox Loc Order-->
            <div class="col-xl-auto mt-3">
                <select class="form-select" id="status" aria-label="Default select example" style="font-weight: bold"
                    onchange="filterByStatus(this.value)">
                    <option class="font-size" value="0">Hiển thị tất cả</option>
                    <option class="font-size" value="1">Mới</option>
                    <option class="font-size" value="2">Đang chế biến</option>
                    <option class="font-size" value="5">Order Ghép bàn</option>
                </select>
            </div>
            <!--End selectbox Loc Order-->
            <!--Start search-->
            <div class=" col-xl-4">
                <input type="text" name="search_order" id="search" class="form-control mt-3"
                    placeholder="Tìm kiếm Order theo mã Order, mã bàn, trạng thái">
            </div>
            <button class="btn col-xl-auto text-white" style="background-color: #06357a"><i
                    class="bi bi-search"></i> Tìm kiếm
            </button>
            <!--End search-->
        </div>
        <div class="row card-body">
            <main style="position: relative">
                <ul class="nav nav-tabs d-flex shadow-sm" id="myTabjustified" role="tablist">
                    <li class="nav-item col-sm flex-fill" role="presentation">
                        <button class="nav-link w-100 active" id="home-tab" data-bs-toggle="tab" data-type="1"
                            data-bs-target="#used-justified" type="button" role="tab" aria-controls="home"
                            aria-selected="true">Đang dùng (<span id="count-using"></span>)
                        </button>
                    </li>
                    <li class="nav-item col-sm flex-fill" role="presentation">
                        <button class="nav-link w-100" id="profile-tab" data-bs-toggle="tab" data-type="2"
                            data-bs-target="#bringback-justified" type="button" role="tab" aria-controls="profile"
                            aria-selected="false">Mang về (<span id="count-bringBack"></span>)
                        </button>
                    </li>
                    <li class="nav-item col-sm flex-fill" role="presentation">
                        <button class="nav-link w-100" id="contact-tab" data-bs-toggle="tab" data-type="4"
                            data-bs-target="#ship-justified" type="button" role="tab" aria-controls="contact"
                            aria-selected="false">Giao hàng (<span id="count-ship"></span>)
                        </button>
                    </li>
                    <li class="nav-item col-sm flex-fill" role="presentation">
                        <button class="nav-link w-100" id="contact-tab" data-bs-toggle="tab" data-type="3"
                            data-bs-target="#resever-justified" type="button" role="tab" aria-controls="contact"
                            aria-selected="false">Đặt trước (<span id="count-resever"></span>)
                        </button>
                    </li>
                </ul>


                <div class="tab-content pt-2 text-center" id="myTabjustifiedContent">
                    <!--Start list order-->
                    <div id="table_data">
                    </div>
                    <!--End list order-->

                </div>

            </main>
        </div>
    @endsection
    @section('script-after')
        <script>
            const URL_SEARCH = @json(route('orders.search_order'));
            let URL_BACK = @json(url()->previous());
            let URL_GET_DATA_TO_PUSHER = @json(route('get_data_to_pusher'));
        </script>
        <script src="{{ asset('order/js/add-table.js') }}"></script>
        <script src="{{ asset('order/assets/js/split-table.js') }}"></script>
        <script src="{{ asset('order/js/list-order.js') }}"></script>
    @endsection
