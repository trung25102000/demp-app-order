@extends('layouts.app')
@section('style-after')
    <link href="{{asset('/order/assets/css/style-modal-split-table.css')}}" rel="stylesheet">
    <link href="{{asset('/order/assets/css/split-bill.css')}}" rel="stylesheet">
@endsection
@section('content')
    <!--Start content-->
    <div class="card content container mt-2" style="top: 20px;">
        <div class=" mt-3">
            <h3 class="mt-6" style="font-size: 25px; color: #0C1451; font-weight: bold">Danh sách tách bill</h3>
        </div>
        <div class="container">
            <div class="content">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="mb-3">
                            <label for="">Tách bill hiện tại</label>
                        </div>
                        <div class="mb-3">
                            <button class="btn btn-outline-warning" data-id="{{$orderTable->id}}" id="billFrom">ID BILL: {{$orderTable->id}}</button>
                        </div>
                        <div id="errorContainerSplitTable" class="alert alert-danger" style="display: none;"></div>
                    </div>
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4">
                        <div class="mb-3">
                            <label for="">Tách order cho bill mới</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div>
                            <select id="listFromBill" size="10" class="form-select"></select>
                        </div>
                    </div>
                    <div class="col-lg-4 mt-3 mb-3">
                        <div class="d-button">
                            <div style="display: flex; flex-direction: row; column-gap: 10px;">
                                <button id="btnMinusBill" class="btn btn-outline-secondary">
                                    <i class="bi bi-dash-lg"></i>
                                </button>
                                <input type="text" id="tbQuantityBill" class="form-control">
                                <button id="btnPlusBill" class="btn btn-outline-success">
                                    <i class="bi bi-plus-lg"></i>
                                </button>
                            </div>
                            <div style="display: flex; flex-direction: row; column-gap: 20px;">
                                <button id="btnPrevBill" class="btn btn-outline-warning" disabled>
                                    <i class="bi bi-arrow-left"></i>
                                </button>
                                <button id="btnNextBill" class="btn btn-outline-primary" disabled>
                                    <i class="bi bi-arrow-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div>
                            <select id="listToBill" size="10" class="form-select"></select>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row mt-2 text-center justify-content-center">
                        <div class="col-3">
                            <button id="btnSplitBill" class="btn btn-outline-success mb-4">Tách thực đơn bill</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <!--End content-->
@endsection
@section('script-after')
    <script src="{{asset('order/assets/js/split-bill.js')}}"></script>
@endsection
