@extends('layouts.app')
@section('style-after')
    <link href="{{asset('/order/assets/css/split-bill.css')}}" rel="stylesheet">
    <link href="{{asset('/order/css/modal-split-table.css')}}" rel="stylesheet">
@endsection
@section('content')
    <!--Start content-->
    <div class="classModalCss">
        <div class="card content container mt-2" style="top: 20px;">
            <div class=" mt-3">
                <h3 class="mt-6" style="font-size: 25px; color: #0C1451; font-weight: bold">Danh sách tách bill</h3>
            </div>
            <div class="container">
                <div class="content">
                    <div class="row">
                        <div class="d-flex">
                            <div class="MuiPaper-root MuiPaper-elevation MuiPaper-rounded MuiPaper-elevation0 MuiCard-root css-39wqca col-6">
                                <div class="MuiCardContent-root css-18mhetb">
                                    <h6 class="row col-4 text-start fw-bold">Order hiện tại của bàn</h6>
                                    <div class="MuiBox-root css-17bj277">
                                        <div class="MuiPaper-root MuiPaper-outlined MuiPaper-rounded css-wkkely">
                                            <div class="MuiBox-root css-1mw3cm6"></div>
                                            <div class="&quot; MuiBox-root css-rmekqa table_group">
                                                <span data-id="{{$orderTable->id}}" id="orderFrom"></span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="MuiPaper-root MuiPaper-elevation MuiPaper-rounded Mui Paper-elevation0 MuiCard-root css-39wqca col-6">
                                <div id="errorContainerSplitOrder" class="alert alert-danger" style="display: none;"></div>
                                <div class="MuiCardContent-root css-18mhetb">
                                    <h6 class="row col-4 text-start fw-bold">Chọn tách order đã có sẵn</h6>
                                    <div class="MuiBox-root css-17bj277" id="orderToList"  style=" height:55px;overflow-y: auto">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6 d-flex mt-5">
                            <div class="col-lg-4">
                                <div>
                                    <select id="listFromOrder" size="10" class="form-select"></select>
                                </div>
                            </div>
                            <div class="col-lg-4 mt-3 mb-3">
                                <div class="d-button">
                                    <div style="display: flex; flex-direction: row; column-gap: 10px; margin-left: 30px">
                                        <button id="btnMinusOrder" class="btn btn-outline-secondary">
                                            <i class="bi bi-dash-lg"></i>
                                        </button>
                                        <input type="text" id="tbQuantityOrder" class="form-control">
                                        <button id="btnPlusOrder" class="btn btn-outline-success">
                                            <i class="bi bi-plus-lg"></i>
                                        </button>
                                    </div>
                                    <div style="display: flex; flex-direction: row; column-gap: 20px; margin-right: 41px;">
                                        <button id="btnPrevOrder" class="btn btn-outline-warning" disabled>
                                            <i class="bi bi-arrow-left"></i>
                                        </button>
                                        <button id="btnNextOrder" class="btn btn-outline-primary" disabled>
                                            <i class="bi bi-arrow-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div>
                                    <select id="listToOrder" size="10" class="form-select"></select>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="container">
                        <div class="row mt-2 text-center justify-content-center">
                            <div class="col-3">
                                <button id="btnSplitOrder" class="btn btn-outline-success mb-4">Tách thực đơn Order</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
    <!--End content-->
@endsection
@section('script-after')
    <script>
        const URL_SPLIT_ORDER = @json(route('split-order-exist-api'));
    </script>
    <script src="{{asset('order/assets/js/split-order-start.js')}}"></script>
@endsection
