@extends('layouts.app')
@section('title', 'Create Order')
@section('content')
    <!--Start #main-->
    <div hidden="" id="order-table-id" id-data="{{ $orderTable->id }}"></div>
    <div class="container-fluid mt-2">
        <div class="row">
            <div class="col-md-7 card">
                <div class="bg-light mt-2">
                    <div class="d-flex mb-3 bg-white">
                        <div class="border btn p-2 me-1 shadow categoryFilter" style="background-color: lightblue"
                            id="all-products">
                            <a class="nav-link me-2 fw-bold" href="#">Tất cả</a>
                        </div>
                        <div class="border btn p-2 me-1 shadow categoryFilter" id="best-seller">
                            <a class="nav-link me-2 fw-bold" href="#">Bán chạy</a>
                        </div>
                        @foreach ($categories as $category)
                            <div class="border btn p-2 me-1 shadow categoryFilter" data-id="{{ $category->id }}">
                                <a class="nav-link me-2 fw-bold" href="#">{{ $category->category_name }}</a>
                            </div>
                        @endforeach
                    </div>
                    <!--Thanh search-->
                    <div class="mb-3">
                        <div class="input-group">
                            <div class="col-11">
                                <input id="searchNameEating" type="text" class="form-control"
                                    placeholder="Tìm kiếm theo mã món, tên viết tắt, tên đầy đủ.">
                            </div>
                            <i class="btn form-control bi bi-search text-white" style="background-color: #06357a"></i>
                        </div>
                    </div>
                    <!-- Nội dung ảnh -->
                    <div id="food-list" class="d-flex flex-wrap bg-white over-flow-auto "
                        style="overflow: auto; height: 722px">
                    </div>
                </div>
            </div>
            <div class="col-md-5 card shadow">
                <div class="bg-light">
                    <div class="mb-3 d-flex align-items-center bg-white mt-2">
                        <div class="search-bar">
                            <form class="search-form d-flex align-items-center" method="POST" action="#">
                                <select name="table_id_edit" id="table_id_edit" class="form-control text-dark"
                                    style="background-color: lightblue; height:38px; width: 100px">
                                    <option value="{{ $orderTable->table_id ?? '' }}" selected class="fw-bold">Bàn
                                        {{ $orderTable->table->tableArea->table_area_key . '' . $orderTable->table->tableArea->table_floor . '' . sprintf('%02d', $orderTable->table->table_name) }}
                                    @foreach ($tables as $table)
                                        @if ($table->status == 1)
                                            <option value="{{ $table->id }}">
                                                Bàn
                                                {{ $table->tableArea->table_area_key . '' . $table->tableArea->table_floor . '' . sprintf('%02d', $table->table_name) }}
                                            </option>
                                        @endif
                                    @endforeach
                                </select>
                            </form>
                        </div>
                        <div class="d-flex">
                            <button class="form-control mx-1" style="height:38px;width: 100px" data-bs-toggle="modal"
                                value="{{ $orderTable->customer_id }}" id="customer-id-edit"
                                data-bs-target="#member-in-edit">
                                <i class="bi bi-person-fill ms-2"></i>
                            </button>
                            <input class="form-control text-dark" placeholder="Số khách"
                                value="{{ $orderTable->count_of_customer }}" id="count-customer-edit"
                                style=" height: 38px; width: 100px; text-align:center; border: none;background-color: lightblue" />
                        </div>
                        <!-- Chọn loại Order-->
                        <div>
                            <div class="dropdown">
                                <button class="btn text-white dropdown-toggle mx-2 type-id-edit" type="button"
                                    id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false"
                                    style="background-color: #06357a" value="{{ $orderTable->order->order_type_id }}">
                                    {{ $orderTable->order->orderType->order_type_name }}
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <li><a class="dropdown-item" href="#" data-value="Tại chỗ" data-bs-toggle="modal"
                                            data-bs-target="#Online">Tại chỗ</a></li>
                                    <li><a class="dropdown-item" href="#" data-value="Mang về" data-bs-toggle="modal"
                                            data-bs-target="#Back">Mang về</a></li>
                                    <li><a class="dropdown-item" href="#" data-value="Đặt trước"
                                            data-bs-toggle="modal" data-bs-target="#Reserved">Đặt trước</a></li>
                                    <li><a class="dropdown-item" href="#" data-value="Giao tận nơi"
                                            data-bs-toggle="modal" data-bs-target="#Ship">Giao tận nơi</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="" style="overflow: auto; height: 500px">
                        <table class="order-detail-eating table table-striped text-active mb-5">
                            <thead>
                                <tr class="" style="background-color: #e7e6e6; font-weight: 600;">
                                    <th scope="col">Tên món</th>
                                    <th scope="col">Đơn vị</th>
                                    <th scope="col">Số lượng</th>
                                    <th scope="col">Thành tiền</th>
                                    <th scope="col">Xoá</th>
                                </tr>
                            </thead>
                            <tbody id="tbody">
                            </tbody>
                        </table>
                    </div>

                    <div>
                        <div class="mb-2">
                            <div class="d-flex align-items-center">
                                <!--Button them mon khac-->
                                <div class="mt-2 pe-2">
                                    <button class="form-control bg-light d-flex align-items-center justify-content-left"
                                        style=" height:48px; width:15rem; " type="button" title="Search"
                                        data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@mdo">
                                        <i class="bi bi-plus-lg text-success" style="font-size: 1.5rem"></i> Thêm
                                        món khác
                                    </button>
                                </div>
                                <!-- Button gift -->
                                <div class="form-control bg-light d-flex align-items-center justify-content-center mt-2"
                                    style=" height:48px; width:64px; font-size: 1.5rem" data-bs-toggle="modal"
                                    id="modal-voucher-in-edit" data-bs-whatever="@mdo">
                                    <i class="bi bi-gift text-danger"></i>
                                </div>
                            </div>
                        </div>
                        <hr class="my-2">
                        <div class="row">
                            <div class="col-md-6 search-bar pe-2 mb-0 mb-sm-1">
                                <label>
                                    <select class="form-select" style="height: 48px; width: 100%" name="user-id"
                                        id="user-id-edit">
                                        <option value="{{ $orderTable->user_id }}">{{ $orderTable->user->user_name }}
                                        </option>
                                        @foreach ($user as $item)
                                            <option value="{{ $item->id }}">{{ $item->user_name }}</option>
                                        @endforeach
                                    </select>
                                </label>
                            </div>
                            <div class="col-md-6 d-flex align-items-center justify-content-between">
                                <div class="d-flex align-items-center">
                                    <i class="bi bi-exclamation-circle text-primary me-1" style="font-size: 1.5rem;"></i>
                                    <span style="font-size: 1.2rem; font-weight: 600;">Tổng tiền</span>
                                </div>
                                <div class="fw-bold" id="total-money-order" style="font-size: 1.2rem; font-weight: 600;">
                                    0 đ
                                </div>
                            </div>
                        </div>
                        <!-- Thanh tính tiền -->

                        <div class="bg-white mt-5">
                            <div class="d-flex justify-content-between align-items-center p-2">
                                <div>
                                    <button class="btn btn-store-order" style="background-color: #0C134F;">
                                        <a href="#" class="d-flex align-items-center text-white fw-bold">
                                            <i class="bi bi-send d-none d-sm-block fs-3"></i>
                                            <span class="pe-2">GỬI</span>
                                        </a>
                                    </button>
                                    <button type="submit" id="save-order-btn" class="btn"
                                        style="background-color: #0C134F;">
                                        <span class="mx-2 d-flex align-items-center text-white fw-bold">
                                            <i class="bi bi-x-lg d-none d-sm-block fs-3"></i>
                                            <span class="pe-2">Lưu</span>
                                        </span>
                                    </button>
                                </div>
                                <div>
                                    <button class="btn btn-danger">
                                        <a href="{{ url()->previous() }}"
                                            class="mx-2 d-flex align-items-center text-white fw-bold">
                                            <i class="bi bi-x-lg d-none d-sm-block fs-3"></i>
                                            <span class="pe-2">HUỶ</span>
                                        </a>
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('order.create-order.list-modal-in-create-order')
    @include('order.edit-order.list-modal-in-edit-order')
@endsection
@section('script-after')
    <script>
        const URL_SAVE_ORDER = @json(route('orders.save_order', $orderTable->id));
        const URL_GET_DETAIL_IN_EDIT = @json(route('order_detail_in_edit', $orderTable->id));
        const URL_LIST_ORDER = @json(route('orders.list_order'));
    </script>
    <script src="{{ asset('/order/js/create-order.js') }}"></script>
    <script src="{{ asset('/order/js/edit-order.js') }}"></script>
@endsection
