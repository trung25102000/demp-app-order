<!--Modal hien thi khach hang trang sua order-->
<div class="modal modal-lg" id="member-in-edit" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Thành viên</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal"
                        aria-label="Close"></button>
            </div>
            <div class="modal-body row">
                <div>
                    <p class="mt-2" style="font-weight: bold; font-size: 18px">Thông tin khách hàng</p>
                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Tên</th>
                            <th scope="col">Ngày sinh</th>
                            <th scope="col">SĐT</th>
                            <th scope="col" colspan="2">Ngày ĐK</th>
                        </tr>
                        </thead>
                        <tbody id="customerTableBody">
                        @if($orderTable->customer!=null)
                        <tr>
                            @if($orderTable->customer_id)
                            <th scope="row">{{$orderTable->customer->id}}</th>
                            <td>{{$orderTable->customer->customer_name}}</td>
                            <td>{{$orderTable->customer->customer_birthday}}</td>
                            <td>{{$orderTable->customer->customer_phone}}</td>
                            <td>{{$orderTable->customer->created_at->format('Y-m-d')}}</td>
                            <td><input type="hidden" name="customer_id" value="{{$orderTable->customer->id}}"></td>
                            @endif
                        </tr>
                            @else
                        <tr>
                            <td colspan="6">Thông tin khách hàng trống!</td>
                        </tr>
                        @endif
                        </tbody>
                        <div id="error-message" style="color: red"></div>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger"
                        data-bs-dismiss="modal">
                    Đóng
                </button>
            </div>
        </div>
    </div>
</div>

<!--Add voucher in edit order-->
<div class="modal fade" id="voucherInEdit" tabindex="-1"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"
                 style="background-color: #0973b9; color: #fff">
                <h5 class="modal-title" id="exampleModalLabel">Chọn chương trình
                    khuyến mãi</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal"
                        aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form>
                    <div>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th scope="col">Chọn</th>
                                <th scope="col">Mã</th>
                                <th scope="col">Thông tin mã</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($voucher as $item)
                                <tr>
                                    <td>
                                        <input type="checkbox" class="voucher-checkbox" id="voucher-in-edit"
                                               value="{{$item->id}}"  discount-amount="{{$item->discount_amount}}"
                                               name="apply-voucher-id"
                                            {{ $orderTable->voucher_id == $item->id ? 'checked' : '' }}>
                                    </td>
                                    <td>{{$item->voucher_key}}</td>
                                    <td>{{$item->voucher_description}}</td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                        data-bs-dismiss="modal">Huỷ
                </button>
                <button type="button" class="btn btn-primary" id="save-modal-voucher-edit2">Lưu</button>
            </div>
        </div>
    </div>
</div>
