@extends('layouts.app')

@section('content')
    <main class="login-form">
        <div class="card container mt-5 w-25" style="border-radius: 10px">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="container">
                        <h3 class="card-header text-center" style="color: #06357A">Đăng nhập</h3>
                        <div class="card-body">
                            <form method="POST" action="{{ route('users.login_custom') }}">
                                @csrf
                                <div class="form-group mb-3">
                                    <input type="text" placeholder="Email" id="user_email" class="form-control" name="user_email" required
                                           autofocus>
                                    @if ($errors->has('user_email'))
                                        <span class="text-danger">{{ $errors->first('user_email') }}</span>
                                    @endif
                                </div>

                                <div class="form-group mb-3">
                                    <input type="password" placeholder="Password" id="password" class="form-control" name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="text-danger">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>

                                <div class="form-group mb-3">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember"> Remember Me
                                        </label>
                                    </div>
                                </div>

                                <div class="d-grid mx-auto">
                                    <button type="submit" class="btn btn-dark btn-block" style="background-color: #06357A">Sign In</button>
                                </div>
                            </form>

                        </div>
                        <div class="footer">
                            <span class="mx-lg-3">Bạn chưa có tài khoản ?  <a href="{{route('users.register')}}">Đăng ký</a></span>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
