@extends('.layouts.app')
@section('content')
    @if(session('errors'))
        <div class="alert alert-danger">
            <ul>
                @foreach(session('errors')->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <main class="signup-form">
        <div class="container w-auto mt-5">
            <div class="row justify-content-center">
                <div class="col-6">
                    <div class="card">
                        <h3 class="card-header text-center fw-bold" style="color: #06357A">Đăng ký thành viên</h3>
                        <div class="card-body">
                            <form action="{{ route('users.register_custom') }}" method="POST">
                                @csrf
                                <div class="form-group mb-3">
                                    <input type="text" placeholder="Họ và tên" id="name" class="form-control"
                                           name="user_name"
                                           required autofocus>
                                    @if ($errors->has('user_name'))
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                                <div class="form-group mb-3">
                                    <input type="text" placeholder="Email" id="email_address" class="form-control"
                                           name="user_email" required autofocus>
                                    @if ($errors->has('user_email'))
                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                                <div class="form-group mb-3">
                                    <input type="password" placeholder="Mật khẩu" id="password" class="form-control"
                                           name="password" required>
                                    @if ($errors->has('user_password'))
                                        <span class="text-danger">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                                <div class="form-group mb-3">
                                    <input type="datetime-local" placeholder="Ngày sinh" id="user_birthday"
                                           class="form-control"
                                           name="user_birthday">
                                    @if ($errors->has('user_birthday'))
                                        <span class="text-danger">{{ $errors->first('user_birthday') }}</span>
                                    @endif
                                </div>
                                <div class="form-group mb-3">
                                    <input type="text" placeholder="Địa chỉ" id="user_address" class="form-control"
                                           name="user_address">
                                    @if ($errors->has('user_address'))
                                        <span class="text-danger">{{ $errors->first('user_address') }}</span>
                                    @endif
                                </div>
                                <div class="form-group mb-3">
                                    <input type="text" placeholder="Điện thoại" id="user_phone" class="form-control"
                                           name="user_phone" required>
                                    @if ($errors->has('user_phone'))
                                        <span class="text-danger">{{ $errors->first('user_phone') }}</span>
                                    @endif
                                </div>
                                <div class="form-group mb-3">
                                    <label>Giới tính:</label>
                                    <select name="gender" id="gender">
                                        <option value="1">Male</option>
                                        <option value="0">Female</option>
                                        <option value="2">Other</option>
                                    </select>
                                @if ($errors->has('gender'))
                                        <span class="text-danger">{{ $errors->first('gender') }}</span>
                                    @endif
                                </div>
                                <div class="form-group mb-3">
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="remember"> Ghi nhớ</label>
                                    </div>
                                </div>
                                <div class="d-grid mx-auto">
                                    <button type="submit" class="btn btn-dark btn-block"
                                            style="background-color: #06357A">Đăng ký
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="footer">
                            <span class="mx-lg-3">Bạn đã có tài khoản ? <a href="{{route('users.login')}}">Đăng nhập</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
