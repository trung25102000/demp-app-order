<script src="{{asset('order/assets/vendor/apexcharts/apexcharts.min.js')}} "></script>
<script src="{{asset('order/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('order/assets/vendor/chart.js/chart.umd.js')}}"></script>
<script src="{{asset('order/assets/vendor/echarts/echarts.min.js')}}"></script>
<script src="{{asset('order/assets/vendor/quill/quill.min.js')}}"></script>
<script src="{{asset('order/assets/vendor/simple-datatables/simple-datatables.js')}}"></script>
<script src="{{asset('order/assets/vendor/tinymce/tinymce.min.js')}}"></script>
<script src="{{asset('order/assets/vendor/php-email-form/validate.js')}}"></script>


<!-- Template Main JS File -->
<script src="{{asset('order/assets/js/main.js')}}"></script>
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>--}}
<script src="https://unpkg.com/currency.js@~2.0.0/dist/currency.min.js"></script>
<script src="https://js.pusher.com/4.3/pusher.min.js"></script>



<script>
    function formatNumber(number) {
        return number.toLocaleString('vi-VN');
    }

    // Hàm chuyển string thành số
    function parseNumber(stringNumber) {
        return parseInt(stringNumber.split('.').join(''));
    }

</script>
<!--End script in create-order-->
<script>
    $(document).ready(function () {
        $('.quantity-input').on('input', function () {
            var quantity = parseInt($(this).val());
            var row = $(this).closest('tr');

            if (quantity === 0) {
                var productId = row.data('row-id');

                // Gửi yêu cầu AJAX đến tuyến đường 'delete-detail' với dữ liệu productId
                $.ajax({
                    url: $(this).data('route'),
                    method: 'POST',
                    data: {productId: productId},
                    success: function (response) {
                        // Xóa dòng sản phẩm khỏi bảng trên giao diện
                        row.remove();
                    },
                    error: function (xhr) {
                        // Xử lý lỗi nếu có

                    }
                });
            }
        });

        // Cập nhật tổng cộng
        function updateTotalPrice() {
            var totalPrice = 0;

            // Lặp qua từng hàng trong bảng chi tiết
            $('#table_detail tbody tr').each(function () {
                var price = parseFloat($(this).find('#price').text());
                var quantity = parseInt($(this).find('.quantity-field').text());
                var subtotal = price * quantity;

                // Cộng tổng thành tiền
                totalPrice += subtotal;
                if (quantity === 0) {
                    var rowId = $(this).closest('tr').data('row-id');

                    $.ajax({
                        url: '/details/' + rowId,
                        type: 'DELETE', // Sử dụng phương thức DELETE
                        data: {_token: '{{ csrf_token() }}'},
                        success: function (response) {
                            $('tr[data-row-id="' + rowId + '"]').remove();
                            // Ví dụ: cập nhật lại tổng cộng
                            $('.total-price').html(response.totalPrice);
                        },
                        error: function (xhr, status, error) {
                            // Xử lý lỗi nếu có
                        }
                    });
                }
            });

            // Cập nhật giá trị tổng cộng
            $('.total-price').text(totalPrice);
        }


        const rows = document.querySelectorAll('#table_detail tbody tr');
        const totalCell = document.querySelector('.total-price');

        rows.forEach(function (row) {
            const quantityField = row.querySelector('.quantity-field');
            const quantityInput = row.querySelector('.quantity-input');
            const noteField = row.querySelector('.note-field');
            const noteInput = row.querySelector('.note-input');
            const editButton = row.querySelector('.edit-button');
            const saveButton = row.querySelector('.save-button');
            const priceCell = row.querySelector('.text-xl-end');

            editButton.addEventListener('click', function (event) {
                event.preventDefault();
                quantityField.classList.add('d-none');
                noteField.classList.add('d-none');
                quantityInput.classList.remove('d-none');
                noteInput.classList.remove('d-none');
                saveButton.classList.remove('d-none');
                editButton.classList.add('d-none');
            });

            saveButton.addEventListener('click', function (event) {
                event.preventDefault();
                const quantityValue = quantityInput.value;
                const noteValue = noteInput.value;
                const rowId = row.dataset.rowId;

                // Gửi dữ liệu cập nhật lên máy chủ bằng AJAX
                $.ajax({
                    url: `/save-detail/${rowId}`,
                    type: 'POST',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        quantity: quantityValue,
                        note: noteValue
                    },
                    success: function (response) {
                        // Xử lý phản hồi thành công


                        // Cập nhật tổng cộng
                        updateTotalPrice();

                        // Hiển thị lại trường dữ liệu
                        quantityField.textContent = quantityValue;
                        noteField.textContent = noteValue;

                        // Ẩn trường nhập liệu
                        quantityField.classList.remove('d-none');
                        noteField.classList.remove('d-none');
                        quantityInput.classList.add('d-none');
                        noteInput.classList.add('d-none');
                        saveButton.classList.add('d-none');
                        editButton.classList.remove('d-none');

                        updateTotalPrice();
                    },
                    error: function (xhr) {
                        // Xử lý lỗi
                        console.error(xhr.responseText);
                    }
                });
            });

        });
    });
</script>


<script>
    setTimeout(function () {
  $('#success-alert').fadeOut('slow', function () {
    $(this).remove();
  });
}, 3000);

setTimeout(function () {
  $('#error-alert').fadeOut('slow', function () {
    $(this).remove();
  });
}, 3000);
</script>
