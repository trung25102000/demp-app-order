@section('style-after')
    <link rel="stylesheet" href="{{ asset('order/css/header.css') }}">
@endsection

<nav class="navbar navbar-expand-lg navbar-light align-items-center bg-white shadow header-nav">
    <div class="container-fluid">
        <div class="d-flex align-items-center">
            <a href="/" class="logo d-flex align-items-center">
                <img src="{{ asset('order/assets/img/logo.png') }}" alt="">
                <span class="d-none d-lg-block">Deha Order</span>
            </a>
        </div>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse align-items-center" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link h-100 {{ request()->routeIs('orders.create_order') || request()->routeIs('orders.list_order') || request()->routeIs('orders.details') ? 'bg-mix active' : '' }}
"
                        href="#" id="orderDropdown" role="button" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        <i class="bi bi-bucket-fill mx-lg-1" style="color: #06357a"></i>
                        <span class="fw-bold" style="color: #06357a">Order</span>
                        <i class="bi bi-caret-down-fill" style="color: #06357a"></i>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="orderDropdown">
                        <li>
                            <a class="dropdown-item" style="font-weight: bolder;color: #06357a"
                                href="{{ route('orders.create_order') }}">Thêm Order</a>
                        </li>
                        <li>
                            <a class="dropdown-item" style="font-weight: bolder; color: #06357a"
                                href="{{ route('orders.list_order') }}">Danh sách Order</a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('orders.home') ? 'bg-mix active' : '' }}"
                        href="{{ route('orders.home') }}">
                        <i class="bi bi-map-fill" style="color: #06357a"></i>
                        <span class="fw-bold" style="color: #06357a">Danh sách bàn</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('orders.reimburse') ? 'bg-mix active' : '' }}"
                        href="{{ route('orders.reimburse') }}">
                        <i class="bi bi-clipboard-check-fill mx-lg-1" style="color: #06357a"></i>
                        <span class="fw-bold" style="color: #06357a">Trả món</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('orders.payment_list') ? 'bg-mix active' : '' }}"
                        href="{{ route('orders.payment_list') }}">
                        <i class="bi bi-building-fill mx-lg-1" style="color: #06357a"></i>
                        <span class="fw-bold" style="color: #06357a">Phiếu tạm tính</span>
                    </a>
                </li>
            </ul>
            @php
                $notify = \App\Models\Notification::orderBy('created_at', 'desc')->get();
            @endphp
            <ul class="ms-auto d-flex align-items-center ul-noti">
                <li class="dropdown ">
                    @if (request()->routeIs('orders.list_order'))
                    <a class="nav-link nav-icon" href="#" data-bs-toggle="dropdown">
                        <i class="bi bi-bell notify"></i>
                        @foreach ($notify as $noti)
                            <span class="badge bg-primary badge-number noti-count"
                                value="{{ \App\Models\Notification::where('status', 1)->count() }}">{{ \App\Models\Notification::where('status', 1)->count() }}</span>
                        @endforeach
                    </a><!-- End Notification Icon -->
                    @endif
                    <ul class="dropdown-menu dropdown-menu-end dropdown-menu-sm-end dropdown-menu-arrow notifications"
                        style="width: 350px; max-height: 500px; overflow-y: auto;">
                        <li class="dropdown-header fw-bold">
                            Thông báo hệ thống
                        </li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <div class="list-notify">
                            @foreach ($notify as $noti)
                                <li class="notification-item">
                                    <i class="bi bi-check-circle text-success"></i>
                                    <div>
                                        <p style="font-size: 15px">{{ $noti->noti_content }}</p>
                                        <p>{{ $noti->created_at }}</p>
                                    </div>
                                </li>
                                <li>
                                    <hr class="dropdown-divider">
                                </li>
                            @endforeach
                        </div>
                    </ul><!-- End Notification Dropdown Items -->

                </li><!-- End Notification Nav -->

                <li class="dropdown">

                    <a class="nav-link nav-icon" href="#" data-bs-toggle="dropdown">
                        <i class="bi bi-chat-left-text"></i>
                        <span class="badge bg-success badge-number">1</span>
                    </a><!-- End Messages Icon -->

                    <ul class="dropdown-menu dropdown-menu-end dropdown-menu-sm-start dropdown-menu-arrow messages">
                        <li class="dropdown-header">
                            Bạn có 1 tin nhắn
                            <a href="#"><span class="badge rounded-pill bg-primary p-2 ms-2">Xem tất cả</span></a>
                        </li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>

                        <li class="message-item">
                            <a href="#">
                                <img src="{{ asset('order/assets/img/messages-1.jpg') }}" alt=""
                                    class="rounded-circle">
                                <div>
                                    <h4>Trung</h4>
                                    <p>Khách muốn chuyển bàn</p>
                                    <p>4 phút trước</p>
                                </div>
                            </a>
                        </li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>

                        <li>
                            <hr class="dropdown-divider">
                        </li>

                        <li class="dropdown-footer">
                            <a href="#">Hiện tất cả</a>
                        </li>

                    </ul><!-- End Messages Dropdown Items -->

                </li><!-- End Messages Nav -->

                <li class="dropdown pe-3">

                    <a class="nav-link nav-profile d-flex align-items-center pe-0" href="#"
                        data-bs-toggle="dropdown">
                        <img src="{{ asset('order/assets/img/user.jpg') }}" alt="Profile" class="rounded-circle"
                            width="40px" height="40px">
                        <span class="d-none d-md-block dropdown-toggle ps-2">Xin chào
                            @if (auth()->check())
                                {{ Auth::user()->user_name }}
                            @endif !
                        </span>
                    </a><!-- End Profile Iamge Icon -->

                    <ul class="dropdown-menu dropdown-menu-xl-end dropdown-menu-sm-start dropdown-menu-arrow profile">
                        @auth
                            <li class="dropdown-header">
                                <h6>{{ Auth::user()->user_name }}</h6>
                                <span>Nhân viên bán hàng</span>

                            </li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li>
                                <a class="dropdown-item d-flex align-items-center" href="{{ route('user') }}">
                                    <i class="bi bi-person"></i>
                                    <span>Hồ sơ</span>
                                </a>
                            </li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                        @endauth

                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        @guest
                            <li>
                                <a class="dropdown-item d-flex align-items-center" href="{{ route('users.login') }}">
                                    <i class="bi bi-box-arrow-right"></i>
                                    <span>Đăng nhập</span>
                                </a>
                            </li>
                        @endguest
                        @guest
                            <li>
                                <a class="dropdown-item d-flex align-items-center" href="{{ route('users.register') }}">
                                    <i class="bi bi-box-arrow-right"></i>
                                    <span>Đăng kí</span>
                                </a>
                            </li>
                        @endguest
                        @auth
                            <li>
                                <a class="dropdown-item d-flex align-items-center" href="{{ route('users.sign_out') }}">
                                    <i class="bi bi-box-arrow-right"></i>
                                    <span>Đăng xuất</span>
                                </a>
                            </li>
                        @endauth
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<script src="{{ asset('order/js/header.js') }}"></script>
