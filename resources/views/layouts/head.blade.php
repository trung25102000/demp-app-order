<head>
    <meta charset="utf-8">
    {{--    <meta content="width=device-width, initial-scale=1.0" name="viewport">--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Deha Order</title>
    <meta content="" name="description">
    <meta content="" name="keywords">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{asset('order/assets/img/favicon.png')}}" rel="icon">
    <link href="{{asset('order/assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.gstatic.com" rel="preconnect">
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{asset('order/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('order/assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
    <link href="{{asset('order/assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('order/assets/vendor/quill/quill.snow.css')}}" rel="stylesheet">
    <link href="{{asset('order/assets/vendor/quill/quill.bubble.css')}}" rel="stylesheet">
    <link href="{{asset('order/assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
    <link href="{{asset('order/assets/vendor/simple-datatables/style.css')}}" rel="stylesheet">
    <link href="{{ asset('/order/css/toast.css') }}" rel="stylesheet">
    <link href="{{ asset('/order/css/loader.css') }}" rel="stylesheet">


    <!-- Template Main CSS File -->
    <link href="{{asset('order/assets/css/style.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('order/css/header.css')}}">
    @yield('style-after')
</head>
